<?php

	ini_set("log_errors", 1);
	error_reporting(E_ALL);
	ini_set("error_log", "/var/www/vhosts/suend.it/crm.suend.it/apiEmail.log");
	ini_set("display_errors", "On");
	set_time_limit(0);
	ini_set('upload_max_filesize', '768M');
	ini_set('max_execution_time ', '0');


	date_default_timezone_set("Europe/Rome");

	require("/var/www/vhosts/suend.it/crm.suend.it/lib/phpmailer/class.phpmailer.php");

  	$host = "localhost";
  	$user = "suendcrm";
  	$pass = "Ttr!q990";
  	$databaseName = "suend_crm";
  	$mysqli=mysqli_connect($host,$user,$pass,$databaseName);

  	/* job notifiche task*/
	$response=[];
		$sql="
			SELECT tasks.id,startDate,status,tasks.task_note,tasks.task_deadline,tasks.schedulato,tasks.schedulato_lv,
			CONCAT(users.email,',',users2.email) as email,
			users.name, users.surname , users.name as incaricato, users2.name as assegnatario
			FROM `tasks` 
			INNER JOIN users ON tasks.user_id=users.id 
			INNER JOIN users users2 ON tasks.user_assigned_id=users2.id 
			WHERE startDate<=CURDATE() AND status='attivo'"; //AND users.id=32
	
	$more="";
	
	/*togliere */
	$result=$mysqli->query($sql);

	while ($row=mysqli_fetch_assoc($result))
		$response[]=$row;

	for($i=0;$i<count($response);$i++){
		$subject = ' - GIORNALIERO';


			/*

				schedulato_lv 0 = GIORNALIERO
				schedulato_lv 1 = SOLO IL GIORNO PRIMA
				schedulato_lv 2 = SETTIMANALE
				schedulato_lv 3 = MENSILE
				schedulato è LA DATA DI CONSEGNA QUANDO schedulato_lv != 0
			*/

		$deadline=new Datetime($response[$i]['task_deadline']);
		$today=new Datetime();
		$deadlineFormattedIta=$deadline->format("d/m/Y");

		/* se ha un livello di schedula ma la data di schedula non è oggi lo salto */
		if($response[$i]['schedulato_lv']!=0&&$response[$i]['schedulato']!=date("Y-m-d")){
			continue;
		}
		if ($response[$i]['schedulato_lv']==1)
			$subject = ' - SCADE DOMANI';



		/* riprogrammo solo se schedulato + riprogrammazione < deadline */
		if($response[$i]['schedulato_lv']==2){

			$subject = " - SETTIMANALE";
			$new_date=new Datetime();
			$new_date->modify("+7 days");
			$new=$new_date->format("Y-m-d");
			$deadline=new Datetime($response[$i]['task_deadline']);
			$sql="UPDATE tasks SET schedulato = '$new' WHERE id=".$response[$i]['id'];

			if($deadline>=$new_date)
				$mysqli->query($sql);
		}

		/* riprogrammo solo se schedulato + riprogrammazione < deadline */
		if($response[$i]['schedulato_lv']==3){
				$subject = " - MENSILE";
			$new_date=new Datetime();
			$new_date->modify("+30 days");
			$new=$new_date->format("Y-m-d");
			$deadline=new Datetime($response[$i]['task_deadline']);

			$sql="UPDATE tasks SET schedulato = '$new' WHERE id=".$response[$i]['id']; 
			if($deadline>=$new_date)
				$mysqli->query($sql);
		}


		if($deadline<=$today){
			$elapsed=" TASK SCADUTO! ";
			$more="<br><br>Se stai ricevendo questo messaggio e hai gia' completato questo task, verifica di aver aggiornato lo stato del task in completato dalla dashboard del crm Suend";
			$subject=$elapsed . " " . $subject;
			$sql="UPDATE tasks SET schedulato_lv = '0' WHERE id=".$response[$i]['id'];
			$mysqli->query($sql);
		}

		$mail = new PHPMailer;
		$data=$response[$i];
		$name=$data['name']." ".$data['surname'];
		$incaricato=$data['incaricato'];
		$assegnatario=$data['assegnatario'];
		$addressList=explode(",", $data['email']);

		$deadline=$data['task_deadline'];
		$body=$data['task_note'];

		$mail->isSMTP();
		$mail->SMTPDebug = 3;
		$mail->Host = 'mail.smtp2go.com';
		$mail->SMTPAuth = true;
		$mail->Username = 'system@suendviaggi.net';
		$mail->Password = 'djR1bDhxbzBkNTkw';
		$mail->SMTPSecure = 'ssl';
		$mail->Port = 465;
		$mail->setFrom('system@suend.it', 'Suend Viaggi');

		foreach ($addressList as $address)
			$mail->addAddress($address, 'Utente');
		
		//<b> $name </b>!

		$mail->Subject = 'Task Reminder '.$subject;
		$mail->isHTML(true);
		$mail->Body = "<b>Crm suen, Task reminder</b>,<br> il task che ti e' stato assegnato deve essere ancora chiuso. <b>Oggetto del task</b> : $body<br> <br>Assegnato da:$assegnatario<br>Incaricato del task: :$incaricato<br> <b>Scadenza tast</b>: $deadlineFormattedIta<br> $more";

		$mail->SMTPOptions = array(
		  'ssl' => array(
		    'verify_peer' => false,
		    'verify_peer_name' => false,
		    'allow_self_signed' => true
		  )
		);

		if(!$mail->send()) {
			echo 'Message was not sent.';
			echo 'Mailer error: ' . $mail->ErrorInfo;
		}
		else
			echo 'Message has been sent.';
		unset($mail);

		if($response[$i]['schedulato_lv']!=0&&$response[$i]['schedulato']==date("Y-m-d")){

			$id=$response[$i]['id'];
			$time=new Datetime();

			if($response[$i]['schedulato_lv']==1)
				$time->modify("+7 days");

			if($response[$i]['schedulato_lv']==2)
				$time->modify("+30 days");

			$date=$time->format("Y-m-d");

			$sql="UPDATE `tasks` SET schedulato = $date WHERE id=$id";
			$result=$mysqli->query($sql);
		}
	}

	/* job notifiche compleanno - 256 */
	$today=date("Y-m-d");

	$sql="SELECT * FROM `business` WHERE data_nascita='$today'";
	$result=$mysqli->query($sql);
	while ($row=mysqli_fetch_assoc($result)) {
		extract($row);
		$head=[];
		$body=[];
		$head['h1']="il cliente business ";

		if(!empty($nome)&&!empty($cognome)){
			$head['h1'].=" $nome $cognome ";
		}
		else{
			if(!empty($ragione)){
				$head['h1'].=" con ragione sociale: $ragione" ;
			}
		}

		if(!empty($fisso)){
			$body['b1']="Numero di telefono fisso: $fisso";
		}

		if(!empty($cell)){
			$body['b2']="Numero di telefono cellulare: $cell";
		}

		if(!empty($mail)){
			$body['b3']="Indirizzo email: $mail";
		}

		$news_title=serialize($head);
		$news_body=serialize($body);
		$news="INSERT INTO `news` (`news_title`, `news_body`, `evidenza`,`zone`,`userid`) VALUES ( '$news_title', '$news_body', '1','Tutte le zone','-3');";
		echo $news;
		$mysqli->query($news);
	}

	$sql="SELECT * FROM `customers` WHERE data_nascita='$today' and adv =0";
	$result=$mysqli->query($sql);
	while ($row=mysqli_fetch_assoc($result)) {
		extract($row);
		$head=[];
		$body=[];
		$head['h1']="il cliente privato ";
		if(!empty($nome)&&!empty($cognome)){
			$head['h1'].=" $nome $cognome ";
		}
		else{
			if(!empty($ragione)){
				$head['h1'].=" con ragione sociale: $ragione" ;
			}
		}

		if(!empty($cell)){
			$body['b2']="Numero di telefono cellulare: $tel1";
		}

		if(!empty($mail)){
			$body['b3']="Indirizzo email: $email1";
		}

		$news_title=serialize($head);
		$news_body=serialize($body);
		$news="INSERT INTO `news` (`news_title`, `news_body`, `evidenza`,`zone`,`userid`) VALUES ( '$news_title', '$news_body', '1','Tutte le zone','-3');";
		echo $news;
		$mysqli->query($news);
	}

	$sql="SELECT * FROM `customers` WHERE data_nascita='$today' and adv =1";
	$result=$mysqli->query($sql);
	while ($row=mysqli_fetch_assoc($result)) {
		extract($row);
		$head=[];
		$body=[];
		$head['h1']=" l'ente adv ";
		if(!empty($nome)&&!empty($cognome)){
			$head['h1'].=" $nome $cognome ";
		}
		else{
			if(!empty($ragione)){
				$head['h1'].=" con ragione sociale: $ragione" ;
			}
		}

		if(!empty($cell)){
			$body['b2']="Numero di telefono cellulare: $tel1";
		}

		if(!empty($mail)){
			$body['b3']="Indirizzo email: $email1";
		}

		$news_title=serialize($head);
		$news_body=serialize($body);
		$news="INSERT INTO `news` (`news_title`, `news_body`, `evidenza`,`zone`,`userid`) VALUES ( '$news_title', '$news_body', '1','Tutte le zone','-3');";
		echo $news;
		$mysqli->query($news);
	}


?>
