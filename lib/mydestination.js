class mydestination { 

    constructor(target='',sourcename='',options={}) {

	  	this.target=target;
	  	this.sourcename=sourcename;
	  	var keys=Object.keys(options);
	  	for(var i=0;i<keys.length;i++)
	  		this[keys[i]]=options[keys[i]];
	  	this.init();
	}
	init(){
		this.parseAttributeOptions();
		this.calculateStandardOptions();
		this.renderThisHtml();
		this.autocomplete();
	}

	getSource(){
		var source=eval(this.sourcename);
			return source;
	}

    parseAttributeOptions() {

    	if(this.target=="")
    		return;
	  	if($(this.target).attr("name")!="")
		  	this.name=$(this.target).attr("name");
	  	if($(this.target).attr("id")!="")
		  	this.id=$(this.target).attr("id");
	  	this.placeholder="Cerca una destinazione";
    }

    calculateStandardOptions() {
    	if(this.id=="")
    		return;
    	
    	this.modalId="modal_"+this.id;
    	this.uid='mydestination_'+this.id;
    }

    renderThisHtml(){

	var html=
		'<div class="form-group col-md-12 mydestination" '+
		'data-my_source_variable_name="'+this.sourcename+'"'+
		'data-resultcontainer="'+this.resultcontainer+'"'+
		'data-target="'+this.target+'"'+
		'data-hiddenname="'+this.hiddenname+'">'+
		   '<div>'+
		      '<label class="col-md-12 control-label" for="textinput">'+this.label+'</label>'+  
		   '</div>'+
		   '<div>'+
		      '<div class="col-md-12">'+
		         '<div class="ui-filter-container-pref col-md-4">'+
		            '<input type="text" style="text-transform: uppercase;" id="'+this.id+'" placeholder="'+this.placeholder+'" class="form-control input-md">'+
		            '<input type="hidden" name="'+this.hiddenname+'">'+
		         '</div>'+
		      '</div>'+
		      '<div class="col-md-12">'+
		         '<'+this.resultcontainer+'>'+
		         '<div class="col-md-12">'+
		               '<ul>'+
		                  '<li>Nome destinazione</li>';
		            if(this.load_source==undefined)
		            	console.error("load_source not found");
					for(var i=0;i<this.load_source.length;i++){
						var item=this.load_source[i];
						var li='<li><b>'+item.label+'</b><deleter> <a href="#" data-id='+item.id+' data-label='+item.label+' data-table='+this.table_name+' data-qr="delete" class="btn btn-danger btn-md" style="height: 25px;font-size: 10px;"data-parent="li" onclick="deletePickedDestination(this,\''+this.variable_name+'\')"> <span class="glyphicon glyphicon-trash"></span> Rimuovi </a> </deleter> </li>';
						var input='<input type="hidden" old="1" name="'+this.hiddenname+'" value='+item.id+'>';  
						html+=li+input;     
					}

                html+=
		               '</ul>'+

		         '</div>'+
		         '</'+this.resultcontainer+'>'+
		      '</div>'+
		      '<div class="col-md-12">'+
		         '<p>Se non trovi la destinazione che stai cercando puoi aggiungerla premendo <a href="#" data-toggle="modal" data-target="#'+this.modalId+'">qui</a></p>'+
		      '</div>'+
		   '</div>'+
		'</div>'; 
		this.html=html;

		var modal=
		'<div id="'+this.modalId+'" class="modal fade" role="dialog">'+
		  '<div class="modal-dialog">'+
		    '<div class="modal-content">'+
		      '<div class="modal-header">'+
		        '<button type="button" class="close" data-dismiss="modal">&times;</button>'+
		        '<h4 class="modal-title">Aggiungi una destinazione</h4>'+
		      '</div>'+
		      '<div class="modal-body">'+
		        '<form class="form-horizontal">'+
		        '<fieldset>'+
		        '<div class="form-group col-md-6">'+
		          '<label class="col-md-4 control-label" for="textinput">Nome destinazione</label>'+  
		          '<div class="col-md-4">'+
		          '<input id="dsnm" type="text" style="width: 150px;" placeholder="Nome.." class="form-control input-md">'+
		          '</div>'+
		        '</div>'+
		        '<div class="form-group col-md-6">'+
		          '<div class="col-md-12" style="text-align: center;">'+
		            '<button id="button1id" type="button" onclick="applydestination(\'#'+this.modalId+'\',\''+this.sourcename+'\')" class="btn btn-success">Salva</button>'+
		          '</div>'+
		        '</div>'+
		        '</fieldset>'+
		        '</form>'+
		          '</div>'+
		          '<div class="modal-footer">'+
		            '<button type="button" class="btn btn-default" data-dismiss="modal">Chiudi</button>'+
		          '</div>'+
		        '</div>'+
		      '</div>'+
		'</div>';
		this.modal=modal;
		$(this.target).replaceWith(html+modal);
    }

	
    autocomplete(){
		$( this.target ).autocomplete({
		  source: this.getSource(),
		    select: function (event, ui) {
		    	/* QUI */
		    	var data=$(this).parents(".mydestination").data();
		    	var	variable_name=data.my_source_variable_name;
		    	var resultcontainer=data.resultcontainer;
		    	var target=data.target;
		    	var hiddenname=data.hiddenname;
		    	var source=eval(variable_name);

		    	$(this).trigger("close");

		        var li='<li><b>'+ui.item.label+'</b><deleter> <a href="#" data-qr="delete" data-table="" data-id='+ui.item.id+' data-label='+ui.item.label+' class="btn btn-danger btn-md" style="height: 25px;font-size: 10px;"data-parent="li" onclick="deletePickedDestination(this,\''+variable_name+'\')"> <span class="glyphicon glyphicon-trash"></span> Rimuovi </a> </deleter> </li>';
		        $(resultcontainer+" ul").append(li);
		        
		        if($('[name="'+hiddenname+'"]').val()==""){
					$('[name="'+hiddenname+'"]').val(ui.item.id);
					$('[name="'+hiddenname+'"]').removeAttr("old");
		        }
		        else
					$(resultcontainer).append('<input type="hidden" name="'+hiddenname+'" value='+ui.item.id+'>');

		        for(var i=0;i<source.length;i++){
			        if(source[i].id==ui.item.id){
						eval(variable_name+' = removeElFromArray(source,i)');
						$(target).autocomplete('option', 'source', eval(variable_name));
						break;
		            }
	     	    }
		      },
		      close:function(){
		        $(this).val("");
		    }
		}); 
	}



	deleteDestPass(el){
        var id=$(el).attr("id");

        $.ajax({
          type: 'POST',
          url: apiUrl+'api.php',
          data: {qr:'removerow',id:id,table:'destinazioniPassate'},
          el:el,
          success: function(res){
            
          },
          async:false
        });   
    }



}

	function removeElFromArray(array,i){
	    var a=array.slice(0,i);
	    var b=array.slice(i+1);
	    return a.concat(b);
    }

	function applydestination(modalId,variable_name) {

      var destinazione=$(modalId +" #dsnm").val().toUpperCase();
      var visibile=1;
      window['tmp']=variable_name;
      window['modalId']=modalId;
      $.post(apiUrl+'api.php',{
      	qr:'addDestination',
      	destinazione:destinazione,
      	visibile:visibile
      },function(response){
		/*qui update su tutte le source con event listener*/    
		addResult($(modalId.replace("modal_","")),JSON.parse(response));
        $(modalId).modal("hide");
        $(modalId+" input").val("");
      });

	}

	function just_remove_container(el){
		$(el).parents("li").remove();
	}

	function deletePickedDestination(el,variable_name){
        var data=$(el).data();
        var array=eval(variable_name);
        eval(variable_name+' =putElFromArray(array,data);');
        $(data.target).autocomplete('option', 'source', eval(variable_name));
	    $.post(apiUrl+'api.php',$(el).data(),function(response){
        	just_remove_container(el);
	    });
    }

	function putElFromArray(array,el){
        if(array.includes(el.label))
            return array;
        var list=[el.label];

        for(var i=0;i<array.length;i++){
            if(array[i].label==el.label)
                return array;
            list.push(array[i].label);
        }
        list.sort();

        var pos=list.indexOf(el.label);
        var a = array.slice(0,pos-1);
        var b = array.slice(pos);

        return a.concat([el].concat(b));
    }
	
function addResult(el,item){
	var data=$(el).parents(".mydestination").data();
	var resultcontainer=data.resultcontainer;
	var hiddenname=data.hiddenname;
	var variable_name=data.variable_name;
	var li='<li><b>'+item.label+'</b><deleter> <a href="#" data-id='+item.id+' data-label='+item.label+' class="btn btn-danger btn-md" style="height: 25px;font-size: 10px;"data-parent="li" onclick="this.parentElement.parentElement.remove()"> <span class="glyphicon glyphicon-trash"></span> Rimuovi </a> </deleter> </li>';
	$(resultcontainer+" ul").append(li);
		        
	if($('[name="'+hiddenname+'"]').val()=="")
		$('[name="'+hiddenname+'"]').val(item.id);
	else
		$(resultcontainer).append('<input type="hidden" name="'+hiddenname+'" value='+item.id+'>');
}


/*

  var destinazioniPreferite=JSON.parse("<?php echo json_encode($destinazioniPreferite) ?>");
  var destinazioniPassate=JSON.parse("<?php echo json_encode($destinazioniPassate) ?>");
  var destinazioniProssime=JSON.parse("<?php echo json_encode($destinazioniProssime) ?>");
  var destinazioniDove=JSON.parse("<?php echo json_encode($destinazioniDove) ?>");

	var md1=new mydestination("#preferite",'passate',
		{
			load_source: destinazioniPreferite,
			variable_name: 'passate',
			resultcontainer:'res_destinazionipreferite',
			hiddenname:' destinazionipreferite[]',
			label: 'Destinazioni preferite'
		}
	);

	var md2=new mydestination("#passate",'passate',
		{
			load_source: destinazioniPassate,
			variable_name: 'passate',
			resultcontainer:'res_destinazionipassate',
			hiddenname:' destinazionipassate[]',
			label: 'Destinazioni passate'
		}
	);

	var md3=new mydestination("#prossime",'passate',
		{
			load_source: destinazioniProssime,
			variable_name: 'passate',
			resultcontainer:'res_destinazioniprossime',
			hiddenname:' destinazioniprossime[]',
			label: 'Destinazioni prossime'
		}
	);

	var md4=new mydestination("#qasuend_doveviaggi",'passate',
		{
			load_source: destinazioniDove,
			variable_name: 'passate',
			resultcontainer:'res_destinazioniDove',
			hiddenname:' destinazioniDove[]',
			label: 'Dove'
		}
	);

*/