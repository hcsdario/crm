var apiUrl="/";


function numberWithCommas(x) {

    var parts = x.toString().split(".");
    var decimal=parts[0];
    var l=decimal.length-1;
    var c=0;
    var newdecimal="";
    for(var i=l;i>=0;i--)
    {
        c++;
        newdecimal=decimal[i]+newdecimal;
        if(c==3){
            if(i>0)
            newdecimal="."+newdecimal;
            c=0;
        }

    }
    parts[0]=newdecimal;

    return parts;
}

function format_to_eur_js(amount)
{
        if(""==amount)
            return '';
 		amount=amount.replaceAll(".","");
 		amount=amount.replaceAll(",",".");
 		amount=Number(amount);
        if(isNaN(amount))
            amount=0;
       var res= numberWithCommas( amount);

        if(res.length==1)
            res.push("");

        switch(res[1].length)
        {
            case 0:
            res[1]="00";
            break;
            case 1:
            res[1]+="0";
            break;
            default:
            break;
        }
        if(res[1].length>2)
            res[1]=res[1][0]+res[1][1];

       return res[0]+","+res[1]+" €";
}

Date.prototype.format_to_ita = function() {
	debugger;
	var y=this.getFullYear();
	var m=this.getMonth()+1;

	if(m<10)
		m="0"+m;
	var d=this.getDate();
	if(d<10)
		d="0"+d;
	return d+"/"+m+"/"+y;
}

String.prototype.format_to_ita = function(search, replacement) {
	debugger;
	var date=new Date(this);
	return date.format_to_ita();
}

String.prototype.replaceAll = function(search, replacement) {
    var target = this;
    if(search==replacement)
    {
    	console.error("replaceAll: search == replacement");
    	return;
    }
	while(target.includes(search))
	  target=target.replace(search,replacement)
    return target;
};

// only for form submit
function duplySelected(el){
	var l=$('[data-duplify="1"]:checked').length;
	if(l==0)
	{
		alert("seleziona i preventivi da duplicare");
		return;
	}
	var data=[];
	$('[data-duplify="1"]:checked').each(function(){
		data.push($(this).data());
	})

	$.post(apiUrl+'api.php',{qr:'duplify',data:JSON.stringify(data)},function(response){
		alert("Il preventivo è stato duplicato! Puoi trovare il nuovo preventivo all'ultima pagina della tabella");
		window.location.reload(true);
	})
}

function requestConfirm(el,text='Sei sicuro di volerlo eliminare definitivamente? '){
	window['tmp']=el;
	$.confirm({
		title: 'Attenzione',
		content: text,
		confirmButton: 'Sì',
		cancelButton: 'No',
		confirmButtonClass: 'btn-warning',
		cancelButtonClass: 'btn btn-success',
		confirm: function () {
			$(window['tmp']).parents('form').find('[name="task_action"]').val("true");
			$(window['tmp']).parents('form').submit();
		},
		cancel: function () {

		}
	});
}

function deleteRecord(el,arg=false){
	window['el']=el;

	var title='Sei sicuro di volerlo eliminare definitivamente?';
	if(el.getAttribute("custom_delete_title")!=null)
		title=el.getAttribute("custom_delete_title");

	if(arg)
	{
		if(arg.length>1){
			window['table']=arg[0];
			window['tableid']=arg[1];
		}
		else
			window['table']=arg[0];
	}
	else
	{
		if(table=="dataInTable")
			window['table']=$(el).parents('table').attr('data-tablename');
		else
			window['table']=table;
	}

	$.confirm({
		title: 'Attenzione',
		content: title,
		confirmButton: 'Sì',
		cancelButton: 'No',
		confirmButtonClass: 'btn-warning',
		cancelButtonClass: 'btn btn-success',
		confirm: function () {

			var id=$(window['el']).parents('tr').find('td:eq(0)').text().trim();
			if(id=="")
				id=$(window['el']).parents('form').find('[name="task_id"]').val();

		    if(window['tableid']!=undefined)
	    		id=window['tableid'];
					$.ajax({
						pointerEl:el,
					    url: apiUrl+'api.php',
					    type: "POST",
					    data: { qr:'delete', table:window['table'],id:id},
					    success: function(res){
							if(window['isntable']==undefined){
							   $(el).parents('tr').hide(700);
							   $(el).parents('tr').remove();
							}
							else{
							    $(el).parents(window['isntable']).hide(700);
							    $(el).parents(window['isntable']).remove();
								window['isntable']=null;
							}
							window['tableid']=null;
						},
						error: function(){
							console.log("ajax error ");
						}
					});
		},
		cancel: function () {
		}
	});
}

function cleanDeleteRecord(el){

	var data=$(el).data();
	$.confirm({
		title: 'Attenzione',
		content: 'Sei sicuro di volerlo eliminare definitivamente?',
		confirmButton: 'Sì',
		cancelButton: 'No',
		confirmButtonClass: 'btn-warning',
		cancelButtonClass: 'btn btn-success',

		confirm: function () {
					$.ajax({
						action:data.action,
						parents:data.parent,
					    url: apiUrl+'api.php',
					    type: "POST",
					    data: { qr:'delete', table:data.table,id:data.id},
					    success: function(res){

							switch(this.action){
								case 'table':
									$(el).parents("tr").remove();
								break;
								case 'container':
									$(el).parents(this.parents).remove();
								break;
							}
						},
						error: function(){
							console.log("ajax error ");
						}
					});
		},
		cancel: function () {
		}
	});
}

function htmlToStruct(target,mode="string"){
	var collection=[];

	$(target).each(function(){
		var input=$(this).find("input");
		var item={};
		for(var i=0;i<input.length;i++){

		var key=$(input[i]).attr("name");
		var datakey="data"+i;
		var value=$(input[i]).prop("value");
			if(value==undefined)
				value="";
		var data=$(input[i]).data();
		item[key]=value;
		item[datakey]=data;
		}

		collection.push(item);
	});

	switch(mode){
		case 'string':
		return JSON.stringify(collection);
		default:
		return collection;
	}

}
// args as array
// html as onclick="jsRequestConfirm(foo,[1,this])"
// SOLO SU CANCEL
// html as onclick="jsRequestConfirm('emptycallback',[this],'foo')"

function preventDefault(el,e){

}

function emptycallback(arg){

}

function jsRequestConfirm(callback, args, cancelcallback=false){

$.confirm({
	title: 'Attenzione',
	content: 'Sei sicuro di volerlo eliminare definitivamente?',
	confirmButton: 'Sì',
	cancelButton: 'No',
	confirmButtonClass: 'btn-warning',
	cancelButtonClass: 'btn btn-success',
	confirm: function () {
		callback.apply(this, args);
	},
	cancel: function () {
		if(cancelcallback)
			cancelcallback.apply(this, args);
	}
});

}

function fakeData(n=1){
	$("input[name!='_token'][name!='id']").val(n);
	$("input[type='date']").val("");
	$("input[type='checkbox']").prop("checked",true);
	$("select option:eq(1)").prop("selected",true);
	$("select,input[type='checkbox'],input[name!='_token'][name!='id']").removeAttr("old").attr("dirty");
}


//sendCurlEmail('manghina.dario@gmail.com','subject','body');
function sendCurlEmail(email,subject,body,url='http://hellocreativestudio.net/mail/mailer.php'){

    $.ajax({
       url : apiUrl+'api.php',
       type : 'POST',
        data : {
        qr:'sendCurl',
        url:url,
        fields:JSON.stringify(
			{
				address:email,
				subject:subject,
				body:body,
			}
		)},
        success: function (response){
        }
  });
}

function addAjaxField(table,data,callback){
    $.ajax({
       //callback:callback,
       url : apiUrl+'api.php',
       type : 'POST',
        data : {
        qr:'addAjaxField',
        data:{data:JSON.stringify(data),table:table}
		},
        success: function (response){
        	debugger;
         	$("#no_reload").show();
          let res = JSON.parse(response)
          if (callback && callback instanceof Function) callback(res);
        }
  });
}

function isDirtySet(els){
	for(var i in els)
	{
		$(els[i]).attr("dirty",true);
    	$(els[i]).removeAttr("old");
	}
}


function just_remove_container(el){
	var data=$(el).data();
	$(el).parents(data.parent).hide(1000);
	$(el).parents(data.parent).remove();
}

function get_window_size(){

	var width=	window.innerWidth;
	var height=	window.innerHeight;
	if(width>height)
		var orientation="landscape";
	else
		var orientation="portrait";
	return {
		width:width,
		height:height,
		orientation:orientation
	};
}

function compare_window_size_with_mediaquery_size(size,max_width,max_height){
	if(size.width<max_width&&size.height<max_height)
		return true;
	return false;
}

function is_tablet(){
	var max_width=810;
	var max_height=1055;
	var size=get_window_size();
	var is_tablet=compare_window_size_with_mediaquery_size(size,max_width,max_height);
	if(is_tablet)
		return size;
	else
		return false;
}

function is_tablet_pro(){
	var max_width=1386;
	var max_height=1075;
	var size=get_window_size();
	var is_tablet=compare_window_size_with_mediaquery_size(size,max_width,max_height);
	if(is_tablet)
		return size;
	else
		return false;
}













(function(funcName, baseObj) {
    // The public function name defaults to window.docReady
    // but you can pass in your own object and own function name and those will be used
    // if you want to put them in a different namespace
    funcName = funcName || "docReady";
    baseObj = baseObj || window;
    var readyList = [];
    var readyFired = false;
    var readyEventHandlersInstalled = false;

    // call this when the document is ready
    // this function protects itself against being called more than once
    function ready() {
        if (!readyFired) {
            // this must be set to true before we start calling callbacks
            readyFired = true;
            for (var i = 0; i < readyList.length; i++) {
                // if a callback here happens to add new ready handlers,
                // the docReady() function will see that it already fired
                // and will schedule the callback to run right after
                // this event loop finishes so all handlers will still execute
                // in order and no new ones will be added to the readyList
                // while we are processing the list
                readyList[i].fn.call(window, readyList[i].ctx);
            }
            // allow any closures held by these functions to free
            readyList = [];
        }
    }

    function readyStateChange() {
        if ( document.readyState === "complete" ) {
            ready();
        }
    }

    // This is the one public interface
    // docReady(fn, context);
    // the context argument is optional - if present, it will be passed
    // as an argument to the callback
    baseObj[funcName] = function(callback, context) {
        if (typeof callback !== "function") {
            throw new TypeError("callback for docReady(fn) must be a function");
        }
        // if ready has already fired, then just schedule the callback
        // to fire asynchronously, but right away
        if (readyFired) {
            setTimeout(function() {callback(context);}, 1);
            return;
        } else {
            // add the function and context to the list
            readyList.push({fn: callback, ctx: context});
        }
        // if document already ready to go, schedule the ready function to run
        if (document.readyState === "complete") {
            setTimeout(ready, 1);
        } else if (!readyEventHandlersInstalled) {
            // otherwise if we don't have event handlers installed, install them
            if (document.addEventListener) {
                // first choice is DOMContentLoaded event
                document.addEventListener("DOMContentLoaded", ready, false);
                // backup is window load event
                window.addEventListener("load", ready, false);
            } else {
                // must be IE
                document.attachEvent("onreadystatechange", readyStateChange);
                window.attachEvent("onload", ready);
            }
            readyEventHandlersInstalled = true;
        }
    }
})("docReady", window);

var confirm_dirty=0;

docReady(function() {
	$('input,select,textarea').change(function(){
	        confirm_dirty=1;
	});

    $("#side_nav li").click(function (e) {
        var href = $(this).find("a").attr("href");
        if(confirm_dirty==0)
            return;
        if ($(window).width() < 468)
        {
            $(window).unbind('beforeunload');
            var value = confirm('Sei sicuro di uscire? Tutte le modifiche non salvate andranno perse ')
            if (value == true)
                return true;
            else
                return false;
        }
        else {
            e.preventDefault();
            e.stopImmediatePropagation();
            $.confirm({
                title: 'Attenzione',
                content: 'Sei sicuro di uscire?<br />Tutte le modifiche non salvate andranno perse ',
                confirmButton: 'Si',
                cancelButton: 'No',
                confirmButtonClass: 'btn-warning',
                cancelButtonClass: 'btn btn-success',
                confirm: function () {
                    window.location.href = href;
                },
                cancel: function () {
                }
            });
        }
    });
});
