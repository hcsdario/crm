<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class InfoTravel extends Model
{
    use SoftDeletes;
    protected $table = 'infoviaggi';


    public function customer() {
        return $this->belongsTo('App\Customer');
    }
}
