<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Priority extends Model
{
    use SoftDeletes;
	protected $table = 'priorities';

	public function task() {
		return $this->belongsTo('App\Task');
	}
}
