<?php

namespace App\Http\Controllers;

use App\Business;
use App\Customer;
use App\Info;
use App\InfoTravel;
use App\Profile;
use App\QaSuend;
use App\QaTravel;
use App\Task;
use Carbon\Carbon;
use Debugbar;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class BusinessController extends Controller
{

    public $comuniCtrl;

    public function __construct()
    {
        $this->comuniCtrl = new ComuniController();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user_id = Auth::user()->id;
        $task_total =  Task::where(['user_id'=>$user_id,'status' => 'attivo'])->orWhere(['user_assigned_id'=>$user_id,'status' => 'attivo'])->orderBy('category_id','asc')->orderBy('task_deadline','asc')->get();
        $task_total = count($task_total);

        if(Auth::user()->permessi)
            $azienda = DB::table('business')->select(
                'business.*',
                DB::raw('REPLACE(business.updated_at,"0000-00-00 00:00:00","") as updated_at ')
                )
                ->limit(200)
                ->get();
        else
            $azienda = DB::table('business')->join('profilo', 'business.id', '=', 'profilo.customer_id')->where('business.authorId', 'like', '%'.'^'.$user_id.'^'.'%')->select(
                'business.*',
                DB::raw('REPLACE(business.updated_at,"0000-00-00 00:00:00","") as updated_at ')
                )
                ->limit(200)
                ->get();


        if (Auth::user()->admin == 1) {
            return view('user.business',['aziende' =>$azienda, 'task_total' => $task_total,'task_open' => $task_total ] );
        }

        return view('user.business',['aziende' =>$azienda, 'task_total' => $task_total,'task_open' => $task_total ] );

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('create-customer');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $data=$request->all();
            try {

                $business = new Business();
                $data=$request->all();
                $keys=array_keys($data);
                $c=[];
                if(!empty($data['data_nascita']))
                {
                    $tmp=explode("/", $data['data_nascita']);
                    $data['data_nascita']=$tmp[2]."/".$tmp[1]."/".$tmp[0];
                }
                for($i=2;$i<count($keys);$i++) {

                    if($keys[$i]=="statiStato"||$keys[$i]=="statiNota"||$keys[$i]=="notaStato")
                        continue;

                    else{
                        $this->log("business","business.store",$i);
                        $business->{$keys[$i]}=$data[$keys[$i]];
                        $c[$keys[$i]]=$data[$keys[$i]];
                    }
                }

                if(empty($data['id']))
	                $business->authorId = "^".Auth::user()->id."^";
	            else{
	                if(strpos($business->authorId, "^".Auth::user()->id."^")<0)
	                	$cust->authorId .= "^".Auth::user()->id."^";
	            }

                if(empty($data['id'])){
                    $business->insertBy = Auth::user()->username;
                    $business->save();
                }
                else
                    DB::table("business")->where("id",$data['id'])->update($c);

                if(isset($data['id']))
                    $bid=$data['id'];
                else
                    $bid=$business->id;


                if(isset($request->notaStato))
                    for($i=0;$i<count($request->statiStato);$i++) {
                        if(empty($request->statiNota[$i]))
                            DB::table('business_relations')->insert(
                                [
                                    'status' => $request->statiStato[$i],
                                    'userid' => Auth::user()->id,
                                    'customerid' => $bid
                                ]
                            );
                        else
                            DB::table('business_relations')->insert(
                                [
                                    'status' => $request->statiStato[$i],
                                    'userid' => Auth::user()->id,
                                    'customerid' => $bid,
                                    'nota' => $request->statiNota[$i]
                                ]
                            );

                    }

                return redirect('user/business/'.$business->id)->with('message', 'Cliente Inserito!');
            }
         catch (\Exception $e) {
            return redirect()->back()->with('error', 'Errore Inserimento Cliente!' . $e);
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user_id = Auth::user()->id;
        $business =DB::table('business')->where("id","=",$id)->get();

        $task_total =  Task::where(['user_id'=>$user_id,'status' => 'attivo'])->orWhere(['user_assigned_id'=>$user_id,'status' => 'attivo'])->orderBy('category_id','asc')->orderBy('task_deadline','asc')->get();
        $task_total = count($task_total);
        $regioni = DB::table('italy_regions')->get();
        $lastId= DB::table('business')->max('id');

        $customersRelations= DB::table('business_relations')
        ->join('users', 'users.id', '=', 'business_relations.userid')
        ->where('business_relations.customerid', $id)
        ->select(['business_relations.*','users.name','users.surname'])
        ->get();


        $giftCard=DB::table('card')
            ->leftJoin("card_business_activity","card_business_activity.coupon_id","card.id")
            ->where("card.business_cards",$id)
            ->select(
                [
                "card.id",
                "card.code",
                "card.label",
                "card.price",
                "card.kind",
                "card.deadline",
                "card.customer_cards",
                "card.business_cards",
                "card.payed",
                "card.created_at",
                "card.updated_at",
                "card.deleted_at"
                ]

                )
            ->selectRaw(" toEur(card.value) value, toEur(coalesce(sum(card_business_activity.spesa),0)) spesa, DATE_FORMAT(max(card_business_activity.data_utilizzo) ,'%d/%m/%Y') data_utilizzo")
            ->groupBy(
                [
                "card.id",
                "card.code",
                "card.label",
                "card.value",
                "card.price",
                "card.kind",
                "card.deadline",
                "card.customer_cards",
                "card.business_cards",
                "card.payed",
                "card.created_at",
                "card.updated_at",
                "card.deleted_at"
                ]
            )
            ->get();


        $coupons=DB::table('coupon')
            ->leftJoin("coupon_business_activity","coupon_business_activity.coupon_id","coupon.id")
            ->where("coupon.customer_coupons",$id)
            ->select(
                [
                "coupon.id",
                "coupon.code",
                "coupon.label",
                "coupon.price",
                "coupon.kind",
                "coupon.deadline",
                "coupon.customer_coupons",
                "coupon.business_coupons",
                "coupon.payed",
                "coupon.created_at",
                "coupon.updated_at",
                "coupon.deleted_at"
                ]

                )
            ->selectRaw(" toEur(coupon.value) value, toEur(coalesce(sum(coupon_business_activity.spesa),0)) spesa, DATE_FORMAT(max(coupon_business_activity.data_utilizzo) ,'%d/%m/%Y') data_utilizzo")
            ->groupBy(
                [
                "coupon.id",
                "coupon.code",
                "coupon.label",
                "coupon.value",
                "coupon.price",
                "coupon.kind",
                "coupon.deadline",
                "coupon.customer_coupons",
                "coupon.business_coupons",
                "coupon.payed",
                "coupon.created_at",
                "coupon.updated_at",
                "coupon.deleted_at"
                ]
            )
            ->get();

        $data=[
                'id' => $id,
                'task_total' => $task_total,
                'task_open' => $task_total,
                'regioni' => $regioni,
                'giftCard' => $giftCard ,
                'coupons' => $coupons ,
                'business' => $business,
                'lastId' => $lastId,
                'customersRelations' => $customersRelations
            ];

                return view('user.newbusiness',
                    $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $cust = Customer::find($id);

        if (£cust) {
            $cust->softDeletes();
        }
    }


}
