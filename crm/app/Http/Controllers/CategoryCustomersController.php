<?php

namespace App\Http\Controllers;

use App\Category_Customers;
use App\User;
use Illuminate\Http\Request;

class CategoryCustomersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categorie = Category_Customers::where('deleted_at', null)->get();
        return  view('admin.category_customers',['categorie' => $categorie]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($id=NULL)
    {
        $cat = NULL;
        if ($id){
          $cat = Category_Customers::findOrFail($id);
        }
        return view('admin.category_customers',['category'=>$cat]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if ($request->input('id')){
          $cat = Category_Customers::findOrFail($request->input('id'));
        }
        else {
          $cat = new Category_Customers();
        }
        $cat->name = $request->input('name');
        $cat->save();
        return redirect()->route('categories_customers.index');
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      $cat = Category_Customers::findOrFail($id)->delete();
    }
}
