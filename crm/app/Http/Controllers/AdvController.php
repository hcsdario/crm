<?php


namespace App\Http\Controllers;

use App\Customer;
use App\Info;
use App\InfoAdv;
use App\InfoTravel;
use App\Task;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Yajra\Datatables\Facades\Datatables;


class AdvController extends Controller
{
    public $comuniCtrl;

    public function __construct()
    {
        $this->comuniCtrl = new ComuniController();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request=null)
    {
        $user_id = Auth::user()->id;
        $task_total =  Task::where(['user_id'=>$user_id,'status' => 'attivo'])->orWhere(['user_assigned_id'=>$user_id,'status' => 'attivo'])->orderBy('category_id','asc')->orderBy('task_deadline','asc')->get();
        $task_total = count($task_total);
        $regioni = $this->comuniCtrl->getRegione();
        $lastId= DB::table('customers')->max('id');

        $where=[];
        $where['adv']=1;
        $where['italiana_o_estera']=0;

        if(isset($request))
        {
            $inputs = $request->all();
            foreach ($inputs as $key => $value){
                if(empty($value))
                   continue;
                $where[str_replace("wjk", ".",$key)]=$value;
            }
        }

        if(isset($request))
        {
            $inputs = $request->all();
            foreach ($inputs as $key => $value)
                if(!empty($value))
                    $where[str_replace("wjk", ".",$key)]=$value;
        }

        $data = DB::table('customers')
          ->leftJoin('info', 'customers.id', '=', 'info.customer_id')
            ->where(function ($query) use ($where) {
                foreach ($where as $key => $value) {
                    switch ($key) {
                        case 'infoviaggi.destinazione':
                            $query->orWhere('infoadv.destinazioni_vendute1', 'like', $value);
                            $query->orWhere('infoadv.destinazioni_vendute2', 'like', $value);
                            $query->orWhere('infoadv.destinazioni_vendute3', 'like', $value);
                            $query->orWhere('infoadv.destinazioni_vendute4', 'like', $value);
                            $query->orWhere('infoadv.destinazioni_vendute5', 'like', $value);
                            $query->orWhere('infoadv.destinazioni_vendute6', 'like', $value);
                            $query->orWhere('infoadv.destinazioni_vendute7', 'like', $value);
                            $query->orWhere('infoadv.destinazioni_vendute8', 'like', $value);
                        break;

                        default:
                            $query->where($key, '=', $value);
                        break;
                    }
                }
            });
        if(!Auth::user()->permessi)
			$data=$data->where('customers.authorId', 'like', '%'.'^'.$user_id.'^'.'%');
        $data=$data
        ->select(
            'customers.*',
            'info.codice_fiscale',
            DB::raw('IF(DATE_FORMAT(customers.updated_at, "%d-%m-%Y")="00-00-0000",DATE_FORMAT(customers.created_at, "%d-%m-%Y"),DATE_FORMAT(customers.updated_at, "%d-%m-%Y")) as data'));
        $this->log("a","a",$data->toSql());
        $aziendeItaliane=$data->get();

            return view('admin.adv',[ 'task_total' => $task_total,'aziendeItaliane' => $aziendeItaliane,'task_open' => $task_total, 'regioni' => $regioni,'lastId' => $lastId ] );
    }

    public function index_ext(Request $request=null)
    {
        $user_id = Auth::user()->id;
        $task_total =  Task::where(['user_id'=>$user_id,'status' => 'attivo'])->orWhere(['user_assigned_id'=>$user_id,'status' => 'attivo'])->orderBy('category_id','asc')->orderBy('task_deadline','asc')->get();
        $task_total = count($task_total);
        $regioni = $this->comuniCtrl->getRegione();
        $lastId= DB::table('customers')->max('id');

        $where=[];
        $where['adv']=1;
        $where['italiana_o_estera']=1;


        if(isset($request))
        {
            $inputs = $request->all();
            foreach ($inputs as $key => $value)
                if(!empty($value))
                    $where[str_replace("wjk", ".",$key)]=$value;
        }

        $data = DB::table('customers')
        ->leftJoin('info', 'customers.id', '=', 'info.customer_id')
        ->where(function ($query) use ($where) {
            foreach ($where as $key => $value) {
                    switch ($key) {
                        case 'infoviaggi.destinazione':
                            $query->orWhere('infoadv.destinazioni_vendute1', 'like', $value);
                            $query->orWhere('infoadv.destinazioni_vendute2', 'like', $value);
                            $query->orWhere('infoadv.destinazioni_vendute3', 'like', $value);
                            $query->orWhere('infoadv.destinazioni_vendute4', 'like', $value);
                            $query->orWhere('infoadv.destinazioni_vendute5', 'like', $value);
                            $query->orWhere('infoadv.destinazioni_vendute6', 'like', $value);
                            $query->orWhere('infoadv.destinazioni_vendute7', 'like', $value);
                            $query->orWhere('infoadv.destinazioni_vendute8', 'like', $value);
                        break;

                        default:
                            $query->where($key, '=', $value);
                        break;
                    }
                }
            });
        if(!Auth::user()->permessi)
            $data=$data->where('customers.authorId', 'like', '%'.'^'.$user_id.'^'.'%');

        $data=$data
        ->select(
            'customers.*',
            'info.codice_fiscale',
            DB::raw('IF(DATE_FORMAT(customers.updated_at, "%d-%m-%Y")="00-00-0000",DATE_FORMAT(customers.created_at, "%d-%m-%Y"),DATE_FORMAT(customers.updated_at, "%d-%m-%Y")) as data'));
        $aziendeEstere=$data->get();

            return view('admin.adv_ext',[ 'task_total' => $task_total,'aziendeItaliane' => $aziendeEstere,'task_open' => $task_total, 'regioni' => $regioni,'lastId' => $lastId ] );
    }

    public function search_adv()
    {
        $user_id = Auth::user()->id;
        $task_total =  Task::where(['user_id'=>$user_id,'status' => 'attivo'])->orWhere(['user_assigned_id'=>$user_id,'status' => 'attivo'])->orderBy('category_id','asc')->orderBy('task_deadline','asc')->get();
        $task_total = count($task_total);

        return view('admin.advSearch',['task_total' => $task_total,'task_open' => $task_total ] );
    }

    public function search_adv_ext()
    {
        $user_id = Auth::user()->id;
        $task_total =  Task::where(['user_id'=>$user_id,'status' => 'attivo'])->orWhere(['user_assigned_id'=>$user_id,'status' => 'attivo'])->orderBy('category_id','asc')->orderBy('task_deadline','asc')->get();
        $task_total = count($task_total);

        return view('admin.advSearch',['task_total' => $task_total,'task_open' => $task_total ] );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.edit-adv');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            if ($request) {
                $this->log("adv-ita","store",$request->all());

                $privacy = false;

                if ($request->info_privacy == 'on')
                    $privacy = true;

                $privacy_commerciale = false;
                if ($request->info_privacy_commerciali == 'on')
                    $privacy_commerciale = true;

                $privacy_profilazione = false;
                if ($request->info_privacy_profilazione == 'on')
                    $privacy_profilazione = true;

                if(empty($request->id))
                    $cust = new Customer();
                else
                    $cust = Customer::find($request->id);

                $cust->societa = $request->societa;
                $cust->cognome = $request->cognome;
                $cust->nome = $request->nome;
                $cust->citta = $request->citta;
                $cust->provincia = $request->provincia;
                $cust->cap = $request->cap;
                $cust->regione = $request->regione;
                $cust->stato = $request->stato;
                $cust->tel1 = $request->tel1;
                $cust->tel2 = $request->tel2;
                $cust->tel3 = $request->tel3;
                $cust->email1 = $request->email1;
                $cust->email2 = $request->email2;
                $cust->status = $request->status;
                $cust->adv = 1;
                $cust->italiana_o_estera = $request->italiana_o_estera;
                $cust->authorId = "^".Auth::user()->id."^";


                if(empty($request->id))
                    $cust->save();
                else
                    $cust->update();

                if(empty($request->id))
                    $infoadv = new InfoAdv();

                else {
                    $infoadv = InfoAdv::where('customer_id', $request->id)->get();
                    if (!empty($infoadv)){
                      $infoadv = InfoAdv::find($infoadv[0]->id);
                    }
                    $this->log("adv-ita","2",$infoadv);
                }

                $infoadv->fatturato_medio = $request->infoadv_fatturato;
                $infoadv->numero_addetti = $request->infoadv_addetti;
                $infoadv->clienti = $request->infoadv_clienti;
                $infoadv->destinazioni_vendute1 = $request->infoadv_destinazione1;
                $infoadv->destinazioni_vendute2 = $request->infoadv_destinazione2;
                $infoadv->destinazioni_vendute3 = $request->infoadv_destinazione3;
                $infoadv->destinazioni_vendute4 = $request->infoadv_destinazione4;
                $infoadv->destinazioni_vendute5 = $request->infoadv_destinazione5;
                $infoadv->destinazioni_vendute6 = $request->infoadv_destinazione6;
                $infoadv->destinazioni_vendute7 = $request->infoadv_destinazione7;
                $infoadv->destinazioni_vendute8 = $request->infoadv_destinazione8;
                $infoadv->info_adv_sito = $request->info_adv_sito;
                $infoadv->business_travel = $request->infoadv_businesstravel;
                $infoadv->leisure = $request->infoadv_leisure;
                $infoadv->biglietteria = $request->infoadv_biglietteria;
                $infoadv->note = $request->advnote;
                $infoadv->customer_id = $cust->id;

                $this->log("adv-ita","post-save",$infoadv);
                $this->log("adv-ita","save2","save2");
                if(empty($request->id))
                    $infoadv->save();
                else
                    $infoadv->update();



                if(empty($request->id))
                    $info = new Info();
                else{
                    $info = Info::where('customer_id', $request->id)->get();
                    $info = Info::find($info[0]->id);
                }

                $info->indirizzo = $request->info_indirizzo;
                $info->sito_web = $request->info_web;
                $info->partita_iva = $request->info_piva;
                $info->data_nascita =  $request->info_data_nascita;

                $info->codice_fiscale = $request->info_codice_fiscale;
                $info->sesso = $request->info_sesso;
                $info->posizione = $request->info_posizione;
                $info->suffisso = $request->info_suffisso;
                $info->tel4 = $request->info_tel4;
                $info->tel5 = $request->info_tel5;
                $info->fax1 = $request->info_fax1;
                $info->fax2 = $request->info_fax2;
                $info->email2 = $request->info_mail2;
                $info->indirizzo2 = $request->info_indirizzo2;
                $info->citta2 = $request->info_citta2;
                $info->provincia2 = $request->info_provincia2;
                $info->cap2 = $request->info_cap2;
                $info->regione2 = $request->info_regione2;
                $info->stato2 = $request->info_stato2;
                $info->privacy = $privacy;
                $info->privacy_commerciali  = $privacy_commerciale;
                $info->privacy_profilazione = $privacy_profilazione;
                $info->customer_id = $cust->id;
                $this->log("adv-ita","save3","save3");

                if(isset($request->statiStato))
                    for($i=0;$i<count($request->statiStato);$i++) {
                        if(empty($request->statiNota[$i]))
                            DB::table('customers_relations')->insert(
                                [
                                    'status' => $request->statiStato[$i],
                                    'userid' => Auth::user()->id,
                                    'customerid' => $cust->id
                                ]
                            );
                        else
                            DB::table('customers_relations')->insert(
                                [
                                    'status' => $request->statiStato[$i],
                                    'userid' => Auth::user()->id,
                                    'customerid' => $cust->id,
                                    'nota' => $request->statiNota[$i]
                                ]
                            );

                    }

                if(empty($request->id))
                    $info->save();
                else
                    $info->update();


                return redirect('admin/adv/edit/'.$cust->id)->with('message', 'Cliente Inserito!');
            }
        }
        catch(\Exception $e){
            return redirect()->back()->with('error', 'Errore Inserimento Cliente!' . $e);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user_id = Auth::user()->id;
        $cust = Customer::find($id);
        $info = Info::where('customer_id', $id)->first();
        $infoadv = InfoAdv::where('customer_id', $id)->first();



        $task_total =  Task::where(['user_id'=>$user_id,'status' => 'attivo'])->orWhere(['user_assigned_id'=>$user_id,'status' => 'attivo'])->orderBy('category_id','asc')->orderBy('task_deadline','asc')->get();
        $task_total = count($task_total);

        if ($cust) {
            if (Auth::user()->admin == 1) {
                return view('admin.show-customer-adv', ['id' => $id,
                            'task_total' => $task_total,
                            'task_open' => $task_total,
                            'customer' => $cust,
                            'info' => $info,
                            'infoadv' => $infoadv,

                ]);
            }
            return view('user.show-customer-adv', ['id' => $id, 'task_total' => $task_total,'task_open' => $task_total, 'customer' => $cust,
                'info' => $info,
                'infoadv' => $infoadv,
            ]);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function newadv()
    {
        $user_id = Auth::user()->id;
        $task_total =  Task::where(['user_id'=>$user_id,'status' => 'attivo'])->orWhere(['user_assigned_id'=>$user_id,'status' => 'attivo'])->orderBy('category_id','asc')->orderBy('task_deadline','asc')->get();
        $cust = new Customer();
        $task_total = count($task_total);
        $regioni = DB::table('italy_regions')->get();
        $info = new Info;
        $infoadv = new InfoAdv;

        return view('admin.edit-adv',
            [
                'task_total' => $task_total,
                'task_open' => $task_total,
                'cust' => $cust,
                'regioni' => $regioni,
                'info' => $info,
                'infoadv' => $infoadv,
                'customersRelations' => []
            ]);
    }

    public function newadv_ext()
    {
        $user_id = Auth::user()->id;
        $task_total =  Task::where(['user_id'=>$user_id,'status' => 'attivo'])->orWhere(['user_assigned_id'=>$user_id,'status' => 'attivo'])->orderBy('category_id','asc')->orderBy('task_deadline','asc')->get();
        $cust = new Customer();
        $task_total = count($task_total);
        $regioni = DB::table('italy_regions')->get();
        $info = new Info;
        $infoadv = new InfoAdv;

            if (Auth::user()->admin == 1) {
                return view('admin.edit-adv_ext',
                    [
                        'task_total' => $task_total,
                        'task_open' => $task_total,
                        'cust' => $cust,
                        'regioni' => $regioni,
                        'info' => $info,
                        'infoadv' => $infoadv,
                        'customersRelations' => []
                    ]);
            }

            return view('user.edit-adv_ext', [
                'task_total' => $task_total,
                'task_open' => $task_total,
                'cust' => $cust,
                'regioni' => $regioni,
                'info' => $info,
                'infoadv' => $infoadv,
                'customersRelations' => []
            ]);

    }

    public function edit($id)
    {
        $user_id = Auth::user()->id;
        $cust = Customer::find($id);
        $task_total =  Task::where(['user_id'=>$user_id,'status' => 'attivo'])->orWhere(['user_assigned_id'=>$user_id,'status' => 'attivo'])->orderBy('category_id','asc')->orderBy('task_deadline','asc')->get();
        $task_total = count($task_total);
        $regioni = DB::table('italy_regions')->get();
        $info = Info::where('customer_id', $id)->first();
        $infoadv = InfoAdv::where('customer_id', $id)->first();
        $lastId= DB::table('customers')->max('id');
        $customersRelations= DB::table('customers_relations')
        ->join('users', 'users.id', '=', 'customers_relations.userid')
        ->where('customers_relations.customerid', $id)
        ->select(['customers_relations.*','users.name','users.surname'])
        ->get();
        $data=[
                        'id' => $id,
                        'task_total' => $task_total,
                        'task_open' => $task_total,
                        'customer' => $cust,
                        'regioni' => $regioni,
                        'info' => $info,
                        'infoadv' => $infoadv,
                        'info_adv' => $infoadv,
                        'lastId' => $lastId,
                        'customersRelations' => $customersRelations
                    ];
        return view('admin.edit-adv',$data);
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function data(Datatables $datatables)
    {

        if(Auth::user()->permessi)
            $query = DB::table('customers')->where('adv', 1)->where('adv', 1)->where('italiana_o_estera', 0)->select('customers.*', DB::raw('IF(DATE_FORMAT(customers.updated_at, "%d-%m-%Y")="00-00-0000",DATE_FORMAT(customers.created_at, "%d-%m-%Y"),DATE_FORMAT(customers.updated_at, "%d-%m-%Y")) as data '));
        else
         $query = DB::table('customers')->where('adv', 1)->where('adv', 1)->where('customers.authorId', 'like', '%'.'^'.$user_id.'^'.'%')->where('italiana_o_estera', 0)->select('customers.*', DB::raw('IF(DATE_FORMAT(customers.updated_at, "%d-%m-%Y")="00-00-0000",DATE_FORMAT(customers.created_at, "%d-%m-%Y"),DATE_FORMAT(customers.updated_at, "%d-%m-%Y")) as data '));

        return Datatables::of($query)
            ->addColumn('azioni', function($query) {
                if (Auth::user()->admin == 1) {
                    $url = '../admin/adv/' . $query->id;
                } else {
                    $url = '../user/adv/' . $query->id;
                }
                if (Auth::user()->admin == 1) {
                    $urlEdit = '../admin/adv/edit/' . $query->id;
                } else {
                    $urlEdit = '../user/adv/edit/' . $query->id;
                }
                return '<a href="' . $url . '" class="btn btn-xs btn-primary"><i class="glyphicon glyphicon-search"></i>&nbsp;Visualizza</a>&nbsp;&nbsp;<a href="' . $urlEdit . '" class="btn btn-xs btn-secondary"><i class="glyphicon glyphicon-edit"></i>&nbsp;Modifica</a><a href="#" class="btn btn-xs btn-danger news-zama" style="margin-left: 10px;" onclick="deleteRecord(this)"><i class="glyphicon glyphicon-minus"></i>&nbsp;Elimina</a>';

            })->rawColumns(['azioni'])->make(true);
    }

    public function data_ext(Datatables $datatables)
    {


        if(Auth::user()->permessi)
            $query = DB::table('customers')->where('adv', 1)->where('italiana_o_estera', 1)->select('customers.*', DB::raw('IF(DATE_FORMAT(customers.updated_at, "%d-%m-%Y")="00-00-0000",DATE_FORMAT(customers.created_at, "%d-%m-%Y"),DATE_FORMAT(customers.updated_at, "%d-%m-%Y")) as data '));
        else
            $query = DB::table('customers')->where('customers.authorId', 'like', '%'.'^'.$user_id.'^'.'%')->where('adv', 1)->where('italiana_o_estera', 1)->select('customers.*', DB::raw('IF(DATE_FORMAT(customers.updated_at, "%d-%m-%Y")="00-00-0000",DATE_FORMAT(customers.created_at, "%d-%m-%Y"),DATE_FORMAT(customers.updated_at, "%d-%m-%Y")) as data '));


        return Datatables::of($query)
            ->addColumn('azioni', function($query) {
                if (Auth::user()->admin == 1) {
                    $url = '../admin/adv/' . $query->id;
                } else {
                    $url = '../user/adv/' . $query->id;
                }
                if (Auth::user()->admin == 1) {
                    $urlEdit = '../admin/adv/edit/' . $query->id;
                } else {
                    $urlEdit = '../user/adv/edit/' . $query->id;
                }
                return '<a href="' . $url . '" class="btn btn-xs btn-primary"><i class="glyphicon glyphicon-search"></i>&nbsp;Visualizza</a>&nbsp;&nbsp;<a href="' . $urlEdit . '" class="btn btn-xs btn-secondary"><i class="glyphicon glyphicon-edit"></i>&nbsp;Modifica</a><a href="#" class="btn btn-xs btn-danger news-zama" style="margin-left: 10px;" onclick="deleteRecord(this)"><i class="glyphicon glyphicon-minus"></i>&nbsp;Elimina</a>';

            })->rawColumns(['azioni'])->make(true);
    }
}
