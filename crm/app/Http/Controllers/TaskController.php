<?php

namespace App\Http\Controllers;

use App\Category;
use App\Mail\TaskCompleted;
use App\Task;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\DB;


class TaskController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request=null)
    {


        $user_id = Auth::user()->id;
        $users = User::orderBy('username')->get();
        $categories = Category::all();
        $reparti=DB::table('reparti')->join('user_reparti', 'reparti.id', '=', 'user_reparti.repid')->join('users', 'users.id', '=', 'user_reparti.userid')
            ->selectRaw('GROUP_CONCAT(users.id) as userIdList , reparti.id, reparti.reparti as nome')->groupBy('reparti.id', 'reparti.reparti')->get();

        $link_to="";
        $msg_link_to="";

        $task_open_where=[
            'status' => 'attivo'
        ];

        $task_closed_where=[
            'status' => 'chiuso'
        ];

        $user_open_task=[
            'user_id'=>$user_id,
            'status'=>'attivo'
        ];

        $user_or_where=[
            'user_assigned_id'=>$user_id,
            'status'=>'attivo'
        ];

        $user_closed_task=[
            'user_id'=>$user_id,
            'status'=>'chiuso'
        ];

        $user_closed_or_where=[
            'user_assigned_id'=>$user_id,
            'status'=>'chiuso'
        ];

        if($request)
        {
            $this->log("task","store",$request->all());
            $filters=$request['action'];
            if($filters=="linkto")
            {

                $id=$request['n'];
                switch ($request['ent']) {
                    case 'business':
                        $msg1=DB::table("business")->where("id",$id)->select("azienda")->get();
                        $msg2=DB::table("business_relations")->where("customerid",$id)->select("status")->get();
                        $this->log("tasks","msg1",$msg1);
                        $this->log("tasks","msg2",$msg2);
                        if(isset($msg1[0]) && isset($msg1[0]->azienda))
                            $msg_link_to=": per l'azienda ".$msg1[0]->azienda;
                        if(isset($msg2[0]) && !empty($msg2[0]->status))
                            $link_to="Azione : ".$msg2[0]->status;
                        else
                            $link_to=$msg1[0]->azienda;

                        $task_open_where['entity_id']=$id;
                        $task_open_where['collegato_a']='business';

                        $task_closed_where['entity_id']=$id;
                        $task_closed_where['collegato_a']='business';

                        $user_open_task['entity_id']=$id;
                        $user_open_task['collegato_a']='business';

                        $user_closed_task['entity_id']=$id;
                        $user_closed_task['collegato_a']='business';

                        $user_or_where['entity_id']=$id;
                        $user_or_where['collegato_a']='business';

                        $user_closed_or_where['entity_id']=$id;
                        $user_closed_or_where['collegato_a']='business';

                    break;

                    case 'customer':
                        $msg1=DB::table("customers")->where("id",$id)->select(["cognome","nome"])->get();
                        $msg2=DB::table("customers_relations")->where("customerid",$id)->select("status")->get();
                        $this->log("tasks","linkto",$msg2);

                        if(isset($msg1[0]) && !empty($msg1[0]->cognome) || !empty($msg1[0]->nome))
                            $msg_link_to=": per il privato ".$msg1[0]->cognome." ".$msg1[0]->nome;
                        else
                            $msg_link_to="";

                        if(isset($msg2[0]))
                            $link_to="Azione : ".$msg2[0]->status;

                        $task_open_where['entity_id']=$id;
                        $task_open_where['collegato_a']='customer';

                        $task_closed_where['entity_id']=$id;
                        $task_closed_where['collegato_a']='customer';

                        $user_open_task['entity_id']=$id;
                        $user_open_task['collegato_a']='customer';

                        $user_closed_task['entity_id']=$id;
                        $user_closed_task['collegato_a']='customer';

                        $user_or_where['entity_id']=$id;
                        $user_or_where['collegato_a']='customer';

                        $user_closed_or_where['entity_id']=$id;
                        $user_closed_or_where['collegato_a']='customer';

                    break;

                    case 'preventivo':
                        $data=DB::table("preventivo")
                        ->join("customers","riferitoA","=","customers.id")
                        ->where("preventivo.id","=",$id)
                        ->select(["preventivo.labeldest","customers.cognome","customers.nome"])->get();

                        $msg_link_to="";

                        if(isset($data[0])){
                            $msg_link_to=": per il preventivo ".$data[0]->labeldest." - ".$data[0]->cognome." ".$data[0]->nome;
                            $link_to="Preventivo ".$data[0]->labeldest." - ".$data[0]->cognome." ".$data[0]->nome;
                        }

                        $task_open_where['entity_id']=$id;
                        $task_open_where['collegato_a']='preventivo';

                        $task_closed_where['entity_id']=$id;
                        $task_closed_where['collegato_a']='preventivo';

                        $user_open_task['entity_id']=$id;
                        $user_open_task['collegato_a']='preventivo';

                        $user_closed_task['entity_id']=$id;
                        $user_closed_task['collegato_a']='preventivo';

                        $user_or_where['entity_id']=$id;
                        $user_or_where['collegato_a']='preventivo';

                        $user_closed_or_where['entity_id']=$id;
                        $user_closed_or_where['collegato_a']='preventivo';
                    break;

                    default:

                        break;
                }
            }
        }

        if (Auth::user()->admin == 1) {

            $tasks = Task::where($task_open_where)->orderBy('category_id','asc')->orderBy('task_deadline','asc')->get();
            $tasksCompleted = Task::where($task_closed_where)->orderBy('task_deadline','desc')->get();
            $task_total = count($tasks)+count($tasksCompleted);
            $task_open = count($tasks);
            $task_done = count($tasksCompleted);
            $myPendingTask = $task_total-$task_done;

            return view('admin.tasks',['users' => $users, 'categories' => $categories, 'tasks' => $tasks, 'task_total' => $task_total, 'task_done' => $task_done, 'myPendingTask' => $myPendingTask, 'task_open' => $task_open, 'tasksCompleted' => $tasksCompleted,'reparti' => $reparti, 'link_to' => $link_to , 'msg_link_to' => $msg_link_to ]);
        } else {

            $tasks = Task::where($user_open_task)->orWhere($user_or_where)->orderBy('category_id','asc')->orderBy('task_deadline','asc')->get();
            $tasksCompleted = Task::where($user_closed_task)->orWhere($user_closed_or_where)->orderBy('task_deadline','desc')->get();
            $task_total = count($tasks)+count($tasksCompleted);
            $task_open = count($tasks);
            $task_done = count($tasksCompleted);

            return view('user.tasks', ['tasks' => $tasks,  'categories' => $categories,'task_total' => $task_total,'users' => $users, 'task_done' => $task_done, 'task_open' => $task_open, 'tasksCompleted' => $tasksCompleted,'reparti' => $reparti , 'link_to' => $link_to , 'msg_link_to' => $msg_link_to]);
        }

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function sendCurl($fields,$url='http://hellocreativestudio.net/mail/mailer.php'){
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, ['data' => $fields]);
        $result = curl_exec($ch);
        curl_close($ch);
        return $result;
    }

    public function create()
    {

        return view('admin.new-task',[
                'msg' => 'succ'
            ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $this->log("tasks","store.request",$request->all());

                $date = Carbon::now();
                $last_task_id=0;
                $userAssignedList = explode("," , $request->userAssigned);
                    if(count($userAssignedList)>1)
                        $request->task_fcfs="1";

                    for($i=1;$i<count($userAssignedList);$i++){

                        if(empty($userAssignedList[$i]))
                            continue;

                        $task = new Task();
                        $task->task_note = $request->task_note;
                        $task->task_deadline = isset($request->task_deadline) ? $request->task_deadline : $date->addDays(5);
                        $task->task_marked = 0;
                        $task->category_id = $request->category_id;
                        $task->user_assigned_id = Auth::user()->id;
                        $task->user_id = $userAssignedList[$i];
                        $task->status = "attivo";
                        $task->startDate = $request->startDate;

                        if(isset($request->entity_id)){
                            $task->collegato_a = $request->collegato_a;
                            $task->entity_id = $request->entity_id;
                        }
                        /*lv 0 nessuna impostazione, Notifica ogni giorno*/
                        if(isset($request->schedulato_lv_0))
                            $task->schedulato_lv = 0;

                        /* lv 1 Solo il giorno prima della scadenza */
                        if(isset($request->schedulato_lv_1)){
                            $task->schedulato_lv = 1;
                                $start = new \Datetime($task->task_deadline);
                                $start->modify("-1 days");
                                $task->schedulato = $start->format("Y-m-d");
                                $task->schedulato = $start->format("Y-m-d");
                        }
                        /* lv 2 ogni settimana */
                        if(isset($request->schedulato_lv_2)){
                            $task->schedulato_lv = 2;
/*                            $start = new \Datetime($task->startDate);
                            $start->modify("+7 days");
                            $task->schedulato = $start->format("Y-m-d");*/
                            $task->schedulato = $request->startDate;
                        }
                        /* lv 3 ogni mese */
                        if(isset($request->schedulato_lv_3)){
                            $task->schedulato_lv = 3;
/*                            $start = new \Datetime($task->startDate);
                            $start->modify("+30 days");
                            $task->schedulato = $start->format("Y-m-d");*/
                            $task->schedulato = $request->startDate;
                        }

                        if(isset($request->task_supervisor))
                            $task->task_supervisor = $request->task_supervisor;

                        //$task->schedulato = $request->schedulato;
                        $task->personal= 0;
                        $task->save();

                        if($request->task_fcfs=="1"){

                          DB::table('task_FCFS')->insert([
                            'id'=>NULL,
                            'taskid'=>$task->id,
                            'descrizione'=>$task->task_note
                            ]);
                        }
                        $this->log("tasks","store.postsave",$task);
                        $last_task_id=$task->id;
                }

                $this->log("tasks","create-email",$last_task_id);

                $email=$this->get_task_users_email($last_task_id);
                $incaricati=$this->get_task_users($last_task_id);

                $body="Assegnato da: ".Auth::user()->name." ".Auth::user()->surname;
                $body.="<br>Incaricato/i del task: ".$incaricati;
                $body.="<br>oggetto: ".$task->task_note;
                $body.="<br> Deadline: ".$task->task_deadline." \n ";

                $curl1=$this->sendCurl(
                    json_encode(
                    [
                    'address'=>$email,
                    'subject'=>"Suend-crm - un nuovo task e' stato assegnato",
                    'body'=>$body
                ]));

                return redirect()->back()->with('message', 'Task Assegnato!');
 }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

        $task  =  Task::find($id);
        return $task;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return view('admin.edit-task', ['id'=> $id]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $task  =  Task::find($id);
        if($task) {
            $task->task_note = $request->note;
            $task->task_deadline = $request->note;
            $task->task_done = 0;
            if (Auth::user()->admin == 1) {
                $task->personal= 0;
            } else {
                $task->personal= 1;
            }
            $task->category_id = $request->categoria_id;
            $task->priority_id = $request->priorita_id;
            $task->startDate = $request->startDate;
            $task->save();
        }
        return $task;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $task = Task::find($id);
            if($task) {
                $task->delete();
                return redirect()->back()->with('success', 'File eliminato!');
            }
        }
        catch(\Exception $e){
            return redirect()->back()->with('error', 'Errore Inserimento File!' . $e);
        }

    }

    public static function countTasks($user_id)
    {
        $task_number  =  Task::where('user_id',Auth::user()->id)->count();
        return $task_number;
    }

    public static function countAdminTasks()
    {
        $task_number  =  Task::where('user_id',Auth::user()->id)->count();
        return $task_number;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function updateTaskStatus($id,$status)
    {
        try {
            $task = Task::find($id);
            if($task) {
                $task = Task::findOrFail($id);
                $task->status=$status;
                $task->save();

            }
        }
        catch(\Exception $e){
            return redirect()->back()->with('error', 'Errore Smarcamento Task!' . $e);
        }

    }


    protected function get_task_users_email($taskid) {
        $response=[];

        $task = Task::findOrFail($taskid);

        $user = User::find($task->user_assigned_id);
        $response[]=$user->email;

        $tasks = DB::table("tasks")
            ->where("task_note",$task->task_note)
            ->where("task_deadline",$task->task_deadline)
            ->select("id","user_id")->get();

        $task_list=[];
        $user_list=[];
        foreach ($tasks as $task_id){
            if(!in_array($task_id->user_id,$user_list)){
                $task_list[]=$task_id->id;
                $user_list[]=$task_id->user_id;
            }
        }

        foreach ($task_list as $task_id) {
            $task = Task::findOrFail($task_id);
            $user = User::find($task->user_id);
            $response[]=$user->email;
        }

        $this->log("tasks","get_task_users_email",$response);
        return $response;


    }
    protected function get_task_users($taskid) {
        $task = Task::findOrFail($taskid);
        $response="";
        $tasks = DB::table("tasks")
            ->where("task_note",$task->task_note)
            ->where("task_deadline",$task->task_deadline)
            ->select("id","user_id")->get();

        $task_list=[];
        $user_list=[];
        foreach ($tasks as $task_id){
            if(!in_array($task_id->user_id,$user_list)){
                $task_list[]=$task_id->id;
                $user_list[]=$task_id->user_id;
            }
        }

        foreach ($task_list as $task_id) {
            $task = Task::findOrFail($task_id);
            $user = User::find($task->user_id);
            $response.=$user->name . " " . $user->surname .",";
        }
        $this->log("tasks","get_task_users",$response);
        $response=rtrim($response,",");

        return $response;


    }

    public function sendMail(Request $request) {

            $task = Task::findOrFail($request->task_id);
            $task_user = User::where('id', $task->user_id)->first();
            $data=$request->all();
            $ricevente="";

            /* se è un reparto, incollo su ricevente le email separate da una virgola, diventano i destinatari dell'email */
            if(isset($data['reparto'])&&$data['reparto']!=0)
            {
                $reparto=explode(",", $data['reparto']);
                $utenti = User::whereIn('id', $reparto)->select("email")->get();
                foreach ($utenti as $user)
                    $ricevente.=$user->email;
                $ricevente=rtrim($ricevente,",");
            }
            else
            {
                /**/
                /*user_assigned_id è chi assegna il task */
                $ricevente = User::where('id', $task->user_assigned_id)->first();
                $ricevente = $ricevente->email;
            }

            if($request->task_action==true)
                $status="chiuso";
            else
                $status="rifiutato";

            if(isset($data['reparto'])&&$data['reparto']!=0)
                $this->updateTaskStatus($request->task_id,$status);
            else
                DB::table("tasks")->where("task_note" ,$task->task_note)->update(["status" => "chiuso"]);

            // se è personale no invio mail
            $no_repeat_email=[];
            if($status=="chiuso"){

                $isFCFS=DB::table("task_FCFS")->where("taskid","=",$request->task_id)->get();
                if(isset($isFCFS[0]))
                   $allTasks=DB::table("tasks")->where("task_note",$isFCFS[0]->descrizione)->update(['status' => "chiuso"]);

                $this->sendCurl(
                    json_encode(
                    [
                    'address'=>$ricevente,
                    'subject'=>"Suend-crm task completato",
                    'body'=>'Il Task: ' . $task->task_note . ' è stato completato da ' . $task_user->username
                ]));

                return redirect()->back()->with('success', 'Task chiuso!');
            }

            if($status=="rifiutato"){

                $this->sendCurl(
                    json_encode(
                    [
                    'address'=>$ricevente,
                    'subject'=>"Suend-crm task rifiutato",
                    'body'=>'Il Task: ' . $task->task_note . ' è stato rifiutato da ' . $task_user->username
                ]));
                return redirect()->back()->with('success', 'Task rifiutato');
            }

    }

}
