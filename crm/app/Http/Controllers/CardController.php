<?php


namespace App\Http\Controllers;

use App\Card;
use App\Task;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Yajra\Datatables\Facades\Datatables;


class CardController extends Controller
{
    public $comuniCtrl;

    public function __construct()
    {
        $this->comuniCtrl = new ComuniController();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request=null)
    {

        $user_id = Auth::user()->id;
        $task_total =  Task::where(['user_id'=>$user_id,'status' => 'attivo'])->orWhere(['user_assigned_id'=>$user_id,'status' => 'attivo'])->orderBy('category_id','asc')->orderBy('task_deadline','asc')->get();
        $task_total = count($task_total);
        $cards=DB::table("card")
        ->leftJoin("customers","customers.id","card.customer_cards")
        ->leftJoin("business","business.id","card.business_cards")
        ->select('card.*')
        ->selectRaw(' toEur(card.value) as value, DATE_FORMAT(deadline ,\'%d/%m/%Y\') as deadline ')
        ->selectRaw('  coalesce(concat(business.ragione),"Non assegnato")  as azienda, business.id as id_azienda ')
        ->selectRaw(' coalesce(concat(customers.cognome," ",customers.nome),"Non assegnato") as privato , customers.id as id_privato ')
        ->get();

        // ../admin/cliente/20?

        return view('admin.cards',[ 'task_total' => $task_total,'cards' => $cards,'task_open' => $task_total] );
    }



    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $user_id = Auth::user()->id;
        $task_total =  Task::where(['user_id'=>$user_id,'status' => 'attivo'])->orWhere(['user_assigned_id'=>$user_id,'status' => 'attivo'])->orderBy('category_id','asc')->orderBy('task_deadline','asc')->get();
        $task_total = count($task_total);

        $code=$this->generateCode();

        $card=new Card();


        return view('admin.form-card',[ 'task_total' => $task_total,'task_open' => $task_total,'code' => $code,'card' => $card ,'preventivi' =>[] ]);
    }

    private function generateCode($startingCode=false){
        if(!$startingCode){
            $startingCode=DB::select("SELECT max(id) as max FROM `card`");
            if(empty($startingCode))
                $startingCode=1;
            else
                $startingCode=$startingCode[0]->max;
        }
        $l=strlen($startingCode);
        return str_repeat("0",(6-$l)).$startingCode."#".date("Y");
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */


    public function store(Request $request)
    {

        $this->log("card","card.store",$request->all());
        $data=$request->all();

        unset($data['_token']);
        $ripetizioni=$data['ripetizioni'];
        unset($data['ripetizioni']);
        unset($data['bah']);
        for($i=0;$i<$ripetizioni;$i++){
            $card= Card::firstOrNew($data);
            $card->save();
            $data['code']=$this->generateCode($card->id);

        }
        return redirect()->back()->with('message', 'Aggiornamento completato!');
    }

    public function copy(Request $request)
    {

        $this->log("card","card.copy",$request->all());
        $data=$request->all();
        $id=$data['id'];
        $ripetizioni=$data['ripetizioni'];
        
        foreach ($id as $card_id) {
            $card=Card::find($card_id);
            for($i=0;$i<$ripetizioni;$i++){
                $newCard= $card->replicate();
                $code=$this->generateCode();
                $newCard->code=$code;
                $newCard->save();
            }
        }
        return json_encode(['message' => 'done']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

        $user_id = Auth::user()->id;
        $card = Card::find($id);
        $task_total =  Task::where(['user_id'=>$user_id,'status' => 'attivo'])->orWhere(['user_assigned_id'=>$user_id,'status' => 'attivo'])->orderBy('category_id','asc')->orderBy('task_deadline','asc')->get();
        $task_total = count($task_total);
         
        $code=$this->generateCode();

        return view('admin.form-card', [
            'id' => $id,
            'task_total' => $task_total,
            'task_open' => $task_total,
            'edit' => false,
            'code' => $code,
            'card' => $card,
        ]);
          
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */



    public function edit($id)
    {

        $user_id = Auth::user()->id;
        $card = Card::find($id);
        $task_total =  Task::where(['user_id'=>$user_id,'status' => 'attivo'])->orWhere(['user_assigned_id'=>$user_id,'status' => 'attivo'])->orderBy('category_id','asc')->orderBy('task_deadline','asc')->get();
        $task_total = count($task_total);

        $preventivi=DB::table("card_user_activity")
        ->join("preventivo","card_user_activity.preventivo_id","preventivo.id")
        ->where("coupon_id",$id)
        ->select(["card_user_activity.*","preventivo.id as preventivo_id","preventivo.labeldest"])
        ->selectRaw(' toEur(card_user_activity.spesa) as spesa, DATE_FORMAT(card_user_activity.data_utilizzo ,\'%d/%m/%Y\') as data_utilizzo ')
        ->get();

        return view('admin.form-card', [
            'id' => $id,
            'task_total' => $task_total,
            'task_open' => $task_total,
            'edit' => true,
            'code' => $card->code,
            'preventivi' => $preventivi,
            'card' => $card,
        ]);
          
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
   
}
