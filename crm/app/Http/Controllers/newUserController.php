<?php

namespace App\Http\Controllers;

use App\Customer;
use App\Info;
use App\InfoAdv;
use App\InfoTravel;
use App\Task;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Yajra\Datatables\Facades\Datatables;

class NewUserController extends Controller
{
    public $comuniCtrl;

    public function __construct()
    {
        $this->comuniCtrl = new ComuniController();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user_id = Auth::user()->id;
        $task_total =  TaskController::countAdminTasks();
        $regioni = $this->comuniCtrl->getRegione();

        if (Auth::user()->admin == 1) {
            return view('admin.new-user',[ 'task_total' => $task_total, 'regioni' => $regioni ] );
        }
        return view('user.customer-adv',[ 'task_total' => $task_total, 'regioni' => $regioni  ] );
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('create-customer');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $admin = false;
        if(isset($request->admin)&&$request->admin == 'on')
            $admin = true;

        try {
                    DB::insert('insert into users (id,name,surname,username,email,password,admin) values (?,?, ?, ?, ?, ?, ?)',array(
                        NULL, 
                        $request->name,
                        $request->surname,
                        $request->username,
                        $request->email,
                        bcrypt($request->password),
                        $admin
                    ));
                    
                return redirect()->back()->with('message', 'Cliente Inserito!');
            }
        catch(\Exception $e){
            return redirect()->back()->with('error', 'Errore Inserimento Cliente!' . $e->getMessage());
        }
    }

    public function storeedit(Request $request)
    {
        try {
            
            if ($request) {
                DB::table('users')
                    ->where('id', $request['id'])
                    ->update([
                        'name'=>$request['name'],
                        'surname'=>$request['surname'],
                        'username'=>$request['username'],
                        'email'=>$request['email']
                        ]);

                        /**/
                $user_id = Auth::user()->id;
                $task_total =  TaskController::countAdminTasks();
                $regioni = $this->comuniCtrl->getRegione();
                return view('admin.new-user',[ 'task_total' => $task_total, 'regioni' => $regioni ] )->with('message', 'Cliente Inserito!');
               // return redirect()->back()->with('message', 'Cliente Inserito!');
            }
        }
        catch(\Exception $e){
            return redirect()->back()->with('error', 'Errore Inserimento Cliente!' . $e);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user_id = Auth::user()->id;
        $cust = Customer::find($id);
        $info = Info::where('customer_id', $id)->first();
        $infoadv = InfoAdv::where('customer_id', $id)->first();



        $task_total = Task::where('task_marked', 0)->where('user_id', $user_id)->count();
        if ($cust) {
            if (Auth::user()->admin == 1) {
                return view('admin.show-customer-adv', ['id' => $id,
                            'task_total' => $task_total,
                            'customer' => $cust,
                            'info' => $info,
                            'infoadv' => $infoadv,

                ]);
            }
            return view('user.show-customer-adv', ['id' => $id, 'task_total' => $task_total, 'customer' => $cust,
                'info' => $info,
                'infoadv' => $infoadv,
            ]);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user= DB::select("select * from users where id=$id");
        $user=$user[0];
        
        return view('admin.edit-user',
            [
              'id'=> $user->id,
              'name'=>  $user->name,
              'surname'=> $user->surname,
              'username'=>  $user->username,
              'email'=>  $user->email
            ]
            );
          
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {
            if ($request) {

                /*
                 * TODO: quando diranno le logiche di validazione decommentare e validare come da esempio scritto qui sotto.
                 * Per eventuali dubbi quardare la doc ufficiale
                 *
                 *  $this->validate($request, [
                 *      'title' => 'bail|required|unique:posts|max:255',
                 *      'body' => 'required',
                 *  ]);
                 *
                */

                // per fare veloce
                $privacy = false;

                if ($request->info_privacy == 'on') {
                    $privacy = true;
                }
                $privacy_commerciale = false;
                if ($request->info_privacy_commerciali == 'on') {
                    $privacy_commerciale = true;
                }
                $privacy_profilazione = false;
                if ($request->info_privacy_profilazione == 'on') {
                    $privacy_profilazione = true;
                }


                $cust = Customer::find($id);
                if(isset($cust)) {
                    $cust->societa = $request->societa;
                    $cust->cognome = $request->cognome;
                    $cust->nome = $request->nome;
                    $cust->citta = $request->citta;
                    $cust->provincia = $request->provincia;
                    $cust->cap = $request->cap;
                    $cust->regione = $request->regione;
                    $cust->stato = $request->stato;
                    $cust->tel1 = $request->tel1;
                    $cust->tel2 = $request->tel2;
                    $cust->tel3 = $request->tel3;
                    $cust->email1 = $request->email1;
                    $cust->adv = 1;
                    $cust->save();
                }




                $info = Info::where('customer_id', $id)->first();
                if(isset($info)) {
                    $info->indirizzo = $request->info_indirizzo;
                    $info->sito_web = $request->info_web;
                    $info->partita_iva = $request->info_piva;
                    if ($request->info_data_nascita) {
                        $info->data_nascita =  Carbon::createFromFormat('d/m/Y', $request->info_data_nascita, 'Europe/Rome');
                    } else {
                        $info->data_nascita = null;
                    }

                    $info->codice_fiscale = $request->info_codice_fiscale;
                    $info->sesso = $request->info_sesso;
                    $info->posizione = $request->info_posizione;
                    $info->suffisso = $request->info_suffisso;
                    $info->tel4 = $request->info_tel4;
                    $info->tel5 = $request->info_tel5;
                    $info->fax1 = $request->info_fax1;
                    $info->fax2 = $request->info_fax2;
                    $info->email2 = $request->info_mail2;
                    $info->indirizzo2 = $request->info_indirizzo2;
                    $info->citta2 = $request->info_citta2;
                    $info->provincia2 = $request->info_provincia2;
                    $info->cap2 = $request->info_cap2;
                    $info->regione2 = $request->info_regione2;
                    $info->stato2 = $request->info_stato2;
                    $info->privacy = $privacy;
                    $info->privacy_commerciali  = $privacy_commerciale;
                    $info->privacy_profilazione = $privacy_profilazione;
                    $info->customer_id = $cust->id;
                    $info->save();
                }



                $infoadv = InfoAdv::where('customer_id', $id)->first();
                if(isset($infoadv)) {
                    $infoadv->fatturato_medio = $request->infoadv_fatturato;
                    $infoadv->numero_addetti = $request->infoadv_addetti;
                    $infoadv->clienti = $request->infoadv_clienti;
                    $infoadv->destinazioni_vendute1 = $request->infoadv_destinazione1;
                    $infoadv->destinazioni_vendute2 = $request->infoadv_destinazione2;
                    $infoadv->destinazioni_vendute3 = $request->infoadv_destinazione3;
                    $infoadv->destinazioni_vendute4 = $request->infoadv_destinazione4;
                    $infoadv->destinazioni_vendute5 = $request->infoadv_destinazione5;
                    $infoadv->destinazioni_vendute6 = $request->infoadv_destinazione6;
                    $infoadv->destinazioni_vendute7 = $request->infoadv_destinazione7;
                    $infoadv->destinazioni_vendute8 = $request->infoadv_destinazione8;

                    $infoadv->business_travel = $request->infoadv_businesstravel;
                    $infoadv->leisure = $request->infoadv_leisure;
                    $infoadv->biglietteria = $request->infoadv_biglietteria;
                    $infoadv->customer_id = $cust->id;
                    $infoadv->save();
                }




                return redirect()->back()->with('message', 'Cliente Inserito!');
            }
        }
        catch(\Exception $e){


            return redirect()->back()->with('error', 'Errore Inserimento Cliente!' . $e);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function data(Datatables $datatables)
    {
        $query = DB::table('customers')->where('adv', 1)->select('id','cognome', 'nome','citta', 'provincia','regione','stato','tel1','email1');

        return Datatables::of($query)
            ->addColumn('azioni', function($query) {
                if (Auth::user()->admin == 1) {
                    $url = '../admin/adv/' . $query->id;
                } else {
                    $url = '../user/adv/' . $query->id;
                }
                if (Auth::user()->admin == 1) {
                    $urlEdit = '../admin/adv/edit/' . $query->id;
                } else {
                    $urlEdit = '../user/adv/edit/' . $query->id;
                }
                return '<a href="' . $url . '" class="btn btn-xs btn-primary"><i class="glyphicon glyphicon-search"></i>&nbsp;Visualizza</a>&nbsp;&nbsp;<a href="' . $urlEdit . '" class="btn btn-xs btn-secondary"><i class="glyphicon glyphicon-edit"></i>&nbsp;Modifica</a>';

            })->rawColumns(['azioni'])->make(true);
    }
}
