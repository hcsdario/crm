<?php

namespace App\Http\Controllers;

use App\Customer;
use App\Info;
use App\InfoTravel;
use App\Profile;
use App\QaSuend;
use App\QaTravel;
use App\Task;
//use App\preventiviClienti;
use Carbon\Carbon;
use Debugbar;
use Illuminate\Http\Request;/**/
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class CustomerController extends Controller
{

    public $comuniCtrl;

    public function __construct()
    {
        $this->comuniCtrl = new ComuniController();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
    */

    public function index()
    {
        $user_id = Auth::user()->id;
        $task_total =  Task::where(['user_id'=>$user_id,'status' => 'attivo'])->orWhere(['user_assigned_id'=>$user_id,'status' => 'attivo'])->orderBy('category_id','asc')->orderBy('task_deadline','asc')->get();
        $task_total = count($task_total);
        $regioni = $this->comuniCtrl->getRegione();
        $lastId= DB::table('customers')->max('id');

        return view('admin.customer', ['task_total' => $task_total,'task_open' => $task_total, 'regioni' => $regioni,'lastId' => $lastId,
            'destinazioniPreferite' =>[],
            'destinazioniPassate' =>[],
            'destinazioniProssime' =>[],
            'giftCard' =>[],
            'coupons' =>[],
            'destinazioniDove' =>[]
         ]);
    }

    public function index_ext()
    {
        $user_id = Auth::user()->id;
        $task_total =  Task::where(['user_id'=>$user_id,'status' => 'attivo'])->orWhere(['user_assigned_id'=>$user_id,'status' => 'attivo'])->orderBy('category_id','asc')->orderBy('task_deadline','asc')->get();
        $task_total = count($task_total);
        $regioni = $this->comuniCtrl->getRegione();
        $lastId= DB::table('customers')->max('id');
        return view('admin.customer_ext', ['task_total' => $task_total,'task_open' => $task_total, 'regioni' => $regioni,'lastId' => $lastId]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('create-customer');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */

    private function store_add_dest($array,$label,$table,$customerid,$userid){

        //$request->destinazionipreferite

        for($i=0;$i<count($array[$label]);$i++) {
            $data=$array[$label][$i];
            if(empty($data))
                continue;

            DB::table($table)->insert(
                [
                    'iddest' => $data,
                    'customerid' => $customerid,
                    'userid' => $userid
                ]
            );
        }


    }

    public function store(Request $request)
    {
        try {
            if ($request) {

                $this->log("customer","store",$request->all());

                $privacy = false;

                if ($request->info_privacy == 'on')
                    $privacy = true;

                $privacy_commerciale = false;

                if ($request->info_privacy_commerciali == 'on')
                    $privacy_commerciale = true;

                $privacy_profilazione = false;
                if ($request->info_privacy_profilazione == 'on')
                    $privacy_profilazione = true;

                $cust = new Customer();
                if(isset($request->societa)&&!empty($request->societa))
                    $cust->societa = $request->societa;
                if(isset($request->cognome)&&!empty($request->cognome))
                    $cust->cognome = $request->cognome;
                if(isset($request->nome)&&!empty($request->nome))
                    $cust->nome = $request->nome;

                if($request->info_data_nascita && !empty($request->info_data_nascita))
                    $cust->data_nascita = $request->info_data_nascita;

                if(isset($request->citta)&&!empty($request->citta))
                    $cust->citta = $request->citta;
                if(isset($request->provincia)&&!empty($request->provincia))
                    $cust->provincia = $request->provincia;
                if(isset($request->cap)&&!empty($request->cap))
                    $cust->cap = $request->cap;
                if(isset($request->regione)&&!empty($request->regione))
                    $cust->regione = $request->regione;
                if(isset($request->stato)&&!empty($request->stato))
                    $cust->stato = $request->stato;

                if($cust->citta&&$cust->provincia&&$cust->cap&&$cust->regione){
                    $residenza=$request->residenza;

                    $nuovaCitta=DB::table("italy_cities")->where("comune",$cust->citta);
                    $nuovaCitta=$nuovaCitta->get();

                    if(!isset($nuovaCitta[0]))
                    {
                        $siglaNumerica=$nuovaCitta=DB::table("italy_cities")->where("estera",1)->count();

                        $data1=[
                            "id_citta" => NULL,
                            "comune" => $cust->citta,
                            "regione" => $cust->regione,
                            "provincia" => $siglaNumerica,
                            "cap" => $cust->cap,
                            "estera" => $residenza
                        ];
                        DB::table("italy_cities")->insert($data1);

                        DB::table("italy_provincies")->insert([
                            'sigla' => $siglaNumerica,
                            'provincia' => $cust->provincia
                        ]);

                    }
                }

                if(isset($request->pref1)&&!empty($request->pref1))
                    $cust->pref1 = $request->pref1;
                if(isset($request->tel1)&&!empty($request->tel1))
                    $cust->tel1 = $request->tel1;
                if(isset($request->pref2)&&!empty($request->pref2))
                    $cust->pref2 = $request->pref2;
                if(isset($request->tel2)&&!empty($request->tel2))
                    $cust->tel2 = $request->tel2;
                if(isset($request->email1)&&!empty($request->email1))
                    $cust->email1 = $request->email1;
                if(isset($request->info_mail2)&&!empty($request->info_mail2))
                    $cust->email2 = $request->info_mail2;
                if(isset($request->categoria))
                    $cust->cat_id = $request->categoria;

                $cust->status = $request->status;
                $cust->insertBy = Auth::user()->username;

                if(!empty($request->status))
                    $cust->ultimocontatto=date("Y-m-d");

                $cust->authorId = "^".Auth::user()->id."^";
                $cust->adv = 0;


                $cust->save();

                $this->store_add_dest($request,'destinazionipassate','destinazioniPassate',$cust->id,Auth::user()->id);
                $this->store_add_dest($request,'destinazionipreferite','destinazioniPreferite',$cust->id,Auth::user()->id);
                $this->store_add_dest($request,'destinazioniprossime','destinazioniProssime',$cust->id,Auth::user()->id);
                $this->store_add_dest($request,'destinazioniDove','destinazioniDove',$cust->id,Auth::user()->id);

                $info = new Info();
                $info->indirizzo = $request->info_indirizzo;
                $info->sito_web = $request->info_web;
                $info->partita_iva = $request->info_piva;
                if($request->info_data_nascita && !empty($request->info_data_nascita))
                        $info->data_nascita = $request->info_data_nascita;
                $info->codice_fiscale = $request->info_codice_fiscale;
                $info->sesso = $request->info_sesso;
                $info->posizione = $request->info_posizione;
                $info->suffisso = $request->info_suffisso;
                $info->tel4 = $request->info_tel4;
                $info->tel5 = $request->info_tel5;
                $info->fax1 = $request->info_fax1;
                $info->fax2 = $request->info_fax2;
                $info->email2 = $request->email2;
                $info->indirizzo2 = $request->info_indirizzo2;
                $info->citta2 = $request->info_citta2;
                $info->provincia2 = $request->info_provincia2;
                $info->cap2 = $request->info_cap2;
                $info->regione2 = $request->info_regione2;
                $info->stato2 = $request->info_stato2;
                $info->privacy = $privacy;
                $info->privacy_commerciali = $privacy_commerciale;
                $info->privacy_profilazione = $privacy_profilazione;
                $info->customer_id = $cust->id;
                $info->save();

                if(isset($request->statiStato))
                    for($i=0;$i<count($request->statiStato);$i++) {
                        if(empty($request->statiNota[$i]))
                            DB::table('customers_relations')->insert(
                                [
                                    'status' => $request->statiStato[$i],
                                    'userid' => Auth::user()->id,
                                    'customerid' => $cust->id
                                ]
                            );
                        else
                            DB::table('customers_relations')->insert(
                                [
                                    'status' => $request->statiStato[$i],
                                    'userid' => Auth::user()->id,
                                    'customerid' => $cust->id,
                                    'nota' => $request->statiNota[$i]
                                ]
                            );

                    }

                $infoviaggi = new InfoTravel();
                if(isset($request->infoviaggi_vacanzeanno))
                  $infoviaggi->vacanze_anno = $request->infoviaggi_vacanzeanno;
                if(isset($request->infoviaggi_preferenze))
                $infoviaggi->preferenza_viaggi = $request->infoviaggi_preferenze;
                if(isset($request->infoviaggi_duratamedia))
                $infoviaggi->durata_media = $request->infoviaggi_duratamedia;
                if(isset($request->infoviaggi_prossimipaesi))
                $infoviaggi->prossimi_paesi = $request->infoviaggi_prossimipaesi;
                if(isset($request->infoviaggi_fonti))
                 $infoviaggi->fonti = $request->infoviaggi_fonti;

                if( $request->infoviaggi_mood){
                    $infoviaggi->infoviaggi_mood = implode(",", $request->infoviaggi_mood) ;
                }

                if(isset($request->infoviaggi_gennaio))
                    $infoviaggi->gennaio = $request->infoviaggi_gennaio == 1 ? 1 : 0;
                if(isset($request->infoviaggi_febbraio))
                    $infoviaggi->febbraio = $request->infoviaggi_febbraio == 1 ? 1 : 0;
                if(isset($request->infoviaggi_marzo))
                    $infoviaggi->marzo = $request->infoviaggi_marzo == 1 ? 1 : 0;
                if(isset($request->infoviaggi_aprile))
                    $infoviaggi->aprile = $request->infoviaggi_aprile == 1 ? 1 : 0;
                if(isset($request->infoviaggi_maggio))
                    $infoviaggi->maggio = $request->infoviaggi_maggio == 1 ? 1 : 0;
                if(isset($request->infoviaggi_giugno))
                    $infoviaggi->giugno = $request->infoviaggi_giugno == 1 ? 1 : 0;
                if(isset($request->infoviaggi_luglio))
                    $infoviaggi->luglio = $request->infoviaggi_luglio == 1 ? 1 : 0;
                if(isset($request->infoviaggi_agosto))
                    $infoviaggi->agosto = $request->infoviaggi_agosto == 1 ? 1 : 0;
                if(isset($request->infoviaggi_settembre))
                    $infoviaggi->settembre = $request->infoviaggi_settembre == 1 ? 1 : 0;
                if(isset($request->infoviaggi_ottobre))
                    $infoviaggi->ottobre = $request->infoviaggi_ottobre == 1 ? 1 : 0;
                if(isset($request->infoviaggi_novembre))
                    $infoviaggi->novembre = $request->infoviaggi_novembre == 1 ? 1 : 0;
                if(isset($request->infoviaggi_dicembre))
                    $infoviaggi->dicembre = $request->infoviaggi_dicembre == 1 ? 1 : 0;

                if(isset($request->infoviaggi_archeologia))
                    $infoviaggi->archeologia = $request->infoviaggi_archeologia == 1 ? 1 : 0;
                if(isset($request->infoviaggi_avventura))
                    $infoviaggi->avventura = $request->infoviaggi_avventura == 1 ? 1 : 0;
                if(isset($request->infoviaggi_cultura))
                    $infoviaggi->cultura = $request->infoviaggi_cultura == 1 ? 1 : 0;
                if(isset($request->infoviaggi_ecoturismo))
                    $infoviaggi->ecoturismo = $request->infoviaggi_ecoturismo == 1 ? 1 : 0;
                if(isset($request->infoviaggi_enogastronomia))
                    $infoviaggi->enogastronomia = $request->infoviaggi_enogastronomia == 1 ? 1 : 0;
                if(isset($request->infoviaggi_fotografico))
                    $infoviaggi->fotografico = $request->infoviaggi_fotografico == 1 ? 1 : 0;
                if(isset($request->infoviaggi_marerelax))
                    $infoviaggi->mare_relax = $request->infoviaggi_marerelax == 1 ? 1 : 0;
                if(isset($request->infoviaggi_montagna))
                    $infoviaggi->montagna = $request->infoviaggi_montagna == 1 ? 1 : 0;
                if(isset($request->infoviaggi_religioso))
                    $infoviaggi->religioso = $request->infoviaggi_religioso == 1 ? 1 : 0;
                if(isset($request->infoviaggi_sportivo))
                    $infoviaggi->sportivo = $request->infoviaggi_sportivo == 1 ? 1 : 0;
                if(isset($request->infoviaggi_storicopolitico))
                    $infoviaggi->storico_politico = $request->infoviaggi_storicopolitico == 1 ? 1 : 0;
                if(isset($request->infoviaggi_altro))
                    $infoviaggi->altro = $request->infoviaggi_altro == 1 ? 1 : 0;
                if(isset($request->solo_volo))
                    $infoviaggi->solo_volo = $request->solo_volo == 1 ? 1 : 0;
                if(isset($request->lavoro))
                    $infoviaggi->lavoro = $request->lavoro == 1 ? 1 : 0;
                if(isset($request->infoviaggi_tematiche_altro))
                    $infoviaggi->note_tematiche = $request->infoviaggi_tematiche_altro;


                $infoviaggi->customer_id = $cust->id;
                $infoviaggi->save();


                $qasuend = new QaSuend();
                if(isset($request->qasuend_conoscenza))
                    $qasuend->conoscenza_suend = $request->qasuend_conoscenza;
                if(isset($request->qasuend_altremotivazione))
                    $qasuend->note_conoscenza_suend = $request->qasuend_altremotivazione;
                if(isset($request->qasuend_codicecliente))
                    $qasuend->codice_cliente = $request->qasuend_codicecliente;
                if(isset($request->qasuend_giaviaggiato))
                    $qasuend->gia_viaggiato = $request->qasuend_giaviaggiato == 1 ? 1 : 0;
                if(isset($request->qasuend_quantiviaggi))
                    $qasuend->quante_volte = $request->qasuend_quantiviaggi;
                if(isset($request->qasuend_doveviaggi))
                    $qasuend->luogo_viaggio = $request->qasuend_doveviaggi;
                if(isset($request->qasuend_conoscesito))
                    $qasuend->conosce_sito = $request->qasuend_conoscesito == 1 ? 1 : 0;
                if(isset($request->qasuend_numero_buono))
                    $qasuend->numero_buono = $request->qasuend_numero_buono;
                if(isset($request->qasuend_tipo_buono))
                    $qasuend->tipo_buono = $request->qasuend_tipo_buono;
                if(isset($request->qasuend_importo_buono))
                    $qasuend->importo_buono = $request->qasuend_importo_buono;

                    $qasuend->note_qa_suend = $request->qasuend_note;
                $qasuend->customer_id = $cust->id;
                $qasuend->save();




                $qaviaggio = new QaTravel();
                if(isset($request->qasuend_importo_buono))
                    $qaviaggio->choices = $request->qasuend_importo_buono;

                if ($request->qaviaggio_datapartenza  && !empty($request->qaviaggio_datapartenza))
                    $info->data_partenza = $request->qaviaggio_datapartenza;

                if ($request->qaviaggio_ultimarilevazione  && !empty($request->qaviaggio_ultimarilevazione))
                    $info->ultima_rilevazione = $request->qaviaggio_ultimarilevazione;

                if(isset($request->qaviaggio_destinazione))
                    $qaviaggio->destinazione = $request->qaviaggio_destinazione;

                if(isset($request->qaviaggio_valutazione))
                    $qaviaggio->valutazione = $request->qaviaggio_valutazione;

                if(isset($request->qaviaggio_preventivo)&&!empty($request->qaviaggio_preventivo)){
                    $this->log("customer","preventivo",$request->qaviaggio_preventivo);
                    $qaviaggio->preventivo = $request->qaviaggio_preventivo;
                    DB::table("preventivo")->where('id', $request->qaviaggio_preventivo)->update(['riferitoA'=>$cust->id]);
                }

                /*
                    0 = privato
                    1 = business // da escludere su questo controller
                    2 = advIta
                    3 = advExt
                */


                if(isset($request->qaviaggio_storico))
                    $qaviaggio->storico = $request->qaviaggio_storico;
                $qaviaggio->customer_id = $cust->id;
                $qaviaggio->save();

                $profilo = new Profile();

                $profilo->data_nascita = $info->data_nascita;

                $profilo->carteidentita = $request->profilo_cartaidentita;

                if ($request->profilo_datascadenzaidentita  && !empty($request->profilo_datascadenzaidentita))
                    $profilo->scadenza_cartaidentita = $request->profilo_datascadenzaidentita;

                $profilo->passaporto = $request->profilo_passaporto;

                if ($request->profilo_datascadenzapassaporto  && !empty($request->profilo_datascadenzapassaporto))
                        $profilo->scadenza_passaporto = $request->profilo_datascadenzapassaporto;
                $profilo->luogonascita = $request->luogonascita;
                $profilo->professione = $request->profilo_professione;
                $profilo->hobby1 = $request->profilo_hobby1;
                $profilo->hobby2 = $request->profilo_hobby2;

                $profilo->frequent_flyer = $request->profilo_frequentflyer;

                if(isset($request->profilo_newsletter)){
                    if($request->profilo_newsletter)
                    $profilo->newsletter = 1;
                }


                if(isset($request->profilo_clientediretto)){
                    if($request->profilo_clientediretto)
                    $profilo->cliente_diretto = 1;
                }

                $profilo->adulti = $request->profilo_adulti;
                $profilo->bambini = $request->profilo_bambini;
                $profilo->neonati = $request->profilo_neonati;
                $profilo->figli = $request->profilo_figli;
                $profilo->anziani = $request->profilo_anziani;
                $profilo->animali = $request->profilo_animali;
                if(isset($request->profilo_iscrittoclub)){
                    if($request->profilo_iscrittoclub)
                    $profilo->iscritto_club = 1;
                }
                $profilo->codice_club = $request->profilo_codiceclub;
                $profilo->note = $request->profilo_note;
                $profilo->customer_id = $cust->id;
                /*qui*/
                $profilo->save();


                return redirect('admin/cliente/'.$cust->id)->with('message', 'Cliente Inserito!');
            }
        } catch (\Exception $e) {
            return redirect()->back()->with('error', 'Errore Inserimento Cliente!' . $e);
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

        $user_id = Auth::user()->id;
        $cust = Customer::find($id);
        $info = Info::where('customer_id', $id)->first();
        $infoviaggi = InfoTravel::where('customer_id', $id)->first();
        $qasuend = QaSuend::where('customer_id', $id)->first();
        $qatravel = QaTravel::where('customer_id', $id)->first();
        $profilo = Profile::where('customer_id', $id)->first();

        $task_total =  Task::where(['user_id'=>$user_id,'status' => 'attivo'])->orWhere(['user_assigned_id'=>$user_id,'status' => 'attivo'])->orderBy('category_id','asc')->orderBy('task_deadline','asc')->get();
        $task_total = count($task_total);


                return view('admin.show-customer', ['id' => $id,
                    'task_total' => $task_total,
                    'customer' => $cust,
                    'info' => $info,
                    'qasuend' => $qasuend,
                    'qatravel' => $qatravel,
                    'profilo' => $profilo,
                    'infoviaggi' => $infoviaggi,
                ]);

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user_id = Auth::user()->id;
        $task_total =  Task::where(['user_id'=>$user_id,'status' => 'attivo'])->orWhere(['user_assigned_id'=>$user_id,'status' => 'attivo'])->orderBy('category_id','asc')->orderBy('task_deadline','asc')->get();
        $task_total = count($task_total);
        $regioni = DB::table('italy_regions')->get();

        $cust = Customer::find($id);
        $info = Info::where('customer_id', $id)->first();
        $infoviaggi = InfoTravel::where('customer_id', $id)->first();
        $qasuend = QaSuend::where('customer_id', $id)->first();
        $qatravel = QaTravel::where('customer_id', $id)->first();
        $profilo = Profile::where('customer_id', $id)->first();
        $lastId= DB::table('customers')->max('id');
        $bonus=DB::table("customerBonus")->where('customerId', $id)->get();
        $datiPreventivo=DB::table('preventivo')->where("riferitoA","=",$id)->select(["id","labeldest","statoPreventivo","updated_at"])->get();
        $customersRelations= DB::table('customers_relations')
        ->join("customers","customers_relations.customerid","customers.id")
        ->where('customerId', $id)->get();

        $valutazioni=DB::table("storico_valutazioni")->where("user_id",$id)->get();

        /* la get su preventivo deve gestire tutti e 3 gli id n in get per avere gli storici */
        $tasks = Task::where('status' , 'attivo')->where('collegato_a', 'customer')->where('entity_id', $id)->orderBy('category_id','asc')->orderBy('task_deadline','asc')->get();
        $tasksCompleted = Task::where('status' , 'chiuso')->where('collegato_a', 'customer')->where('entity_id', $id)->orderBy('task_deadline','desc')->get();

        $this->log("tasks","tasksCompleted",$tasksCompleted);


            $destinazioniPreferite=DB::table('destinazioniPreferite')
            ->join('destinazioni', 'destinazioni.id', '=', 'destinazioniPreferite.iddest')
            ->where('destinazioniPreferite.customerid', $id)
            ->select(['destinazioniPreferite.id','destinazioni.destinazione as label'])
            ->get();

            $destinazioniPassate=DB::table('destinazioniPassate')
            ->join('destinazioni', 'destinazioni.id', '=', 'destinazioniPassate.iddest')
            ->where('destinazioniPassate.customerid', $id)
            ->select(['destinazioniPassate.id','destinazioni.destinazione as label'])
            ->get();

            $destinazioniProssime=DB::table('destinazioniProssime')
            ->join('destinazioni', 'destinazioni.id', '=', 'destinazioniProssime.iddest')
            ->where('destinazioniProssime.customerid', $id)
            ->select(['destinazioniProssime.id','destinazioni.destinazione as label'])
            ->get();

            $destinazioniDove=DB::table('destinazioniDove')
            ->join('destinazioni', 'destinazioni.id', '=', 'destinazioniDove.iddest')
            ->where('destinazioniDove.customerid', $id)
            ->select(['destinazioniDove.id','destinazioni.destinazione as label'])
            ->get();


        $giftCard=DB::table('card')
            ->leftJoin("card_user_activity","card_user_activity.coupon_id","card.id")
            ->where("card.customer_cards",$id)
            ->select(
                [
                "card.id",
                "card.code",
                "card.label",
                "card.price",
                "card.kind",
                "card.deadline",
                "card.customer_cards",
                "card.business_cards",
                "card.payed",
                "card.created_at",
                "card.updated_at",
                "card.deleted_at"
                ]

                )
            ->selectRaw(" toEur(card.value) value, toEur(coalesce(sum(card_user_activity.spesa),0)) spesa, DATE_FORMAT(max(card_user_activity.data_utilizzo) ,'%d/%m/%Y') data_utilizzo")
            ->groupBy(
                [
                "card.id",
                "card.code",
                "card.label",
                "card.value",
                "card.price",
                "card.kind",
                "card.deadline",
                "card.customer_cards",
                "card.business_cards",
                "card.payed",
                "card.created_at",
                "card.updated_at",
                "card.deleted_at"
                ]
            )
            ->get();

        $coupons=DB::table('coupon')
            ->leftJoin("coupon_user_activity","coupon_user_activity.coupon_id","coupon.id")
            ->where("coupon.customer_coupons",$id)
            ->select(
                [
                "coupon.id",
                "coupon.code",
                "coupon.label",
                "coupon.price",
                "coupon.kind",
                "coupon.deadline",
                "coupon.customer_coupons",
                "coupon.business_coupons",
                "coupon.payed",
                "coupon.created_at",
                "coupon.updated_at",
                "coupon.deleted_at"
                ]

                )
            ->selectRaw(" toEur(coupon.value) value, toEur(coalesce(sum(coupon_user_activity.spesa),0)) spesa, DATE_FORMAT(max(coupon_user_activity.data_utilizzo) ,'%d/%m/%Y') data_utilizzo")
            ->groupBy(
                [
                "coupon.id",
                "coupon.code",
                "coupon.label",
                "coupon.value",
                "coupon.price",
                "coupon.kind",
                "coupon.deadline",
                "coupon.customer_coupons",
                "coupon.business_coupons",
                "coupon.payed",
                "coupon.created_at",
                "coupon.updated_at",
                "coupon.deleted_at"
                ]
            )
            ->get();




            $data=[
                    'id' => $id,
                        'task_total' => $task_total,
                        'task_open' => $task_total,
                        'customer' => $cust,
                        'regioni' => $regioni,
                        'qasuend' => $qasuend,
                        'qaviaggio' => $qatravel,
                        'infoviaggi' => $infoviaggi,
                        'profilo' => $profilo,
                        'info' => $info,
                        'lastId' => $lastId,
                        'bonusList' => $bonus,
                        'customersRelations' => $customersRelations,
                        'datiPreventivo' => $datiPreventivo,
                        'destinazioniPreferite' =>$destinazioniPreferite,
                        'destinazioniPassate' =>$destinazioniPassate,
                        'destinazioniProssime' =>$destinazioniProssime,
                        'destinazioniDove' =>$destinazioniDove,
                        'tasks' =>$tasks,
                        'task_done' =>count($tasksCompleted),
                        'tasksCompleted' =>$tasksCompleted,
                        'tasks_totali_cliente' =>count($tasksCompleted)+count($tasks),
                        'valutazioni' => $valutazioni,
                        'giftCard' => $giftCard,
                        'coupons' => $coupons,
                        'path' => getcwd()
                    ];
        return view('admin.edit-customer',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        try {
            if ($request) {

                $this->log("customer","update",$request->all());

                $privacy = false;

                if ($request->info_privacy == 'on') {
                    $privacy = true;
                }
                $privacy_commerciale = false;
                if ($request->info_privacy_commerciali == 'on') {
                    $privacy_commerciale = true;
                }
                $privacy_profilazione = false;
                if ($request->info_privacy_profilazione == 'on') {
                    $privacy_profilazione = true;
                }

                $cust = Customer::find($id);

                if(isset($request->societa))
                    $cust->societa = $request->societa;
                if(isset($request->cognome))
                    $cust->cognome = $request->cognome;
                if(isset($request->nome))
                    $cust->nome = $request->nome;
                if(isset($request->citta))
                    $cust->citta = $request->citta;
                if(isset($request->info_data_nascita))
                    $cust->data_nascita = $request->info_data_nascita;
                if(isset($request->provincia))
                    $cust->provincia = $request->provincia;

                if($cust->citta&&$cust->provincia&&$cust->cap&&$cust->regione){
                    $residenza=$request->residenza;

                    $nuovaCitta=DB::table("italy_cities")->where("comune",$cust->citta);
                    $nuovaCitta=$nuovaCitta->get();

                    if(!isset($nuovaCitta[0]))
                    {
                        $siglaNumerica=$nuovaCitta=DB::table("italy_cities")->where("estera",1)->count();

                        $data1=[
                            "id_citta" => NULL,
                            "comune" => $cust->citta,
                            "regione" => $cust->regione,
                            "provincia" => $siglaNumerica,
                            "cap" => $cust->cap,
                            "estera" => $residenza
                        ];
                        DB::table("italy_cities")->insert($data1);

                        DB::table("italy_provincies")->insert([
                            'sigla' => $siglaNumerica,
                            'provincia' => $cust->provincia
                        ]);

                    }
                }

                if(isset($request->cap))
                    $cust->cap = $request->cap;
                if(isset($request->regione))
                    $cust->regione = $request->regione;
                if(isset($request->stato))
                    $cust->stato = $request->stato;
                if(isset($request->pref1))
                    $cust->pref1 = $request->pref1;
                if(isset($request->tel1))
                    $cust->tel1 = $request->tel1;
                if(isset($request->pref2))
                    $cust->pref2 = $request->pref2;
                if(isset($request->tel2))
                    $cust->tel2 = $request->tel2;
                if(isset($request->tel3))
                    $cust->tel3 = $request->tel3;
                if(isset($request->email1))
                    $cust->email1 = $request->email1;
                if(isset($request->email2))
                    $cust->email2 = $request->email2;
                if(isset($request->status))
                    $cust->status = $request->status;
                if(isset($request->categoria))
                    $cust->cat_id = $request->categoria;
                $cust->ultimocontatto=date("Y-m-d");
                if(strpos($cust->authorId, "^".Auth::user()->id."^")<0)
                	$cust->authorId .= "^".Auth::user()->id."^";
                $cust->adv = 0;
                $cust->update();



                $this->store_add_dest($request,'destinazionipassate','destinazioniPassate',$cust->id,Auth::user()->id);
                $this->store_add_dest($request,'destinazionipreferite','destinazioniPreferite',$cust->id,Auth::user()->id);
                $this->store_add_dest($request,'destinazioniprossime','destinazioniProssime',$cust->id,Auth::user()->id);
                $this->store_add_dest($request,'destinazioniDove','destinazioniDove',$cust->id,Auth::user()->id);

                // Oggetto info che va dentro Customer! <3 Eloquent!
                $info = Info::where('customer_id', $id)->first();

                    if(isset($request->info_indirizzo))
                        $info->indirizzo = $request->info_indirizzo;

                    if(isset($request->info_web))
                        $info->sito_web = $request->info_web;

                    if(isset($request->info_piva))
                        $info->partita_iva = $request->info_piva;

                    if(isset($request->info_data_nascita))
                        $info->data_nascita = $request->info_data_nascita;

                    if(isset($request->info_codice_fiscale))
                        $info->codice_fiscale = $request->info_codice_fiscale;

                    if(isset($request->info_sesso))
                        $info->sesso = $request->info_sesso;

                    if(isset($request->info_posizione))
                        $info->posizione = $request->info_posizione;

                    if(isset($request->info_suffisso))
                        $info->suffisso = $request->info_suffisso;

                    if(isset($request->info_tel4))
                        $info->tel4 = $request->info_tel4;

                    if(isset($request->info_tel5))
                        $info->tel5 = $request->info_tel5;

                    if(isset($request->info_fax1))
                        $info->fax1 = $request->info_fax1;

                    if(isset($request->info_fax2))
                        $info->fax2 = $request->info_fax2;

                    if(isset($request->info_mail2))
                        $info->email2 = $request->info_mail2;

                    if(isset($request->info_indirizzo2))
                        $info->indirizzo2 = $request->info_indirizzo2;

                    if(isset($request->info_citta2))
                        $info->citta2 = $request->info_citta2;

                    if(isset($request->info_provincia2))
                        $info->provincia2 = $request->info_provincia2;

                    if(isset($request->info_cap2))
                        $info->cap2 = $request->info_cap2;

                    if(isset($request->info_regione2))
                        $info->regione2 = $request->info_regione2;

                    if(isset($request->info_stato2))
                        $info->stato2 = $request->info_stato2;

                    $info->privacy = $privacy;
                    $info->privacy_commerciali = $privacy_commerciale;
                    $info->privacy_profilazione = $privacy_profilazione;
                    $info->customer_id = $cust->id;
                    $info->update();


                if(isset($request->statiStato))
                    for($i=0;$i<count($request->statiStato);$i++) {
                        if(empty($request->statiNota[$i]))
                            DB::table('customers_relations')->insert(
                                [
                                    'status' => $request->statiStato[$i],
                                    'userid' => Auth::user()->id,
                                    'customerid' => $cust->id
                                ]
                            );
                        else
                            DB::table('customers_relations')->insert(
                                [
                                    'status' => $request->statiStato[$i],
                                    'userid' => Auth::user()->id,
                                    'customerid' => $cust->id,
                                    'nota' => $request->statiNota[$i]
                                ]
                            );

                    }


                    $infoviaggi = InfoTravel::where('customer_id', $id)->first();
                    if(isset($request->infoviaggi_vacanzeanno))
                        $infoviaggi->vacanze_anno = $request->infoviaggi_vacanzeanno;
                    if(isset($request->infoviaggi_preferenze))
                        $infoviaggi->preferenza_viaggi = $request->infoviaggi_preferenze;
                    if(isset($request->infoviaggi_duratamedia))
                        $infoviaggi->durata_media = $request->infoviaggi_duratamedia;
                    if(isset($request->infoviaggi_prossimipaesi))
                        $infoviaggi->prossimi_paesi = $request->infoviaggi_prossimipaesi;
                    if(isset($request->infoviaggi_fonti))
                        $infoviaggi->fonti = $request->infoviaggi_fonti;
                    if(isset($request->infoviaggi_gennaio)){
                        if($request->infoviaggi_gennaio)
                        $infoviaggi->gennaio = 1;
                    }
                    if(isset($request->infoviaggi_febbraio)){
                        if($request->infoviaggi_febbraio)
                        $infoviaggi->febbraio = 1;
                    }
                    if(isset($request->infoviaggi_marzo)){
                        if($request->infoviaggi_marzo )
                        $infoviaggi->marzo = 1;
                    }
                    if(isset($request->infoviaggi_aprile)){
                        if($request->infoviaggi_aprile)
                        $infoviaggi->aprile = 1;
                    }
                    if(isset($request->infoviaggi_maggio)){
                        if($request->infoviaggi_maggio)
                        $infoviaggi->maggio = 1;
                    }
                    if(isset($request->infoviaggi_giugno)){
                        if($request->infoviaggi_giugno)
                        $infoviaggi->giugno = 1;
                    }
                    if(isset($request->infoviaggi_luglio)){
                        if($request->infoviaggi_luglio)
                        $infoviaggi->luglio = 1;
                    }
                    if(isset($request->infoviaggi_agosto)){
                        if($request->infoviaggi_agosto)
                        $infoviaggi->agosto = 1;
                    }
                    if(isset($request->infoviaggi_settembre)){
                        if($request->infoviaggi_settembre)
                        $infoviaggi->settembre = 1;
                    }
                    if(isset($request->infoviaggi_ottobre)){
                        if($request->infoviaggi_ottobre)
                        $infoviaggi->ottobre = 1;
                    }
                    if(isset($request->infoviaggi_novembre)){
                        if($request->infoviaggi_novembre)
                        $infoviaggi->novembre = 1;
                    }

                    if(isset($request->infoviaggi_dicembre)){
                        if($request->infoviaggi_dicembre)
                        $infoviaggi->dicembre = 1;
                    }
                    if(isset($request->infoviaggi_archeologia)){
                        if($request->infoviaggi_archeologia)
                        $infoviaggi->archeologia = 1;
                    }
                    if(isset($request->infoviaggi_avventura)){
                        if($request->infoviaggi_avventura)
                        $infoviaggi->avventura = 1;
                    }
                    if(isset($request->infoviaggi_cultura)){
                        if($request->infoviaggi_cultura)
                        $infoviaggi->cultura = 1;
                    }
                    if(isset($request->infoviaggi_ecoturismo)){
                        if($request->infoviaggi_ecoturismo)
                        $infoviaggi->ecoturismo = 1;
                    }
                    if(isset($request->infoviaggi_enogastronomia)){
                        if($request->infoviaggi_enogastronomia)
                        $infoviaggi->enogastronomia = 1;
                    }
                    if(isset($request->infoviaggi_fotografico)){
                        if($request->infoviaggi_fotografico)
                        $infoviaggi->fotografico = 1;
                    }
                    if(isset($request->infoviaggi_marerelax)){
                        if($request->infoviaggi_marerelax)
                        $infoviaggi->mare_relax = 1;
                    }
                    if(isset($request->infoviaggi_montagna)){
                        if($request->infoviaggi_montagna)
                        $infoviaggi->montagna = 1;
                    }
                    if(isset($request->infoviaggi_religioso)){
                        if($request->infoviaggi_religioso)
                        $infoviaggi->religioso = 1;
                    }
                    if(isset($request->infoviaggi_sportivo)){
                        if($request->infoviaggi_sportivo)
                        $infoviaggi->sportivo = 1;
                    }
                    if(isset($request->infoviaggi_storicopolitico)){
                        if($request->infoviaggi_storicopolitico)
                        $infoviaggi->storico_politico = 1;
                    }
                    if(isset($request->infoviaggi_altro)){
                        if($request->infoviaggi_altro)
                        $infoviaggi->altro = 1;
                    }

                    if(isset($request->solo_volo))
                        $infoviaggi->altro = $request->solo_volo;

                    if(isset($request->lavoro))
                        $infoviaggi->altro = $request->lavoro;

                    if(isset($request->infoviaggi_tematiche_altro))
                        $infoviaggi->note_tematiche = $request->infoviaggi_tematiche_altro;

                    $infoviaggi->customer_id = $cust->id;
                    $infoviaggi->update();

                    $qasuend = QaSuend::where('customer_id', $id)->first();
                    if(isset($request->qasuend_conoscenza))
                        $qasuend->conoscenza_suend = $request->qasuend_conoscenza;
                    if(isset($request->qasuend_altremotivazione))
                        $qasuend->note_conoscenza_suend = $request->qasuend_altremotivazione;
                    if(isset($request->qasuend_codicecliente))
                        $qasuend->codice_cliente = $request->qasuend_codicecliente;
                    if(isset($request->qasuend_giaviaggiato)){
                        if($request->qasuend_giaviaggiato)
                        $qasuend->gia_viaggiato = 1;
                    }
                    if(isset($request->qasuend_quantiviaggi))
                        $qasuend->quante_volte = $request->qasuend_quantiviaggi;
                    if(isset($request->qasuend_doveviaggi))
                        $qasuend->luogo_viaggio = $request->qasuend_doveviaggi;
                    if(isset($request->qasuend_conoscesito)){
                        if($request->qasuend_conoscesito)
                        $qasuend->conosce_sito = 1;
                    }
                    if(isset($request->qasuend_note))
                        $qasuend->note_qa_suend = $request->qasuend_note;
                    if(isset($request->qasuend_numero_buono))
                        $qasuend->numero_buono = $request->qasuend_numero_buono;
                    if(isset($request->qasuend_tipo_buono))
                        $qasuend->tipo_buono = $request->qasuend_tipo_buono;
                    if(isset($request->qasuend_importo_buono))
                        $qasuend->importo_buono = $request->qasuend_importo_buono;
                    $qasuend->customer_id = $cust->id;
                    $qasuend->update();

                    $nuova_valutazione = ['user_id' => $cust->id];

                    $qaviaggio = QaTravel::where('customer_id', $id)->first();
                    if(isset($request->qaviaggio_soddisfazione)){
                        $qaviaggio->choices = $request->qaviaggio_soddisfazione;
                        $nuova_valutazione['field1'] = $request->qaviaggio_soddisfazione;
                    }
                    if(isset($request->qaviaggio_datapartenza)){
                        $qaviaggio->data_partenza = $request->qaviaggio_datapartenza;
                        $nuova_valutazione['field2'] = $request->qaviaggio_datapartenza;
                    }

                    if(isset($request->qaviaggio_ultimarilevazione)){
                        $qaviaggio->ultima_rilevazione = $request->qaviaggio_ultimarilevazione;
                        $nuova_valutazione['field3'] = $request->qaviaggio_ultimarilevazione;
                    }

                    if(isset($request->qaviaggio_destinazione)){
                        $qaviaggio->destinazione = $request->qaviaggio_destinazione;
                        $nuova_valutazione['field4'] = $request->qaviaggio_destinazione;
                    }

                    if(isset($request->qaviaggio_valutazione)){
                        $qaviaggio->valutazione = $request->qaviaggio_valutazione;
                        $nuova_valutazione['field5'] = $request->qaviaggio_valutazione;
                    }

                    if(isset($request->qaviaggio_preventivo)){
                        $qaviaggio->preventivo = $request->qaviaggio_preventivo;
                        $nuova_valutazione['field6'] = $request->qaviaggio_preventivo;

                        DB::table("preventivo")->where('id', $request->qaviaggio_preventivo)->update(['riferitoA'=>$cust->id]);

                        DB::table('preventiviClienti')->insert(
                            [
                                'prevId'=>$request->qaviaggio_preventivo,
                                'entityId'=>$cust->id,
                                'authorId'=>Auth::user()->id
                            ]
                        );

                    }

                    if(isset($request->qaviaggio_storico)){
                        $qaviaggio->storico = $request->qaviaggio_storico;
                        $nuova_valutazione['field7'] = $request->qaviaggio_storico;
                    }
                    if(count($nuova_valutazione)>1){
                        $nuova_valutazione['insertBy'] = Auth::user()->name . " " . Auth::user()->surname;
                        DB::table("storico_valutazioni")->insert($nuova_valutazione);
                    }
                    $qaviaggio->update();
                    $profilo = Profile::where('customer_id', $id)->first();
                    if(isset($request->data_nascita))
                        $profilo->data_nascita = $request->data_nascita;
                    if(isset($request->profilo_cartaidentita))
                        $profilo->carteidentita = $request->profilo_cartaidentita;
                    if(isset($request->profilo_datascadenzaidentita))
                        $profilo->scadenza_cartaidentita = $request->profilo_datascadenzaidentita;
                    if(isset($request->profilo_passaporto))
                        $profilo->passaporto = $request->profilo_passaporto;
                    if(isset($request->profilo_datascadenzapassaporto))
                        $profilo->scadenza_passaporto = $request->profilo_datascadenzapassaporto;
                    if(isset($request->profilo_luogonascita))
                        $profilo->luogonascita = $request->profilo_luogonascita;
                    if(isset($request->profilo_professione))
                        $profilo->professione = $request->profilo_professione;
                    if(isset($request->profilo_hobby1))
                        $profilo->hobby1 = $request->profilo_hobby1;
                    if(isset($request->profilo_hobby2))
                        $profilo->hobby2 = $request->profilo_hobby2;
                    if(isset($request->profilo_frequentflyer))
                        $profilo->frequent_flyer = $request->profilo_frequentflyer;
                    if(isset($request->profilo_newsletter)&&$request->profilo_newsletter)
                        $profilo->newsletter = 1;
                    if(isset($request->profilo_clientediretto)&&$request->profilo_clientediretto)
                            $profilo->cliente_diretto = 1;
                    if(isset($request->profilo_adulti))
                        $profilo->adulti = $request->profilo_adulti;
                    if(isset($request->profilo_bambini))
                        $profilo->bambini = $request->profilo_bambini;
                    if(isset($request->profilo_neonati))
                        $profilo->neonati = $request->profilo_neonati;
                    if(isset($request->profilo_figli))
                        $profilo->figli = $request->profilo_figli;
                    if(isset($request->profilo_anziani))
                        $profilo->anziani = $request->profilo_anziani;
                    if(isset($request->profilo_animali))
                        $profilo->animali = $request->profilo_animali;
                    if(isset($request->profilo_iscrittoclub)&&$request->profilo_iscrittoclub)
                        $profilo->iscritto_club = 1;
                    if(isset($request->profilo_codiceclub))
                        $profilo->codice_club = $request->profilo_codiceclub;
                    if(isset($request->profilo_note))
                        $profilo->note = $request->profilo_note;
                    $profilo->customer_id = $cust->id;

                    $profilo->update();
                return redirect()->back()->with('message', 'Cliente inserito!');
            }
        } catch (\Exception $e) {
            return redirect()->back()->with('error', 'Errore Inserimento Cliente!' . $e);
        }


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $cust = Customer::find($id);

        if ($cust) {
            $cust->softDeletes();
        }
    }

}
