<?php

namespace App\Http\Controllers;

use App\Category;
use App\News;
use App\Task;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
class DashboardController extends Controller
{

	public function __construct(TaskController $tasks)
	{
        $this->middleware('auth');
	}

	/**
	 * Show the application dashboard.
	 *
	 * @return \Illuminate\Http\Response
	 */
    public function index()
    {
        $user_id = Auth::user()->id;
        $today=date("Y-m-d");
        $news = DB::table('news')
        ->join('users', 'users.id', '=', 'news.authorId')
        ->orWhere('deadline','>',$today)
        ->orWhere('deadline','=',NULL)
        ->select(['news.*','users.username'])
        ->orderBy('evidenza', 'desc')->orderBy('created_at', 'desc')
        ->get();

        $this->log("newssql","news",$news);

        session_start();
        $_SESSION['userid']=$user_id;


        if (Auth::user()->admin == 1)
            $tasks = Task::where(['status' => 'attivo'])->orderBy('category_id','asc')->orderBy('task_deadline','asc')->get();

         else
            $tasks = Task::where(['user_id'=>$user_id,'status'=>'attivo'])->orWhere(['user_assigned_id'=>$user_id,'status'=>'attivo'])->orderBy('category_id','asc')->orderBy('task_deadline','asc')->get();
        $task_total = count($tasks);
        $task_done = Task::where(['status' => 'chiuso'])->count();
        $myPendingTask = $task_total-$task_done;
        $users = User::all();
        $categories = Category::all();
            return view('admin.dashboard',[
                'tasks' => $tasks,
                'users' => $users,
                'categories' => $categories,
                'task_total' => $task_total,
                'news' => $news,
                'task_done' => $task_done,
                'myPendingTask' => $myPendingTask,
                'task_open' => $myPendingTask
                ] );

    }
    public function test()
    {
        $user_id = Auth::user()->id;
        $today=date("Y-m-d");
        $news = DB::table('news')
        ->join('users', 'users.id', '=', 'news.authorId')
        ->orWhere('deadline','>',$today)
        ->orWhere('deadline','=',NULL)
        ->select(['news.*','users.username'])
        ->orderBy('evidenza', 'desc')->orderBy('created_at', 'desc')
        ->get();

        $this->log("newssql","news",$news);

        session_start();
        $_SESSION['userid']=$user_id;

            $users = User::all();
            $categories = Category::all();
            $tasks = Task::where(['status' => 'attivo'])->orderBy('category_id','asc')->orderBy('task_deadline','asc')->get();
            $task_total = count($tasks);
            $task_done = Task::where(['status' => 'chiuso'])->count();
            $myPendingTask = $task_total-$task_done;

            return view('admin.newhome',[
                'users' => $users,
                'categories' => $categories,
                'tasks' => $tasks,
                'task_total' => $task_total,
                'news' => $news,
                'task_done' => $task_done,
                'myPendingTask' => $myPendingTask,
                'task_open' => $myPendingTask
            ]);



    }

    public static function countTasks($user_id)
    {
        $task_number  =  Task::where('user_id', $user_id)->count();
        return $task_number;
    }

    public static function whoHasCreatedTask($user_id)
    {
        $user  =  User::where('id', $user_id)->select('username')->first();
        if(isset($user)) {
            return $user->username;
        } else {
            return "";
        }

    }


    public static function countAdminTasks()
    {
        $task_number  =  Task::where('user_id',Auth::user()->id)->count();
        return $task_number;
    }


}
