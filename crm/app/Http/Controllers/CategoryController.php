<?php

namespace App\Http\Controllers;

use App\Category;
use App\User;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categorie = Category::where('deleted_at', null)->get();
        return $categorie->toJson();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $categoria = Category::where('deleted_at', null)->where('id', $id)->get();
        if (isset($categoria)) {
            return $categoria->toJson();
        }

    }

    public static function showCategoryName($id)
    {
        $categoria = Category::where('deleted_at', null)->where('id', $id)->first();
        if (isset($categoria)) {
            return $categoria->category_name;
        }

    }

    public static function showCategoryColor($id)
    {
        $categoria = Category::where('deleted_at', null)->where('id', $id)->first();
        if (isset($categoria)) {
            return $categoria->color;
        }
    }

    public static function getUserData($id)
    {

        $user = User::where('deleted_at', null)->where('id', $id)->first();
        if (isset($user)) {
            return $user->name . ' ' . $user->surname;
        }

    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
