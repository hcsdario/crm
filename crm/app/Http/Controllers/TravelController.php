<?php

namespace App\Http\Controllers;

use App\Category;
use App\Task;
use App\Travel;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Yajra\Datatables\Facades\Datatables;

class TravelController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user_id = Auth::user()->id;

        if (Auth::user()->admin == 1) {
            $users = User::all();
            $categories = Category::all();
            $tasks = Task::all();
            $task_total =  TaskController::countAdminTasks();
            return view('admin.travel',['users' => $users, 'categories' => $categories, 'tasks' => $tasks, 'task_total' => $task_total] );
        } else {
            $task_total =  TaskController::countTasks($user_id);
            $tasks = Task::where('user_id', $user_id);
            return view('user.travel', ['tasks' => $tasks,'task_total' => $task_total]);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            if ($request) {
                // per fare veloce



                $travel = new Travel();
                $travel->choices = $request->soddisfazione;
                $travel->data_partenza = Carbon::createFromFormat('d/m/Y', $request->data_partenza);
                $travel->ultima_rilevazione = Carbon::createFromFormat('d/m/Y', $request->ultima_rilevazione);
                $travel->destinazione = $request->destinazione;
                $travel->storico = $request->storico;
                $travel->customer_id = $request->customer_id;

                $travel->save();
                return redirect()->back()->with('message', 'Viaggio Inserito!');
            }
        }
        catch(\Exception $e){
            //    Debugbar::error('Error!'. ' ' . $e);
            return redirect()->back()->with('error', 'Errore Inserimento Viaggio!' . $e);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }


    public function getTravelByCustomerId($customerId) {
        $query = DB::table('travels')->where('customer_id',$customerId)->select('id','choices', 'data_partenza','ultima_rilevazione', 'destinazione');
        return Datatables::of($query)
            ->addColumn('azioni', function($query) {
                return '<a href="' . $query->id . '" class="btn btn-xs btn-primary"><i class="glyphicon glyphicon-search" disabled></i> Visualizza</a>';
            })->rawColumns(['azioni'])->make(true);
    }
}
