<?php


namespace App\Http\Controllers;

use App\Coupon;
use App\Task;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Yajra\Datatables\Facades\Datatables;


class CouponController extends Controller
{
    public $comuniCtrl;

    public function __construct()
    {
        $this->comuniCtrl = new ComuniController();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request=null)
    {

        $user_id = Auth::user()->id;
        $task_total =  Task::where(['user_id'=>$user_id,'status' => 'attivo'])->orWhere(['user_assigned_id'=>$user_id,'status' => 'attivo'])->orderBy('category_id','asc')->orderBy('task_deadline','asc')->get();
        $task_total = count($task_total);
        $coupons=DB::table("coupon")
        ->leftJoin("customers","customers.id","coupon.customer_coupons")
        ->leftJoin("business","business.id","coupon.business_coupons")
        ->select('coupon.*')
        ->selectRaw(' toEur(coupon.value) as value, DATE_FORMAT(deadline ,\'%d/%m/%Y\') as deadline ')
        ->selectRaw('  coalesce(concat(business.ragione),"Non assegnato")  as azienda, business.id as id_azienda ')
        ->selectRaw(' coalesce(concat(customers.cognome," ",customers.nome),"Non assegnato") as privato , customers.id as id_privato ')
        ->get();

        return view('admin.coupons',[ 'task_total' => $task_total,'coupons' => $coupons,'task_open' => $task_total] );
    }



    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $user_id = Auth::user()->id;
        $task_total =  Task::where(['user_id'=>$user_id,'status' => 'attivo'])->orWhere(['user_assigned_id'=>$user_id,'status' => 'attivo'])->orderBy('category_id','asc')->orderBy('task_deadline','asc')->get();
        $task_total = count($task_total);

        $code=$this->generateCode();

        $coupon=new Coupon();


        return view('admin.form-coupon',[ 'task_total' => $task_total,'task_open' => $task_total,'code' => $code,'coupon' => $coupon ,'preventivi' =>[]]);
    }

    private function generateCode($startingCode=false){
        if(!$startingCode){
            $startingCode=DB::select("SELECT max(id) as max FROM `coupon`");
            if(empty($startingCode))
                $startingCode=1;
            else
                $startingCode=$startingCode[0]->max;
        }
        $l=strlen($startingCode);
        return str_repeat("0",(6-$l)).$startingCode."#".date("Y");
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */


    public function store(Request $request)
    {

        $this->log("coupon","coupon.store",$request->all());
        $data=$request->all();

        unset($data['_token']);
        $ripetizioni=$data['ripetizioni'];
        unset($data['ripetizioni']);
        unset($data['bah']);
        for($i=0;$i<$ripetizioni;$i++){
            $coupon= Coupon::firstOrNew($data);
            $coupon->save();
            $data['code']=$this->generateCode($coupon->id);

        }
        return redirect()->back()->with('message', 'Aggiornamento completato!');
    }

    public function copy(Request $request)
    {

        $this->log("coupon","coupon.copy",$request->all());
        $data=$request->all();
        $id=$data['id'];
        $ripetizioni=$data['ripetizioni'];
        
        foreach ($id as $coupon_id) {
            $coupon=Coupon::find($coupon_id);
            for($i=0;$i<$ripetizioni;$i++){
                $newCoupon= $coupon->replicate();
                $code=$this->generateCode();
                $newCoupon->code=$code;
                $newCoupon->save();
            }
        }
        return json_encode(['message' => 'done']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

        $user_id = Auth::user()->id;
        $coupon = Coupon::find($id);
        $task_total =  Task::where(['user_id'=>$user_id,'status' => 'attivo'])->orWhere(['user_assigned_id'=>$user_id,'status' => 'attivo'])->orderBy('category_id','asc')->orderBy('task_deadline','asc')->get();
        $task_total = count($task_total);
         
        $code=$this->generateCode();

        return view('admin.form-coupon', [
            'id' => $id,
            'task_total' => $task_total,
            'task_open' => $task_total,
            'edit' => false,
            'code' => $code,
            'coupon' => $coupon,
        ]);
          
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */



    public function edit($id)
    {

        $user_id = Auth::user()->id;
        $coupon = Coupon::find($id);
        $task_total =  Task::where(['user_id'=>$user_id,'status' => 'attivo'])->orWhere(['user_assigned_id'=>$user_id,'status' => 'attivo'])->orderBy('category_id','asc')->orderBy('task_deadline','asc')->get();
        $task_total = count($task_total);

        $preventivi=DB::table("coupon_user_activity")
        ->join("preventivo","coupon_user_activity.preventivo_id","preventivo.id")
        ->where("coupon_id",$id)
        ->select(["coupon_user_activity.*","preventivo.id as preventivo_id","preventivo.labeldest"])
        ->selectRaw(' toEur(coupon_user_activity.spesa) as spesa, DATE_FORMAT(coupon_user_activity.data_utilizzo ,\'%d/%m/%Y\') as data_utilizzo ')
        ->get();

        return view('admin.form-coupon', [
            'id' => $id,
            'task_total' => $task_total,
            'task_open' => $task_total,
            'edit' => true,
            'code' => $coupon->code,
            'preventivi' => $preventivi,
            'coupon' => $coupon,
        ]);
          
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
   
}
