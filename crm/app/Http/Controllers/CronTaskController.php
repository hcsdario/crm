<?php

namespace App\Http\Controllers;

use App\Category;
use App\Mail\TaskCompleted;
use App\Task;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Response;

class CronTaskController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $user_id = Auth::user()->id;
        $users = User::orderBy('username')->get();
        $categories = Category::all();
        if (Auth::user()->admin == 1) {

            $tasks = Task::orderBy('category_id','asc')->orderBy('category_id','asc')->get();
            $task_total = $this->countAdminTasks();
            return view('admin.tasks',['users' => $users, 'categories' => $categories, 'tasks' => $tasks, 'task_total' => $task_total] );
        } else {
            //$task_total = $this->countTasks($user_id);
            $task_total = $this->countAdminTasks();
            $tasks = Task::orderBy('category_id','asc')->orderBy('category_id','asc')->get();
           // dd($tasks);
            return view('user.tasks', ['tasks' => $tasks,  'categories' => $categories,'task_total' => $task_total,'users' => $users]);
        }

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.new-task');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            if($request) {
                $date = Carbon::now();

                $userAssignedList = explode("," , $request->userAssigned);

                if (Auth::user()->admin == 1) {

                    for($i=0;$i<count($userAssignedList);$i++){
                        if(empty($userAssignedList[$i]))
                            continue;

                        $task = new Task();
                        $task->task_note = $request->task_note;
                        $task->task_deadline = isset($request->task_deadline) ? $request->task_deadline : $date->addDays(5);
                        $task->task_marked = 0;
                        $task->category_id = $request->category_id;
                        $task->user_assigned_id = Auth::user()->id;
                        $task->user_id = $userAssignedList[$i];
                        $task->status = "attivo";

                        if (Auth::user()->admin == 1) {
                            $task->personal= 0;
                        } else {
                            $task->personal= 1;
                        }

                        $task->save();
                    }
                }
                else{
                    $task = new Task();
                    $task->task_note = $request->task_note;
                    $task->task_deadline = isset($request->task_deadline) ? $request->task_deadline : $date->addDays(5);
                    $task->task_marked = 0;
                    $task->category_id = $request->category_id;
                    $task->user_assigned_id = Auth::user()->id;
                    $task->user_id = $request->user_id;
                    $task->status = "attivo";   
                    $task->save();                 
                }

                return redirect()->back()->with('message', 'Task Assegnato!');
            }
        }
        catch(\Exception $e){
            return redirect()->back()->with('error', 'Errore Inserimento Task!'.$e);
        }


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

        $task  =  Task::find($id);
        return $task;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return view('admin.edit-task', ['id'=> $id]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $task  =  Task::find($id);
        if($task) {
            $task->task_note = $request->note;
            $task->task_deadline = $request->note;
            $task->task_done = 0;
            if (Auth::user()->admin == 1) {
                $task->personal= 0;
            } else {
                $task->personal= 1;
            }
            $task->category_id = $request->categoria_id;
            $task->priority_id = $request->priorita_id;
            $task->save();
        }
        return $task;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $task = Task::find($id);
            if($task) {
                $task->delete();
                return redirect()->back()->with('success', 'File eliminato!');
            }
        }
        catch(\Exception $e){
            return redirect()->back()->with('error', 'Errore Inserimento File!' . $e);
        }

    }

    public static function countTasks($user_id)
    {
        $task_number  =  Task::where('task_marked',0)->where('user_id', $user_id)->count();
        return $task_number;
    }

    public static function countAdminTasks()
    {
        $task_number  =  Task::where('task_marked',0)->count();
        return $task_number;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function updateTaskStatus($id,$status)
    {
//file_put_contents("crm/z-mail", print_r($userAssignedList,true));


        try {
            $task = Task::find($id);
            if($task) {

                $task = Task::findOrFail($id);
                $task->status=$status;
                $task->save();

            }
        }
        catch(\Exception $e){
            return redirect()->back()->with('error', 'Errore Smarcamento Task!' . $e);
        }

    }


    public function sendMail(Request $request) {

      
        try {

            $task = Task::findOrFail($request->task_id);
        

            $task_user = User::where('id', $task->user_id)->first();

            $user = User::where('id', $task->user_assigned_id)->first();

            // se è personale no invio mail
            if($task->personal != 1) {
                Mail::send('emails.task-completed', ['task' => $task], function ($m) use ($task, $task_user, $user) {
                    $m->from('noreply@hcslab.it', 'Suend-crm task marked');
                    $m->to($user->email, $user->name . ' ' .$user->surname)->subject('Il Task: ' . $task->task_note . ' è stato completato da ' . $task_user->username);
                });
            }
            
            if(empty($request->task_action))
                $status="chiuso";
            else
                $status="rifiutato";

            $this->updateTaskStatus($request->task_id,$status);
            return redirect()->back()->with('success', 'Task Smarcato!');

        } catch(\Exception $e) {

            return redirect()->back()->with('error', 'Errore Smarcamento Task!' . $e);
        }

    }


}

/*$cron=new CronTaskController();
file_put_contents(getcwd()."/crm/z-mail", print_r($cron,true));*/