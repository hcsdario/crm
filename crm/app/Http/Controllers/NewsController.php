<?php

namespace App\Http\Controllers;

use App\Category;
use App\News;
use App\Task;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Yajra\Datatables\Facades\Datatables;
use Illuminate\Support\Facades\DB;
class NewsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $news =  DB::table('news')->orderBy('evidenza', 'desc')->orderBy('created_at', 'desc')->get();
        $users = User::all();
        $user_id = Auth::user()->id;
        $task_total =  Task::where(['user_id'=>$user_id,'status' => 'attivo'])->orWhere(['user_assigned_id'=>$user_id,'status' => 'attivo'])->orderBy('category_id','asc')->orderBy('task_deadline','asc')->get();
        $task_total = count($task_total);

        if (Auth::user()->admin == 1) {
            return view('admin.news',['users' => $users, 'news' => $news, 'task_total' => $task_total, 'task_open ' => $task_total] );
        } else {
            return view('user.news',['users' => $users, 'news' => $news, 'task_total' => $task_total, 'task_open' => $task_total] );
        }


    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->log("news","store.request",$request->all());

        try {
                if(isset($request->id))
                    $news = News::find($request->id);
                else
                    $news = new News();

                $news->news_title = $request->news_title;
                $news->news_body = $request->wysiwg_full;
                $news->zone = $request->zone;
                if(!isset($request->id))
                    $news->breaking=1;

                $news->evidenza=$request->evidenza_news;
             
                if(isset($request->deadline))
                    $news->deadline = $request->deadline;
                if(isset($request->evidenzadeadline))
                    $news->evidenzadeadline = $request->evidenzadeadline;
                
                $news->userid = Auth::user()->id;
                $news->authorId = Auth::user()->id;
                $this->log("news","store.presave",$news);
                
    
                if(!isset($request->id))
                    $news->save();
                else
                    $news->update();
                
                return redirect()->back()->with('message', 'News Inserita!');
        }
        catch(\Exception $e){
            return redirect()->back()->with('error', 'Errore Inserimento News!' . $e);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        $user_id = Auth::user()->id;
        $task_total =  Task::where(['user_id'=>$user_id,'status' => 'attivo'])->orWhere(['user_assigned_id'=>$user_id,'status' => 'attivo'])->orderBy('category_id','asc')->orderBy('task_deadline','asc')->get();
        $task_total = count($task_total);

        $news = News::find($id);
        return view('user.edit-news',['news' => $news, 'task_total' => $task_total, 'task_open' => $task_total] );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public static function countTasks($user_id)
    {
        $task_number  =  Task::where('task_marked',0)->where('user_id', $user_id)->count();
        return $task_number;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function data(Datatables $datatables)
    {

            $query = DB::table('news')->select('id','news_title',
                 
                DB::raw('(CASE WHEN evidenza = 0 THEN "semplice" WHEN evidenza = 1 AND breaking=0 THEN "In evidenza" WHEN evidenza = 1  AND breaking=1 THEN "Breaking News" END) AS evidenza'),
                'news_body','created_at','userid')->orderBy('created_at', 'desc');

        return Datatables::of($query)
            ->addColumn('azioni', function($query) {

                $url = "#";
                $modal='    <div class="modal fade" id="news-modal-'.$query->id.'">
        <div class="modal-dialog">
            <div class="modal-content">
            <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">'.$query->news_title.'</h4>
                  </div>            
                <div class="modal-body">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <div class="panel panel-default">
                            <div class="panel-body">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="form-group">
                                            <label for="reg_input">'.$query->news_body.'</label>
                                            <h4></h4>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                </div>
            </div>
        </div>
    </div>';
               
                    return '<a href="'.$url.'" class="btn btn-xs btn-primary news-zama" data-toggle="modal" data-target="#news-modal-'.$query->id.'" ><i class="glyphicon glyphicon-search" data-toggle="tooltip" title="" data-original-title="Visualizza" class="disabled"></i></a><a href="#" onclick="loadnews(\''.$query->id.'\')" class="btn btn-xs btn-secondary" propietario="'.$query->userid.'"><i class="glyphicon glyphicon-edit" data-toggle="tooltip" title="" data-original-title="Modifica"></i></a><a href="#" class="btn btn-xs btn-danger news-zama" style="margin-left: 10px;" onclick="deleteRecord(this)" propietario="'.$query->id.'"><i class="glyphicon glyphicon-minus" data-toggle="tooltip" title="" data-original-title="Elimina" class="disabled"></i></a>'.$modal;

            })->rawColumns(['azioni','news_title', 'news_body'])->make(true);
    }
    //data-toggle="modal" data-target="#yourModal{{$t->id}}"

    public function  readNews($id) {
        $news = News::find($id);

        if(isset($news)) {
            return view('shared/read',['news' => $news] );
        }
    }
}
