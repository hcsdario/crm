<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Support\Facades\Auth;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    protected function redirectTo()
    {
        ini_set("log_errors", 1);
        error_reporting(E_ALL);
        ini_set("error_log", getcwd()."/crm/log/Controller.log");
        ini_set("display_errors", "On");

        if (Auth::check()) {
            if (Auth::user()->admin == 1) {
                return redirect()->route( 'dashboard_admin');
            } else {
                return redirect()->route('dashboard_user');
            }
        }
    }

    public function log($dir,$filename,$data){
        $path=getcwd()."/crm/log/".$dir;
        if(!file_exists($path))
            mkdir($path);
        file_put_contents($path."/".$filename, print_r($data,true)."\n",FILE_APPEND);
    }

}
