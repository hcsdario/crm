<?php

namespace App\Http\Controllers;

use App\Category;
use App\Reparti;
use App\Task;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Yajra\Datatables\Facades\Datatables;
use Illuminate\Support\Facades\DB;

class RepartiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $user_id = Auth::user()->id;
        $task_total =  TaskController::countAdminTasks();
        $reparti = Reparti::orderBy('reparti','asc')->get();

        $utentiIngruppo=DB::table('reparti')->join('user_reparti', 'reparti.id', '=', 'user_reparti.repid')->join('users', 'users.id', '=', 'user_reparti.userid')
            ->selectRaw('GROUP_CONCAT(users.id) as userIdList , reparti.id as repId')->groupBy('reparti.id')->get();

        $allusers=DB::table('users')->select('id','name')->whereNotIn('id', [1,31,32])->get();

        if (Auth::user()->admin == 1)
            return view('admin.reparti',[ 'task_total' => $task_total, 'reparti' => $reparti, 'allusers' => $allusers, 'utentiIngruppo' => $utentiIngruppo ] );
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       
                $data=[
                    'id' => $request->id, 
                    'reparti' => $request->name
                    ];

                
                if($request->id=="-1"){
                    $data['id']=NULL;
                    DB::table('reparti')->insert($data);
                }
                else
                    DB::table('reparti')->where('id', $request->id)->update(['reparti' => $request->name]);

                return redirect()->back()->with('message', 'Reparto aggiunto!');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $task_total =  TaskController::countAdminTasks();
        $reparti = Reparti::orderBy('reparti','asc')->get();   
        return view('admin.reparti',[ 'task_total' => $task_total, 'reparti' => $reparti ] );
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        $user_id = Auth::user()->id;
        $task_total =  TaskController::countAdminTasks();
        $reparto = Reparti::find($id);
        $reparti = Reparti::orderBy('reparti','asc')->get();
        
        $utentiIngruppo=DB::table('reparti')->join('user_reparti', 'reparti.id', '=', 'user_reparti.repid')->join('users', 'users.id', '=', 'user_reparti.userid')
            ->selectRaw('GROUP_CONCAT(users.id) as userIdList , reparti.id as repId')->groupBy('reparti.id')->get();

        $allusers=DB::table('users')->select('id','name')->whereNotIn('id', [1,31,32])->get();


        if (Auth::user()->admin == 1)
            return view('admin.reparti',[ 'task_total' => $task_total, 'reparti' => $reparti,'reparto' =>$reparto , 'allusers' => $allusers, 'utentiIngruppo' => $utentiIngruppo , 'isedit'=>1] );

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public static function countTasks($user_id)
    {
        $task_number  =  Task::where('task_marked',0)->where('user_id', $user_id)->count();
        return $task_number;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */


 
}
