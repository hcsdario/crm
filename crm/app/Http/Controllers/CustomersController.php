<?php

namespace App\Http\Controllers;

use App\Customer;
use App\Task;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Yajra\Datatables\Facades\Datatables;

class CustomersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request=null)
    {

        $fields=[
            'customers.*',
            'profilo.luogonascita',
            'profilo.professione',
            'profilo.hobby1',
            'profilo.hobby2',
            'profilo.scadenza_passaporto',
            'profilo.passaporto',
            'profilo.carteidentita',
            'profilo.scadenza_cartaidentita',
            'profilo.frequent_flyer',
            'profilo.newsletter',
            'profilo.cliente_diretto',
            'profilo.adulti',
            'profilo.bambini',
            'profilo.neonati',
            'profilo.figli',
            'profilo.anziani',
            'profilo.animali',
            'profilo.iscritto_club',
            'profilo.codice_club',
            'profilo.note'
        ];
        $where=[];
        $where['customers.adv']=0;

        
        if(isset($request))
        {
            $inputs = $request->all();
            foreach ($inputs as $key => $value){
                
                if(empty($value))
                   continue;
                $where[str_replace("wjk", ".",$key)]=$value;                
            }
        }

        $user_id = Auth::user()->id;
        $task_total =  Task::where(['user_id'=>$user_id,'status' => 'attivo'])->orWhere(['user_assigned_id'=>$user_id,'status' => 'attivo'])->orderBy('category_id','asc')->orderBy('task_deadline','asc')->get();
        $task_total = count($task_total);

        $privato = DB::table('customers')
       ->leftJoin('profilo', 'customers.id', '=', 'profilo.customer_id')
       ->leftJoin('infoviaggi', 'customers.id', '=', 'infoviaggi.customer_id')
            ->where(function ($query) use ($where) {
                foreach ($where as $key => $value) {
                    switch ($key) {

                        case 'infoviaggi.dal':
                            $query->where('ultimoviaggio', '>', implode('-', array_reverse(explode('/', $value))));
                        break;
                        case 'infoviaggi.al':
                            $query->where('ultimoviaggio', '<', implode('-', array_reverse(explode('/', $value))));
                        break;

                        case 'customers.nome':
                            $query->where(DB::raw("concat(ifnull(nome,''),' ',ifnull(cognome,''))"),'like', "%"."$value"."%");

                          //  $query->where('ultimocontatto', '>', implode('-', array_reverse(explode('/', $value))));
                        break;

                        case 'customers.dal':
                            $query->where('ultimocontatto', '>', implode('-', array_reverse(explode('/', $value))));
                        break;
                        case 'customers.al':
                            $query->where('ultimocontatto', '<', implode('-', array_reverse(explode('/', $value))));
                        break;
                        case 'customers.data_nascita':
                            $query->where('customers.data_nascita', '=', implode('-', array_reverse(explode('/', $value))));
                        break;
                        case 'customers.provincia':
                            foreach ($value as $item){
                                $query->orWhere('citta', 'like', '%$item%');
                                $query->orWhere('provincia', 'like', '%$item%');
                                $query->orWhere('regione', 'like', '%$item%');
                            }
                        break;
                        case 'infoviaggi.destinazione':
                            $query->orWhere('infoviaggi.destinazioni_preferite', 'like', '%$value%');
                            $query->orWhere('infoviaggi.destinazioni_passate', 'like', '%$value%');
                        break;
                        case 'infoviaggi.destinazioni_preferite':
                            foreach ($value as $item)
                              $query->orWhere('infoviaggi.destinazioni_preferite', 'like', '%$item%');
                        break;
                        case 'infoviaggi.destinazioni_passate':
                            foreach ($value as $item)
                                $query->orWhere('infoviaggi.destinazioni_passate', 'like', '%$item%');
                        break;
                        
                        default:
                            $query->where($key, '=', $value);
                        break;
                    }
                }

            });
       
       if(!Auth::user()->permessi)
            $privato=$privato->where('customers.authorId', 'like', '%'.'^'.$user_id.'^'.'%');
        $privato=$privato
       ->select(
            $fields,
            DB::raw('REPLACE(customers.updated_at,"0000-00-00 00:00:00","") as updated_at ')
        );
        $sql=$privato;
        $this->log("customers","search",$sql->toSql());
        $this->log("customers","search",$sql->getBindings());
        $privato=$privato->get();   
            return view('admin.customers',['privati' =>$privato, 'task_total' => $task_total,'task_open' => $task_total ] );

    }

    public function index_ext(Request $request=null)
    {
        $user_id = Auth::user()->id;
        $task_total =  Task::where(['user_id'=>$user_id,'status' => 'attivo'])->orWhere(['user_assigned_id'=>$user_id,'status' => 'attivo'])->orderBy('category_id','asc')->orderBy('task_deadline','asc')->get();
        $task_total = count($task_total);
        if(!isset($request)||empty($request))
            return view('user.business',['task_total' => $task_total,'task_open' => $task_total ] );

        $fields=[
            'business.*',
            DB::raw('REPLACE(business.updated_at,"0000-00-00 00:00:00","") as updated_at ')
        ];
        $where=[];

        if(isset($request))
        {
            $inputs = $request->all();
            foreach ($inputs as $key => $value) {
                if(!empty($value)){
                     $where[str_replace("wjk", ".",$key)]=$value;
                }
            }
        }

        $aziende = DB::table('business')
        ->where(function ($query) use ($where) {
                foreach ($where as $key => $value) {
                            $query->where($key, 'like', "%$value%");
                    }
            });
        if(!Auth::user()->permessi)
            $aziende=$aziende->where('business.authorId', 'like', '%'.'^'.$user_id.'^'.'%');
        $aziende = $aziende->orderBy('business.updated_at', 'desc')->take(150);
        $aziende =$aziende->select($fields); 
        $sql = $aziende;
        $aziende =$aziende->get();   

        return view('user.business',[ 'aziende' =>$aziende, 'task_total' => $task_total,'task_open' => $task_total ] );
    }

    public function search_privato()
    {
        $user_id = Auth::user()->id;
        $task_total =  Task::where(['user_id'=>$user_id,'status' => 'attivo'])->orWhere(['user_assigned_id'=>$user_id,'status' => 'attivo'])->orderBy('category_id','asc')->orderBy('task_deadline','asc')->get();
        $task_total = count($task_total);

        return view('admin.customersSearch',['task_total' => $task_total,'task_open' => $task_total ] );
    }

    public function search_privato_ext()
    {
        $user_id = Auth::user()->id;
        $task_total =  Task::where(['user_id'=>$user_id,'status' => 'attivo'])->orWhere(['user_assigned_id'=>$user_id,'status' => 'attivo'])->orderBy('category_id','asc')->orderBy('task_deadline','asc')->get();
        $task_total = count($task_total);

        return view('admin.businessSearch',['task_total' => $task_total,'task_open' => $task_total ] );
    }



    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function data(Datatables $datatables)
    {

        if(Auth::user()->permessi)
            $query = DB::table('customers')->join('profilo', 'customers.id', '=', 'profilo.customer_id')->where('adv', 0)->select(
                'customers.*','profilo.luogonascita',
                'profilo.professione',
                'profilo.hobby1',
                'profilo.hobby2',
                'profilo.scadenza_passaporto',
                'profilo.passaporto',
                'profilo.carteidentita',
                'profilo.scadenza_cartaidentita',
                'profilo.frequent_flyer',
                'profilo.newsletter',
                'profilo.cliente_diretto',
                'profilo.adulti',
                'profilo.bambini',
                'profilo.neonati',
                'profilo.figli',
                'profilo.anziani',
                'profilo.animali',
                'profilo.iscritto_club',
                'profilo.codice_club',
                'profilo.note', 
                DB::raw('REPLACE(customers.updated_at,"0000-00-00 00:00:00","") as updated_at ')
                );
        else
            $query = DB::table('customers')->join('profilo', 'customers.id', '=', 'profilo.customer_id')->where('adv', 0)->where('customers.authorId', 'like', '%'.'^'.$user_id.'^'.'%')->select(
                'customers.*','profilo.luogonascita',
                'profilo.professione',
                'profilo.hobby1',
                'profilo.hobby2',
                'profilo.scadenza_passaporto',
                'profilo.passaporto',
                'profilo.carteidentita',
                'profilo.scadenza_cartaidentita',
                'profilo.frequent_flyer',
                'profilo.newsletter',
                'profilo.cliente_diretto',
                'profilo.adulti',
                'profilo.bambini',
                'profilo.neonati',
                'profilo.figli',
                'profilo.anziani',
                'profilo.animali',
                'profilo.iscritto_club',
                'profilo.codice_club',
                'profilo.note', 
                DB::raw('REPLACE(customers.updated_at,"0000-00-00 00:00:00","") as updated_at ')
                );
        return Datatables::of($query)
            ->addColumn('azioni', function($query) {
                if (Auth::user()->admin == 1) {
                    $url = '../admin/cliente/' . $query->id;
                } else {
                    $url = '../user/cliente/' . $query->id;
                }
                if (Auth::user()->admin == 1) {
                    $urlEdit = '../admin/cliente/edit/' . $query->id;
                } else {
                    $urlEdit = '../user/cliente/edit/' . $query->id;
                }
                return '<a href="' . $url . '" class="btn btn-xs btn-primary"><i class="glyphicon glyphicon-search" data-toggle="tooltip" title="Visualizza"></i></a>&nbsp;&nbsp;<a href="' . $urlEdit . '" class="btn btn-xs btn-secondary"><i class="glyphicon glyphicon-edit" data-toggle="tooltip" title="Modifica"></i></a><a href="#" class="btn btn-xs btn-danger news-zama" style="margin-left: 10px;" onclick="deleteRecord(this)"><i class="glyphicon glyphicon-trash" data-toggle="tooltip" title="Elimina"></i></a>';

            })->rawColumns(['azioni'])->make(true);
    }

    public function data_ext(Datatables $datatables)
    {
            $fields=[
                'business.id',
                'business.codice',
                'business.azienda',
                'business.fiscale_iva',
                'business.ragione',
                'business.status',
                'business.fisso',
                'business.mail',
                 DB::raw('REPLACE(business.updated_at,"0000-00-00 00:00:00","") as updated_at '),
                'business.insertBy'
            ];

            $user_id = Auth::user()->id;
            $aziende = DB::table('business');

            if(!Auth::user()->permessi)
                $aziende=$aziende->where('business.authorId', 'like', '%'.'^'.$user_id.'^'.'%');

            $aziende = $aziende->orderBy('business.updated_at', 'desc');
            $aziende =$aziende->select($fields); 
            $sql = $aziende;
       
            return Datatables::of($sql)
            ->addColumn('azioni', function($sql) {
                $url = '../user/business/' . $sql->id.'?action=view';
                $urlEdit = '../user/business/' . $sql->id;

                return 
                    '<a href="' . $url . '" class="btn btn-xs btn-info">'.
                        '<i class="glyphicon glyphicon-search" data-toggle="tooltip" title="Visualizza"></i>'.
                    '</a>'.
                    '&nbsp;&nbsp;'.
                    '<a href="' . $urlEdit . '" class="btn btn-xs btn-secondary">'.
                        '<i class="glyphicon glyphicon-edit" data-toggle="tooltip" title="Modifica"></i>'.
                    '</a>'.
                    '<a href="#" class="btn btn-xs btn-danger news-zama" style="margin-left: 10px;" onclick="deleteRecord(this,[\'business\','.$sql->id.'])">'.
                        '<i class="glyphicon glyphicon-trash" data-toggle="tooltip" title="Elimina"></i>'.
                    '</a>';


            })->rawColumns(['azioni'])->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
