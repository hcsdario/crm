<?php

namespace App\Http\Controllers;

use App\Preventivo;
use App\PreventivoAzioni;
use App\PreventivoPartecipanti;
use App\Task;
use App\User;
use App\Category;
use Carbon\Carbon;
use Debugbar;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Spipu\Html2Pdf\Html2Pdf;




class PreventivoController extends Controller
{

    public $comuniCtrl;

    public function __construct()
    {
        $this->comuniCtrl = new ComuniController();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
    */

    public function index(Request $request=null)
    {
        $user_id = Auth::user()->id;
        $task_total =  Task::where(['user_id'=>$user_id,'status' => 'attivo'])->orWhere(['user_assigned_id'=>$user_id,'status' => 'attivo'])->orderBy('category_id','asc')->orderBy('task_deadline','asc')->get();
        $task_total = count($task_total);
        $get=$request->all();
            if(!isset($request)||empty($get))
                $get=['view' => 'all'];

            switch ($get['view']) {
                case 'all':
                $preventivo=DB::table("preventivo")
                ->leftJoin('customers', 'customers.id', '=', 'preventivo.riferitoA')
                ->join('users', 'users.id', '=', 'preventivo.authorId')
                ->select(["preventivo.*","users.username","customers.cognome","customers.nome"])
                ->orderBy("id","desc");
                ;
                break;
                case 'pv':
                $preventivo=DB::table("preventivo")
                ->join('customers', 'customers.id', '=', 'preventivo.riferitoA')
                ->join('users', 'users.id', '=', 'preventivo.authorId')
               ->select(["preventivo.*","users.username","customers.cognome","customers.nome"]);
                    break;

                case 'search':

                $where=[];
                $filters=$request->all();
                unset($filters['view']);

                foreach ($filters as $key => $value)
                    if(!empty($value))
                        $where[str_replace("wjk", ".",$key)]=$value;


                $preventivo = DB::table('preventivo')
                ->join('customers', 'customers.id', '=', 'preventivo.riferitoA')
                ->join('users', 'users.id', '=', 'preventivo.authorId')
                ->where(function ($query) use ($where) {
                    foreach ($where as $key => $value)
                        $query->where($key, 'like', "%$value%");
                    })
                ->select(["preventivo.*","users.username","customers.cognome","customers.nome"]);

                $sql = $preventivo;
                break;

                default:
                    // code...
                    break;
            }




        $preventivo=$preventivo->get();


        return view('admin.preventivo', ['task_total' => $task_total,'task_open' => $task_total,'preventivi' => $preventivo ]);
    }


    public function preventiviSearch()
    {
        $user_id = Auth::user()->id;
        $task_total =  Task::where(['user_id'=>$user_id,'status' => 'attivo'])->orWhere(['user_assigned_id'=>$user_id,'status' => 'attivo'])->orderBy('category_id','asc')->orderBy('task_deadline','asc')->get();
        $task_total = count($task_total);

        return view('admin.preventiviSearch', ['task_total' => $task_total,'task_open' => $task_total]);
    }

    public function sendCurl($fields,$url='http://hellocreativestudio.net/mail/mailer.php'){
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, ['data' => $fields]);
        $result = curl_exec($ch);
        curl_close($ch);
        return $result;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function create()
    {
        $user_id = Auth::user()->id;
        $task_total =  Task::where(['user_id'=>$user_id,'status' => 'attivo'])->orWhere(['user_assigned_id'=>$user_id,'status' => 'attivo'])->orderBy('category_id','asc')->orderBy('task_deadline','asc')->get();
        $task_total = count($task_total);
        $privati=DB::select("SELECT id AS value ,concat(nome,' ', cognome) as label FROM `customers` WHERE adv=0");
        $destinazioni=DB::table('destinazioni')->where("visibile","=","1")->select(["id","tag","destinazione","visibile"])->orderByRaw('destinazione ASC')->get();

        $users = User::orderBy('username')->get();
        $categories = Category::all();

        $reparti=DB::table('reparti')->join('user_reparti', 'reparti.id', '=', 'user_reparti.repid')->join('users', 'users.id', '=', 'user_reparti.userid')
            ->selectRaw('GROUP_CONCAT(users.id) as userIdList , reparti.id, reparti.reparti as nome')->groupBy('reparti.id', 'reparti.reparti')->get();

        $tasks = Task::where(['status' => 'attivo'])->where('collegato_a', 'customer')->where('entity_id', 0)->orderBy('category_id','asc')->orderBy('task_deadline','asc')->get();
        $tasksCompleted = Task::where(['status' => 'chiuso'])->where('collegato_a','=', 'preventivo')->where('entity_id','=', 0)->orderBy('task_deadline','desc')->get();
        $task_total = count($tasks)+count($tasksCompleted);
        $task_open = count($tasks);
        $task_done = count($tasksCompleted);
        $myPendingTask = $task_total-$task_done;


        return view('admin.form-preventivo',
        	[
        	'users' => $users,
        	'categories' => $categories,
        	'reparti' => $reparti,
        	'task_total' => $task_total,
        	'task_open' => $task_total,
        	'destinazioni' => $destinazioni,
        	'action' =>'create',
        	'preventivo' =>[],
        	'preventivoAzioni' =>[],
        	'preventivoPartecipanti' =>[] ,
            'giftCard' => [],
            'coupons' => [],
            'tasks' => $tasks,
            'tasksCompleted' => $tasksCompleted,
            'task_total' => $task_total,
            'task_open' => $task_open,
            'task_done' => $task_done,
            'myPendingTask' => $myPendingTask,
        	'privati' =>$privati
        	]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->log("prev","prev.store",$request->all());
        try {
            if ($request) {
                $ispreventivo=false;

                $preventivo = new Preventivo();

                if($request->prev_id && !empty($request->prev_id)){
                    $preventivo=Preventivo::find($request->prev_id);
               	}
                $data=$request->all();

                foreach ($data as $k => $value) {
                    if(strpos($k, "preventivo_")!==false)
                    {
                        $key=str_replace("preventivo_", "", $k);
                        $preventivo->$key=$value;
                        $ispreventivo=true;
                    }
                }



                if($preventivo->destinazioneRichiesta)
                {
                    $label=DB::table("destinazioni")->where("id","=",$preventivo->destinazioneRichiesta)->select("destinazione")->get();
                    $preventivo->labeldest=$label[0]->destinazione;
                }

                if($preventivo->riferitoA)
                {
                    $newid=DB::table("customers")->whereRaw("concat(cognome,' ',nome)='".$preventivo->riferitoA."'")->select("id")->get();
                    if(!$newid->isEmpty()){
                        $preventivo->riferitoA=$newid[0]->id;
                    }

                }

                $preventivo->authorId = Auth::user()->id;


                $this->log("prev","prev.update",$preventivo);
                if($request->prev_id){
                    $preventivo->save();
                }
                else{

                    $preventivo->save();
                }

                $id=$preventivo->id;


                if($preventivo->riferitoA)
                {
                    if(!$newid->isEmpty()){
                        DB::table("qa_viaggio")->where('customer_id', "=",$newid[0]->id)->update(['preventivo'=>$id]);
                    }

                }
                    if($request->preventivoAzioni_azione)
                    {

                        for($i=0;$i<count($request->preventivoAzioni_azione);$i++){
                            $preventivoAzioni = new PreventivoAzioni();
                            $preventivoAzioni->azione=$request->preventivoAzioni_azione[$i];

                            if(isset($request->preventivoAzioni_scadenza))
                                $preventivoAzioni->scadenza=$request->preventivoAzioni_scadenza[$i];
                            if(isset($request->preventivoAzioni_scadenza))
                                $preventivoAzioni->scadenza=$request->preventivoAzioni_scadenza[$i];
                            if(isset($request->preventivoAzioni_nota)&&isset($request->preventivoAzioni_nota[$i]))
                                $preventivoAzioni->nota=$request->preventivoAzioni_nota[$i];
                            if(isset($request->preventivoAzioni_eseguito)&&isset($request->preventivoAzioni_eseguito[$i]))
                                $preventivoAzioni->eseguito=1;

                            $preventivoAzioni->preventivoId=$id;
                            $preventivoAzioni->userid= Auth::user()->id;
                            $preventivoAzioni->save();
                        }

                    }
/*
                    if($request->dest_label){
                        DB::table('destinazioni')->insert(
                            ['label' => $request->dest_label]
                        );
                    }
*/
                    if($request->preventivoAzioni_azione_id)
                    {

                        for($i=0;$i<count($request->preventivoAzioni_azione_id);$i++){
                            $preventivoAzioni = PreventivoAzioni::find($request->preventivoAzioni_azione_id[$i]);
                            $preventivoAzioni->eseguito=$request->preventivoAzioni_eseguito_edit[$i];
                            $preventivoAzioni->save();
                        }

                    }

                    if($request->preventivoPartecipanti_tipo_edit)
                    {

                        for($i=0;$i<count($request->preventivoPartecipanti_id);$i++){
                            $preventivoPartecipanti = preventivoPartecipanti::find($request->preventivoPartecipanti_id[$i]);
                            $preventivoPartecipanti->tipo=$request->preventivoPartecipanti_tipo_edit[$i];
                            $preventivoPartecipanti->eta=$request->preventivoPartecipanti_eta_edit[$i];
                            $preventivoPartecipanti->numero=$request->preventivoPartecipanti_numero_edit[$i];
                            $preventivoPartecipanti->update();
                        }

                    }


                     if($request->preventivoPartecipanti_tipo)
                    {

                        for($i=0;$i<count($request->preventivoPartecipanti_tipo)/2;$i++){

                            $preventivoPartecipanti = new preventivoPartecipanti();

                            $preventivoPartecipanti->tipo=$request->preventivoPartecipanti_tipo[$i];

                            if(isset($request->preventivoPartecipanti_numero)&&isset($request->preventivoPartecipanti_numero[$i]))
                                $preventivoPartecipanti->numero=$request->preventivoPartecipanti_numero[$i];

                            if(isset($request->preventivoPartecipanti_eta)&&isset($request->preventivoPartecipanti_eta[$i]))
                                $preventivoPartecipanti->eta=$request->preventivoPartecipanti_eta[$i];

                            $preventivoPartecipanti->preventivoId=$id;

                            $preventivoPartecipanti->save();
                        }

                    }

                 return redirect('admin/preventivo/edit/'.$preventivo->id)->with('message', 'Preventivo inserito!');
            }
        } catch (\Exception $e) {

            // return redirect()->back()->with('message', 'Preventivo Inserito!');
            return redirect()->back()->with('error', 'Errore Inserimento Preventivo!' . $e);
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function view($id)
    {

        $user_id = Auth::user()->id;
        $preventivo = Preventivo::find($id);
        $preventivoPartecipanti=DB::table("preventivoPartecipanti")->where("preventivoId","=",$id)->get();
        $preventivoAzioni=DB::table("preventivoAzioni")->where("preventivoId","=",$id)->get();

        $task_total =  Task::where(['user_id'=>$user_id,'status' => 'attivo'])->orWhere(['user_assigned_id'=>$user_id,'status' => 'attivo'])->orderBy('category_id','asc')->orderBy('task_deadline','asc')->get();
        $task_total = count($task_total);

        return view('admin.preventivo', ['id' => $id,
                    'task_total' => $task_total,
                    'preventivo' => $preventivo,
                    'preventivoPartecipanti' => $preventivoPartecipanti,
                    'preventivoAzioni' => $preventivoAzioni,
                    'action' =>'view'
         ]);

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user_id = Auth::user()->id;
        $preventivo = Preventivo::find($id);


        $privati=DB::select("SELECT id AS value ,concat(nome,' ', cognome) as label FROM `customers` WHERE adv=0");
        $preventivo=DB::table("preventivo")->where("id","=",$id)->get();
        $preventivoPartecipanti=DB::table("preventivoPartecipanti")->where("preventivoId","=",$id)->get();
        $preventivoAzioni=DB::table("preventivoAzioni")
        ->join("users","preventivoAzioni.userid","=","users.id")
        ->where("preventivoId","=",$id)
        ->select("preventivoAzioni.*","users.username")
        ->get();

        $destinazioni=DB::table('destinazioni')->where("visibile","=","1")->select(["id","tag","destinazione","visibile"])->orderByRaw('destinazione ASC')->get();

        $users = User::orderBy('username')->get();
        $categories = Category::all();
        $reparti=DB::table('reparti')->join('user_reparti', 'reparti.id', '=', 'user_reparti.repid')->join('users', 'users.id', '=', 'user_reparti.userid')
            ->selectRaw('GROUP_CONCAT(users.id) as userIdList , reparti.id, reparti.reparti as nome')->groupBy('reparti.id', 'reparti.reparti')->get();



        $tasks = Task::where(['status' => 'attivo'])->where('collegato_a','=', 'preventivo')->where('entity_id','=', $id)->orderBy('category_id','asc')->orderBy('task_deadline','asc')->get();
        $tasksCompleted = Task::where(['status' => 'chiuso'])->where('collegato_a','=', 'preventivo')->where('entity_id','=', $id)->orderBy('task_deadline','desc')->get();
        $task_total = count($tasks)+count($tasksCompleted);
        $task_open = count($tasks);
        $task_done = count($tasksCompleted);
        $myPendingTask = $task_total-$task_done;




        $giftCard=DB::table('card')
            ->leftJoin("card_user_activity","card_user_activity.coupon_id","card.id")
            ->where("card.customer_cards",$preventivo[0]->riferitoA)
            ->select(
                [
                "card.id",
                "card.code",
                "card.label",
                "card.price",
                "card.kind",
                "card.deadline",
                "card.customer_cards",
                "card.business_cards",
                "card.payed",
                "card.created_at",
                "card.updated_at",
                "card.deleted_at"
                ]

                )
            ->selectRaw(" toEur(card.value) value, toEur(coalesce(sum(card_user_activity.spesa),0)) spesa, DATE_FORMAT(max(card_user_activity.data_utilizzo) ,'%d/%m/%Y') data_utilizzo")
            ->groupBy(
                [
                "card.id",
                "card.code",
                "card.label",
                "card.value",
                "card.price",
                "card.kind",
                "card.deadline",
                "card.customer_cards",
                "card.business_cards",
                "card.payed",
                "card.created_at",
                "card.updated_at",
                "card.deleted_at"
                ]
            )
            ->get();

        $coupons=DB::table('coupon')
            ->leftJoin("coupon_user_activity","coupon_user_activity.coupon_id","coupon.id")
            ->where("coupon.customer_coupons",$preventivo[0]->riferitoA)
            ->select(
                [
                "coupon.id",
                "coupon.code",
                "coupon.label",
                "coupon.price",
                "coupon.kind",
                "coupon.deadline",
                "coupon.customer_coupons",
                "coupon.business_coupons",
                "coupon.payed",
                "coupon.created_at",
                "coupon.updated_at",
                "coupon.deleted_at"
                ]

                )
            ->selectRaw(" toEur(coupon.value) value, toEur(coalesce(sum(coupon_user_activity.spesa),0)) spesa, DATE_FORMAT(max(coupon_user_activity.data_utilizzo) ,'%d/%m/%Y') data_utilizzo")
            ->groupBy(
                [
                "coupon.id",
                "coupon.code",
                "coupon.label",
                "coupon.value",
                "coupon.price",
                "coupon.kind",
                "coupon.deadline",
                "coupon.customer_coupons",
                "coupon.business_coupons",
                "coupon.payed",
                "coupon.created_at",
                "coupon.updated_at",
                "coupon.deleted_at"
                ]
            )
            ->get();

        $data=[
            'id' => $id,
            'preventivo' => $preventivo,
            'preventivoPartecipanti' => $preventivoPartecipanti,
            'preventivoAzioni' => $preventivoAzioni,
            'action' =>'edit',
            'users' => $users,
            'categories' => $categories,
            'reparti' => $reparti,
            'privati' => $privati,
            'giftCard' => $giftCard,
            'coupons' => $coupons,
            'tasks' => $tasks,
            'tasksCompleted' => $tasksCompleted,
            'task_total' => $task_total,
            'task_open' => $task_open,
            'task_done' => $task_done,
            'myPendingTask' => $myPendingTask,
            'destinazioni' => $destinazioni
        ];

        return view('admin.form-preventivo',$data);

    }


    public function topdf($id)
    {

		//include Classe Html2Pdf//
		require_once(base_path().'/vendor/spipu/html2pdf/src/Html2Pdf.php');
		require_once(base_path().'/vendor/spipu/html2pdf/src/Locale.php');
		require_once(base_path().'/vendor/tecnickcom/tcpdf/tcpdf.php');
		require_once(base_path().'/vendor/spipu/html2pdf/src/MyPdf.php');
		require_once(base_path().'/vendor/spipu/html2pdf/src/CssConverter.php');
		require_once(base_path().'/vendor/spipu/html2pdf/src/Debug/DebugInterface.php');
		require_once(base_path().'/vendor/spipu/html2pdf/src/Debug/Debug.php');

		foreach (glob(base_path().'/vendor/spipu/html2pdf/src/Parsing/*.php') as $filename)
		{
			require_once($filename);
		}

		require_once(base_path().'/vendor/spipu/html2pdf/src/Extension/ExtensionInterface.php');
		foreach (glob(base_path().'/vendor/spipu/html2pdf/src/Extension/*.php') as $filename)
		{
			require_once($filename);
		}
		foreach (glob(base_path().'/vendor/spipu/html2pdf/src/Exception/*.php') as $filename)
		{
			require_once($filename);
		}
		require_once(base_path().'/vendor/spipu/html2pdf/src/Tag/TagInterface.php');
		require_once(base_path().'/vendor/spipu/html2pdf/src/Tag/AbstractTag.php');
		require_once(base_path().'/vendor/spipu/html2pdf/src/Tag/AbstractDefaultTag.php');
		require_once(base_path().'/vendor/spipu/html2pdf/src/Tag/I.php');
		require_once(base_path().'/vendor/spipu/html2pdf/src/Tag/S.php');
		require_once(base_path().'/vendor/spipu/html2pdf/src/Tag/Span.php');
		require_once(base_path().'/vendor/spipu/html2pdf/src/Tag/U.php');
		foreach (glob(base_path().'/vendor/spipu/html2pdf/src/Tag/*.php') as $filename)
		{
			require_once($filename);
		}

      ini_set('max_execution_time', 120);
      $original_mem = ini_get('memory_limit');
      ini_set('memory_limit', '640M');

//        $debug = new \Spipu\Html2Pdf\Debug\Debug();

        $user_id = Auth::user()->id;
        $preventivo = Preventivo::find($id);
        $content = $_POST['html'];
        try {
            ob_start();
            $html2pdf = new Html2Pdf('P', 'A4', 'it', true);
//            $html2pdf->setModeDebug($debug);
		    $html2pdf->writeHTML($content);
            $pdf_name =$_POST['name'].'_'.time().'.pdf';
			$html2pdf->output( base_path().'/upload/pdf/'.$pdf_name, 'F');
    		} catch (Html2PdfException $e) {
    			echo $html2pdf->clean();
    			$formatter = new ExceptionFormatter($e);
    			echo $formatter->getHtmlMessage();
    		}

        ini_set('memory_limit',$original_mem);
        return json_encode(['pdf_name'=>$pdf_name]);

    }

    public function getpdf ($path){
      if($path && strpos($path, '..') == false){
        return response()->file(base_path().'/upload/pdf/'.$path);
      }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $preventivo = Preventivo::find($Preventivo);

        if ($Preventivo) {
            $Preventivo->softDeletes();
        }
    }

}
