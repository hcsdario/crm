<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class InfoAdv extends Model
{
    use SoftDeletes;
    protected $table = 'info_adv';


    public function customer() {
        return $this->belongsTo('App\Customer');
    }
}
