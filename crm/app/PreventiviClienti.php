<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class preventiviClienti extends Model
{

    use SoftDeletes;
    protected $table = 'preventiviClienti';

}
