<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Business extends Model
{

    use SoftDeletes;
    protected $table = 'business';


    public function files() {
        return $this->hasMany('App\File');
    }
    public function info() {
        return $this->hasOne('App\Info');
    }
    public function info_travels() {
        return $this->hasOne('App\InfoTravel');
    }
    public function profiles() {
        return $this->hasOne('App\Profile');
    }
    public function qa_suends() {
        return $this->hasOne('App\QaSuend');
    }
    public function qua_travels() {
        return $this->hasMany('App\QaTravel');
    }
    public function infoadv() {
        return $this->hasMany('App\InfoAdv');
    }
}
