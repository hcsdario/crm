<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CityModel
{
    public $cap;
    public $comune;
    public $provincia;
    public $regione;
    public $sigla;


    public function __construct()
    {
        $this->cap = '';
        $this->comune = '';
        $this->provincia = '';
        $this->regione = '';
        $this->sigla = '';
        $this->stato = '';
    }


}
