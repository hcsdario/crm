<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Travel extends Model
{
    use SoftDeletes;
	protected $table = 'travels';


	public function customer() {
		return $this->belongsTo('App\Customer');
	}
}
