<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class QaSuend extends Model
{
    use SoftDeletes;
    protected $table = 'qa_suend';


    public function customer() {
        return $this->belongsTo('App\Customer');
    }
}
