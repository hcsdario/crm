<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Coupon extends Model
{
    use SoftDeletes;
    protected $table = 'coupon';

    protected $fillable = [
	    'code',
		'label',
		'donatore',
		'azienda_emittente',
		'value',
		'price',
		'kind',
		'deadline',
		'customer_cards',
		'business_cards',
		'payed'
	];

}
