<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;

class Reparti extends Model
{
    use SoftDeletes;
    use Notifiable;

    protected $table = 'reparti';

    protected $fillable = [
        'reparti'    ];

    public function categories() {
        return $this->hasMany('App\Category');
    }

    public function priorities() {
        return $this->hasMany('App\Priority');
    }

    public function user() {
        return $this->hasOne('App\User');
    }
}
