<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Info extends Model
{
    use SoftDeletes;
    protected $table = 'info';


    public function customer() {
        return $this->belongsTo('App\Customer');
    }
}
