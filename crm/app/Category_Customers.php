<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Category_Customers extends Model
{
    use SoftDeletes;
	protected $table = 'categories_customers';

	public function customers() {
		return $this->hasMany('App\Customers');
	}
}
