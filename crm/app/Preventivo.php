<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Preventivo extends Model
{

    use SoftDeletes;
    protected $table = 'preventivo';

}
