<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class QaTravel extends Model
{
    use SoftDeletes;
    protected $table = 'qa_viaggio';


    public function customer() {
        return $this->belongsTo('App\Customer');
    }
}
