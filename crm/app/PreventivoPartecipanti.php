<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PreventivoPartecipanti extends Model
{

    use SoftDeletes;
    protected $table = 'preventivoPartecipanti';

}
