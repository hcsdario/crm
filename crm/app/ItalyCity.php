<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ItalyCity extends Model
{
    protected $table = 'italy_cities';

    public $fillable = ['provincia','sigla', 'id_regione'];


}
