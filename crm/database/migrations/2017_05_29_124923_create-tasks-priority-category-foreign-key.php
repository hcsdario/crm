<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTasksPriorityCategoryForeignKey extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('task', function (Blueprint $table) {
			//$table->unsignedInteger('priority_id');
			$table->unsignedInteger('category_id');
            $table->unsignedInteger('user_id');
			//$table->foreign('priority_id')->references('id')->on('Priorities');
			$table->foreign('category_id')->references('id')->on('Categories');
            $table->foreign('user_id')->references('id')->on('Users');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{

		Schema::table('task', function (Blueprint $table) {
			//$table->dropForeign('tasks_priority_id_foreign');
			$table->dropForeign('tasks_category_id_foreign');
            $table->dropForeign('tasks_user_id_foreign');
		});

	}
}
