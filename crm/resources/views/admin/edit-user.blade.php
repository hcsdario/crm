@extends('layouts.master')
@section('header-page-personalization')
    <style>
        .twitter-typeahead, .tt-hint, .tt-input, .tt-menu { width: 100%;  }
        .tt-menu {
            background: #ffffff;
        }
    </style>
@endsection
@section('content')
    <div class="page_content">

        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                  <div class="panel panel-default">
                            <div class="panel-body">
                  <form id="register_form" role="form" method="GET" action="{{ route('storeedit') }}">
                      {{ csrf_field() }}
                      <h1 class="login_heading"><legend><span class="big-title">Modifica utente</span></legend></h1>
                      <div class="form-group">
                          <label for="register_username">Nome</label>

                          <input type="hidden" name="id" id="id" value="<?php if(isset($id)) echo $id?>" required>
                          <input type="text" class="form-control input-lg" name="name" id="name" value="<?php if(isset($name)) echo $name?>" required
                                 autofocus>
                          @if ($errors->has('name'))
                              <span class="help-block">
                                  <strong>{{ $errors->first('name') }}</strong>
                              </span>
                          @endif
                      </div>
                      <div class="form-group">
                          <label for="register_surname">Cognome</label>
                          <input type="text" class="form-control input-lg" id="surname" name="surname" value="<?php if(isset($surname)) echo $surname?>" required>
                          @if ($errors->has('surname'))
                              <span class="help-block">
                              <strong>{{ $errors->first('surname') }}</strong>
                          </span>
                          @endif
                      </div>
                      <div class="form-group">
                          <label for="register_surname">Username</label>
                          <input type="text" class="form-control input-lg" id="username" name="username" value="<?php if(isset($username)) echo $username?>"
                                 required>
                          @if ($errors->has('username'))
                              <span class="help-block">
                              <strong>{{ $errors->first('username') }}</strong>
                          </span>
                          @endif
                      </div>
                      <div class="form-group">
                          <label for="register_email">Email</label>
                          <input type="text" class="form-control input-lg" id="email" name="email" value="<?php if(isset($email)) echo $email?>"
                                 required>
                          @if ($errors->has('email'))
                              <span class="help-block">
                                  <strong>{{ $errors->first('email') }}</strong>
                              </span>
                          @endif
                      </div>


                      <div class="submit_section">
                          <button type="submit" class="btn btn-lg btn-success btn-block">MODIFICA UTENTE</button>

                      </div>
                  </form>
                </div>
              </div>
                </div>

            </div>
        </div>
    </div>

@endsection
@section('footer-plugin')

@endsection