@extends('layouts.master')
@section('header-page-personalization')
    <style>
        #news_table thead,
        #news_table th {text-align: center;background: #61b1c1!important;}
        #news_table tr {text-align: center;}
        .disabled {
            pointer-events: none;
            cursor: default;
        }

    </style>
@endsection
@section('content')

    <div class="page_content page_news col-lg-6 pull-left">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <form id="login_form" role="form" method="POST" action="{{ route('news.store') }}">
                        {{ csrf_field() }}
                        <div class="panel panel-default">

                            <div class="panel-body">
                                <fieldset>
                                    <legend><span class="big-title">News</span></legend>
                                </fieldset>
                                @if(session()->has('message'))
                                    <div class="alert alert-success">
                                        {{ session()->get('message') }}
                                    </div>
                                @endif
                                @if(session()->has('error'))
                                    <div class="alert alert-error">
                                        {{ session()->get('error') }}
                                    </div>
                                @endif
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="form-group">
                                            <label for="reg_input">Titolo News</label>
                                            <input type="text" id="news_title" name="news_title" class="form-control">
                                        </div>
                                    </div>
                                    <div class="col-lg-12">
                                        <div class="form-group">
                                            <label for="evidenza_news">In evidenza?</label>
                                            	<select id="evidenza_news" name="evidenza_news" class="form-control" onchange="evidenzadeadline(this.value)">
		                                            <option value="0" selected>No</option>
		                                            <option value="1">Si</option>
	                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group col-lg-12">
                                        <label for="evidenza_news">In evidenza fino al</label>
                                        <input disabled="" type="date" id="evidenzadeadline" name="evidenzadeadline" class="form-control" style="text-transform: uppercase;">
                                    </div>
                                    <div class="form-group col-lg-12">
                                        <label for="evidenza_news">Visibile fino al</label>
                                        <input type="date" id="deadline" name="deadline" class="form-control" style="text-transform: uppercase;">
                                    </div>
                                </div>

                                    <div class="col-lg-12">
                                        <div class="form-group">
                                            <label for="reg_input">Zona News - Scegli la zona in cui sarà visualizzata la News</label>
                                            <select id="zone" name="zone" class="form-control">
                                            <option disabled>Scegli una zona</option>
                                            <?php
                                                $zone=DB::select("SELECT * FROM `newsZone`");
                                                for($i=0;$i<count($zone);$i++){?>
                                                <option value="{{$zone[$i]->zone}}">{{$zone[$i]->zone}}</option>
                                            <?php } ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-lg-12">
                                        <div class="">
                                            <!--<div class="panel-heading"></div>-->
                                            <div class="panel_body_a">
            									<textarea name="wysiwg_full" id="wysiwg_full" cols="30" rows="4" class="form-control">

            									</textarea>
                                            </div>
                                        </div>
                                    </div>
                            </div>
                            <button class="btn btn-lg btn-success btn-block confirmsave">Salva</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <div class="page_content pagelist main-temporaneo col-lg-6 pull-left" style="margin-left: 0px;">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-body">
                            <fieldset>
                                <legend><span class="big-title">Elenco News</span></legend>
                            </fieldset>

                            <div class="row">
                                <div class="col-lg-12">
                                    <table class="table table-bordered table-striped" id="news_table">
                                        <thead>
                                        <tr>
                                            <th>Id</th>
                                            <th>Titolo News</th>
                                            <th>Visibilità</th>
                                            <th>Data Creazione</th>
                                            <th style="width: 90px;">Azioni</th>
                                        </tr>
                                        </thead>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="modal_news_file">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-body">
                   <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h3>Titolo News</h3>

                        <div class="">
                            <div class="">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="form-group">
                                            <label for="reg_input">Testo News</label>
                                            <h4></h4>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                </div>
            </div>
        </div>
    </div>


@endsection
@section('footer-plugin')
    <!-- wysiwg editor -->
    <script src="{{ asset('/lib/ckeditor/ckeditor.js') }}"></script>
    <script src="{{ asset('/lib/ckeditor/adapters/jquery.js') }}"></script>
    <!-- wizard functions -->
    <script src="{{ asset('/js/apps/tisa_wysiwg.js') }}"></script>
    <!-- datatables -->
    <script src="{{ asset('/lib/DataTables/media/js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('/lib/DataTables/media/js/dataTables.bootstrap.js') }}"></script>
    <script src="{{ asset('/lib/DataTables/extensions/TableTools/js/dataTables.tableTools.min.js') }}"></script>
    <script src="{{ asset('/lib/DataTables/extensions/Scroller/js/dataTables.scroller.min.js') }}"></script>

    <!-- datatables functions -->
    <script src="{{ asset('/js/apps/tisa_datatables.js') }}"></script>

    <script>
    $('#evidenza_news').change(function(){
      if (this.value=='1')
      $('#evidenzadeadline').prop('disabled',false)
      else
      $('#evidenzadeadline').prop('disabled',true)
      })

        $('#news_table').DataTable({
            processing: true,
            serverSide: true,
            ajax: '{!! route('news_data') !!}',
            "ordering": false,
            columns: [
                {data: 'id'},
                {data: 'news_title'},
                {data: 'evidenza'},
                {data: 'created_at'},
                {data: 'azioni'}
            ],oLanguage: {sSearch: "Filtra: "},
            "aoColumnDefs": [
                {
                    "bSearchable": true,
                    "bVisible": true,
                    "aTargets": [ 0 ]
                },
                {
                    "bSearchable": true,
                    "bVisible": true,
                    "aTargets": [ 1 ]
                },
                {
                    "bSearchable": true,
                    "bVisible": true,
                    "aTargets": [ 2 ]
                },
                {
                    "bSearchable": true,
                    "bVisible": true,
                    "aTargets": [ 3 ]
                },
                {
                    "bSearchable": false,
                    "bVisible": true,
                    "aTargets": [ 4 ]
                }
            ]
        });

        $("table:eq(0) th:last").removeClass("sorting");

        window['table']="news";

    function loadnews(id){

        $.post(apiUrl+'api.php',{qr:'loadnews',id:id},function(response){
            response=JSON.parse(response);
            response=response[0];
            $("#news_title").val(response.news_title);

            if(response.breaking==1)
                $("#evidenza_news option[value='2']").prop("selected",true);
            else
                $("#evidenza_news option[value='"+response.evidenza+"']").prop("selected",true);

            $("#zone option[value='"+response.zone+"']").prop("selected",true);
            $("#deadline").val(response.deadline);
            CKEDITOR.instances.wysiwg_full.setData(response.news_body);
            if($("#idn").length==0)
                $("form").append("<input type='hidden' name='id' id='idn' value='"+response.id+"'>");
            else
               $("#idn").val(response.id);
        })
    }

    function evidenzadeadline(i){
        if(i=="1")
            $("#evidenzadeadline").prop("disabled",true);
        else
            $("#evidenzadeadline").prop("disabled",false);
    }

    </script>
@endsection
