  @extends('layouts.master')
@section('header-page-personalization')
    <style>
        .twitter-typeahead, .tt-hint, .tt-input, .tt-menu { width: 100%;  }
        .tt-menu {
            background: #ffffff;
        }
        [readonly]{
          background-color: white;
          opacity: 1;
        }
        templatebuoni{
            display: none;
        }
        listapreferite li{
          margin: 10px;
        }
        deleter{
          float: right;
        }
    </style>

@endsection
@section('content')

<?php $name=Auth::user()->name." ".Auth::user()->surname; ?>

    <div class="page_content page_customer page_edit_customer">
        <div class="container-fluid">

            <div class="row">
                <div class="col-lg-12">
                <div id="addDestination" class="modal fade" role="dialog">
                  <div class="modal-dialog">
                    <div class="modal-content">
                      <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Desideri aggiungere una nota per questo contatto?</h4>
                      </div>
                      <div class="modal-body">
                        <form class="form-horizontal">
                        <fieldset>

                        <div class="form-group col-md-6">
                          <label class="col-md-4 control-label" for="textinput">Nome destinazione</label>
                          <div class="col-md-4">
                          <input id="dsnm" type="text" placeholder="Nome.." class="form-control input-md">
                          <span class="help-block">Nome</span>
                          </div>
                        </div>


                        <div class="form-group col-md-6">
                          <div class="col-md-12" style="text-align: center;">
                            <button id="button1id" type="button" onclick="addDestination()" name="button1id" class="btn btn-success">Salva</button>
                          </div>
                        </div>

                        </fieldset>
                        </form>

                          </div>
                          <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Chiudi</button>
                          </div>
                        </div>

                      </div>
                    </div>

                <!-- Modal -->
                <div id="addDestination2" class="modal fade" role="dialog">

                  <div class="modal-dialog">

                    <!-- Modal content-->
                    <div class="modal-content">
                      <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Desideri aggiungere una nota per questo contatto?</h4>
                      </div>
                      <div class="modal-body">
                        <form class="form-horizontal">
                        <fieldset>

                        <div class="form-group col-md-6">
                          <label class="col-md-4 control-label" for="textinput">Nome destinazione</label>
                          <div class="col-md-4">
                          <input id="dsnm" type="text" placeholder="Nome.." class="form-control input-md">
                          <span class="help-block">Nome</span>
                          </div>
                        </div>


                        <div class="form-group col-md-6">
                          <div class="col-md-12" style="text-align: center;">
                            <button id="button1id" type="button" onclick="addDestination2()" name="button1id" class="btn btn-success">Salva</button>
                          </div>
                        </div>

                        </fieldset>
                        </form>

                          </div>
                          <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Chiudi</button>
                          </div>
                        </div>

                      </div>
                    </div>

                <!-- Modal -->
                <div id="addDestination3" class="modal fade" role="dialog">

                  <div class="modal-dialog">

                    <!-- Modal content-->
                    <div class="modal-content">
                      <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Desideri aggiungere una nota per questo contatto?</h4>
                      </div>
                      <div class="modal-body">
                        <form class="form-horizontal">
                        <fieldset>

                        <div class="form-group col-md-6">
                          <label class="col-md-4 control-label" for="textinput">Nome destinazione</label>
                          <div class="col-md-4">
                          <input id="dsnm" type="text" placeholder="Nome.." class="form-control input-md">
                          <span class="help-block">Nome</span>
                          </div>
                        </div>


                        <div class="form-group col-md-6">
                          <div class="col-md-12" style="text-align: center;">
                            <button id="button1id" type="button" onclick="addDestination3()" name="button1id" class="btn btn-success">Salva</button>
                          </div>
                        </div>

                        </fieldset>
                        </form>

                          </div>
                          <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Chiudi</button>
                          </div>
                        </div>

                      </div>
                    </div>


                    <form id="login_form" role="form" method="get" action="../customerUpdate/{{$customer->id}}" class="customForm">
                        {{ csrf_field() }}
                        {{ method_field('PUT') }}

                        <div class="panel panel-default">
                            <div class="panel-body">
                                <div class="heading_a"><legend><span class="big-title">MODIFICA PROFILO PRIVATO</span></legend></div>
                                <div class="tabbable tabs-left">
                                    <ul class="nav nav-tabs" id="tabs_c">
                                        <li class="active"><a data-toggle="tab" href="#info">Info</a></li>
                                        <li><a data-toggle="tab" href="#profilo">Profilo</a></li>
                                        <li><a data-toggle="tab" href="#categoria">Categoria</a></li>
                                        <li><a data-toggle="tab" href="#infoviaggi">Info Viaggi</a></li>
                                        <li><a data-toggle="tab" href="#qasuend">Info Suend</a></li>
                                        <li><a data-toggle="tab" href="#qaviaggio">Valutazione Viaggio</a></li>
                                        <li><a data-toggle="tab" href="#preventivi">Preventivi del cliente</a></li>
                                        <li><a data-toggle="tab" href="#relazione">Stato relazione</a></li>
                                       <li><a data-toggle="tab" href="#coupon">Gift Card / Coupon </a></li>
                                        <li id="editScheda" style="display: none;">
                                            <a href="/admin/cliente/<?php echo $customer->id ?>"><i class="glyphicon glyphicon-edit"></i>&nbsp;Modifica</a>
                                        </li>
                                        <li><a id="tast_relazione" target="_blank" href="/admin/tasks?action=linkto&amp;ent=customer&amp;n=#">Tasks</a></li>

                                    </ul>
                                    @if(session()->has('message'))
                                        <div class="alert alert-success">
                                            {{ session()->get('message') }}
                                        </div>
                                    @endif
                                    @if(session()->has('error'))
                                        <div class="alert alert-danger">
                                            {{ session()->get('error') }}
                                        </div>
                                    @endif
                                    <div class="tab-content" id="tabs_content_c">
                                        <div id="info" class="tab-pane fade in active">
                                            <div class="row">
                                                <div class="col-lg-12">
                                                    <div class="form-group col-md-6">
                                                        <label for="reg_input">Cognome</label>
                                                        <input type="text" id="cognome" name="cognome"
                                                               class="form-control"
                                                               value="{{ isset($customer->cognome) ? $customer->cognome : ''  }}"
                                                               tabindex="3">
                                                    </div>

                                                    <div class="form-group col-md-6">
                                                        <label for="reg_input">Nome</label>
                                                        <input type="text" id="nome" name="nome" class="form-control"
                                                               tabindex="2"
                                                               value="{{ isset($customer->nome) ? $customer->nome : ''  }}">
                                                    </div>
                                                    <div class="form-group col-md-6">
                                                        <label for="reg_input">Codice Fiscale</label>
                                                        <input type="text" id="info_codice_fiscale"

                                                               value="{{ isset($info->codice_fiscale) ? $info->codice_fiscale : '' }}"
                                                               name="info_codice_fiscale"
                                                               class="form-control" tabindex="14" onfocusout="validateFs(this)">
                                                    </div>

                                                    <div class="form-group col-md-6">
                                                        <label for="reg_input">Data di Nascita</label>
                                                        <input type="date" id="info_data_nascita"
                                                        value="{{ isset($info->data_nascita) ? $info->data_nascita : ''  }}"
                                                        name="info_data_nascita" class="form-control" tabindex="15">
                                                    </div>
                                                    <div class="form-group col-md-6">
                                                        <label for="reg_input">Sesso</label>
                                                        <select id="info_sesso" name="info_sesso" class="form-control"
                                                                tabindex="16">
                                                            <option value="{{ isset($info->sesso) ? $info->sesso : '' }}">
                                                                {{ isset($info->sesso) ? $info->sesso : ''}}
                                                            </option>
                                                            <option value="MASCHIO">MASCHIO</option>
                                                            <option value="FEMMINA">FEMMINA</option>
                                                        </select>
                                                    </div>
                                                    <div class="form-group col-md-6">
                                                        <label for="reg_input">Indirizzo</label>
                                                        <input type="text" id="info_indirizzo" name="info_indirizzo"

                                                               value="{{ isset($info->indirizzo) ? $info->indirizzo : '' }}"
                                                               class="form-control" tabindex="11">
                                                    </div>

                                                    <div class="spacer"></div>
                                                    <div class="form-group col-md-6">
                                                      <label class="col-md-12 control-label" for="radios" style="padding-left: 0px;">Residenza Italiana?</label>
                                                      <div class="col-md-12" style="padding-left: 0px;">
                                                        <label class="radio-inline" for="radios-0">
                                                          <input type="radio" name="residenza" id="residenza" value="0" checked="checked" onclick="setAutocompile(0)">
                                                          Si
                                                        </label>
                                                        <label class="radio-inline" for="radios-1">
                                                          <input type="radio" name="residenza" id="residenza" value="1" onclick="setAutocompile(1)">
                                                          No
                                                        </label>
                                                      </div>
                                                    </div>

                                                    <div class="form-group col-md-6">
                                                        <label for="reg_input">Citta'</label>
                                                        <input type="text" id="citta" name="citta" class="form-control"
                                                               tabindex="4"
                                                               value="{{ isset($customer->citta) ? $customer->citta : ''  }}">
                                                    </div>
                                                    <div class="form-group col-md-6">
                                                        <label for="reg_input">Cap</label>
                                                        <input type="text" id="cap" name="cap" class="form-control"
                                                               tabindex="7"
                                                               value="{{ isset($customer->cap) ? $customer->cap : ''  }}">
                                                    </div>
                                                    <div class="form-group col-md-6">
                                                        <label for="reg_input">Provincia</label>
                                                        <input type="text" id="provincia" name="provincia"
                                                               class="form-control"
                                                               value="{{ isset($customer->provincia) ? $customer->provincia : ''  }}"
                                                               tabindex="5">
                                                    </div>
                                                    <div class="form-group col-md-6">
                                                        <label for="reg_input">Regione</label>
                                                        <input type="text" id="regione" name="regione"
                                                               class="form-control"
                                                               value="{{ isset($customer->regione) ? $customer->regione : ''  }}"
                                                               tabindex="6">
                                                    </div>
                                                    <div class="form-group col-md-6">
                                                        <label for="reg_input">Stato</label>
                                                        <input type="text" id="stato" name="stato" class="form-control"
                                                               tabindex="8"
                                                               value="{{ isset($customer->stato) ? $customer->stato : ''  }}">
                                                    </div>

                                                    <div class="form-group col-md-6 col-md-12" style="padding: 0px;">
                                                      <div class="col-md-2">
                                                          <label for="reg_input">Prefisso</label>
                                                          <input type="text" id="pref1" name="pref1" class="form-control"
                                                                 tabindex="9"
                                                                 value="{{ isset($customer->pref1) ? $customer->pref1 : ''  }}"  placeholder="+39">
                                                      </div>
                                                      <div class="col-md-10">
                                                          <label for="reg_input">Telefono Cellulare</label>
                                                          <input type="text" id="tel1" name="tel1" class="form-control"
                                                                 tabindex="9"
                                                                 value="{{ isset($customer->tel1) ? $customer->tel1 : ''  }}">
                                                      </div>
                                                    </div>

                                                    <div class="form-group col-md-6 col-md-12" style="padding: 0px;">
                                                      <div class="col-md-2">
                                                          <label for="reg_input">Prefisso</label>
                                                          <input type="text" id="pref2" name="pref2"

                                                                 value="{{ isset($customer->pref2) ? $customer->pref2 : ''  }}"
                                                                 class="form-control"
                                                                 tabindex="22" placeholder="035">
                                                      </div>
                                                      <div class="col-md-10">
                                                          <label for="reg_input">Telefono Fisso</label>
                                                          <input type="text" id="tel2" name="tel2"
                                                                 value="{{ isset($customer->tel2) ? $customer->tel2 : ''  }}"
                                                                 class="form-control"
                                                                 tabindex="22">
                                                      </div>
                                                    </div>

                                                    <div class="form-group col-md-6">
                                                        <label for="reg_input" style="clear: both;">Email Principale</label>
                                                        <input type="text" id="email1" name="email1"
                                                               class="form-control"
                                                               value="{{ isset($customer->email1) ? $customer->email1 : ''  }}">
                                                    </div>
                                                    <div class="form-group col-md-6">
                                                        <label for="reg_input">Email Secondaria</label>
                                                        <textarea id="email2" name="email2" class="form-control" tabindex="22">{{ @$customer->email2 ? $customer->email2 : ''  }}</textarea>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div id="categoria"  class="tab-pane fade">
                                          <div class="row">
                                              <div class="col-lg-8">
                                                  <div class="form-group">
                                                      <label for="reg_input">Categoria</label>
                                                       <select name="categoria" class="form-control">
                                                           <option disabled="" selected="">Seleziona la Categoria</option>
                                                           @foreach (App\Category_Customers::all() as $cat)
                                                           <option value="{{$cat->id}}" {{$customer->cat_id == $cat->id? 'selected': ''}}>{{$cat->name}}</option>
                                                          @endforeach
                                                       </select>
                                                  </div>
                                                  <!--a href="{{url('admin/clienti/categories')}}" class="btn btn-info">MODIFICA LE CATEGORIE</a-->
                                              </div>
                                            </div>
                                        </div>
                                        <div id="infoviaggi" class="tab-pane fade">
                                            <div class="row">
                                                <div class="col-lg-8">

                                                    <div class="form-group col-md-12">
                                                        <label for="reg_input">Vacanze/Anno</label>

                                                         <select id="infoviaggi_vacanzeanno" name="infoviaggi_vacanzeanno" class="form-control">
                                                             <option disabled="" {{ isset($infoviaggi->vacanze_anno) ? '' : 'selected' }}>Scegli la frequenza</option>
                                                             <option value="1 / anno"  {{ isset($infoviaggi->vacanze_anno)&&$infoviaggi->vacanze_anno=="1 / anno" ? 'selected' : '' }}>1 / anno</option>
                                                             <option value="1-2 / anno"{{ isset($infoviaggi->vacanze_anno)&&$infoviaggi->vacanze_anno=="1-2 / anno" ? 'selected' : '' }}>1-2 / anno</option>
                                                             <option value="3-4 / anno"{{ isset($infoviaggi->vacanze_anno)&&$infoviaggi->vacanze_anno=="3-4 / anno" ? 'selected' : '' }}>3-4 / anno</option>
                                                             <option value="più di 5"{{ isset($infoviaggi->vacanze_anno)&&$infoviaggi->vacanze_anno=="più di 5" ? 'selected' : '' }}>più di 5</option>
                                                         </select>
                                                    </div>

                                                    <div class="form-group col-md-12">
                                                        <label for="reg_input">Preferenza Viaggi</label>
                                                        <select id="infoviaggi_preferenze" name="infoviaggi_preferenze" class="form-control">
                                                            <option disabled="" {{ isset($infoviaggi->preferenza_viaggi) ? '' : 'selected' }}>Scegli la preferenza</option>
                                                            <option value="Tour di gruppo" {{ isset($infoviaggi->preferenza_viaggi)&&$infoviaggi->preferenza_viaggi=="Tour di gruppo" ? 'selected' : '' }}>Tour di gruppo</option>
                                                            <option value="Tour individuali" {{ isset($infoviaggi->preferenza_viaggi)&&$infoviaggi->preferenza_viaggi=="Tour individuali" ? 'selected' : '' }}>Tour individuali</option>
                                                            <option value="Vacanza gruppo" {{ isset($infoviaggi->preferenza_viaggi)&&$infoviaggi->preferenza_viaggi=="Vacanza gruppo" ? 'selected' : '' }}>Vacanza gruppo</option>
                                                            <option value="Vacanza individuale" {{ isset($infoviaggi->preferenza_viaggi)&&$infoviaggi->preferenza_viaggi=="Vacanza individuale" ? 'selected' : '' }}>Vacanza individuale</option>
                                                            <option value="Solo volo" {{ isset($infoviaggi->preferenza_viaggi)&&$infoviaggi->preferenza_viaggi=="Solo volo" ? 'selected' : '' }}>Solo volo</option>
                                                        </select>
                                                    </div>

                                                    <div class="form-group col-md-12">
                                                        <label for="reg_input">Durata Media</label>
                                                        <select id="infoviaggi_duratamedia" name="infoviaggi_duratamedia" class="form-control">
                                                            <option disabled="" {{ isset($infoviaggi->durata_media) ? '' : 'selected' }}>Durata media notti</option>
                                                            <option value="1-3 notti" {{ isset($infoviaggi->durata_media)&&$infoviaggi->durata_media=="1-3 notti" ? 'selected' : '' }}>1-3 notti</option>
                                                            <option value="3-7 notti" {{ isset($infoviaggi->durata_media)&&$infoviaggi->durata_media=="3-7 notti" ? 'selected' : '' }}>3-7 notti</option>
                                                            <option value="7-10 notti" {{ isset($infoviaggi->durata_media)&&$infoviaggi->durata_media=="7-10 notti" ? 'selected' : '' }}>7-10 notti</option>
                                                            <option value="10-15 notti" {{ isset($infoviaggi->durata_media)&&$infoviaggi->durata_media=="10-15 notti" ? 'selected' : '' }}>10-15 notti</option>
                                                            <option value="più di 15 notti" {{ isset($infoviaggi->durata_media)&&$infoviaggi->durata_media=="più di 15 notti" ? 'selected' : '' }}>più di 15 notti</option>
                                                        </select>
                                                    </div>

                                                    <?php
                                                        $destinazioni=DB::select('SELECT id,UPPER(destinazione) AS label FROM destinazioni WHERE visibile=1 ORDER BY destinazione ASC');
                                                    ?>

                                                    <div class="form-group col-md-12">
                                                                <div class="ui-filter-container-pref col-md-12">
                                                                    <input type="text" id="prossime" placeholder="Cerca una destinazione" class="form-control input-md ui-autocomplete-input" autocomplete="off">
                                                                    <input type="hidden" name="destinazioniprossime[]">
                                                                </div>
                                                                <div class="ui-filter-container-pref col-md-12">
                                                                    <input type="text" id="preferite" placeholder="Cerca una destinazione" class="form-control input-md ui-autocomplete-input" autocomplete="off">
                                                                    <input type="hidden" name="destinazionipreferite[]">
                                                                </div>
                                                                <div class="ui-filter-container-pref col-md-12">
                                                                    <input type="text" id="passate" placeholder="Cerca una destinazione" class="form-control input-md ui-autocomplete-input" autocomplete="off">
                                                                    <input type="hidden" name="destinazionipassate[]">
                                                                </div>
                                                    </div>






                                                    <div class="form-group col-md-6" style="display: none;">
                                                        <label for="reg_input">Fonti</label>
                                                        <input type="text" id="infoviaggi_fonti"
                                                               name="infoviaggi_fonti"
                                                               value="{{ isset($infoviaggi->fonti) ? $infoviaggi->fonti : ''  }}"

                                                               class="form-control">
                                                    </div>
                                                </div>
                                                <div class="col-lg-4">
                                                    <label style="margin-left: 40px;">Mesi Preferiti</label>
                                                    <div class="col-lg-3">
                                                        <div class="checkbox">
                                                            <label>
                                                                <input type="checkbox" name="infoviaggi_gennaio"
                                                                       @if(isset($infoviaggi->gennaio) and $infoviaggi->gennaio == 1)
                                                                       checked
                                                                       @endif
                                                                       value="1">
                                                                Gennaio
                                                            </label>
                                                        </div>
                                                        <div class="checkbox">
                                                            <label>
                                                                <input type="checkbox" name="infoviaggi_febbraio"
                                                                       @if(isset($infoviaggi->febbraio) and $infoviaggi->febbraio == 1)
                                                                       checked
                                                                       @endif
                                                                       value="1">
                                                                Febbraio
                                                            </label>
                                                        </div>
                                                        <div class="checkbox">
                                                            <label>
                                                                <input type="checkbox" name="infoviaggi_marzo"
                                                                       @if(isset($infoviaggi->marzo) and $infoviaggi->marzo == 1)
                                                                       checked
                                                                       @endif
                                                                       value="1">
                                                                Marzo
                                                            </label>
                                                        </div>
                                                        <div class="checkbox">
                                                            <label>
                                                                <input type="checkbox" name="infoviaggi_aprile"
                                                                       @if(isset($infoviaggi->aprile) and $infoviaggi->aprile == 1)
                                                                       checked
                                                                       @endif
                                                                       value="1">
                                                                Aprile
                                                            </label>
                                                        </div>
                                                        <div class="checkbox">
                                                            <label>
                                                                <input type="checkbox" name="infoviaggi_maggio"
                                                                       @if(isset($infoviaggi->maggio) and $infoviaggi->maggio == 1)
                                                                       checked
                                                                       @endif
                                                                       value="1">
                                                                Maggio
                                                            </label>
                                                        </div>
                                                        <div class="checkbox">
                                                            <label>
                                                                <input type="checkbox" name="infoviaggi_giugno"
                                                                       @if(isset($infoviaggi->giugno) and $infoviaggi->giugno == 1)
                                                                       checked
                                                                       @endif
                                                                       value="1">
                                                                Giugno
                                                            </label>
                                                        </div>
                                                      </div>
                                                      <div class="col-lg-3">
                                                        <div class="checkbox">
                                                            <label>
                                                                <input type="checkbox" name="infoviaggi_luglio"
                                                                       @if(isset($infoviaggi->luglio) and $infoviaggi->luglio == 1)
                                                                       checked
                                                                       @endif
                                                                       value="1">
                                                                Luglio
                                                            </label>
                                                        </div>
                                                        <div class="checkbox">
                                                            <label>
                                                                <input type="checkbox" name="infoviaggi_agosto"
                                                                       @if(isset($infoviaggi->agosto) and $infoviaggi->agosto == 1)
                                                                       checked
                                                                       @endif
                                                                       value="1">
                                                                Agosto
                                                            </label>
                                                        </div>
                                                        <div class="checkbox">
                                                            <label>
                                                                <input type="checkbox" name="infoviaggi_settembre"
                                                                       @if(isset($infoviaggi->settembre) and $infoviaggi->settembre == 1)
                                                                       checked
                                                                       @endif
                                                                       value="1">
                                                                Settembre
                                                            </label>
                                                        </div>
                                                        <div class="checkbox">
                                                            <label>
                                                                <input type="checkbox" name="infoviaggi_ottobre"
                                                                       @if(isset($infoviaggi->ottobre) and $infoviaggi->ottobre == 1)
                                                                       checked
                                                                       @endif
                                                                       value="1">
                                                                Ottobre
                                                            </label>
                                                        </div>
                                                        <div class="checkbox">
                                                            <label>
                                                                <input type="checkbox" name="infoviaggi_novembre"
                                                                       @if(isset($infoviaggi->novembre) and $infoviaggi->novembre == 1)
                                                                       checked
                                                                       @endif
                                                                       value="1">
                                                                Novembre
                                                            </label>
                                                        </div>
                                                        <div class="checkbox">
                                                            <label>
                                                                <input type="checkbox" name="infoviaggi_dicembre"
                                                                       @if(isset($infoviaggi->dicembre) and $infoviaggi->dicembre == 1)
                                                                       checked
                                                                       @endif
                                                                       value="1">
                                                                Dicembre
                                                            </label>
                                                        </div>
                                                    </div>
                                                  </div>
                                            </div>




                                            <div class="row">
                                                <div class="col-lg-8">
                                                    <div class="col-lg-4">
                                                        <label>Tematiche</label>
                                                        <div class="checkbox">
                                                            <label>
                                                                <input type="checkbox" name="infoviaggi_archeologia"
                                                                       @if(isset($infoviaggi->archeologia) and $infoviaggi->archeologia == 1)
                                                                       checked
                                                                       @endif
                                                                       value="1">
                                                                Archeologia
                                                            </label>
                                                        </div>
                                                        <div class="checkbox">
                                                            <label>
                                                                <input type="checkbox" name="infoviaggi_avventura"
                                                                       @if(isset($infoviaggi->avventura) and $infoviaggi->avventura == 1)
                                                                       checked
                                                                       @endif
                                                                       value="1">
                                                                Avventura
                                                            </label>
                                                        </div>
                                                        <div class="checkbox">
                                                            <label>
                                                                <input type="checkbox" name="infoviaggi_cultura"
                                                                       @if(isset($infoviaggi->cultura) and $infoviaggi->cultura == 1)
                                                                       checked
                                                                       @endif
                                                                       value="1">
                                                                Cultura
                                                            </label>
                                                        </div>
                                                        <div class="checkbox">
                                                            <label>
                                                                <input type="checkbox" name="infoviaggi_ecoturismo"
                                                                       @if(isset($infoviaggi->ecoturismo) and $infoviaggi->ecoturismo == 1)
                                                                       checked
                                                                       @endif
                                                                       value="1">
                                                                Ecoturismo
                                                            </label>
                                                        </div>
                                                        <div class="checkbox">
                                                            <label>
                                                                <input type="checkbox" name="infoviaggi_altro"
                                                                       @if(isset($infoviaggi->altro) and $infoviaggi->altro == 1)
                                                                       checked
                                                                       @endif
                                                                       value="1">
                                                                Altro
                                                            </label>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-4">
                                                        <div class="checkbox">
                                                            <label>
                                                                <input type="checkbox"
                                                                       name="infoviaggi_enogastronomia"
                                                                       @if(isset($infoviaggi->enograstronomia) and $infoviaggi->enogastronomia == 1)
                                                                       checked
                                                                       @endif
                                                                       value="1">
                                                                Enogastronomia
                                                            </label>
                                                        </div>
                                                        <div class="checkbox">
                                                            <label>
                                                                <input type="checkbox" name="infoviaggi_fotografico"
                                                                       @if(isset($infoviaggi->fotografico) and $infoviaggi->fotografico == 1)
                                                                       checked
                                                                       @endif
                                                                       value="1">
                                                                Fotografico
                                                            </label>
                                                        </div>
                                                        <div class="checkbox">
                                                            <label>
                                                                <input type="checkbox" name="infoviaggi_marerelax"
                                                                       @if(isset($infoviaggi->mare_relax) and $infoviaggi->mare_relax == 1)
                                                                       checked
                                                                       @endif
                                                                       value="1">
                                                                Mare e relax
                                                            </label>
                                                        </div>
                                                        <div class="checkbox">
                                                            <label>
                                                                <input type="checkbox" name="infoviaggi_montagna"
                                                                       @if(isset($infoviaggi->montagna) and $infoviaggi->montagna == 1)
                                                                       checked
                                                                       @endif
                                                                       value="1">
                                                                Montagna
                                                            </label>
                                                        </div>

                                                    </div>
                                                    <div class="col-lg-4">
                                                        <div class="checkbox">
                                                            <label>
                                                                <input type="checkbox" name="infoviaggi_religioso"
                                                                       @if(isset($infoviaggi->religioso) and $infoviaggi->religioso == 1)
                                                                       checked
                                                                       @endif
                                                                       value="1">
                                                                Religioso
                                                            </label>
                                                        </div>
                                                        <div class="checkbox">
                                                            <label>
                                                                <input type="checkbox" name="infoviaggi_sportivo"
                                                                       @if(isset($infoviaggi->sportivo) and $infoviaggi->sportivo == 1)
                                                                       checked
                                                                       @endif
                                                                       value="1">
                                                                Sportivo
                                                            </label>
                                                        </div>
                                                        <div class="checkbox">
                                                            <label>
                                                                <input type="checkbox"
                                                                       name="infoviaggi_storicopolitico"
                                                                       @if(isset($infoviaggi->storico_politico) and $infoviaggi->storico_politico == 1)
                                                                       checked
                                                                       @endif
                                                                       value="1">
                                                                Storico Politico
                                                            </label>
                                                        </div>

                                                        <div class="checkbox">
                                                            <label>
                                                                <input type="checkbox" name="infoviaggi_solo_volo" value="1"
                                                                @if(isset($infoviaggi->lavoro) and $infoviaggi->lavoro == 1)
                                                                       checked
                                                                       @endif>
                                                                Lavoro
                                                            </label>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-4">
                                                    <textarea cols="30" rows="5" class="form-control input-sm"
                                                              name="infoviaggi_tematiche_altro"
                                                              placeholder="Altre tematiche..." value='{{ isset($infoviaggi->note_tematiche) ? $infoviaggi->note_tematiche : ''  }}'>{{ isset($infoviaggi->note_tematiche) ? $infoviaggi->note_tematiche : ''  }}</textarea>
                                                </div>


                                            </div>

                                        </div>

                                        <div id="qasuend" class="tab-pane fade">
                                            <div class="row">

                                                <div class="col-lg-12">
                                                    <div class="form-group col-md-6">
                                                        <label for="reg_input">Conoscenza Suend</label>
                                                        <input type="text" id="qasuend_conoscenza"
                                                               name="qasuend_conoscenza"

                                                               value="{{ isset($qasuend->conoscenza_suend) ? $qasuend->conoscenza_suend : ''  }}"
                                                               class="form-control">
                                                    </div>
                                                </div>

                                                <div class="col-lg-12">
                                                    <div class="form-group col-md-6">
                                                        <label for="reg_input">Altre motivazioni conoscenza
                                                            Suend</label>
                                                        <input type="text" id="qasuend_altremotivazione"
                                                               name="qasuend_altremotivazione"

                                                               value="{{ isset($qasuend->note_conoscenza_suend) ? $qasuend->note_conoscenza_suend : ''  }}"
                                                               class="form-control">
                                                    </div>
                                                </div>
                                                <div class="col-lg-12" style="display: none;">
                                                    <div class="form-group col-md-6">
                                                        <label for="reg_input">Codice Cliente</label>
                                                        <input type="text" id="qasuend_codicecliente"
                                                               name="qasuend_codicecliente"
                                                               class="form-control"

                                                               value="{{ isset($qasuend->codice_cliente) ? $qasuend->codice_cliente : ''  }}"
                                                               tabindex="0">
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-lg-4">
                                                    <div class="form-group col-md-12 inlineCheckbox">
                                                        <label for="reg_input" style="text-align: center;">Ha viaggiato con noi?</label>
                                                        <input type="checkbox" class="input-sm"
                                                               name="qasuend_giaviaggiato" id="qasuend_giaviaggiato"
                                                               @if(isset($qasuend->gia_viaggiato) and $qasuend->gia_viaggiato == 1)
                                                               checked
                                                               @endif
                                                               value="true">
                                                    </div>
                                                </div>
                                                <div class="col-lg-4">
                                                    <div class="form-group col-md-12">
                                                        <label for="reg_input">Quante Volte</label>
                                                        <input type="text" id="qasuend_quantiviaggi"
                                                               name="qasuend_quantiviaggi"

                                                               value="{{ isset($qasuend->quante_volte) ? $qasuend->quante_volte : ''  }}"
                                                               class="form-control"
                                                               tabindex="0">
                                                    </div>
                                                </div>
                                            <div class="row">
                                                <div class="col-lg-4">
                                                    <div class="form-group col-md-12 inlineCheckbox">
                                                        <label for="reg_input" style="text-align: center;">Conosce
                                                            Sito</label>
                                                        <input type="checkbox" class="input-sm"
                                                               name="qasuend_conoscesito" id="qasuend_conoscesito"
                                                               @if(isset($qasuend->conosce_sito) and $qasuend->conosce_sito == 1)
                                                               checked
                                                               @endif
                                                               value="true">
                                                    </div>
                                                </div>
                                            </div>

                                                <div class="col-lg-12">
                                                    <div class="form-group col-md-12">
                                                        <input type="text" id="qasuend_doveviaggi"
                                                               name="qasuend_doveviaggi"

                                                               value="{{ isset($qasuend->luogo_viaggio) ? $qasuend->luogo_viaggio : ''  }}"
                                                               class="form-control"
                                                               tabindex="0">
                                                    </div>
                                                </div>
                                            </div>


                                            <div class="col-lg-12" id="bonus">
                                              <legend> Storico buoni</legend>
                                              <p>
                                                <a href="#" class="btn btn-success btn-md" onclick="addBonus(this)">
                                                  <span class="glyphicon glyphicon-plus"></span> Aggiungi buono
                                                </a>
                                              </p>
                                              <listabuoni>
                                                    <?php
                                                    //print_r($bonusList);
                                                    ?>
                                                    @foreach($bonusList as $bonus)
                                                    <div class="col-lg-12 bonus oldbonus">
                                                      <p>Creato il <strong>{{$bonus->timer}}</strong></p>
                                                        <div class="form-group col-md-6 col-lg-2">
                                                            <label for="reg_input">Numero buono</label>
                                                            <input type="text" data-bonus="new" class="form-control" tabindex="52" name="qasuend_numero_buono" id="qasuend_numero_buono" placeholder="identificativo buono" value="{{$bonus->bonusId}}" data-id="{{$bonus->Value}}">
                                                        </div>
                                                        <div class="form-group col-md-6 col-lg-2">
                                                            <label for="reg_input">Tipo buono</label>
                                                            <input type="text" data-bonus="new" class="form-control" tabindex="52" name="qasuend_tipo_buono" id="qasuend_tipo_buono" placeholder="tipologia buono" value="{{$bonus->Kind}}" data-id="{{$bonus->Value}}">
                                                        </div>
                                                        <div class="form-group col-md-6 col-lg-2">
                                                            <label for="reg_input">Importo buono</label>
                                                            <input type="text" data-bonus="new" class="form-control" tabindex="52" name="qasuend_importo_buono" id="qasuend_importo_buono" placeholder="importo" value="{{$bonus->Value}}" data-id="{{$bonus->Value}}">
                                                        </div>
                                                        <div class="form-group col-md-6 col-lg-2">
                                                            <label for="reg_input">Azioni</label>
                                                            <a href="#" class="btn btn-danger btn-md"  onclick="jsRequestConfirm(deleteBonus,[{{$bonus->id}},this])">
                                                              <span class="glyphicon glyphicon-trash"></span> Rimuovi
                                                            </a>
                                                        </div>
                                                    </div>
                                                    @endforeach
                                              </listabuoni>

                                                <templatebuoni>
                                                    <div class="col-lg-12 bonus newbonus">
                                                        <div class="form-group col-md-6 col-lg-2">
                                                            <label for="reg_input">Numero buono</label>
                                                            <input type="text" data-bonus="new" class="form-control" tabindex="52" name="qasuend_numero_buono" id="qasuend_numero_buono" placeholder="identificativo buono">
                                                        </div>
                                                        <div class="form-group col-md-6 col-lg-2">
                                                            <label for="reg_input">Tipo buono</label>
                                                            <input type="text" data-bonus="new" class="form-control" tabindex="52" name="qasuend_tipo_buono" id="qasuend_tipo_buono" placeholder="tipologia buono">
                                                        </div>
                                                        <div class="form-group col-md-6 col-lg-2">
                                                            <label for="reg_input">Importo buono</label>
                                                            <input type="text" data-bonus="new" class="form-control" tabindex="52" name="qasuend_importo_buono" id="qasuend_importo_buono" placeholder="importo">
                                                        </div>
                                                        <div class="form-group col-md-6 col-lg-2">
                                                            <label for="reg_input">Azioni</label>
                                                            <a href="#" class="btn btn-danger btn-md" onclick="jsRequestConfirm(deleteBonus,['',this])">
                                                              <span class="glyphicon glyphicon-trash" ></span> Rimuovi
                                                            </a>
                                                        </div>
                                                    </div>
                                                </templatebuoni>
                                            </div>

                                            <div class="row">
                                                <div class="col-lg-12">
                                                    <div class="form-group col-md-6">
                                                        <label for="reg_input">Note</label>
                                                        <textarea name="qasuend_note" id="qasuend_note" cols="10"
                                                                  rows="3"
                                                                  class="form-control">
                                                         {{ isset($qasuend->note_qa_suend) ? $qasuend->note_qa_suend : ''  }}
                                                        </textarea>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div id="qaviaggio" class="tab-pane fade">
                                            <div class="row">
                                                <div class="col-lg-12">
                                                    <div class="form-group col-md-6">
                                                        <label for="reg_input">Soddisfazione Generale</label>
                                                        <select name="qaviaggio_soddisfazione" id="qaviaggio_soddisfazione" class="form-control">
                                                            <option value="eccellente" {{ isset($qaviaggio->choices) && $qaviaggio->choices  == 'eccellente' ? 'selected' : '' }}>eccellente</option>
                                                            <option value="buono" {{ isset($qaviaggio->choices) && $qaviaggio->choices  == 'buono' ? 'selected' : '' }}>buono</option>
                                                            <option value="male" {{ isset($qaviaggio->choices) && $qaviaggio->choices  == 'male' ? 'selected' : '' }}>male</option>
                                                            <option value="N/A"  {{ isset($qaviaggio->choices) && $qaviaggio->choices == 'N/A' ? 'selected' : '' }}>N/A</option>
                                                        </select>
                                                    </div>
                                                    <?php $preventivi=DB::table("preventivo")->whereNull("riferitoA")->get();?>
                                                    <?php
                                                    $preventivo_assegnato=DB::table("preventivo")->where("riferitoA",$customer->id)->get();
                                                    if(count($preventivo_assegnato))
                                                      $preventivo_assegnato=$preventivo_assegnato[0];
                                                    else
                                                      $preventivo_assegnato=false;
                                                    ?>
                                                    <div class="form-group col-md-6">
                                                        <label for="reg_input">Collega a un preventivo</label>
                                                        <select name="qaviaggio_preventivo" class="form-control" onchange="apply_prev_args()">

                                                        <?php if(empty($preventivo_assegnato)){ ?>
                                                          <option selected="" disabled="">Scegli un preventivo</option>
                                                        <?php }else{ ?>

                                                          <option selected="">Scegli un preventivo</option>
                                                          <option value="{{$preventivo_assegnato->id}}" data-dest="{{$preventivo_assegnato->labeldest}}" data-dal="{{$preventivo_assegnato->dal}}" selected="">{{$preventivo_assegnato->labeldest}}</option>

                                                        <?php } ?>

                                                           @foreach($preventivi as $preventivo)
                                                            <option value="{{$preventivo->id}}" data-dest="{{$preventivo->labeldest}}" data-dal="{{$preventivo->dal}}">{{$preventivo->labeldest}}</option>
                                                           @endforeach
                                                        </select>
                                                    </div>
                                                    <div class="form-group col-md-6">
                                                        <label for="reg_input">Data Partenza</label>
                                                        <input class="form-control ts_datepicker" type="date"
                                                               name="qaviaggio_datapartenza"
                                                               id="qaviaggio_datapartenza"
                                                               value="{{ isset($qaviaggio->data_partenza) ? $qaviaggio->data_partenza : ''  }}"
                                                               placeholder="gg/mm/aaaa">
                                                    </div>
                                                    <div class="form-group col-md-6">
                                                        <label for="reg_input">Destinazione</label>
                                                        <input type="text" id="qaviaggio_destinazione"
                                                               name="qaviaggio_destinazione"

                                                               value="{{ isset($qaviaggio->destinazione) ? $qaviaggio->destinazione : ''  }}"
                                                               class="form-control">
                                                    </div>
                                                    <div class="form-group col-md-6">
                                                        <label for="reg_input">Soddisfazione servizi Suend</label>
                                                        <select name="qaviaggio_valutazione" id="qaviaggio_valutazione" class="form-control" {{ $qaviaggio->valutazione}}>
                                                            <option value="eccellente" {{ $qaviaggio->valutazione=='eccellente' ? 'selected' : ''  }}>eccellente</option>
                                                            <option value="ottimo" {{ $qaviaggio->valutazione=='ottimo' ? 'selected' : ''  }}>ottimo</option>
                                                            <option value="buono" {{ $qaviaggio->valutazione=='buono' ? 'selected' : ''  }}>buono</option>
                                                            <option value="discreto" {{ $qaviaggio->valutazione=='discreto' ? 'selected' : ''  }}>discreto</option>
                                                            <option value="sufficiente" {{ $qaviaggio->valutazione=='sufficiente' ? 'selected' : ''  }}>sufficiente</option>
                                                            <option value="mediocre" {{ $qaviaggio->valutazione=='mediocre' ? 'selected' : ''  }}>mediocre</option>
                                                            <option value="pessimo" {{ $qaviaggio->valutazione=='pessimo' ? 'selected' : ''  }}>pessimo</option>


                                                        </select>
                                                    </div>
                                                    <div class="form-group col-md-6">
                                                        <label for="reg_input">Ultima Rilevazione</label>
                                                        <input class="form-control ts_datepicker" type="date"
                                                               name="qaviaggio_ultimarilevazione"
                                                               id="qaviaggio_ultimarilevazione"

                                                               value="{{ isset($qaviaggio->ultima_rilevazione) ? $qaviaggio->ultima_rilevazione : ''  }}"
                                                               placeholder="gg/mm/aaaa">
                                                    </div>
                                                    <div class="form-group col-md-6">
                                                        <label for="reg_input">Commenti</label>
                                                        <textarea cols="30" name="qaviaggio_storico"
                                                                  id="qaviaggio_storico" rows="3"
                                                                  class="form-control input-sm">
                                                            {{ isset($qaviaggio->storico) ? $qaviaggio->storico : ''  }}

                                                        </textarea>
                                                    </div>
                                                </div>
                                            </div>
                                            <script type="text/javascript">
                                              function apply_prev_args(){

                                                var data=$('[name="qaviaggio_preventivo"] option:selected').data();
                                                $('[name="qaviaggio_datapartenza"]').val(data.dal);
                                                $('[name="qaviaggio_destinazione"]').val(data.dest);
                                                $('[name="qaviaggio_datapartenza"],[name="qaviaggio_destinazione"]').trigger("change");
                                              }
                                            </script>
                                        <div class="row">
                                          <div class="col-lg-12">
                                            <ul class="todo_list_wrapper">
                                              @foreach($valutazioni as $valutazione)

                                              <li data-task-title="t4" data-task-label="t4">
                                                 <span class="label color_c" style="text-transform: uppercase;">Preventivo: {{$valutazione->field4}}</span><br>
                                                 <span>Valutazione creata da: <strong class="nome-utente-task"> {{$valutazione->insertBy}} </strong></span>

                                                 <span> Soddisfazione Generale: <strong>{{$valutazione->field1}} </strong></span><br>
                                                 <span> Data Partenza: <strong>{{$valutazione->field2}} </strong></span><br>
                                                 <span> Destinazione: <strong>{{$valutazione->field4}} </strong></span><br>
                                                 <span> Soddisfazione servizi Suend: <strong>{{$valutazione->field5}} </strong></span><br>
                                                 <span> Ultima Rilevazione: <strong>{{$valutazione->field3}} </strong></span><br>
                                                 <span> Commenti: <strong>{{$valutazione->field7}} </strong></span>
                                              </li>


                                              @endforeach
                                            </ul>
                                          </div>
                                        </div>

                                        </div>
                                        <div id="profilo" class="tab-pane fade">
                                            <div class="row">

                                                <div class="col-md-4" style="display: none;">

                                                    <label>Data di nascita</label>
                                                    <input class="form-control ts_datepicker" type="text"

                                                           value="{{ isset($profilo->data_nascita) ? $profilo->data_nascita : ''  }}"
                                                           name="profilo_datanascita" id="profilo_datanascita">
                                                </div>
                                                <div class="col-md-8">
                                                    <div class="form-group col-md-6">
                                                        <label>Luogo di nascita</label>
                                                        <input type="text" id="profilo_luogonascita"
                                                               name="profilo_luogonascita"
                                                               value="{{ isset($profilo->luogonascita) ? $profilo->luogonascita : ''  }}"

                                                               class="form-control">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-8">
                                                    <div class="form-group col-md-6">
                                                        <label>Professione</label>
                                                        <input type="text" id="profilo_professione"
                                                               name="profilo_professione"
                                                               value="{{ isset($profilo->professione) ? $profilo->professione : ''  }}"

                                                               class="form-control">
                                                    </div>
                                                    <div class="form-group col-md-6">
                                                        <label>Hobby 1</label>
                                                        <input type="text" id="profilo_hobby1" name="profilo_hobby1"
                                                               value="{{ isset($profilo->hobby1) ? $profilo->hobby1 : ''  }}"
                                                               class="form-control">
                                                    </div>
                                                    <div class="form-group col-md-6">
                                                        <label>Hobby 2</label>
                                                        <input type="text" id="profilo_hobby2" name="profilo_hobby2"
                                                               value="{{ isset($profilo->hobby2) ? $profilo->hobby2 : ''  }}"

                                                               class="form-control">
                                                    </div>
                                                    <div class="form-group col-md-6">
                                                        <label>Numero Carta Identita</label>
                                                        <input class="form-control" type="text"
                                                               name="profilo_cartaidentita" id="profilo_cartaidentita"
                                                               value="{{ isset($profilo->carteidentita) ? $profilo->carteidentita : ' '  }}">
                                                    </div>
                                                    <div class="form-group col-md-6">
                                                        <label>Scadenza Carta identita'</label>
                                                        <input class="form-control ts_datepicker" type="date"
                                                               name="profilo_datascadenzaidentita"
                                                               id="profilo_datascadenzaidentita"
                                                               value="{{ isset($profilo->scadenza_passaporto) ? $profilo->scadenza_passaporto : ''  }}"

                                                               placeholder="gg/mm/aaaa">
                                                    </div>
                                                    <div class="form-group col-md-6">
                                                        <label>Numero Passaporto</label>
                                                        <input class="form-control" type="text"
                                                               name="profilo_passaporto" id="profilo_passaporto"
                                                               value="{{ isset($profilo->passaporto) ? $profilo->passaporto : ''  }}">
                                                    </div>
                                                    <div class="form-group col-md-6">
                                                        <label>Scadenza passaporto</label>
                                                        <input class="form-control ts_datepicker" type="date"
                                                               name="profilo_datascadenzapassaporto"
                                                               id="profilo_datascadenzapassaporto"
                                                               value="{{ isset($profilo->scadenza_passaporto) ? $profilo->scadenza_passaporto : ''  }}"

                                                               placeholder="gg/mm/aaaa">
                                                    </div>
                                                    <div class="form-group col-md-6">
                                                        <div class="checkbox">
                                                            <label>
                                                                <input type="checkbox" name="profilo_newsletter"
                                                                       @if(isset($profilo->newsletter) and $profilo->newsletter == 1)
                                                                       checked
                                                                       @endif
                                                                       value="true">
                                                                Newsletter
                                                            </label>
                                                        </div>
                                                    </div>
                                                    <div class="form-group col-md-6">
                                                        <div class="checkbox">
                                                            <label>
                                                                <input type="checkbox" name="profilo_clientediretto"
                                                                       @if(isset($profilo->cliente_diretto) and $profilo->cliente_diretto == 1)
                                                                       checked
                                                                       @endif
                                                                       value="true">
                                                                Cliente diretto
                                                            </label>
                                                        </div>
                                                    </div>

                                                </div>
                                                <div class="col-md-4">
                                                    <br>
                                                    <div class="form-group col-md-6">
                                                        <label>Frequent Flyer</label>
                                                        <textarea cols="30" rows="20" class="form-control input-sm"
                                                                  name="profilo_frequentflyer"
                                                                  placeholder="">
                                                        {{ isset($profilo->frequent_flyer) ? $profilo->frequent_flyer : ''  }}
                                                        </textarea>
                                                    </div>
                                                </div>

                                            </div>
                                            <div class="row">
                                                <div class="col-md-8">
                                                    <div class="col-md-2">

                                                        <label>Adulti</label>
                                                        <input type="text" id="profilo_adulti" name="profilo_adulti"
                                                               value="{{ isset($profilo->adulti) ? $profilo->adulti : ''  }}"

                                                               class="form-control">
                                                    </div>
                                                    <div class="col-md-2">
                                                        <label>Bambini</label>
                                                        <input type="text" id="profilo_bambini"
                                                               name="profilo_bambini"
                                                               value="{{ isset($profilo->bambini) ? $profilo->bambini : ''  }}"

                                                               class="form-control">
                                                    </div>
                                                    <div class="col-md-2">
                                                        <label>Neonati</label>
                                                        <input type="text" id="profilo_neonati"
                                                               name="profilo_neonati"
                                                               value="{{ isset($profilo->neonati) ? $profilo->neonati : ''  }}"

                                                               class="form-control">
                                                    </div>
                                                    <div class="col-md-2">

                                                        <label>Figli</label>
                                                        <input type="text" style="display: none;" id="profilo_figli" name="profilo_figli"
                                                               value="{{ isset($profilo->figli) ? $profilo->figli : ''  }}"

                                                               class="form-control">
                                                    </div>
                                                    <div class="col-md-2">
                                                        <label>Anziani</label>
                                                        <input type="text" id="profilo_anziani"
                                                               name="profilo_anziani"
                                                               value="{{ isset($profilo->anziani) ? $profilo->anziani : ''  }}"
                                                               class="form-control">
                                                    </div>
                                                    <div class="col-md-2">
                                                        <label>Animali</label>
                                                        <input type="text" id="profilo_animali"
                                                               name="profilo_animali"

                                                               value="{{ isset($profilo->animali) ? $profilo->animali : ''  }}"
                                                               class="form-control">
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group col-md-6">
                                                        <label>Note</label>
                                                        <textarea style="height: 300px;" cols="30" rows="5" class="form-control input-sm"
                                                                  name="profilo_note"
                                                                  placeholder="">
                                                           {{ isset($profilo->note) ? $profilo->note : ' '  }}
                                                        </textarea>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-8">
                                                    <div class="col-md-2">
                                                        <div class="form-group col-md-6">
                                                            <div class="checkbox">
                                                                <label>
                                                                    <input type="checkbox" value="true"
                                                                           @if(isset($profilo->iscritto_club) and $profilo->iscritto_club == 1)
                                                                           checked
                                                                           @endif
                                                                           name="profilo_iscrittoclub">
                                                                    Iscritto Club
                                                                </label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-10">
                                                        <div class="form-group col-md-6">
                                                            <label>Codice Club</label>
                                                            <input type="text" id="profilo_codiceclub"
                                                                   name="profilo_codiceclub"
                                                                   value="{{ isset($profilo->codice_club) ? $profilo->codice_club : ' '  }}"
                                                                   class="form-control">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div id="preventivi" class="tab-pane fade">
                                                <div id="mostraPreventivi">
                                                    <div class="form-group col-md-6">
                                                        <label for="reg_input">Preventivi collegati al cliente : {{ count($datiPreventivo) }} </label>
                                                        <ul>
                                                        @foreach($datiPreventivo as $preventivo)
                                                            <li style="margin: 20px;">
                                                                <p>Destinazione preventivo: {{ $preventivo->labeldest }}</p>
                                                                <p>Ultimo stato: {{ $preventivo->statoPreventivo }}</p>
                                                                <p>Ultima modifica: {{ $preventivo->updated_at }}</p>
                                                                <p>Per visualizzare i dettagli clicca <a href="/admin/preventivo/edit/{{ $preventivo->id }}?action=view"> qui </a> </p>
                                                            </li>
                                                         @endforeach
                                                        </ul>
                                                    </div>
                                                </div>
                                        </div>

                                        <div id="relazione" class="tab-pane fade">

                                                <div id="storicoContatti">

                                                    <div class="form-group">
                                                        <label for="reg_input">Aggiorna relazione</label>
                                                        <select type="text" id="status" name="status" class="form-control">
                                                            <option selected disabled value="">Scegli uno step della relazione</option>
                                                            <option>contattato - Sms </option>
                                                            <option>contattato - Mail </option>
                                                            <option>contattato - Recall  </option>
                                                            <option>contattato - Visita Diretta  </option>
                                                            <option>Da risentire</option>
                                                            <option>Interessato</option>
                                                            <option>Non interessato</option>
                                                            <option>Cliente suend</option>
                                                        </select>
                                                    </div>

                                                    <div class="form-group">
                                                      <a href="#" class="btn btn-success btn-md applyStatus" onclick="addStatus()">
                                                      <span class="glyphicon glyphicon-plus"></span> Applica
                                                    </a>
                                                    </div>
                                                    <div class="form-group">


                                                        <!-- Modal -->
                                                        <div id="notaStato" class="modal fade" role="dialog">
                                                          <div class="modal-dialog">

                                                            <!-- Modal content-->
                                                            <div class="modal-content">
                                                              <div class="modal-header">
                                                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                                <h4 class="modal-title">Desideri aggiungere una nota per questo contatto?</h4>
                                                              </div>
                                                              <div class="modal-body">
                                                                <p><input type="text" name="notaStato" placeholder="aggiungi qui la tua nota"></p>
                                                              </div>
                                                              <div class="modal-footer">
                                                                <button type="button" class="btn btn-default" data-dismiss="modal" onclick="addStatusStep2()">Applica e chiudi</button>
                                                              </div>
                                                            </div>

                                                          </div>
                                                        </div>


                                                        <label for="reg_input">Stato relazione</label>
                                                        <listaStati>
                                                            @foreach($customersRelations as $relations)
                                                            <p>l'utente <strong>
                                                              {{ $relations->nome }} {{ $relations->cognome }}
                                                            </strong> il
                                                            <strong> {{ $relations->timer }} </strong>
                                                            ha aggiornato lo stati in: <br> {{ $relations->status }}
                                                            @if($relations->nota)
                                                              <p>Con la seguente nota: </p>
                                                              <p> <strong> {{ $relations->nota }} </strong></p>
                                                            @endif
                                                          @if(!$customersRelations)
                                                            <p class="defaultMsg">Non sono stati inseriti dati su questo cliente</p>
                                                          @endif
                                                          @endforeach
                                                        </listaStati>
                                                    </div>
                                                </div>
                                        </div>


                                        <div id="coupon" class="tab-pane fade">
                                                    <div class="col-md-10">
                                                        <div class="form-group">
                                                            <label>Gift Card associate</label>
                                                            @if(empty($giftCard[0]))
                                                                <p><b>Non</b> sono presenti coupon assegnate a questo utente</p>
                                                            @endif
                                                            <ul>
                                                              @foreach($giftCard as $card)
                                                              <li>
                                                                <a href="{{route('card edit',['id' => $card->id])}}" target="blank"> Carta: {{$card->code}} </a><br>
                                                                  Creata il {{$card->created_at}} <br>
                                                                  Ultimo utilizzo: {{$card->data_utilizzo}} <br>
                                                                  Credito : {{$card->spesa}} / {{$card->value}} <br>
                                                              </li>
                                                              @endforeach
                                                            </ul>

                                                        </div>
                                                    </div>
                                                    <div class="col-md-10">
                                                        <div class="form-group">
                                                            <label>Coupon associato</label>
                                                            @if(empty($coupons[0]))
                                                                <p><b>Non</b> sono presenti coupon assegnate a questo utente</p>
                                                            @endif
                                                            <ul>
                                                              @foreach($coupons as $coupon)

                                                              <li>
                                                                  <a href="{{route('coupon edit',['id' => $coupon->id])}}" target="blank"> Coupon: {{$coupon->code}} </a><br>
                                                                  Creata il {{$coupon->created_at}} <br>
                                                                  Ultimo utilizzo: {{$coupon->data_utilizzo}} <br>
                                                                  Credito usato: {{$coupon->spesa}} / {{$coupon->value}} <br>
                                                              </li>
                                                              @endforeach
                                                            </ul>

                                                        </div>
                                                    </div>

                                        </div>

                                      <div id="tasks" class="tab-pane fade">

                                        <div>
                                          <div class="page_content tasklist col-lg-6 pull-left" style="height: 400px;overflow-y: -webkit-paged-y;">
                                              <div class="container-fluid">
                                                  <div class="row">
                                                      <div class="">
                                                          <div class="todo_section panel panel-default">
                                                              <div class="activeTask">
                                                                <div class="todo_date">
                                                                    <h4><span class="big-title">{{ $tasks_totali_cliente }} Task degli utenti totali</span></h4>
                                                                    <div class="pull-right">
                                                                        <span class="label label-success td_resolved_tasks">{{ $task_done }}</span> / <span
                                                                                class="label label-default">{{ $tasks_totali_cliente }}</span>
                                                                    </div>
                                                                </div>
                                                              @foreach($tasks as $task)
                                                                  <ul class="todo_list_wrapper">

                                                                      <li data-task-title="{{ $task->task_note}}" data-task-label="{{ $task->task_note}}"
                                                                          data-task-date="{{ $task->created_at}}">
                                                                          <div class="contentTask">

                                                                              @if($task->user_id)
                                                                                  <span class="label color_e pull-right">{{ \App\Http\Controllers\CategoryController::getUserData($task->user_id) }}</span>
                                                                              @endif


                                                                          <?php if($task->status!="chiuso"){ ?>
                                                                              <span class="label color_{{ \App\Http\Controllers\CategoryController::showCategoryColor($task->category_id) }} pull-right">{{ \App\Http\Controllers\CategoryController::showCategoryName($task->category_id)}}</span>
                                                                              <span>Task creato il: <strong>{{\Carbon\Carbon::parse($task->created_at)->format('d-m-Y H:i:s') }}</strong>
                                                                          <?php } else{ ?>
                                                                              <span class="label pull-right" style="background-color: #64b92a">Completato!</span>
                                                                          <?php } ?>

                                                                              <span> creato da: <strong> {{\App\Http\Controllers\DashboardController::whoHasCreatedTask($task->user_assigned_id)}} </strong></span>
                                                                              <h4 class="todo_title">

                                                                                  <a href="#todo_task_modal_{{ $task->id }}">{{ $task->task_note}}</a>

                                                                              </h4>
                                                                              <span>Il task scade il: <strong> {{\Carbon\Carbon::parse($task->task_deadline)->format('d-m-Y') }}</strong></span>
                                                                              <br>
                                                                          </div>
                                                                          <div class="actionTask">
                                                                          <table>
                                                                          <tr>
                                                                              <td style="display: none;">{{ $task->id }}</td>
                                                                              <td >
                                                                              <a href="#" class="btn btn-xs btn-danger news-zama" style="margin-top: 5px;" onclick="deleteRecord(this)"
                                                                              custom_delete_title="Sei sicuro rimuovere questo task ?"><i class="glyphicon glyphicon-minus" class="disabled"></i>&nbsp;Elimina</a>
                                                                              </td>
                                                                          </tr>
                                                                          </table>
                                                                          </div>
                                                                      </li>
                                                                  </ul>

                                                                  <div class="modal fade" id="todo_task_modal_{{ $task->id }}">
                                                                      <div class="modal-dialog">
                                                                          <div class="modal-content">

                                                                                  <form  id="login_form" role="form" method="POST" action="{{ route('send-mail') }}">

                                                                                  {{ csrf_field() }}
                                                                                  <input type="hidden" name="task_id" value="{{ $task->id}}">
                                                                                  <input type="hidden" name="task_action" value="">

                                                                                  <div class="modal-header">
                                                                                      <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                                                                      <h4 class="modal-title">Hai svolto questo Task?</h4>
                                                                                  </div>
                                                                                  <div class="modal-body">
                                                                                      <div class="form-group">
                                                                                          <div class="row">
                                                                                              <div class="col-sm-12">

                                                                                                  <br>
                                                                                                  <h5>{{ $task->task_note}}</h5>
                                                                                              </div>
                                                                                          </div>
                                                                                      </div>
                                                                                      <div class="form-group">
                                                                                      </div>
                                                                                  </div>
                                                                                  <div class="modal-footer">
                                                                                      <button type="submit" class="btn btn-success">Segna come fatto</button>
                                                                                      <button type="button" class="btn btn-danger" onclick="requestConfirm(this)">Rifiuta task</button>
                                                                                  </div>

                                                                                  </form>
                                                                              </div>
                                                                       </div>
                                                                  </div>

                                                              @endforeach
                                                          </div>


                                                      <div class="completedTask">
                                                              <div class="todo_date">
                                                                  <h4><span class="big-title">{{ $task_done }} Task Completati</span></h4>

                                                              </div>
                                                              @foreach($tasksCompleted as $task)
                                                                  <ul class="todo_list_wrapper">

                                                                      <li data-task-title="{{ $task->task_note}}" data-task-label="{{ $task->task_note}}"
                                                                          data-task-date="{{ $task->created_at}}">
                                                                          <div class="actionTask">
                                                                          <table>
                                                                          <tr>
                                                                              <td style="display: none;">{{ $task->id }}</td>
                                                                              <td ></td>
                                                                          </tr>
                                                                          </table>
                                                                          </div>
                                                                          <div class="contentTask">

                                                                              @if($task->user_id)
                                                                                  <span class="label color_e pull-right">{{ \App\Http\Controllers\CategoryController::getUserData($task->user_id) }}</span>
                                                                              @endif

                                                                              <span>Task creato il: <strong>{{\Carbon\Carbon::parse($task->created_at)->format('d-m-Y H:i:s') }}</strong> </span><span class="label pull-right" style="background-color: #64b92a">Completato!</span>

                                                                              </span>

                                                                              <span> creato da: <strong> {{\App\Http\Controllers\DashboardController::whoHasCreatedTask($task->user_assigned_id)}} </strong></span>
                                                                              <h4 class="todo_title">

                                                                                  <a href="#todo_task_modal_{{ $task->id }}">{{ $task->task_note}}</a>

                                                                              </h4>
                                                                              <span>Il task scade il: <strong> {{\Carbon\Carbon::parse($task->task_deadline)->format('d-m-Y') }}</strong></span>
                                                                              <br>
                                                                          </div>
                                                                      </li>
                                                                  </ul>

                                                                  <div class="modal fade" id="todo_task_modal_{{ $task->id }}">
                                                                      <div class="modal-dialog">
                                                                          <div class="modal-content">

                                                                                  <form  id="login_form" role="form" method="POST" action="{{ route('send-mail') }}">

                                                                                  {{ csrf_field() }}
                                                                                  <input type="hidden" name="task_id" value="{{ $task->id}}">
                                                                                  <input type="hidden" name="task_action" value="">
                                                                                  <div class="modal-header">
                                                                                      <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                                                                      <h4 class="modal-title">Task concluso</h4>
                                                                                  </div>
                                                                                  <div class="modal-body">
                                                                                      <div class="form-group">
                                                                                          <div class="row">
                                                                                              <div class="col-sm-12">

                                                                                                  <br>
                                                                                                  <h5>{{ $task->task_note}}</h5>
                                                                                              </div>
                                                                                          </div>
                                                                                      </div>
                                                                                      <div class="form-group">
                                                                                      </div>
                                                                                  </div>
                                                                                  <div class="modal-footer">

                                                                                  </div>

                                                                                  </form>
                                                                              </div>
                                                                       </div>
                                                                  </div>

                                                              @endforeach
                                                          </div>
                                                      </div>
                                                      </div>
                                                  </div>
                                              </div>
                                          </div>
                                          <div class="col-lg-6 pull-left">
                                            <button class="btn btn-lg btn-success btn-block">
                                              <?php if(Auth::user()->admin){ ?>
                                                  <a id="qui" style="color: white" target="_blank" href="/admin/tasks?action=linkto&ent=customer&n=<?php echo $customer->id ?>">Crea un task</a>
                                              <?php  }else{ ?>
                                                  <a id="qui" style="color: white" target="_blank" href="/user/tasks?action=linkto&ent=customer&n=<?php echo $customer->id ?>">Crea un task</a>
                                              <?php  } ?>
                                            </button>
                                          </div>
                                            <script type="text/javascript">
                                                window['table']="tasks";
                                                window['isntable']="li";

                                                function addUserLine(value,text){
                                                    var newValue=jQuery('#userAssigned').val()+","+value;
                                                    jQuery('#userAssigned').val(newValue);

                                                    var newText=jQuery('#userAssignedText').text()+text+"\n";
                                                    jQuery('#userAssignedText').text(newText);
                                                }

                                                function addUser(el){
                                                    var value=jQuery(el).find('option:selected').val();
                                                    var text=jQuery(el).find('option:selected').text();
                                                    addUserLine(value,text);
                                                }

                                                function applyRep(el){

                                                    var list=jQuery(el).find('option:selected').data().list.split(",");
                                                       jQuery("[name='task_fcfs']").val("1");
                                                        list.map(function(currentValue){

                                                            var value=currentValue;
                                                            var text=jQuery("#user_id option[value='"+value+"']:first").text();
                                                            addUserLine(value,text);
                                                        })
                                                }

                                                $(document).ready(function(){
                                                  $("#qaviaggio input,#qaviaggio select,#qaviaggio textarea").change(function(){
                                                    $("#qaviaggio input,#qaviaggio select,#qaviaggio textarea").removeAttr("old").attr("dirty",true);
                                                  })
                                                    $("#task_deadline").focusout(function(){
                                                        var date=new Date($("#task_deadline").val());
                                                        var week=date.setDate(date.getDate() + 7);
                                                        $('[name="schedulato"]:eq(1)').val(week.getFullYear()+"-"+week.getMonth()+"-"+week.getDate());

                                                        var month=date.setDate(week.getDate() + 21);
                                                        $('[name="schedulato"]:eq(2)').val(month.getFullYear()+"-"+month.getMonth()+"-"+month.getDate());


                                                    })
                                                })

                                                console.error("user/tasks");
                                            </script>
                                        </div>


                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12">
                              <button class="btn btn-lg btn-success btn-block confirmsave">Modifica Cliente</button>
                            </div>
                        </div>



                    </form>

                </div>
            </div>
        </div>
    </div>



@endsection
@section('footer-plugin')
    <!-- datatables -->
    <script src="{{ asset('/lib/DataTables/media/js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('/lib/DataTables/media/js/dataTables.bootstrap.js') }}"></script>
    <script src="{{ asset('/lib/DataTables/extensions/TableTools/js/dataTables.tableTools.min.js') }}"></script>
    <script src="{{ asset('/lib/DataTables/extensions/Scroller/js/dataTables.scroller.min.js') }}"></script>
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script src="{{ asset('/lib/mydestination.js') }}"></script>
    <script src="{{ asset('/js/apps/tisa_datatables.js') }}"></script>
    <script src="{{ asset('/lib/utils.js') }}"></script>

    <script>

    var customerId=<?php echo $customer->id ?>;
    $(document).ready(function(){
      var href=$("#tast_relazione").attr('href');
      $("#tast_relazione").attr('href',href.replace("n=#","n="+customerId));
    })

        var passate=JSON.parse('<?php echo addslashes(json_encode($destinazioni));?>');


            function addBonus(el){

               $("listabuoni").prepend($("templatebuoni").html());
                 $(".bonus.newbonus:eq(0) input").attr("dirty",true);
                 $(".bonus.newbonus:eq(0) input").removeAttr("old");
            }
            function deleteBonus(id,el){


                if(id==""){
                  jQuery(el).parents(".bonus").remove();
                  return;
                }
                $.ajax({
                  type: 'POST',
                  url: apiUrl+'api.php',
                  data: {qr:'removebonus',id:id},
                  el:el,
                  success: function(res){
                    jQuery(this.el).parents(".bonus").remove();
                  },
                  async:false
                });
            }



            var name="<?php echo $name?>";
            function addStatus(){

                      if($("#status option:selected").val()=="")
                        return;
                      $("#notaStato input").val("");
                      $("#notaStato").modal();
            }

            function addStatusStep2(){
                      $(".defaultMsg").remove();
                      var time=new Date();
                        time=time.getDate()+"/"+time.getMonth()+"/"+time.getFullYear();
                      var status=$("#status option:selected").val();
                        if($('[name="notaStato"]').val()!="")
                          var nota="<p>Con la seguente nota: "+$('[name="notaStato"]').val()+"</p>";
                        else
                          var nota="";
                      var row="<p>l'utente <strong>"+name+"</strong> il <strong>"+time+"</strong> ha aggiornato lo stati in: <br><strong>"+status+"</strong>"+nota+"</p>";
                      $("listaStati").prepend(row);
                      $("listaStati").append("<input type='hidden' id='statiStato[]' name='statiStato[]' value='"+status+"'>");
                      $("listaStati").append("<input type='hidden' id='statiNota[]' name='statiNota[]' value='"+$('[name="notaStato"]').val()+"'>");
            }

            var path = "{{ route('autocomplete') }}";
            var engine = new Bloodhound({
                datumTokenizer: Bloodhound.tokenizers.whitespace('username'),
                queryTokenizer: Bloodhound.tokenizers.whitespace,
                remote:{
                    url: path + '?term=%QUERY',
                    wildcard: '%QUERY'
                }
            });

            engine.initialize();

            $("#citta").typeahead({
                hint: true,
                highlight: true,
                minLength: 2
            }, {
                source: engine.ttAdapter(),
                limit:1000,
                name: 'comune',
                displayKey: 'comune',
                templates: {
                    empty: [
                        '<div class="empty-message">Nessun risultato</div>'
                    ].join('\n'),

                    suggestion: function (data) {
                        return '<div class="citta-search-result"><h4>'+ data.comune +'</h4></div>'
                    }
                },

            });
            $('#citta').bind('typeahead:selected', function(obj, datum, name) {
                 var cap=datum.cap.split("-");
                if(cap.length>1)
                    cap=cap[0];
                else
                    cap=datum.cap;
                $("#provincia").val(datum.provincia);
                $("#provincia").trigger("change");
                $("#cap").val(cap);
                $("#cap").trigger("change");
                $("#regione").val(datum.regione);
                $("#regione").trigger("change");
                $("#stato").val(datum.stato);
                $("#stato").trigger("change");

            })

            $("#info-citta2").typeahead({
                hint: true,
                highlight: true,
                minLength: 2
            }, {
                source: engine.ttAdapter(),
                // This will be appended to "tt-dataset-" to form the class name of the suggestion menu.
                name: 'comune',
                // the key from the array we want to display (name,id,email,etc...)
                displayKey: 'comune',
                templates: {
                    empty: [
                        '<div class="empty-message">Nessun risultato</div>'
                    ].join('\n'),

                    suggestion: function (data) {
                        return '<div class="citta2-search-result"><h4>'+ data.comune +'</h4></div>'
                    }
                },

            });


          console.error("admin/edit-customer");


        $(document).ready(function () {

          $("#info-citta2").bind('typeahead:selected', function(obj, datum, name) {
              $("#info-provincia2").val(datum.provincia);
              $("#info-cap2").val(datum.cap);
              $("#info-regione2").val(datum.regione);
              $("#info-stato2").val(datum.stato);
            });

          $('input').css('text-transform','uppercase');

          $("form").on("submit",function(){
              $("[old]").remove();
              var data=htmlToStruct("listabuoni:eq(0) .newbonus");
              $("#bonus").remove();
              $.ajax({
                type: 'POST',
                url: apiUrl+'api.php',
                data: {qr:'addbonus',customerId:customerId,data:data},
                success: function(res){

                },
                async:false
              });

          });

            $("oldbonus input").change(function(){
                $(this).parents(".oldbonus").removeClass("oldbonus").addClass("newbonus");
            })

          $("select,textarea").attr("old",true);
          $("input[name!='_token'][name!='prev_id']").attr("old",true);
          $("select,input,textarea").change(function(){
            $(this).attr("dirty",true);
            $(this).removeAttr("old");
          });




            if(window.location.search.includes("view")){
              $("input,select,textarea").attr("readonly","true");
              $("input[type='checkbox']").attr("disabled","true");
              $(".btn.btn-lg.btn-success.btn-block").hide();
              $("#editScheda").show();
              $(".applyStatus").hide();
              $("#bonus .btn").hide();
            }

          });

            function validateFs(el){
                if(el.value=="")
                  return;
                if(el.value.length<16)
                    alert("Inserisci un codice fiscale valido");
            }



  var destinazioniPreferite=JSON.parse("<?php echo addslashes(json_encode($destinazioniPreferite)) ?>");
  var destinazioniPassate=JSON.parse("<?php echo addslashes(json_encode($destinazioniPassate)) ?>");
  var destinazioniProssime=JSON.parse("<?php echo addslashes(json_encode($destinazioniProssime)) ?>");
  var destinazioniDove=JSON.parse("<?php echo addslashes(json_encode($destinazioniDove)) ?>");


  var md1=new mydestination("#preferite",'passate',
    {
      load_source: destinazioniPreferite,
      variable_name: 'passate',
      resultcontainer:'res_destinazionipreferite',
      hiddenname:' destinazionipreferite[]',
      table_name: 'destinazioniPreferite',
      label: 'Destinazioni preferite'
    }
  );

  var md2=new mydestination("#passate",'passate',
    {
      load_source: destinazioniPassate,
      variable_name: 'passate',
      resultcontainer:'res_destinazionipassate',
      hiddenname:' destinazionipassate[]',
      table_name: 'destinazioniPassate',
      label: 'Destinazioni passate'
    }
  );

  var md3=new mydestination("#prossime",'passate',
    {
      load_source: destinazioniProssime,
      variable_name: 'passate',
      resultcontainer:'res_destinazioniprossime',
      hiddenname:' destinazioniprossime[]',
      table_name: 'destinazioniProssime',
      label: 'Destinazioni prossime'
    }
  );

  var md4=new mydestination("#qasuend_doveviaggi",'passate',
    {
      load_source: destinazioniDove,
      variable_name: 'passate',
      table_name: 'destinazioniDove',
      resultcontainer:'res_destinazioniDove',
      hiddenname:' destinazioniDove[]',
      label: 'Dove'
    }
  );

function just_remove_container(el){
  var data=$(el).data();
  $(el).parents(data.parent).hide(1000);
  $(el).parents(data.parent).remove();
    $.post(apiUrl+'api.php',data,function(response){
    debugger;
  });

}

</script>



@endsection
