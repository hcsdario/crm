@extends('layouts.master')
@section('header-page-personalization')
    <style>
        .twitter-typeahead, .tt-hint, .tt-input, .tt-menu { width: 100%;  }
        .tt-menu {
            background: #ffffff;
        }
        [readonly]{
          background-color: white; 
          opacity: 1;         
        }
    </style>
@endsection
@section('content')
    <div class="page_content page_adv_edit">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">

                    <form id="login_form" role="form" method="POST" action="{{ route('customer_adv.store') }}" class="customForm">
                    {{ csrf_field() }}
 
                        <div class="panel panel-default">
                            <div class="panel-body">
                                <div class="heading_a"><legend><span class="big-title">MODIFICA ADV ESTERA</span></legend></div>
                                <div class="tabbable tabs-left">
                                    <ul class="nav nav-tabs" id="tabs_c">
                                        <li class="active"><a data-toggle="tab" href="#info">Info</a></li>
                                        <li><a data-toggle="tab" href="#infosdv">Info ADV</a></li>
                                        <li id="editScheda" style="display: none;">
                                            <a href="/admin/adv/edit/{{isset($customer->id) ? $customer->id : ''}}"><i class="glyphicon glyphicon-edit"></i>&nbsp;Modifica</a>
                                        </li>                                        
                                    </ul>
                                    <div class="tab-content" id="tabs_content_c">
                                        <div id="info" class="tab-pane fade in active">
                                            <div class="row">
                                                <div class="col-lg-10">
                                                <input type="hidden" name="id"  value="{{ isset($customer->id) ? $customer->id : ''  }}">
                                                    <div class="form-group">
                                                        <label for="reg_input">Agenzia</label>
                                                        <input type="text" id="nome" name="nome" class="form-control" tabindex="2" value="{{ isset($customer->nome) ? $customer->nome : ''  }}">
                                                    </div>

                                                    <div class="form-group">
                                                        <label for="reg_input">Ragione sociale</label>
                                                        <input type="text" id="societa" name="societa" class="form-control" tabindex="2" value="{{ isset($customer->societa) ? $customer->societa : ''  }}">
                                                    </div>


                                                    <div class="form-group">
                                                        <label for="reg_input">Partita iva/ Codice Fiscale</label>
                                                        <input type="text" id="info_codice_fiscale" name="info_codice_fiscale"
                                                               class="form-control" tabindex="15" value="{{ isset($info->codice_fiscale) ? $info->codice_fiscale : ''  }}">
                                                    </div>


                                                        <input type="hidden" id="italiana_o_estera" name="italiana_o_estera" 
                                                        value="<?php 
                                                                if(isset($_GET) && isset($_GET['adv'])&&isset($_GET['?adv=ext'])) 
                                                                    echo 1; 
                                                                else echo 0; 
                                                            ?>">

                                                    <div class="form-group">
                                                        <label for="reg_input">Licenza</label>
                                                        <input type="text" id="cognome" name="cognome"  value="{{ isset($customer->cognome) ? $customer->cognome : ''  }}"
                                                               class="form-control" tabindex="5">
                                                    </div>

                                                    <div class="form-group">
                                                        <label for="reg_input">Indirizzo</label>
                                                        <input type="text" id="info_indirizzo" name="info_indirizzo"  value="{{ isset($customer->id) ? $customer->id : ''  }}"
                                                               class="form-control" tabindex="5">
                                                    </div>

                                                    <div class="form-group">
                                                        <label for="reg_input">Citta'</label>
                                                        <input type="text" id="citta" name="citta" class="form-control" tabindex="6" placeholder="Inserisci citta"  value="{{ isset($customer->citta) ? $customer->citta : ''  }}">
                                                    </div>

                                                    <div class="form-group">
                                                        <label for="reg_input">Cap</label>
                                                        <input type="text" id="cap" name="cap" class="form-control" tabindex="7"  value="{{ isset($customer->cap) ? $customer->cap : ''  }}">
                                                    </div>

                                                    <div class="form-group">
                                                        <label for="reg_input">Provincia</label>
                                                        <input type="text" id="provincia" name="provincia" class="form-control" tabindex="8" value="{{ isset($customer->provincia) ? $customer->provincia : ''  }}">
                                                    </div>

                                                    <div class="form-group">
                                                        <label for="reg_input">Regione</label>
                                                        <input type="text" id="regione" name="regione" class="form-control" tabindex="9" value="{{ isset($customer->regione) ? $customer->regione : ''  }}">

                                                    </div>

                                                    <div class="form-group">
                                                        <label for="reg_input">Stato</label>
                                                        <input type="text" id="stato" name="stato" class="form-control" value="{{ isset($customer->stato) ? $customer->stato : ''  }}"
                                                               tabindex="10">
                                                    </div>

                                                    <div class="form-group">
                                                        <label for="reg_input">Telefono principale</label>
                                                        <input type="text" id="tel1" name="tel1" class="form-control" tabindex="11" value="{{ isset($customer->tel1) ? $customer->tel1 : ''  }}">
                                                    </div>   

                                                    <div class="form-group">
                                                        <label for="reg_input">Fax</label>
                                                        <input type="text" id="fax" name="fax" class="form-control" tabindex="11" value="{{ isset($customer->fax) ? $customer->fax : ''  }}">
                                                    </div>
                                                    
                                                    <div class="form-group">
                                                        <label for="reg_input">Referente</label>
                                                        <input type="text" id="referente" name="referente" class="form-control" tabindex="11" value="{{ isset($customer->referente) ? $customer->referente : ''  }}">
                                                    </div>
                                                    
                                                    <div class="form-group">
                                                        <label for="reg_input">Ruolo</label>
                                                        <input type="text" id="ruolo" name="ruolo" class="form-control" tabindex="11" value="{{ isset($customer->ruolo) ? $customer->ruolo : ''  }}">
                                                    </div>

                                                    <div class="form-group">
                                                        <label for="reg_input">Cellulare</label>
                                                        <input type="text" id="tel2" name="tel2" class="form-control" tabindex="12" value="{{ isset($customer->tel2) ? $customer->tel2 : ''  }}">
                                                    </div>

                                                    <div class="form-group">
                                                        <label for="reg_input">Email</label>
                                                        <input type="text" id="email1" name="email1" class="form-control"
                                                               tabindex="13" value="{{ isset($customer->email1) ? $customer->email1 : ''  }}">
                                                    </div>


                                                </div>
                                            </div>

                                        </div>
                                        <div id="infosdv" class="tab-pane fade">
                                            <div class="row">
                                                <div class="col-lg-12">
                                                    <div class="form-group">
                                                        <label for="reg_input">Fatturato medio Annuo</label>
                                                        <input type="text" id="infoadv_fatturato"
                                                               name="infoadv_fatturato"
                                                               class="form-control" value="{{ isset($info_adv->fatturato_medio) ? $info_adv->fatturato_medio : ''  }}">
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="reg_input">Numero addetti</label>
                                                        <input type="text" id="infoadv_addetti" name="infoadv_addetti"
                                                               class="form-control" value="{{ isset($info_adv->numero_addetti) ? $info_adv->numero_addetti : ''  }}">
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="reg_input">Passeggeri trasportati/anno</label>
                                                        <input type="text" id="infoadv_clienti" name="infoadv_clienti"
                                                               class="form-control" value="{{ isset($info_adv->clienti) ? $info_adv->clienti : ''  }}">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-lg-12">
                                                    <div class="col-lg-3">
                                                        <div class="form-group">
                                                            <label for="reg_input">Destinazioni più vendute</label>
                                                            <input type="text" id="infoadv_destinazione1"
                                                                   name="infoadv_destinazione1"
                                                                   class="form-control" value="{{ isset($info_adv->destinazioni_vendute1) ? $info_adv->destinazioni_vendute1 : ''  }}">
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-3">
                                                        <div class="form-group">
                                                            <label for="reg_input">&nbsp;</label>
                                                            <input type="text" id="infoadv_destinazione2"
                                                                   name="infoadv_destinazione2"
                                                                   class="form-control" value="{{ isset($info_adv->destinazioni_vendute2) ? $info_adv->destinazioni_vendute2 : ''  }}">
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-3">
                                                        <div class="form-group">
                                                            <label for="reg_input">&nbsp;</label>
                                                            <input type="text" id="infoadv_destinazione3"
                                                                   name="infoadv_destinazione3"
                                                                   class="form-control" value="{{ isset($info_adv->destinazioni_vendute3) ? $info_adv->destinazioni_vendute3 : ''  }}">
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-3">
                                                        <div class="form-group">
                                                            <label for="reg_input">&nbsp;</label>
                                                            <input type="text" id="infoadv_destinazione4"
                                                                   name="infoadv_destinazione4"
                                                                   class="form-control" value="{{ isset($info_adv->destinazioni_vendute4) ? $info_adv->destinazioni_vendute4 : ''  }}">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-lg-12">
                                                    <div class="col-lg-3">
                                                        <div class="form-group">
                                                            <label for="reg_input">&nbsp;</label>
                                                            <input type="text" id="infoadv_destinazione5" value="{{ isset($info_adv->destinazioni_vendute5) ? $info_adv->destinazioni_vendute5 : ''  }}"
                                                                   name="infoadv_destinazione5"
                                                                   class="form-control">
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-3">
                                                        <div class="form-group">
                                                            <label for="reg_input">&nbsp;</label>
                                                            <input type="text" id="infoadv_destinazione6" value="{{ isset($info_adv->destinazioni_vendute6) ? $info_adv->destinazioni_vendute6 : ''  }}"
                                                                   name="infoadv_destinazione6"
                                                                   class="form-control">
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-3">
                                                        <div class="form-group">
                                                            <label for="reg_input">&nbsp;</label>
                                                            <input type="text" id="infoadv_destinazione7" value="{{ isset($info_adv->destinazioni_vendute7) ? $info_adv->destinazioni_vendute7 : ''  }}"
                                                                   name="infoadv_destinazione7"
                                                                   class="form-control">
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-3">
                                                        <div class="form-group">
                                                            <label for="reg_input">&nbsp;</label>
                                                            <input type="text" id="infoadv_destinazione8" value="{{ isset($info_adv->destinazioni_vendute8) ? $info_adv->destinazioni_vendute8 : ''  }}"
                                                                   name="infoadv_destinazione8"
                                                                   class="form-control">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-lg-12">

                                                    <div class="form-group">
                                                        <label for="reg_input">Fatturato Business Travel</label>
                                                        <input type="text" id="infoadv_businesstravel" name="infoadv_businesstravel" value="{{ isset($info_adv->business_travel) ? $info_adv->business_travel : ''  }}"
                                                               class="form-control">
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="reg_input">Leisure </label>
                                                        <input type="text" id="infoadv_leisure" name="infoadv_leisure" value="{{ isset($info_adv->leisure) ? $info_adv->leisure : ''  }}"
                                                               class="form-control">
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="reg_input">Biglietteria </label>
                                                        <input type="text" id="infoadv_biglietteria" name="infoadv_biglietteria" value="{{ isset($info_adv->biglietteria) ? $info_adv->biglietteria : ''  }}"
                                                               class="form-control">
                                                    </div>

                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                                <button class="btn btn-lg btn-success btn-block confirmsave">MODIFICA ADV</button>
                            </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

@endsection
@section('footer-plugin')
    <script src="{{ asset('/lib/bootstrap-datepicker/js/bootstrap-datepicker.js') }}"></script>



    <script type="text/javascript">
        $(document).ready(function () {

            if(window.location.search.includes("view")){
                debugger;
              $("input,select,textarea").attr("readonly","true");
              $("input[type='checkbox']").attr("disabled","true");
              $(".btn.btn-lg.btn-success.btn-block").hide();
              $("#editScheda").show();
            }


            var defaultText = '';

            var path = "{{ route('autocomplete') }}";
            var engine = new Bloodhound({
                datumTokenizer: Bloodhound.tokenizers.whitespace('username'),
                queryTokenizer: Bloodhound.tokenizers.whitespace,
                remote:{
                    url: path + '?term=%QUERY',
                    wildcard: '%QUERY'
                }
            });

            engine.initialize();

            $("#citta").typeahead({
                hint: true,
                highlight: true,
                minLength: 2
            }, {
                source: engine.ttAdapter(),
                // This will be appended to "tt-dataset-" to form the class name of the suggestion menu.
                name: 'comune',
                // the key from the array we want to display (name,id,email,etc...)
                displayKey: 'comune',
                templates: {
                    empty: [
                        '<div class="empty-message">Nessun risultato</div>'
                    ].join('\n'),

                    suggestion: function (data) {
                        return '<div class="citta-search-result"><h4>'+ data.comune +'</h4></div>'
                    }
                },

            });
            $('#citta').bind('typeahead:selected', function(obj, datum, name) {

                $("#provincia").val(datum.provincia);
                $("#cap").val(datum.cap);
                $("#regione").val(datum.regione);
                $("#stato").val(datum.stato);
            });



            $("#info-citta2").typeahead({
                hint: true,
                highlight: true,
                minLength: 2
            }, {
                source: engine.ttAdapter(),
                // This will be appended to "tt-dataset-" to form the class name of the suggestion menu.
                name: 'comune',
                // the key from the array we want to display (name,id,email,etc...)
                displayKey: 'comune',
                templates: {
                    empty: [
                        '<div class="empty-message">Nessun risultato</div>'
                    ].join('\n'),

                    suggestion: function (data) {
                        return '<div class="citta2-search-result"><h4>'+ data.comune +'</h4></div>'
                    }
                },

            });

            $("#info-citta2").bind('typeahead:selected', function(obj, datum, name) {

                $("#info-provincia2").val(datum.provincia);
                $("#info-cap2").val(datum.cap);
                $("#info-regione2").val(datum.regione);
                $("#info-stato2").val(datum.stato);
            });
        });
      window['table']="customers";
              $(document).ready(function(){
                $('input').css('text-transform','uppercase');
                var navbar=$("#side_nav").detach();
                $("body").append(navbar);
            })

    console.error("admin/edit-adv");
    </script>
@endsection
