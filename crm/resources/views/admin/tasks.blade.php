@extends('layouts.master')
@section('header-page-personalization')
    <!-- page specific stylesheets -->
    <?php  $userid=Auth::user()->id; ?>

    <!-- iCheck -->
    <link rel="stylesheet" href="{{ asset('/lib/iCheck/skins/minimal/green.css') }}">
    <!-- bootstrap-datepicker -->
    <link rel="stylesheet" href="{{ asset('/lib/bootstrap-datepicker/css/datepicker3.css') }}">

    <!-- main stylesheet -->
    <link href="{{ asset('/css/style.css') }}" rel="stylesheet" media="screen">

    <!-- google webfonts -->
    <link href='https://fonts.googleapis.com/css?family=Source+Sans+Pro:400&amp;subset=latin-ext,latin' rel='stylesheet'
          type='text/css'>

    <!-- moment.js (date library) -->
    <script src="{{ asset('/lib/moment-js/moment.min.js') }}"></script>
    <style type="text/css">

        .todo_section .todo_list_wrapper li {
            display: inline-flex;
            list-style: none;
            padding: 14px 10px;
            width: 100%;
        }

        .actionTask {
            float: left;
            width: 100px!important;
            height: 100px;
            display: table-cell;
        }

        .contentTask {
            clear: left;
            float: left;
            width: 100%;
            display: table-cell;
        }
        [for="radios-2"]{
           margin-bottom: 5px;
        }

    </style>
@endsection
@section('content')
    <div class="page_content page_tasks col-lg-6 pull-left">
        <div class="container-fluid">
            <div class="row">
                <div class="">
                    <div class="panel panel-default">
                        <div class="panel-body">
                            <fieldset>
                                <legend><span class="big-title">Nuovo Task {{ $msg_link_to }}</span></legend>
                                <secret>
                                <p>
                                    <?php echo date("Y m d H:i:s"); ?>
                                </p>
                                <p>
                                    <?php print_r(new Datetime()); ?>
                                </p>
                                </secret>
                            </fieldset>
                            @if(session()->has('message'))
                                <div class="alert alert-success">
                                    {{ session()->get('message') }}
                                </div>
                            @endif
                            <form id="login_form" role="form" method="POST" action="{{ route('tasks.store') }}">
                                {{ csrf_field() }}
                                <div class="row">
                                    <div class="col-lg-12 formLeft">
                                    <input type="hidden" name="task_fcfs" value="0">
                                    <div class="col-lg-12">
                                        <div class="form-group">
                                            <label for="reg_input">Nota del task</label>
                                            <input type="text" id="task_note" name="task_note" class="form-control" required value="{{  $link_to }}">
                                        </div>
                                    </div>
                                    <div class="col-lg-12">
                                        <div class="form-group">
                                            <label for="reg_input">Data inizio Task</label>
                                            <input class="form-control ts_datepicker" type="date" name="startDate"
                                                   id="task_deadline" required value="<?php echo date('Y-m-d');?>">
                                        </div>
                                    </div>

                                    <div class="col-lg-12">
                                        <div class="form-group">
                                            <label for="reg_input">Data Scadenza Task</label>
                                            <input class="form-control ts_datepicker" type="date" name="task_deadline" id="task_deadline" required value="<?php echo date("Y-m-d", strtotime("+1 day"));?>">
                                        </div>
                                    </div>


                                    <div class="col-lg-12" style="padding-left: 0px;">
                                        <div class="form-group">
                                            <div class="form-group">
                                                <label class="col-md-4 control-label emaillabel" for="radios">Imposta ripetizione email</label>
                                                <div class="col-md-12 ripemail">
                                                    <div class="radio" style="padding-left: 0px;">
                                                        <label for="radios-3" class="col-md-12" style="padding-left: 0px;margin-bottom: 5px;">
                                                            <input id="radios-3" type="radio" onclick="toggle_checked(this)" name="schedulato_lv_1" value="0000-00-00" checked="checked"> Solo il giorno prima della scadenza
                                                        </label>
                                                        <label for="radios-2" class="col-md-12" style="padding-left: 0px;margin-bottom: 5px;">
                                                            <input id="radios-2" type="radio" onclick="toggle_checked(this)" name="schedulato_lv_0" value="0000-00-00"> Notifica ogni giorno
                                                        </label>
                                                        <label for="radios-0" class="col-md-12" style="padding-left: 0px;margin-bottom: 5px;">
                                                            <input id="radios-0" type="radio" onclick="toggle_checked(this)" name="schedulato_lv_2" value="0000-00-00"> Notifica ogni settimana
                                                        </label>
                                                        <label for="radios-1" class="col-md-12" style="padding-left: 0px;margin-bottom: 5px;">
                                                            <input id="radios-1" type="radio" onclick="toggle_checked(this)" name="schedulato_lv_3" value="0000-00-00"> Notifica ogni mese
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>



                                    <div class="col-lg-12">
                                        <div class="form-group">
                                            <label for="reg_input">Categoria</label>
                                            <select id="category_id" name="category_id" class="form-control">
                                                @foreach ($categories as $category)
                                                    <option value="{{ $category->id }}">{{ $category->category_name }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-lg-12">
                                        <div class="col-lg-12" style="padding-left: 0;">
                                            <div class="form-group">
                                                <label for="user_id">Incarica uno o più Utenti di eseguire il task</label>
                                                <select class="rq form-control" id="user_id" name="user_id" class="form-control" onchange="addUser(this)" required="">
                                                    <option selected="" disabled="" value="">Scegli un utente</option>
                                                    @foreach ($users as $user)
                                                        <option value="{{ $user->id }}">{{ $user->name . ' ' . $user->surname }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-lg-12" style="padding-left: 0px;">
                                            <div class="form-group">
                                                <label for="user_id_list">Incarica un reparto di eseguire il task</label>
                                                <select class="rq form-control" id="user_id_list" name="user_id_list" class="form-control" onchange="applyRep(this)" required="">
                                                <option selected  value="">Scegli un reparto</option>
                                                    @foreach ($reparti as $reparto)
                                                        <option value="{{ $reparto->id }}" data-list="{{$reparto->userIdList}}">{{ $reparto->nome}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-12">
                                        <div class="col-lg-12" style="padding-left: 0;">
                                            <div class="form-group">
                                                <label for="user_id">Incarica un Utente di supervisionare il task</label>
                                                <select id="task_supervisor" name="task_supervisor" class="form-control">
                                                    <option selected="" disabled="" value="">Scegli un utente</option>
                                                    @foreach ($users as $user)
                                                        <option value="{{ $user->id }}">{{ $user->name . ' ' . $user->surname }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                    </div>

                                </div>

                                <!-- form left section -->
                                 <div class="col-lg-4 formRight">
                                    <div class="form-group">
                                        <label for="reg_input">Utenti assegnati al task</label>
                                        <input type="hidden" id="userAssigned" name="userAssigned">
                                        <?php if(isset($_GET['ent'])){ ?>

                                            <input type="hidden" id="collegato_a" name="collegato_a" value="<?php echo $_GET['ent'] ?>">
                                            <input type="hidden" id="entity_id" name="entity_id" value="<?php echo $_GET['n'] ?>">
                                        <?php } ?>
                                        <textarea id="userAssignedText" class="form-control " rows="7"></textarea>
                                    </div>
                                 </div>

                                </div>
                                <button class="btn btn-lg btn-success btn-block confirmsave">Aggiungi Task</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="page_content tasklist col-lg-6 pull-left">
        <div class="container-fluid">
            <div class="row">
                <div class="">
                    <div class="todo_section panel panel-default">
                        <div class="activeTask">
                        <div class="todo_date">
                            <h4 class="big-title">{{ $task_open }} Task degli utenti totali</h4>
                            <div class="pull-right">
                                <span class="label label-success td_resolved_tasks">{{ $task_done }}</span> / <span
                                        class="label label-default">{{ $task_total }}</span>
                            </div>
                            <div class="form-group">
                              <label for="task-search-current"><i class="fa fa-search"></i> Filtra Task</label>
                              <input type="text" class="form-control" id="task-search-current" aria-describedby="task-search-current" placeholder="Filtra Task">
                            </div>
                        </div>
                        <div id="task-search-list-current">
                        @foreach($tasks as $task)
                            <ul class="todo_list_wrapper task-item" data-task-id="{{$task->id}}" data-task-title="{{ $task->task_note}}" data-task-label="{{ $task->task_note}}"
                              data-task-user="{{ \App\Http\Controllers\CategoryController::getUserData($task->user_id) }}"
                              data-task-category="{{ \App\Http\Controllers\CategoryController::showCategoryName($task->category_id)}}"
                                data-task-created_at="{{\Carbon\Carbon::parse($task->created_at)->format('d-m-Y H:i:s') }}"
                                data-task-task_deadline="{{\Carbon\Carbon::parse($task->task_deadline)->format('d-m-Y') }}"
                                data-task-user_assigned = " {{\App\Http\Controllers\DashboardController::whoHasCreatedTask($task->user_assigned_id)}}" >

                                <li data-task-id="{{$task->id}}" data-task-title="{{ $task->task_note}}" data-task-label="{{ $task->task_note}}"
                                    data-task-created_at="{{\Carbon\Carbon::parse($task->created_at)->format('d-m-Y H:i:s') }}"
                                    >

                                    <div class="contentTask">
                                        @if($task->user_id)
                                            <span class="label color_e pull-right">{{ \App\Http\Controllers\CategoryController::getUserData($task->user_id) }}</span>
                                        @endif
                                    <?php if($task->status!="chiuso"){ ?>
                                        <span class="label color_{{ \App\Http\Controllers\CategoryController::showCategoryColor($task->category_id) }} pull-right">{{ \App\Http\Controllers\CategoryController::showCategoryName($task->category_id)}}</span>

                                        <span>Task creato il: <strong>{{\Carbon\Carbon::parse($task->created_at)->format('d-m-Y H:i:s') }}</strong>
                                    <?php } else{ ?>
                                        <span class="label pull-right" style="background-color: #64b92a">Completato!</span>
                                    <?php } ?>
                                        <span> creato da: <strong> {{\App\Http\Controllers\DashboardController::whoHasCreatedTask($task->user_assigned_id)}} </strong></span>
                                        <h4 class="todo_title">
                                            <a href="#todo_task_modal_{{ $task->id }}">{{ $task->task_note}}</a>
                                        </h4>
                                        <span>Il task scade il: <strong> {{\Carbon\Carbon::parse($task->task_deadline)->format('d-m-Y') }}</strong></span>
                                        <br>
                                    </div>
                                    <div class="actionTask">
                                    <table>
                                    <tr>
                                        <td style="display: none;">{{ $task->id }}</td>
                                        <td >
                                        <a href="#" class="btn btn-xs btn-danger news-zama" style="margin-top: 5px;"                                         custom_delete_title="Sei sicuro rimuovere questo task ?" onclick="deleteRecord(this)"><i class="glyphicon glyphicon-minus" class="disabled"></i>&nbsp;Elimina</a>
                                        </td>
                                    </tr>
                                    </table>
                                    </div>
                                </li>
                            </ul>


                            <div class="modal fade taskpopup" id="todo_task_modal_{{ $task->id }}" data-id="{{ $task->id }}">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <form  id="login_form" role="form" method="POST" action="{{ route('send-mail') }}">
                                            {{ csrf_field() }}
                                            <input type="hidden" name="task_id" value="{{ $task->id}}">
                                            <input type="hidden" name="task_action" value="">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                                <h4 class="modal-title">Dettagli task</h4>
                                            </div>
                                            <div class="modal-body">
                                                <div class="form-group">
                                                    <div class="row">
                                                        <div class="col-sm-12">
                                                            <h5 class="taskNote">{{ $task->task_note}}</h5>
                                                        </div>
                                                        <notelist class="col-sm-12">

                                                        <p>Note:</p>
                                                        <ul>
                                                            <li class="default">
                                                                <p>Nessuna nota</p>
                                                            </li>
                                                        </ul>
                                                         </notelist>
                                                        <noteform class="col-sm-12">
                                                                <div>

                                                                <h4>
                                                                    <a id="addnote" href="#" class="addnote btn btn-xs btn-primary news-zama" onclick="addnote(this,{{ $task->id}})">
                                                                        <i class="glyphicon glyphicon-plus"></i>&nbsp;Aggiungi nota
                                                                    </a>
                                                                    <a style="display: none;" id="savenote" class="savenote btn btn-success news-zama" href="#" onclick="savenote(this,{{ $task->id}})">
                                                                        <i class="glyphicon glyphicon-check"></i>&nbsp;Salva nota
                                                                    </a>
                                                                    <!-- glyphicon-floppy-disk -->
                                                                </h4>
                                                                </div>

                                                        </noteform>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                </div>
                                            </div>
                                            <div class="modal-footer">
                                            <button type="button" class="btn btn-success" onclick="requestConfirm(this,'Confermi che il compito di questo task è stato svolto?')">Segnala il task come concluso</button>
                                                @if(Auth::user()->admin == 1)
                                                   <button type="button" class="btn btn-danger" onclick="requestConfirm(this)">Rifiuta task</button>
                                                @endif
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>

                        @endforeach
                    </div>


                <div class="completedTask">
                        <div class="todo_date">
                            <h4 class="big-title">{{ $task_done }} Task Completati </h4>
                            <div class="form-group">
                              <label for="task-search-current"><i class="fa fa-search"></i> Filtra Task</label>
                              <input type="text" class="form-control" id="task-search-completed" aria-describedby="task-search-current" placeholder="Filtra Task">
                            </div>
                        </div>
                        <div id="task-list-completed">
                        @foreach($tasksCompleted->slice(0,400) as $task)
                            <ul class="todo_list_wrapper task-item" data-task-id="{{$task->id}}" data-task-title="{{ $task->task_note}}" data-task-label="{{ $task->task_note}}"
                              data-task-user="{{ \App\Http\Controllers\CategoryController::getUserData($task->user_id) }}"
                              data-task-category="{{ \App\Http\Controllers\CategoryController::showCategoryName($task->category_id)}}"
                                data-task-created_at="{{\Carbon\Carbon::parse($task->created_at)->format('d-m-Y H:i:s') }}"
                                data-task-task_deadline="{{\Carbon\Carbon::parse($task->task_deadline)->format('d-m-Y') }}"
                                data-task-user_assigned = " {{\App\Http\Controllers\DashboardController::whoHasCreatedTask($task->user_assigned_id)}}"  >

                                <li data-task-title="{{ $task->task_note}}" data-task-label="{{ $task->task_note}}"
                                    data-task-date="{{ $task->created_at}}">
                                    <div class="actionTask">
                                    <table>
                                    <tr>
                                        <td style="display: none;">{{ $task->id }}</td>
                                        <td ></td>
                                    </tr>
                                    </table>
                                    </div>
                                    <div class="contentTask">

                                        @if($task->user_id)
                                            <span class="label color_e pull-right">{{ \App\Http\Controllers\CategoryController::getUserData($task->user_id) }}</span>
                                        @endif

                                        <span>Task creato il: <strong>{{\Carbon\Carbon::parse($task->created_at)->format('d-m-Y H:i:s') }}</strong> </span><span class="label pull-right" style="background-color: #64b92a">Completato!</span>

                                        </span>

                                        <span> creato da: <strong> {{\App\Http\Controllers\DashboardController::whoHasCreatedTask($task->user_assigned_id)}} </strong></span>
                                        <h4 class="todo_title">

                                            <a href="#todo_task_modal_{{ $task->id }}">{{ $task->task_note}}</a>

                                        </h4>
                                        <span>Il task scade il: <strong> {{\Carbon\Carbon::parse($task->task_deadline)->format('d-m-Y') }}</strong></span>
                                        <br>
                                    </div>
                                </li>
                            </ul>

                            <div class="modal fade" id="todo_task_modal_{{ $task->id }}">
                                <div class="modal-dialog">
                                    <div class="modal-content">

                                            <form  id="login_form" role="form" method="POST" action="{{ route('send-mail') }}">

                                            {{ csrf_field() }}
                                            <input type="hidden" name="task_id" value="{{ $task->id}}">
                                            <input type="hidden" name="task_action" value="">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                                <h4 class="modal-title">Task concluso</h4>
                                            </div>
                                            <div class="modal-body">
                                                <div class="form-group">
                                                    <div class="row">
                                                        <div class="col-sm-12">

                                                            <br>
                                                            <h5>{{ $task->task_note}}</h5>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                </div>
                                            </div>
                                            <div class="modal-footer">

                                            </div>

                                            </form>
                                        </div>
                                 </div>
                            </div>

                        @endforeach
                    </div>
                </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('footer-plugin')
    <!--  datepicker -->

    <!-- date range picker -->

    <!-- textarea autosize -->
    <script src="{{ asset('/lib/autosize/jquery.autosize.min.js') }}"></script>
    <!-- iCheck -->
    <script src="{{ asset('/lib/iCheck/jquery.icheck.min.js') }}"></script>
    <!-- clndr -->
    <script src="{{ asset('/lib/underscore-js/underscore-min.js') }}"></script>
    <script src="{{ asset('/lib/CLNDR/src/clndr.js') }}"></script>
    <!-- flot charts-->
    <script src="{{ asset('/lib/flot/jquery.flot.min.js') }}"></script>
    <script src="{{ asset('/lib/flot/jquery.flot.pie.min.js') }}"></script>
    <script src="{{ asset('/lib/flot/jquery.flot.resize.min.js') }}"></script>
    <script src="{{ asset('/lib/utils.js') }}"></script>

    <!-- masked inputs -->
    <script src="{{ asset('/lib/jquery.inputmask/dist/jquery.inputmask.bundle.min.js') }}"></script>

    <!-- todo functions -->
    <script src="{{ asset('/js/apps/tisa_todo.js') }}"></script>

<script type="text/javascript">
    var userid="<?php echo $userid;?>";
    window['table']="tasks";
    window['isntable']="li";

    function toggle_checked(el){
        var label=$(el).attr("name");
        $('[name="schedulato_lv_0"]').prop("checked",false)
        $('[name="schedulato_lv_1"]').prop("checked",false)
        $('[name="schedulato_lv_2"]').prop("checked",false)
        $('[name="schedulato_lv_3"]').prop("checked",false)
        $(el).prop("checked",true);
    }

    function addUserLine(value,text){
        $(".rq").removeAttr("required");
        var newValue=jQuery('#userAssigned').val()+","+value;
        jQuery('#userAssigned').val(newValue);

        var newText=jQuery('#userAssignedText').text()+text+"\n";
        jQuery('#userAssignedText').text(newText);
    }

    function addUser(el){
        var value=jQuery(el).find('option:selected').val();
        var text=jQuery(el).find('option:selected').text();
        addUserLine(value,text);
    }

    function cleanReparto(){
        jQuery('#userAssigned').val("");
        jQuery('#userAssignedText').text("");
        $(".rq").attr("required",true);
    }

    function applyRep(el){

        if(el.value==""){
            cleanReparto();
            return;
        }

        var list=jQuery(el).find('option:selected').data().list.split(",");
           jQuery("[name='task_fcfs']").val("1");
            list.map(function(currentValue){
                var value=currentValue;
                var text=jQuery("#user_id option[value='"+value+"']:first").text();
                addUserLine(value,text);
            })
    }

    $(document).ready(function(){
        $(".todo_title a").on('click', function () {

            var href=$(this).attr("href");
            var id=href.replace("#todo_task_modal_","");
            window['popup']=href;

            $.post("../api.php",{qr:'loadnote',id:id},function(response){
                response=JSON.parse(response);
                $(window['popup']+" reparto").hide();
                if(response.length==0)
                    return;
                var el=window['popup'];
                $(el+" notelist ul").html("");

                for(var i=0;i<response.data.length;i++){
                    var pre=response.data[i].username+" / alle ore : "+response.data[i].timer+" ha aggiunto una nota"+'<i class="fa fa-times" style="margin-left:15px;color:#9F2F24" onclick="deletenote('+response.data[i].id+',this)" aria-hidden="true"></i>';

                    if($(el).find("notelist ul li.default"))
                        $(el).find("notelist ul li.default").remove();

                    if(userid==response.data[i].userid)
                       $(el).find("notelist ul").append("<li><p>"+pre+"</p>"+response.data[i].note+"</li>");
                    else
                       $(el).find("notelist ul").append("<li><p>"+pre+"</p>"+response.data[i].note+"</li>");
                }

                /* task di gruppo */
                if(response.info[0].user_assigned_id==response.info[0].task_supervisor)
                {
                    $(el+" .modal-footer button:first").hide()
                    $(window['popup']+" reparto").show();
                    $(window['popup']+" repartonote").show();
                    $(window['popup']+" [name='reparto']").val(response.info);
                }

            })



        })
        $('#task-search-current').keyup(function (){
          filterIn('#task-search-list-current', '.task-item', $(this).val().toLowerCase())
        })
        $('#task-search-completed').keyup(function (){
          filterIn('#task-list-completed', '.task-item', $(this).val().toLowerCase())
        })
    })

    function filterIn(container, items, value){
      $(container).find(items).each(function(){
        let data = $(this).data()
        let find = false
        for (let val of Object.keys(data)){
          if(data[val].toString().toLowerCase().includes(value))
            find=true
        }
        if (!find) $(this).css('display','none')
        else $(this).css('display','block')
      })
    }



    function deletenote(id,el){
        window['tmp']=el
        $.post("../api.php",{qr:'delnote',id:id},function(response){
            $(window['tmp']).parents("li").remove();
        })
    }

    var savenoteflag=false;
    function savenote(el,taskid){
        var value=$(el).parents(".modal-body").find("textarea").val();
        var note=$(el).parents(".modal-body").find(".taskNote").text();
        var title=$(el).parents(".modal-body").find(".taskNote").text();
        if(value==""){
            $(el).parents("noteform").html("");
            return;
        }
        if(savenoteflag)
            return;
        savenote=true;
        window['el']=el;

        $.post("../api.php",{qr:'addnote',taskid:taskid,text:value,note:note,title:title},function(response){
            window.location.reload(true);
        })
    }

    function addnote(el,id){
        if($(".noteformcontainer:visible").length)
            return;
        var html=
            '<div class="form-group noteformcontainer">'+
                '<div class="">'+
                    '<textarea id="textinput" name="textinput" type="text" placeholder="Nota di testo" style="width:300px;height:100px;" class="form-control"></textarea>'+
                '<span class="help-block">'+'</span>  '+
            '</div>';
        $(el).parents("noteform").prepend(html);
        $("#savenote").attr("data-id",id);
        $(el).parents("noteform").find(".addnote").toggle();
        $(el).parents("noteform").find(".savenote").toggle();
    }

</script>

@endsection
