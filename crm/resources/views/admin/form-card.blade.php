@extends('layouts.master')
@section('header-page-personalization')
@endsection
@section('content')


<style type="text/css">
    #cercaAzienda{
        display: none;
    }
<?php 
$privati= DB::select('SELECT id, concat(customers.cognome," ",customers.nome) as label FROM `customers` ORDER BY label ASC');
$aziende=$privati;
?>
</style>
    <div class="page_content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">

                    <form id="login_form" role="form" method="POST" action="{{ route('card.store') }}" class="customForm">


                    {{ csrf_field() }}
                      <input type="hidden" name="id" value="@if(!empty($card->id)) {{$card->id}} @endif">
                    <?php $name=Auth::user()->name." ".Auth::user()->surname;?>
                        <div class="panel panel-default">
                            <div class="panel-body">
                                <div class="heading_a"><legend><span class="big-title">Aggiungi una Gift card</span></legend></div>
                                <div class="tabbable tabs-left">
                                    <ul class="nav nav-tabs" id="tabs_c">
                                        <li class="active"><a data-toggle="tab" href="#carta">Card</a></li>
                                        <li><a data-toggle="tab" href="#genera">Copia</a></li>
                                        <li><a data-toggle="tab" href="#notifiche">Notifiche</a></li>
                                        <li><a data-toggle="tab" href="#storico">Storico</a></li>
                                        <li><a data-toggle="tab" href="#pdf">Stampa PDF</a></li>
                                    </ul>
                                    @if(session()->has('message'))
                                        <div class="alert alert-success">
                                            {{ session()->get('message') }}
                                        </div>
                                    @endif
                                    @if(session()->has('error'))
                                        <div class="alert alert-danger">
                                            {{ session()->get('error') }}
                                        </div>
                                    @endif
                                    <div class="tab-content" id="tabs_content_c">
                                        <div id="carta" class="tab-pane fade in active">

                                            <div class="row">
                                                <div class="col-lg-8">
                                                    <legend>Impostazioni principali</legend>

                                                    <div class="col-md-12 col-md-12">
                                                      <label class="col-md-3 control-label" for="Nome carta">Codice carta</label>  
                                                      <div class="col-md-6">
                                                        {{$code}}
                                                        <input type="hidden" name="code" value="{{$code}}">
                                                      </div>
                                                    </div>
                                                <!--  beneficiario -->
                                            <input type="hidden" name="customer_cards" value="@if(!empty($card->customer_cards)) {{$card->customer_cards}} @endif">
                                            <input type="hidden" name="business_cards" value="@if(!empty($card->business_cards)) {{$card->business_cards}} @endif">

                                              <div id="donatore" class="col-md-12">
                                                <label class="col-md-3 control-label" for="textinput">Nome e cognome di chi la regala</label>  
                                                <div class="col-md-6">
                                                <input id="donatore" type="text" name="donatore" placeholder="donatore" value="@if(!empty($card->donatore)) {{$card->donatore}} @endif" name="donatore" class="form-control input-md">
                                                </div>
                                              </div>

                                              <div id="azienda_emittente" class="col-md-12">
                                                <label class="col-md-3 control-label" for="textinput">Azienda che emette la carta</label>  
                                                <div class="col-md-6">
                                                <input id="azienda_emittente"  type="text" placeholder="azienda emittente" name="azienda_emittente" value="@if(!empty($card->azienda_emittente)) {{$card->azienda_emittente}} @endif" name="donatore" class="form-control input-md" class="form-control input-md">
                                                </div>
                                              </div>


                                              <div class="col-md-12">
                                                <label class="col-md-3 control-label" for="Beneficiario card">Beneficiario card</label>
                                                <div class="col-md-6">
                                                  <div class="radio">
                                                    <label for="Beneficiario card-0">
                                                      <?php 
                                                        if(!empty($card->customer_cards))
                                                          $name=DB::select("SELECT concat(nome,' ',cognome) as name FROM `customers` WHERE id=".$card->customer_cards);
                                                        if(!empty($card->business_cards))
                                                          $bname=DB::select("SELECT ragione as name FROM `business` WHERE id=".$card->business_cards);

                                                      ?>
                                                      <input type="radio" id="Beneficiario card-0" name="bah" onclick="showSearch(0)" value="0" @if(!empty($card->customer_cards)) checked @endif>
                                                      Privato <?php if(!empty($card->customer_cards)) echo $name[0]->name;?>
                                                    </label>
                                                  </div>
                                                  <div class="radio">
                                                    <label for="Beneficiario card-1">
                                                      <input type="radio" id="Beneficiario card-1" name="bah" onclick="showSearch(1)" @if(!empty($card->business_cards)) checked @endif>
                                                      Azienda <?php if(!empty($card->business_cards)) echo $name[0]->name;?>
                                                    </label>
                                                  </div>
                                                </div>
                                              </div>

                                              <div id="cercaPrivato" class="col-md-12">
                                                <label class="col-md-3 control-label" for="textinput">Ricerca privato</label>  
                                                <div class="col-md-6">
                                                <input id="privato" type="text" placeholder="nome cognome" class="form-control input-md">
                                                </div>
                                              </div>

                                              <div id="cercaAzienda" class="col-md-12">
                                                <label class="col-md-3 control-label" for="textinput">Ricerca azienda</label>  
                                                <div class="col-md-6">
                                                <input id="azienda" type="text" placeholder="nome cognome" class="form-control input-md">
                                                </div>
                                              </div>


                                                <div class="form-col-md-12" style="display: none;">
                                                  <label class="col-md-3 control-label" for="note">Note generali</label>
                                                  <div class="col-md-6">                     
                                                    <textarea class="form-control" id="note" name="note"> @if(!empty($card->note)) {{$card->note}} @endif</textarea>
                                                  </div>
                                                </div>

                                                <!-- fine beneficiario -->

                                                    <div class="col-md-12 col-md-12">
                                                      <label class="col-md-3 control-label" for="Nome carta">Nome carta</label>  
                                                      <div class="col-md-6">
                                                      <input id="Nome carta" name="label" type="text" placeholder="Nome carta" class="form-control input-md" value="@if(!empty($card->label)) {{$card->label}} @endif">
                                                      </div>
                                                    </div>

                                                    <div class="col-md-12 col-md-12">
                                                      <label class="col-md-3 control-label" for="Importo">Importo</label>  
                                                      <div class="col-md-6">
                                                      <input id="Importo" name="value" type="number" placeholder="Importo" class="form-control input-md" value="@if(!empty($card->value)){{$card->value}}@endif">
                                                        
                                                      </div>
                                                    </div>

                                                    <div class="col-md-12 col-md-12">
                                                      <label class="col-md-3 control-label" for="Prezzo">Prezzo</label>  
                                                      <div class="col-md-2">
                                                      <input id="Prezzo" name="price" type="number" placeholder="35,00" class="form-control input-md" value="@if(!empty($card->price)){{$card->price}}@endif">
                                                        
                                                      </div>
                                                    </div>

                                                    <div class="col-md-12 col-md-12">
                                                      <label class="col-md-3 control-label" for="Tipologia">Tipologia</label>
                                                      <div class="col-md-6">
                                                        <select id="Tipologia" name="kind" class="form-control">
                                                          <option value="VIP" @if(!empty($card->kind) && $card->kind=="VIP") selected @endif>VIP</option>
                                                          <option value="Utilizzo singolo" @if(!empty($card->kind) && $card->kind=="Utilizzo singolo") selected @endif>Utilizzo singolo</option>
                                                          <option value="Generica" @if(!empty($card->kind) && $card->kind=="Generica") selected @endif>Generica</option>
                                                        </select>
                                                      </div>
                                                    </div>

                                                    <div class="col-md-12 col-md-12">
                                                      <label class="col-md-3 control-label" for="Scadenza carta">Scadenza carta</label>  
                                                      <div class="col-md-6">
                                                      <input id="Scadenza carta" name="deadline" type="date" placeholder="10/10/2019" class="form-control input-md" value="@if(!empty($card->deadline)){{$card->deadline}}@endif">
                                                      </div>
                                                    </div>

                                                    <div class="col-md-12 col-md-12">
                                                      <label class="col-md-3 control-label" for="Saldata">Saldata</label>
                                                      <div class="col-md-6">
                                                      <div class="radio">
                                                        <label for="Saldata-0">
                                                          <input type="radio" name="payed" id="Saldata-0" value="0"  @if(!empty($card->payed) && $card->payed==0) selected @endif>
                                                          No
                                                        </label>
                                                        </div>
                                                      <div class="radio">
                                                        <label for="Saldata-1">
                                                          <input type="radio" name="payed" id="Saldata-1" value="1" @if(!empty($card->payed) && $card->payed==1) selected @endif >
                                                          Si
                                                        </label>
                                                        </div>
                                                      </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>


                                        <div id="genera" class="tab-pane fade">
                                           <legend>Genera copie  della gift card</legend>
                                            <div class="col-md-12 col-md-12">
                                                <label class="col-md-3 control-label" for="Nome carta">Genera il numero indicato di copie per questa gift card</label>  
                                                <div class="col-md-6">
                                                    <input id="ripetizioni" name="ripetizioni" type="number" max="100" placeholder="1" value="1" class="form-control input-md">
                                                    <help>Numero copie</help>
                                                </div>
                                            </div>
                                        </div>

                                        <div id="notifiche" class="tab-pane fade">
                                          <legend>Imposta un reminder per monitorare la scadenza di questa carta</legend>
                                          @if(empty($card->deadline))
                                          <b>Attenzione!</b> E' necessario impostare una scadenza per la carta e aggiornare la pagina per utilizzare questa funzionalità
                                          @else
                                            Scadenza carta: {{$card->deadline}}
                                            
                                          @endif
                                        </div>

                                       
                                        <div id="storico" class="tab-pane fade">
                                           <label>Storico utilizzo card</label>
                                            <div id="coupon">
                                            La gift card è stata utilizzata nei seguenti preventivi:<br>
                                              <ul>
                                                @foreach($preventivi as $preventivo)

                                                <li>
                                                    Preventivo per: <a href="{{route('preventivo edit',['id' => $preventivo->preventivo_id])}}" target="blank"> {{$preventivo->labeldest}} </a><br>
                                                    Spesa: {{$preventivo->spesa}} <br>
                                                    Data Utilizzo: {{$preventivo->data_utilizzo}} <br>
                                                </li>
                                                @endforeach
                                              </ul>
                                            </div>
                                        </div>

                                        <div id="pdf" class="tab-pane fade">
                                          <legend>Generazione pdf gift card</legend>
                                          Puoi generare il pdf di questa e altre gift card premendo <a href="http://hcslab.it/suendcrm/admin/cards"> qui</a>
                                        </div>



                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-12">
                                    <button class="btn btn-lg btn-success btn-block confirmsave">Aggiorna</button>
                                </div>
                           </div>
                        </div>

                </form>

                </div>
            </div>
        </div>
    </div>

@endsection
@section('footer-plugin')
  <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
  <link rel="stylesheet" href="/resources/demos/style.css">
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

    <script type="text/javascript">

    var availableTags1= JSON.parse("<?php echo addslashes(json_encode($privati)) ;?>");
    var availableTags2= JSON.parse('<?php echo addslashes(json_encode($aziende)) ;?>');

        $(document).ready(function(){
            $('input').css('text-transform','uppercase');

        if(window.location.search&&window.location.search.includes("view"))
        {
            $("form button").hide();
            $("form .applyStatus").hide();
            $("input,select,textarea").attr("readonly","true");
            $("input[type='checkbox'],input[type='radio']").attr("disabled","true");
            $("#editScheda").show();
        }
        if(window.location.search&&window.location.search.includes("edit")){
            $(".big-title").text("Modifica una gift card");
        }
            $( "#privato" ).autocomplete({
              source: availableTags1,
                select: function (a, b) {

                      $('[name="customer_cards"]').val(b.item.id);
                },
                response: function(event, ui) {
                     // ui.content is the array that's about to be sent to the response callback.
                     if (ui.content.length === 0) {
                         $("#privati-message").text("Il cliente inserito non è presente nel elenco privati");
                     } else {
                         $("#privati-message").empty();
                     }
                 }
            });

            $( "#azienda" ).autocomplete({
              source: availableTags2,
                select: function (a, b) {
                    $('[name="business_cards"]').val(b.item.id);
                },
                response: function(event, ui) {
                     // ui.content is the array that's about to be sent to the response callback.
                     if (ui.content.length === 0) {
                         $("#privati-message").text("Il cliente inserito non è presente nel elenco privati");
                     } else {
                         $("#privati-message").empty();
                     }
                 }
            });

        })

            function showSearch(kind){
                if(kind==0){
                     $("#cercaPrivato").show();
                     $("#cercaAzienda").hide();
                }
                else{
                     $("#cercaPrivato").hide();
                     $("#cercaAzienda").show();
                 }
            }

    </script>
@endsection
