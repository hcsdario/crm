@extends('layouts.master')
@section('header-page-personalization')
    <!-- page specific stylesheets -->

    <!-- datatables -->
    <link rel="stylesheet" href="{{ asset('/lib/DataTables/media/css/jquery.dataTables.min.css') }}">
    <link rel="stylesheet" href="{{ asset('/lib/DataTables/extensions/TableTools/css/dataTables.tableTools.min.css') }}">
    <link rel="stylesheet" href="{{ asset('/lib/DataTables/extensions/Scroller/css/dataTables.scroller.min.css') }}">

    <!-- main stylesheet -->
    <link href="{{ asset('/css/style.css') }}" rel="stylesheet" media="screen">

    <!-- google webfonts -->
    <link href='http://fonts.googleapis.com/css?family=Source+Sans+Pro:400&amp;subset=latin-ext,latin' rel='stylesheet' type='text/css'>


    <!-- moment.js (date library) -->
    <script src="{{ asset('/lib/moment-js/moment.min.js') }}"></script>
    <style>
        #customers_table thead,
        #customers_table th {text-align: center;}
        #customers_table tr {text-align: center;}

        #customers_adv_table thead,
        #customers_adv_table th {text-align: center;}
        #customers_adv_table tr {text-align: center;}
        [readonly]{
          background-color: white;
          opacity: 1;
        }
    </style>
@endsection
@section('content')

    <div class="page_content page_advs">
        <div class="container-fluid">
            <div class="row">

                <div class="col-lg-12">
                    <div class="panel panel-default">

                        <div class="panel-body">
                            <fieldset>
                                <legend><span class="big-title">Elenco agenzie di viaggio</span></legend>
                            </fieldset>


                            <div class="row">
                                <div class="col-lg-12">
                                    <table class="table table-bordered table-striped" id="customers_adv_table" >
                                                <thead>
                                                    <th>Id</th>
                                                    <th>Agenzia</th>
                                                    <th>Ragione sociale</th>
                                                    <th>P. iva / Codice fiscale</th>

                                                    <th>Citta'</th>
                                                    <th>Provincia</th>
                                                    <th>Cap</th>

                                                    <th>Telefono fisso</th>

                                                    <th>Referente</th>
                                                    <th>Cellulare</th>
                                                    <th>Email</th>

                                                    <th>Azioni</th>
                                                </thead>
                                                <tbody>

                                                @foreach($aziendeItaliane as $azienda)
                                                    <tr>
                                                        <td>{{ $azienda->id }}</td>
                                                        <td>{{ $azienda->nome }}</td>
                                                        <td>{{ $azienda->societa }}</td>
                                                        <td>{{ $azienda->codice_fiscale }}</td>

                                                        <td>{{ $azienda->citta }}</td>
                                                        <td>{{ $azienda->provincia }}</td>
                                                        <td>{{ $azienda->cap }}</td>

                                                        <td>{{ $azienda->tel1 }}</td>

                                                        <td>{{ $azienda->insertBy }}</td>
                                                        <td>{{ $azienda->tel2 }}</td>
                                                        <td>{{ $azienda->email1 }}</td>
                                                        <td>
                                                            <a href="../admin/adv/edit/{{ $azienda->id }}?action=view" class="btn btn-xs btn-info"><i class="glyphicon glyphicon-search" data-toggle="tooltip" title="Visualizza"></i></a>
                                                            <a href="../admin/adv/edit/{{ $azienda->id }}" class="btn btn-xs btn-secondary"><i class="glyphicon glyphicon-edit" data-toggle="tooltip" title="Modifica"></i></a>
                                                            <a href="#" class="btn btn-xs btn-danger news-zama" style="margin-left: 10px;" onclick="deleteRecord(this,['customers',{{ $azienda->id }}])"><i class="glyphicon glyphicon-trash" data-toggle="tooltip" title="Elimina"></i></a>
                                                        </td>
                                                    </tr>
                                                     @endforeach
                                                </tbody>
                                    </table>
                                </div>
                            </div>
                            <button class="btn btn-lg btn-success btn-block" ><a href="{{ route('newadv') }}" style="color:#fff;text-decoration: none;">Aggiungi una nuova Adv Italiana</a></button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>



@endsection
@section('footer-plugin')
    <!-- datatables -->
    <script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.18/pdfmake.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.2.4/js/dataTables.buttons.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.2.4/js/buttons.bootstrap.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.2.4/js/buttons.print.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.2.4/js/buttons.html5.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <script src="//cdn.datatables.net/buttons/1.4.2/js/buttons.colVis.min.js"></script>
    <script src="{{ asset('/lib/DataTables/extensions/Scroller/js/dataTables.scroller.min.js') }}"></script>
    <script src="http://tinynav.com/tinynav.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.perfect-scrollbar/0.8.1/js/perfect-scrollbar.jquery.min.js"></script>
    <script src="{{ asset('/js/apps/tisa_datatables.js') }}"></script>
    <script src="{{ asset('/lib/moment-js/moment.min.js') }}"></script>

    <script src="{{ asset('/lib/DataTables/media/js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('/lib/DataTables/media/js/dataTables.bootstrap.js') }}"></script>
    <script src="{{ asset('/lib/DataTables/extensions/TableTools/js/dataTables.tableTools.min.js') }}"></script>
    <script src="{{ asset('/lib/DataTables/extensions/Scroller/js/dataTables.scroller.min.js') }}"></script>
    <script src="{{ asset('/js/apps/tisa_datatables.js') }}"></script>

    <script>

        if(is_tablet()||is_tablet_pro())
            var tablet_hide_col={
                    "targets": [0,3,5,9,11],
                    "visible": false,
                };
        else
            var tablet_hide_col={
                    "targets": [0,3,4,9,11],
                    "visible": false,
                };

        $('#customers_adv_table').DataTable({
            oLanguage: {sSearch: "Filtra: "},
            responsive: true,
            pageLength:my_preference,
            dom: 'Bfrtip',
            buttons: [
                "excelHtml5"
            ],
           "columnDefs": [
                tablet_hide_col
            ]
        });

        var my_preference=localStorage.getItem('page_length_preference');
        if(my_preference==null)
            my_preference=10;

    function add_page(el){
        localStorage.setItem('page_length_preference',el.value);
        window.location.reload(true);
    }

    function get_page_length(my_preference){
        var page_content=
        '<div class="dataTables_length" id="example_length" style="float: left;"><label>'+
        'Showing <select onchange="add_page(this)" name="example_length" aria-controls="example" class="">';
        var dim=[10,25,50,100,250,500,1000,2000];

        for(var i =0;i<dim.length;i++){
            if(my_preference==dim[i])
                page_content+='<option value="'+dim[i]+'" selected>'+dim[i]+'</option>'
            else
                page_content+='<option value="'+dim[i]+'">'+dim[i]+'</option>'
        }

        page_content+='</select> entries </label></div>'  ;
        return page_content;
    }

    $(document).ready(function(){
        var pl=get_page_length(my_preference);
        $(".btn-default.buttons-excel.buttons-html5").after(pl);
    });

    window['table']="customers";
    console.error("admin/adv");
    </script>
@endsection
