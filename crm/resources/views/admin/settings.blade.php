@extends('layouts.master')
@section('header-page-personalization')
    <style>
        #manuale{
            margin-top: 50px;
        }
        .twitter-typeahead, .tt-hint, .tt-input, .tt-menu { width: 100%;  }
        .tt-menu {
            background: #ffffff;
        }
        note{
            color: white;
            font-weight: 900;
        }
        textarea{
            overflow: hidden;
        }
        td.fa-file-text-container{
            text-align: center;
        }
        .fa-file-text:hover{
            color: rgb(66, 139, 202);
        }
        .fa-file-text{
            cursor: pointer;
            font-size: 23px;
        }
        th{
            text-align: center;            
        }

        .label{
            font-size: 12px;
            padding-left: 0px!important;
            text-align: left;
            display: block;            
        }

    </style>

            @endsection
            @section('content')
                <div class="page_content page_uploads">

                    <div class="container-fluid">

             @if(session()->has('message'))
                <div class="alert alert-success">
                    {{ session()->get('message') }}
                </div>
            @endif
            @if(session()->has('error'))
                <div class="alert alert-danger">
                    {{ session()->get('error') }}
                </div>
            @endif

        <div class="row">
            <div class="container-fluid">
                <div class="row well">
                    <div class="col-md-12" style="text-align: center;">
                        <div>
                            <h1>IMPOSTAZIONI</h1>
                        </div>
                        <div>
                            <h1>Manuale di utilizzo</h1>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12" style="text-align: center;">
                    <div>
                        <a href="#" class="btn btn-xs btn-secondary" onclick="showform()"><i class="glyphicon glyphicon-edit"></i>&nbsp;Modifica</a>
                        <a href="#" style="display: none;" class="btn btn-xs btn-primary" onclick="showview()"><i class="glyphicon glyphicon-search"></i>&nbsp;Visualizza</a>
                    </div>

                                            <?php 
                                                if(file_exists(getcwd()."/crm/resources/man.html"))
                                                    $data=file_get_contents(getcwd()."/crm/resources/man.html");
                                                else
                                                    $data="";
                                                
                                            ?>

                        <div id="manuale" class="panel">
                            <manuale><?php echo $data;?></manuale>
                        </div>
                        
                        <div id="form_manuale" style="display: none;">
                    <form id="login_form" role="form" method="post">
                        {{ csrf_field() }}
                        <div class="panel panel-default">

                            <div class="panel-body">
<!--                                 <div class="row">
                                    <div class="col-lg-12">
                                        <div class="form-group">
                                            <label for="reg_input">Titolo News</label>
                                            <input type="text" id="news_title" name="news_title" class="form-control">
                                        </div>
                                    </div>

                                </div>   --> 
                                <div class="col-lg-12">
                                    <div class="">
                                        <div class="panel_body_a">
                                            
                                            <label for="reg_input">Paragrafo</label>
                                            <textarea name="wysiwg_full" id="wysiwg_full" cols="30" rows="4" class="form-control"><?php                                                 echo $data;?></textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div>
                        <button class="btn btn-lg btn-success btn-block">Salva</button>
                    
                        </div>
                </form>
                    </div>
                </div>
            </div>
            </div>
        </div>
    </div>
</div>


<script type="text/javascript">
    /*
            CKEDITOR.instances.wysiwg_full.setData(response.news_body);
    */
    function showform() {
        $(".btn.btn-xs.btn-secondary").hide();
        $(".btn.btn-xs.btn-success").show();
        $(".btn.btn-xs.btn-primary").show();
        $("#manuale").hide();
        $("#form_manuale").show();
    }
    function showview() {
        $(".btn.btn-xs.btn-secondary").show();
        $(".btn.btn-xs.btn-success").hide();
        $(".btn.btn-xs.btn-primary").hide();
        $("#manuale").show();
        $("#form_manuale").hide();
    }
    function saveform(){

    }
</script>
@endsection
@section('footer-plugin')

    <!-- wysiwg editor -->
    <script src="{{ asset('/lib/ckeditor/ckeditor.js') }}"></script>
    <script src="{{ asset('/lib/ckeditor/adapters/jquery.js') }}"></script>
    <!-- wizard functions -->
    <script src="{{ asset('/js/apps/tisa_wysiwg.js') }}"></script>
    <!-- datatables -->
    <script src="{{ asset('/lib/DataTables/media/js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('/lib/DataTables/media/js/dataTables.bootstrap.js') }}"></script>
    <script src="{{ asset('/lib/DataTables/extensions/TableTools/js/dataTables.tableTools.min.js') }}"></script>
    <script src="{{ asset('/lib/DataTables/extensions/Scroller/js/dataTables.scroller.min.js') }}"></script>

    <!-- datatables functions -->
    <script src="{{ asset('/js/apps/tisa_datatables.js') }}"></script>

    <script type="text/javascript">
    var data='<?php echo $data ?>';
        $(document).ready(function(){

//            CKEDITOR.instances.wysiwg_full.setData(data);
        })
    </script>

@endsection