@extends('layouts.master')
@section('header-page-personalization')
    <!-- page specific stylesheets -->
<?php
ini_set('get_max_size' , '20M');
?>
    <link href="{{ asset('/css/style.css') }}" rel="stylesheet" media="screen">

    <!-- google webfonts -->
    <link href='https://fonts.googleapis.com/css?family=Source+Sans+Pro:400&amp;subset=latin-ext,latin' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="{{ asset('/lib/DataTables/media/css/jquery.dataTables.min.css') }}">

    <style>
        #customers_table thead,
        #customers_table th {text-align: center;}
        #customers_table tr {text-align: center;}

        #customers_adv_table thead,
        #customers_adv_table th {text-align: center;}
        #customers_adv_table tr {text-align: center;}
        .hideBecauseDatatablesSucks{
          /*  display: none;*/
        }
        [data-duplifyTrigger="1"].active{
            background-color: rgb(41, 134, 185);
            color: white;
        }
    </style>
@endsection
@section('content')
<?php $i=0;?>
    <div class="page_content page_preventivo">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <!--<button class="btn btn-lg btn-success btn-block" onclick="window.location='{{ url("/admin/preventivo") }}'">Aggiungi un preventivo</button>-->
                        <div class="panel panel-default">
                            <div class="panel-body">
                                <fieldset>
                                    <legend><span class="big-title">Preventivi</span></legend>
                                </fieldset>
                                <div class="row">
                                            <table class="table table-bordered privati table-striped" id="preventivo_table" data-tablename="preventivo">
                                                <thead>
                                                    <th style="display: none;"></th>
                                                    <th>N.</th>
                                                    <th>Destinazione</th>
                                                    <th>Tipologia di viaggio</th>
                                                    <th>Prezzo totale</th>
                                                    <th>Feedback</th>
                                                    <th>Stato</th>
                                                    <th>Ultima modifica</th>
                                                    <th>Ultimo editore</th>
                                                    <th>Riferito a </th>

                                                    <th>Azioni</th>
                                                </thead>
                                                <tbody>
                                                @foreach($preventivi as $preventivo)
                                                    <td style="display: none;">{{ $preventivo->id }}</td>
                                                    <td><input type="checkbox" data-duplify="1" data-id="{{ $preventivo->id }}" data-auth="{{ Auth::user()->id }}">  N.<?php echo $i++;?></td>
                                                    <td>{{ $preventivo->labeldest }}</td>
                                                    <td>{{ $preventivo->TipologiaDiViaggio }}</td>
                                                    <td>{{ $preventivo->prezzoTotalePreventivo }}</td>
                                                    <td>{{ $preventivo->feedbackCliente }}</td>
                                                    <td>{{ $preventivo->statoPreventivo }}</td>
                                                    <td>{{ $preventivo->updated_at }}</td>
                                                    <td>{{ $preventivo->username }}</td>
                                                    <td>{{ $preventivo->cognome }} {{ $preventivo->nome }}</td>
                                                        <td style="text-align: center;">
                                                            <a href="/admin/preventivo/edit/{{ $preventivo->id }}?action=view" class="btn btn-xs btn-info"><i class="glyphicon glyphicon-search" data-toggle="tooltip" title="Visualizza"></i></a>
                                                            <a href="/admin/preventivo/edit/{{ $preventivo->id }}" class="btn btn-xs btn-secondary"><i class="glyphicon glyphicon-edit" data-toggle="tooltip" title="Modifica"></i></a>
                                                            <a href="#" class="btn btn-xs btn-danger news-zama" style="margin-left: 10px;" onclick="deleteRecord(this)"><i class="glyphicon glyphicon-trash" data-toggle="tooltip" title="Elimina"></i></a>
                                                        </td>
                                                    </tr>
                                                @endforeach
                                                </tbody>
                                            </table>
                                    </div>
                                </div>
                                <button class="btn btn-lg btn-success btn-block" onclick="window.location='{{ url("/admin/preventivo") }}'">Aggiungi un preventivo</button>
                            </div>
                        </div>
                </div>
            </div>
        </div>
    </div>



@endsection
@section('footer-plugin')
    <!-- datatables -->
    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.18/pdfmake.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.2.4/js/dataTables.buttons.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.2.4/js/buttons.bootstrap.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.2.4/js/buttons.print.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.2.4/js/buttons.html5.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <script src="//cdn.datatables.net/buttons/1.4.2/js/buttons.colVis.min.js"></script>
    <script src="{{ asset('/lib/DataTables/extensions/Scroller/js/dataTables.scroller.min.js') }}"></script>
    <script src="https://crm.suend.it/js/tinynav.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.perfect-scrollbar/0.8.1/js/perfect-scrollbar.jquery.min.js"></script>
    <script src="{{ asset('/js/apps/tisa_datatables.js') }}"></script>
    <script src="{{ asset('/lib/moment-js/moment.min.js') }}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.2.0/jquery-confirm.min.js"></script>
    <script src="{{ asset('/lib/jquery.confirm/jquery-confirm.min.js') }}"></script>


    <script src="{{ asset('/lib/DataTables/media/js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('/lib/DataTables/media/js/dataTables.bootstrap.js') }}"></script>
    <script src="{{ asset('/lib/DataTables/extensions/TableTools/js/dataTables.tableTools.min.js') }}"></script>
    <script src="{{ asset('/lib/DataTables/extensions/Scroller/js/dataTables.scroller.min.js') }}"></script>
    <script src="{{ asset('/js/apps/tisa_datatables.js') }}"></script>

    <script>

        if(is_tablet()||is_tablet_pro())
            var tablet_hide_col={
                    "targets": [1,4,5],
                    "visible": false,
                };
        else
            var tablet_hide_col={};


        var my_preference=localStorage.getItem('page_length_preference');
        if(my_preference==null)
            my_preference=10;

        var dtable = $('#preventivo_table').DataTable({
            responsive: true,
            order: [[ 0, "desc" ]],
            pageLength:my_preference,
            oLanguage: {sSearch: "Filtra: "},
            dom: 'Bfrtip',
            buttons: [
                "excelHtml5"
            ],
            "columnDefs": [
                tablet_hide_col
            ]
        });



    function add_page(el){
        localStorage.setItem('page_length_preference',el.value);
        window.location.reload(true);
    }

    function get_page_length(my_preference){
        var page_content=
        '<div class="dataTables_length" id="example_length" style="float: left;"><label>'+
        'Showing <select onchange="add_page(this)" name="example_length" aria-controls="example" class="">';
        var dim=[10,25,50,100,250,500,1000,2000];

        for(var i =0;i<dim.length;i++){
            if(my_preference==dim[i])
                page_content+='<option value="'+dim[i]+'" selected>'+dim[i]+'</option>'
            else
                page_content+='<option value="'+dim[i]+'">'+dim[i]+'</option>'
        }

        page_content+='</select> entries </label></div>'  ;
        return page_content;
    }

    $(document).ready(function(){
        var pl=get_page_length(my_preference);
        $(".btn-default.buttons-excel.buttons-html5").after(pl);
    });

    window['table']="preventivo";
    $(".dt-buttons.btn-group").append('<a class="btn btn-default" onclick="duplySelected(this)" data-duplifyTrigger="1" tabindex="0" href="#"><span> Duplica preventivo</span></a>');

    </script>
@endsection
