@extends('layouts.master')
@section('header-page-personalization')
    <style>
        .twitter-typeahead, .tt-hint, .tt-input, .tt-menu { width: 100%;  }
        .tt-menu {
            background: #ffffff;
        }
        note{
        	color: white;
		    font-weight: 900;
        }
        textarea{
            overflow: hidden;
        }
        td.fa-file-text-container{
            text-align: center;
        }
        .fa-file-text:hover{
            color: rgb(66, 139, 202);
        }
        .fa-file-text{
            cursor: pointer;
            font-size: 23px;
        }
        th{
            text-align: center;            
        }

        .label{
            font-size: 12px;
            padding-left: 0px!important;
            text-align: left;
            display: block;            
        }

    </style>

            @endsection
            @section('content')
                <div class="page_content page_uploads">

                    <div class="container-fluid">

             @if(session()->has('message'))
                <div class="alert alert-success">
                    {{ session()->get('message') }}
                </div>
            @endif
            @if(session()->has('error'))
                <div class="alert alert-danger">
                    {{ session()->get('error') }}
                </div>
            @endif

        <div class="row">
            <div class="container-fluid">
                <div class="row well">
                    <div class="col-md-12" style="text-align: center;">
                        <div>
                            <h1>IMPOSTAZIONI</h1>
                        </div>
                        <div>
                            <h1>Manuale di utilizzo</h1>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12" style="text-align: center;">
                    <div>
                        <a href="#" class="btn btn-xs btn-secondary"><i class="glyphicon glyphicon-edit"></i>&nbsp;Modifica</a>
                        <a href="#" style="display: none;" class="btn btn-xs btn-success"><i class="glyphicon glyphicon-edit"></i>&nbsp;Salva</a>
                    </div>
                        <div id="manuale">
                            <manuale>                                
                            </manuale>
                        </div>
                        <div id="form_manuale">
                    <form id="login_form" role="form" method="POST" action="{{ route('news.store') }}">
                        {{ csrf_field() }}
                        <div class="panel panel-default">

                            <div class="panel-body">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="form-group">
                                            <label for="reg_input">Titolo News</label>
                                            <input type="text" id="news_title" name="news_title" class="form-control">
                                        </div>
                                    </div>

                                </div>   
                                <div class="col-lg-12">
                                    <div class="">
                                        <div class="panel_body_a">
                                            <textarea name="wysiwg_full" id="wysiwg_full" cols="30" rows="4" class="form-control">

                                            </textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                </form>
                    </div>
                </div>
            </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('footer-plugin')

@endsection