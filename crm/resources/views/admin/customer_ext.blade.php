@extends('layouts.master')
@section('header-page-personalization')
    <style>
        .twitter-typeahead, .tt-hint, .tt-input, .tt-menu { width: 100%;  }
        .tt-menu {
            background: #ffffff;
        }
    </style>
@endsection
@section('content')
    <div class="page_content page_customer_ext">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <form id="login_form" role="form" method="POST" action="{{ route('customer.store') }}">
                    {{ csrf_field() }}

                        <div class="panel panel-default">
                            <div class="panel-body">
                                <div class="heading_a">Suend CRM</div>
                                <div class="tabbable tabs-left">
                                    <ul class="nav nav-tabs" id="tabs_c">
                                        <li class="active"><a data-toggle="tab" href="#info">Info</a></li>
                                        <li><a data-toggle="tab" href="#profilo">Profilo</a></li>
                                        <li><a data-toggle="tab" href="#infoviaggi">Info Viaggi</a></li>
                                        <li><a data-toggle="tab" href="#qasuend">Info Suend</a></li>
                                        <li><a data-toggle="tab" href="#qaviaggio">Valutazione Viaggio</a></li>
                                    </ul>
                                    @if(session()->has('message'))
                                        <div class="alert alert-success">
                                            {{ session()->get('message') }}
                                        </div>
                                    @endif
                                    @if(session()->has('error'))
                                        <div class="alert alert-danger">
                                            {{ session()->get('error') }}
                                        </div>
                                    @endif
                                    <div class="tab-content" id="tabs_content_c">
                                        <div id="info" class="tab-pane fade in active">
                                            <div class="row">
                                                <div class="col-lg-8">
                                                    
                                                    <div class="form-group" style="display: none;">
                                                        <label for="reg_input">Codice</label>
                                                        <input type="text" id="codice" name="codice" class="form-control"
                                                               tabindex="1" disabled>
                                                    </div>

                                                    <div class="form-group">
                                                        <label for="reg_input">Nome</label>
                                                        <input type="text" id="nome" name="nome" class="form-control" tabindex="2">
                                                    </div>

                                                    <div class="form-group">
                                                        <label for="reg_input">Cognome</label>
                                                        <input type="text" id="cognome" name="cognome" class="form-control"
                                                               tabindex="1">
                                                    </div>
                                                    

                                                    <div class="form-group">
                                                        <label for="reg_input">Data di Nascita</label>
                                                        <input type="text" id="info_data_nascita" name="info_data_nascita"
                                                               class="form-control" tabindex="3">
                                                    </div>

                                                    <div class="form-group">
                                                        <label for="reg_input">Sesso</label>
                                                        <select id="info_sesso" name="info_sesso" class="form-control"
                                                                tabindex="4">
                                                            <option value="MASCHIO">MASCHIO</option>
                                                            <option value="FEMMINA">FEMMINA</option>
                                                        </select>
                                                    </div>

                                                    <div class="form-group">
                                                        <label for="reg_input">Indirizzo</label>
                                                        <input type="text" id="info_indirizzo" name="info_indirizzo"
                                                               class="form-control" tabindex="5">
                                                    </div>

                                                    <div class="form-group">
                                                        <label for="reg_input">Citta'</label>
                                                        <input type="text" id="citta" name="citta" class="form-control" tabindex="6" placeholder="Inserisci citta">
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="reg_input">Cap</label>
                                                        <input type="text" id="cap" name="cap" class="form-control" tabindex="7">

                                                    </div>
                                                    <div class="form-group">
                                                        <label for="reg_input">Provincia</label>
                                                        <input type="text" id="provincia" name="provincia" class="form-control" tabindex="8">
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="reg_input">Regione</label>
                                                        <input type="text" id="regione" name="regione" class="form-control" tabindex="9">

                                                    </div>
                                                    <div class="form-group">
                                                        <label for="reg_input">Stato</label>
                                                        <input type="text" id="stato" name="stato" class="form-control"
                                                               tabindex="10">
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="reg_input">Telefono Principale</label>
                                                        <input type="text" id="tel1" name="tel1" class="form-control" tabindex="11">
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="reg_input">Telefono Secondario</label>
                                                        <input type="text" id="tel2" name="tel2" class="form-control" tabindex="12">
                                                    </div>

                                                    <div class="form-group">
                                                        <label for="reg_input">Email Principale</label>
                                                        <input type="text" id="email1" name="email1" class="form-control"
                                                               tabindex="13">
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="reg_input">Email Secondaria</label>
                                                        <input type="text" id="info_mail2" name="info_mail2" class="form-control"
                                                               tabindex="14">
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="reg_input">Codice Fiscale</label>
                                                        <input type="text" id="info_codice_fiscale" name="info_codice_fiscale"
                                                               class="form-control" tabindex="15">

                                                    </div>
                                                    <select type="text" id="status" name="status" class="form-control">
                                                        <option selected disabled>Stato</option> 
                                                        <option>contattato - SMS </option>
                                                        <option>contattato - Recall  </option>
                                                        <option>contattato - Visita Diretta  </option>
                                                        <option>da risentire</option>
                                                        <option>interessato</option>
                                                        <option>non interessato</option>
                                                        <option>cliente suend</option>
                                                    </select>
                                                </div>
                                            </div>
                                           
                                        </div>
                                        <div id="infoviaggi" class="tab-pane fade">
                                            <div class="row">
                                                <div class="col-lg-8">
                                                    <div class="form-group">
                                                        <label for="reg_input">Vacanze/Anno</label>
                                                        <input type="text" id="infoviaggi_vacanzeanno" name="infoviaggi_vacanzeanno"
                                                               class="form-control" tabindex="16">
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="reg_input">Preferenza Viaggi</label>
                                                        <input type="text" id="infoviaggi_preferenze" name="infoviaggi_preferenze" tabindex="17"
                                                               class="form-control">
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="reg_input">Durata Media</label>
                                                        <input type="text" id="infoviaggi_duratamedia" name="infoviaggi_duratamedia" tabindex="18"
                                                               class="form-control">
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="reg_input">Prossimi Paesi</label>
                                                        <input type="text" id="infoviaggi_prossimipaesi" name="infoviaggi_prossimipaesi" tabindex="19"
                                                               class="form-control">
                                                    </div>

                                                    <div class="form-group">
                                                        <label for="reg_input">Fonti</label>
                                                        <input type="text" id="infoviaggi_fonti"
                                                               name="infoviaggi_fonti"
                                                               class="form-control">
                                                    </div>


                                                </div>
                                                <div class="col-lg-4">
                                                    <div class="col-lg-2">
                                                        <label>Mesi Preferiti</label>
                                                        <div class="checkbox">
                                                            <label>
                                                                <input type="checkbox" name="infoviaggi_gennaio" value="1"  tabindex="20">
                                                                Gennaio
                                                            </label>
                                                        </div>
                                                        <div class="checkbox">
                                                            <label>
                                                                <input type="checkbox" name="infoviaggi_febbraio" value="1"  tabindex="21">
                                                                Febbraio
                                                            </label>
                                                        </div>
                                                        <div class="checkbox">
                                                            <label>
                                                                <input type="checkbox" name="infoviaggi_marzo" value="1"  tabindex="22">
                                                                Marzo
                                                            </label>
                                                        </div>
                                                        <div class="checkbox">
                                                            <label>
                                                                <input type="checkbox" name="infoviaggi_aprile" value="1"  tabindex="23">
                                                                Aprile
                                                            </label>
                                                        </div>
                                                        <div class="checkbox">
                                                            <label>
                                                                <input type="checkbox" name="infoviaggi_maggio" value="1"  tabindex="24">
                                                                Maggio
                                                            </label>
                                                        </div>
                                                        <div class="checkbox">
                                                            <label>
                                                                <input type="checkbox" name="infoviaggi_giugno" value="1"  tabindex="25">
                                                                Giugno
                                                            </label>
                                                        </div>


                                                        <div class="checkbox">
                                                            <label>
                                                                <input type="checkbox" name="infoviaggi_luglio" value="1"   tabindex="26">
                                                                Luglio
                                                            </label>
                                                        </div>
                                                        <div class="checkbox">
                                                            <label>
                                                                <input type="checkbox" name="infoviaggi_agosto" value="1"   tabindex="27">
                                                                Agosto
                                                            </label>
                                                        </div>
                                                        <div class="checkbox">
                                                            <label>
                                                                <input type="checkbox" name="infoviaggi_settembre" value="1"   tabindex="28">
                                                                Settembre
                                                            </label>
                                                        </div>
                                                        <div class="checkbox">
                                                            <label>
                                                                <input type="checkbox" name="infoviaggi_ottobre" value="1"   tabindex="29">
                                                                Ottobre
                                                            </label>
                                                        </div>
                                                        <div class="checkbox">
                                                            <label>
                                                                <input type="checkbox" name="infoviaggi_novembre" value="1"   tabindex="30">
                                                                Novembre
                                                            </label>
                                                        </div>
                                                        <div class="checkbox">
                                                            <label>
                                                                <input type="checkbox" name="infoviaggi_dicembre" value="1"   tabindex="31">
                                                                Dicembre
                                                            </label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-lg-8">
                                                    <div class="col-lg-4">
                                                        <label>Tematiche</label>
                                                        <div class="checkbox">
                                                            <label>
                                                                <input type="checkbox" name="infoviaggi_archeologia" value="1"   tabindex="32">
                                                                Archeologia
                                                            </label>
                                                        </div>
                                                        <div class="checkbox">
                                                            <label>
                                                                <input type="checkbox" name="infoviaggi_avventura" value="1"   tabindex="33">
                                                                Avventura
                                                            </label>
                                                        </div>
                                                        <div class="checkbox">
                                                            <label>
                                                                <input type="checkbox" name="infoviaggi_cultura" value="1"   tabindex="34">
                                                                Cultura
                                                            </label>
                                                        </div>
                                                        <div class="checkbox">
                                                            <label>
                                                                <input type="checkbox" name="infoviaggi_ecoturismo" value="1"  tabindex="35">
                                                                Ecoturismo
                                                            </label>
                                                        </div>

                                                    </div>
                                                    <div class="col-lg-4">
                                                        <div class="checkbox">
                                                            <label>
                                                                <input type="checkbox" name="infoviaggi_enogastronomia" value="1"   tabindex="36">
                                                                Enogastronomia
                                                            </label>
                                                        </div>
                                                        <div class="checkbox">
                                                            <label>
                                                                <input type="checkbox" name="infoviaggi_fotografico" value="1"   tabindex="37">
                                                                Fotografico
                                                            </label>
                                                        </div>
                                                        <div class="checkbox">
                                                            <label>
                                                                <input type="checkbox" name="infoviaggi_marerelax" value="1"  tabindex="38">
                                                                Mare e relax
                                                            </label>
                                                        </div>
                                                        <div class="checkbox">
                                                            <label>
                                                                <input type="checkbox" name="infoviaggi_montagna" value="1"  tabindex="39">
                                                                Montagna
                                                            </label>
                                                        </div>

                                                    </div>
                                                    <div class="col-lg-4">
                                                        <div class="checkbox">
                                                            <label>
                                                                <input type="checkbox" name="infoviaggi_religioso" value="1"   tabindex="40">
                                                                Religioso
                                                            </label>
                                                        </div>
                                                        <div class="checkbox">
                                                            <label>
                                                                <input type="checkbox" name="infoviaggi_sportivo" value="1"   tabindex="41">
                                                                Sportivo
                                                            </label>
                                                        </div>
                                                        <div class="checkbox">
                                                            <label>
                                                                <input type="checkbox" name="infoviaggi_storicopolitico" value="1" tabindex="42">
                                                                Storico Politico
                                                            </label>
                                                        </div>
                                                        <div class="checkbox">
                                                            <label>
                                                                <input type="checkbox" name="infoviaggi_altro" value="1" tabindex="43">
                                                                Altro
                                                            </label>
                                                        </div>

                                                    </div>
                                                </div>
                                                <div class="col-lg-4">
                                                    <textarea cols="30" rows="5" class="form-control input-sm" name="infoviaggi_tematiche_altro" tabindex="44"
                                                              placeholder="Altre tematiche..."></textarea>
                                                </div>


                                            </div>
                                            <div class="row">
                                                <div class="col-lg-8">
                                                    <div class="form-group">
                                                        <label for="reg_input">Destinazioni Preferite</label>
                                                        <input type="text" id="infoviaggi_destinazionipreferite" tabindex="45"
                                                               name="infoviaggi_destinazionipreferite"
                                                               class="form-control" tabindex="11">
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="reg_input">Destinazioni Passate</label>
                                                        <input type="text" id="infoviaggi_destinazionipassate" tabindex="46"
                                                               name="infoviaggi_destinazionipassate"
                                                               class="form-control" tabindex="11">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div id="qasuend" class="tab-pane fade">
                                            <div class="row">

                                                <div class="col-lg-12">
                                                    <div class="form-group">
                                                        <label for="reg_input">Conoscenza Suend</label>
                                                        <input type="text" id="qasuend_conoscenza" name="qasuend_conoscenza"   tabindex="47"
                                                               class="form-control">
                                                    </div>
                                                </div>

                                                <div class="col-lg-12">
                                                    <div class="form-group">
                                                        <label for="reg_input">Altre motivazioni conoscenza
                                                            Suend</label>
                                                        <input type="text" id="qasuend_altremotivazione" name="qasuend_altremotivazione"   tabindex="48"
                                                               class="form-control">
                                                    </div>
                                                </div>

                                            </div>
                                            <div class="row">
                                                <div class="col-lg-2">
                                                    <div class="form-group">
                                                        <label for="reg_input" style="text-align: center;">Già viaggiato
                                                            con noi</label>
                                                        <input type="checkbox" class="form-control input-sm"   tabindex="49"
                                                               name="qasuend_giaviaggiato" id="qasuend_giaviaggiato" value="true">
                                                    </div>
                                                </div>
                                                <div class="col-lg-2">
                                                    <div class="form-group">
                                                        <label for="reg_input">Quante Volte</label>
                                                        <input type="text" id="qasuend_quantiviaggi" name="qasuend_quantiviaggi"   tabindex="50"
                                                               class="form-control"
                                                               tabindex="0">
                                                    </div>
                                                </div>
                                                <div class="col-lg-8">
                                                    <div class="form-group">
                                                        <label for="reg_input">Dove</label>
                                                        <input type="text" id="qasuend_doveviaggi" name="qasuend_doveviaggi"   tabindex="51"
                                                               class="form-control"
                                                               tabindex="0">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-lg-2">
                                                    <div class="form-group">
                                                        <label for="reg_input" style="text-align: center;">Conosce
                                                            Sito</label>
                                                        <input type="checkbox" class="form-control input-sm"   tabindex="52"
                                                               name="qasuend_conoscesito" id="qasuend_conoscesito" value="true">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-lg-12">
                                                    <div class="form-group">
                                                        <label for="reg_input">Numero buono</label>
                                                        <input type="text" class="form-control" tabindex="52" name="qasuend_numero_buono" id="qasuend_numero_buono">
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="reg_input">Tipo buono</label>
                                                        <input type="text" class="form-control" tabindex="52" name="qasuend_tipo_buono" id="qasuend_tipo_buono">
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="reg_input">Importo buono</label>
                                                        <input type="text" class="form-control" tabindex="52" name="qasuend_importo_buono" id="qasuend_importo_buono">
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-lg-12">
                                                    <div class="form-group">
                                                        <label for="reg_input">Note</label>
                                                        <textarea name="qasuend_note" id="qasuend_note" cols="10" rows="3"   tabindex="53"
                                                                  class="form-control"></textarea>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div id="qaviaggio" class="tab-pane fade">
                                            <div class="row">
                                                <div class="col-lg-12">
                                                    <div class="form-group">
                                                        <label for="reg_input">Soddisfazione Generale</label>
                                                        <input type="text" id="qaviaggio_soddisfazione" name="qaviaggio_soddisfazione"   tabindex="54"
                                                               class="form-control">
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="reg_input">Data Partenza</label>
                                                        <input class="form-control ts_datepicker" type="text"
                                                               name="qaviaggio_datapartenza" id="qaviaggio_datapartenza"   tabindex="55"
                                                               placeholder="gg/mm/aaaa">
                                                    </div>

                                                    <div class="form-group">
                                                        <label for="reg_input">Destinazione</label>
                                                        <input type="text" id="qaviaggio_destinazione" name="qaviaggio_destinazione"   tabindex="56"
                                                               class="form-control">
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="reg_input">Storico</label>
                                                        <textarea cols="30" name="qaviaggio_storico" id="qaviaggio_storico" rows="3"   tabindex="57"
                                                                  class="form-control input-sm"></textarea>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div id="profilo" class="tab-pane fade">
 
                                            <div class="row">
                                                <div class="col-md-8">

                                                <div class="form-group">
                                                    <label>Luogo di nascita</label>
                                                    <input type="text" id="luogonascita" name="luogonascita"   tabindex="58" class="form-control">                                                   
                                                </div>

                                                    <div class="form-group">
                                                        <label>Professione</label>
                                                        <input type="text" id="profilo_professione" name="profilo_professione"   tabindex="58"
                                                               class="form-control">
                                                    </div>
                                                    <div class="form-group">
                                                        <label>Hobby 1</label>
                                                        <input type="text" id="href="#profilo"_hobby1" name="profilo_hobby1"   tabindex="59"
                                                               class="form-control">
                                                    </div>
                                                    <div class="form-group">
                                                        <label>Hobby 2</label>
                                                        <input type="text" id="profilo_hobby2" name="profilo_hobby2"   tabindex="60"
                                                               class="form-control">
                                                    </div>
                                                    <div class="form-group">
                                                        <label>Numero Carta Identita</label>
                                                        <input class="form-control ts_datepicker" type="text"
                                                               name="profilo_cartaidentita" id="profilo_cartaidentita" tabindex="61">
                                                    </div>
                                                    <div class="form-group">
                                                        <label>Scadenza Carta identita'</label>
                                                        <input class="form-control ts_datepicker" type="text"
                                                               name="profilo_datascadenzaidentita" id="profilo_datascadenzaidentita" tabindex="62"
                                                               placeholder="gg/mm/aaaa">
                                                    </div>
                                                    <div class="form-group">
                                                        <label>Passaporto</label>
                                                        <input class="form-control ts_datepicker" type="text"
                                                               name="profilo_passaporto" id="profilo_passaporto" tabindex="63">
                                                    </div>
                                                    <div class="form-group">
                                                        <label>Scadenza Passaporto</label>
                                                        <input class="form-control ts_datepicker" type="text"
                                                               name="profilo_datascadenzapassaporto" id="profilo_datascadenzapassaporto" tabindex="64"
                                                               placeholder="gg/mm/aaaa">
                                                    </div>
                                                    <div class="form-group">
                                                        <div class="checkbox">
                                                            <label>
                                                                <input type="checkbox" name="profilo_newsletter" tabindex="65" value="true">
                                                                Newsletter
                                                            </label>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <div class="checkbox">
                                                            <label>
                                                                <input type="checkbox" name="profilo_clientediretto" tabindex="66" value="true">
                                                                Cliente diretto
                                                            </label>
                                                        </div>
                                                    </div>

                                                </div>
                                                <div class="col-md-4">
                                                    <br>
                                                    <div class="form-group">
                                                        <label>Frequent Flyer</label>
                                                        <textarea cols="30" rows="20" class="form-control input-sm" name="profilo_frequentflyer" tabindex="67"
                                                                  placeholder=""></textarea>
                                                    </div>
                                                </div>

                                            </div>
                                            <div class="row">
                                                <div class="col-md-8">
                                                    <label>Formazione Nucleo Familiare</label>
                                                    <br>
                                                    <div class="col-md-2">

                                                        <label>Adulti</label>
                                                        <input type="text" id="profilo_adulti" name="profilo_adulti" tabindex="68"
                                                               class="form-control">
                                                    </div>
                                                    <div class="col-md-2">
                                                        <label>Bambini</label>
                                                        <input type="text" id="profilo_bambini" name="profilo_bambini" tabindex="69"
                                                               class="form-control">
                                                    </div>
                                                    <div class="col-md-2">
                                                        <label>Neonati</label>
                                                        <input type="text" id="profilo_neonati" name="profilo_neonati" tabindex="70"
                                                               class="form-control">
                                                    </div>
                                                    <div class="col-md-2">

                                                        <label>Figli</label>
                                                        <input type="text" id="profilo_figli" name="profilo_figli" tabindex="71"
                                                               class="form-control">
                                                    </div>
                                                    <div class="col-md-2">
                                                        <label>Anziani</label>
                                                        <input type="text" id="profilo_anziani" name="profilo_anziani" tabindex="72"
                                                               class="form-control">
                                                    </div>
                                                    <div class="col-md-2">
                                                        <label>Animali</label>
                                                        <input type="text" id="profilo_animali" name="profilo_animali" tabindex="73"
                                                               class="form-control">
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label>Note</label>
                                                        <textarea cols="30" rows="5" class="form-control input-sm" name="profilo_note"  tabindex="74"
                                                                  placeholder=""></textarea>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-8">
                                                    <div class="col-md-2">
                                                        <div class="form-group">
                                                            <div class="checkbox">
                                                                <label>
                                                                    <input type="checkbox" value="true" name="profilo_iscrittoclub"  tabindex="75">
                                                                    Iscritto Club
                                                                </label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-10">
                                                        <div class="form-group">
                                                            <label>Codice Club</label>
                                                            <input type="text" id="profilo_codiceclub" name="profilo_codiceclub"  tabindex="76"
                                                                   class="form-control">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <button class="btn btn-lg btn-success btn-block">Aggiungi Cliente</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

@endsection
@section('footer-plugin')
    <script src="{{ asset('/lib/bootstrap-datepicker/js/bootstrap-datepicker.js') }}"></script>



    <script type="text/javascript">
        $(document).ready(function () {

            ResetForm();
            ResetFormInfo();

            function ResetForm() {
                $("#provincia").val('');
                $("#cap").val('');
                $("#regione").val('');
                $("#stato").val('');
            }


            function ResetFormInfo() {
                $("#info-provincia2").val('');
                $("#info-cap2").val('');
                $("#info-regione2").val('');
                $("#info-stato2").val('');
            }



            $("#citta").focus(function() {
                ResetForm();
            });

            $("#info-citta2").focus(function() {
                ResetFormInfo();
            });


            var path = "{{ route('autocomplete') }}";
            var engine = new Bloodhound({
                datumTokenizer: Bloodhound.tokenizers.whitespace('username'),
                queryTokenizer: Bloodhound.tokenizers.whitespace,
                remote:{
                    url: path + '?term=%QUERY',
                    wildcard: '%QUERY'
                }
            });

            engine.initialize();

            $("#citta").typeahead({
                hint: true,
                highlight: true,
                minLength: 2
            }, {
                source: engine.ttAdapter(),
                name: 'comune',
                displayKey: 'comune',
                templates: {
                    empty: [
                        '<div class="empty-message">Nessun risultato</div>'
                    ].join('\n'),

                    suggestion: function (data) {
                        return '<div class="citta-search-result"><h4>'+ data.comune +'</h4></div>'
                    }
                },

            });
            var lastAvaibleId=Number('<?php echo $lastId?>');

            $('#citta').bind('typeahead:selected', function(obj, datum, name) {
                $("#provincia").val(datum.provincia);
                $("#cap").val(datum.cap);
                $("#regione").val(datum.regione);
                $("#stato").val(datum.stato);
                $("#codice").val(datum.sigla+"-"+lastAvaibleId);
            });



            $("#info-citta2").typeahead({
                hint: true,
                highlight: true,
                minLength: 2
            }, {
                source: engine.ttAdapter(),
                // This will be appended to "tt-dataset-" to form the class name of the suggestion menu.
                name: 'comune',
                // the key from the array we want to display (name,id,email,etc...)
                displayKey: 'comune',
                templates: {
                    empty: [
                        '<div class="empty-message">Nessun risultato</div>'
                    ].join('\n'),

                    suggestion: function (data) {
                        return '<div class="citta2-search-result"><h4>'+ data.comune +'</h4></div>'
                    }
                },

            });

            $("#info-citta2").bind('typeahead:selected', function(obj, datum, name) {

                $("#info-provincia2").val(datum.provincia);
                $("#info-cap2").val(datum.cap);
                $("#info-regione2").val(datum.regione);
                $("#info-stato2").val(datum.stato);
            });
        });

        $(document).ready(function(){
            $('input').css('text-transform','uppercase');
        })
    </script>
@endsection

