@extends('layouts.master')
@section('header-page-personalization')


    <link href="https://fonts.googleapis.com/css?family=Montserrat|Montserrat+Subrayada|helvetica+Display" rel="stylesheet">
    <style>
        .pdf_body{

        }
        @media print
        {
            * {-webkit-print-color-adjust:exact;}
        }
        #preview #output{

        }

        .twitter-typeahead, .tt-hint, .tt-input, .tt-menu { width: 100%;  }
        .tt-menu {
            background: #ffffff;
        }
        .applyStatus{
            float: right;
        }
        .pdfpage{

        }
        [name="notaStato"]{
            width: 90%;
            height: 150px;
        }
        textarea{
            padding: 0px;
        }
        [name="nota"]{
            width: 90%;
            height: 150px;
        }
        table{
            text-align: center!important;
        }
        th{
            width: 150px;
            text-align: center!important;
        }
        #taskForm{
            clear: both;
            display: none;
        }


    </style>

@endsection
@section('content')


    <div class="page_content page_preventivo">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <form id="login_form" role="form" method="POST" action="{{ route('preventivo.store') }}" class="customForm">
                    {{ csrf_field() }}
                    <input type='hidden' name='prev_id' value="" old="true">
                        <div class="panel panel-default">
                            <div class="panel-body">
                                <div class="heading_a"><legend><span class="big-title">INSERIMENTO PREVENTIVO</span></legend></div>
                                <div class="tabbable tabs-left">
                                    <ul class="nav nav-tabs" id="tabs_c">
                                        <li class="active"><a data-toggle="tab" href="#richiesta">Richiesta</a></li>
                                        <li><a data-toggle="tab" href="#partecipanti">Partecipanti viaggio</a></li>
                                        <li><a data-toggle="tab" href="#destinazioni">Modifica destinazioni</a></li>

                                        <?php  if (Auth::user()->admin) {
    ?>
                                            <li><a id="qui" target="_blank" href="/admin/tasks?action=linkto&ent=preventivo&n=#">Tasks</a></li>
                                        <?php

} else {
    ?>
                                            <li><a id="qui" target="_blank" href="/user/tasks?action=linkto&ent=preventivo&n=#">Tasks</a></li>
                                        <?php

} ?>

                                        <li id="editScheda" style="display: none;">
                                            <a href="/admin/preventivo/edit/"><i class="glyphicon glyphicon-edit"></i>&nbsp;Modifica</a>
                                        </li>
                                        <li><a data-toggle="tab" href="#coupon">Coupon / Gift Card</a></li>
                                        <li><a data-toggle="tab" href="#preview">Genera PDF</a></li>
                                    </ul>
                                    @if(session()->has('message'))
                                        <div class="alert alert-success">
                                            {{ session()->get('message') }}
                                        </div>
                                    @endif
                                    @if(session()->has('error'))
                                        <div class="alert alert-danger">
                                            {{ session()->get('error') }}
                                        </div>
                                    @endif
                                    <div class="tab-content" id="tabs_content_c">
                                        <div id="richiesta" class="tab-pane fade in active">
                                            <div class="row">
                                                <div class="col-lg-6">

                                                    <div class="form-group ui-widget">
                                                        <label for="reg_input">Cliente richiedente</label>
                                                        <input type="text" id="preventivo_riferitoA" class="form-control">
                                                        <input type="hidden" name="preventivo_riferitoA" value="0">
                                                        <div id="privati-message" style="color:red;"></div>
                                                    </div>

                                                    <div class="form-group">
                                                        <label for="reg_input">Destinazione richiesta</label>
                                                            <select type="text" id="status" name="preventivo_destinazioneRichiesta" class="form-control">
                                                                <option selected disabled value="">Scegli una destinazione</option>
                                                                @foreach($destinazioni as $destinazione)
                                                                    <option value="{{ $destinazione->id }}">{{ $destinazione->destinazione }}</option>
                                                                @endforeach
                                                            </select>
                                                            Se non trovi la destinazione nell'elenco, clicca
                                                        <a href="#" class="" onclick="goDest()">
                                                           qui
                                                        </a>
                                                        per aggiungerla
                                                    </div>

                                                    <div class="form-group">
                                                        <label for="reg_input">Tipologia di viaggio</label>
                                                            <select type="text" id="status" name="preventivo_TipologiaDiViaggio" class="form-control">
                                                                <option selected disabled value="">Scegli una tipologia</option>
                                                                <option value="solo hotel">solo hotel</option>
                                                                <option value="solo treno">solo treno</option>
                                                                <option value="solo volo">solo volo</option>
                                                                <option value="volo + hotel">volo + hotel</option>
                                                                <option value="tour">tour</option>
                                                                <option value="combinato">combinato</option>
                                                                <option value="noleggio auto">noleggio auto</option>
                                                                <option value="solo trasferimento">solo trasferimento</option>
                                                                <option value="traghetto">traghetto</option>
                                                                <option value="crociera">crociera</option>
                                                            </select>
                                                    </div>


                                                    <div class="form-group">
                                                        <label for="reg_input">Scadenza preventivo</label>
                                                        <input type="date" id="preventivo_scadenza" name="preventivo_scadenza" class="form-control"  tabindex="1" placeholder="dd-mm-yyyy">
                                                    </div>


                                                    <div class="form-group">
                                                        <label for="reg_input">Data del Viaggio</label>
                                                        <label for="preventivo_dal">Dal</label>
                                                        <input type="date" id="preventivo_dal" name="preventivo_dal" class="form-control" tabindex="2" placeholder="GG/MM/AAAA">
                                                        <label for="preventivo_al">Al</label>
                                                        <input type="date" id="preventivo_al" name="preventivo_al" class="form-control" tabindex="2" placeholder="GG/MM/AAAA">
                                                    </div>

                                                    <div class="form-group">
                                                        <label for="reg_input">Sistemazione</label>
                                                            <select type="text" id="status" name="preventivo_sistemazione" class="form-control">
                                                                <option selected disabled value="">Scegli una sistemazione</option>
                                                                <option value="1*">1*</option>
                                                                <option value="2*">2*</option>
                                                                <option value="3*">3*</option>
                                                                <option value="4*">4*</option>
                                                                <option value="5*">5*</option>
                                                                <option value="appartamento">appartamento</option>
                                                                <option value="non selezionato">non selezionato</option>
                                                            </select>
                                                    </div>

                                                    <div class="form-group">
                                                        <label for="reg_input">Trattamento</label>
                                                            <select type="text" id="status" name="preventivo_trattamento" class="form-control">
                                                                <option selected disabled value="">Scegli un trattamento</option>
                                                                <option value="solo pernottamento">solo pernottamento</option>
                                                                <option value="pernottamento e colazione">pernottamento e colazione</option>
                                                                <option value="mezza pensione">mezza pensione</option>
                                                                <option value="pensione completa">pensione completa</option>
                                                                <option value="all inclusive">all inclusive</option>
                                                                <option value="non selezionato">non selezionato</option>
                                                            </select>
                                                    </div>


                                                    <div class="form-group">
                                                        <label for="reg_input">Prezzo totale preventivo €</label>
                                                        <input type="text" id="cognome" name="preventivo_prezzoTotalePreventivo" class="form-control"
                                                               tabindex="1" value="0.00">
                                                    </div>

                                                    <div class="form-group">
                                                        <label for="reg_input">Tempo impiegato per l’elaborazione del preventivo</label>
                                                            <select type="text" id="status" name="preventivo_TempoElaborazione" class="form-control">
                                                                <option selected disabled value="">Scegli una tempistica</option>
                                                                <option value="meno di 10 minuti">meno di 10 minuti</option>
                                                                <option value="tra 10 e 20 minuti">tra 10 e 20 minuti</option>
                                                                <option value="30 minuti">30 minuti</option>
                                                                <option value="tra 40 e 50 minuti">tra 40 e 50 minuti</option>
                                                                <option value="1 ora">1 ora</option>
                                                                <option value="più di 1 ora">più di 1 ora</option>
                                                                <option value="mezza giornata">mezza giornata</option>
                                                                <option value="1 giorno">1 giorno</option>
                                                                <option value="più di 1 giorno">più di 1 giorno</option>

                                                            </select>
                                                    </div>

                                                    <div class="form-group">
                                                        <label for="reg_input">Feedback cliente</label>
                                                            <select type="text" id="status" name="preventivo_feedbackCliente" class="form-control">
                                                                <option selected disabled value="">Scegli una tempistica</option>
                                                                <option value="cambio preventivo">cambio preventivo</option>
                                                                <option value="cambio destinazione">cambio destinazione</option>
                                                                <option value="cambio data">cambio data</option>
                                                                <option value="supero budget">supero budget</option>
                                                                <option value="non più interessato">non più interessato</option>
                                                            </select>
                                                    </div>

                                                    <div class="form-group">
                                                        <label for="reg_input">Stato preventivo</label>
                                                            <select type="text" id="status" name="preventivo_statoPreventivo" class="form-control">
                                                                <option selected disabled value="">Scegli uno stato</option>
                                                                <option value="inviato via mail">inviato via mail</option>
                                                                <option value="consegnato in agenzia">consegnato in agenzia</option>
                                                                <option value="ricontattato telefonicamente">ricontattato telefonicamente</option>
                                                                <option value="ricontattato via mail">ricontattato via mail</option>
                                                                <option value="ricontattato via SMS/Whatsapp">ricontattato via SMS/Whatsapp</option>
                                                                <option value="preventivo confermato">preventivo confermato</option>
                                                                <option value="preventivo non confermato">preventivo non confermato</option>
                                                                <option value="in lavorazione">in lavorazione</option>
                                                            </select>
                                                    </div>

                                                <div class="col-lg-12">
                                                    <label for="reg_input">Note generali preventivo</label>
                                                    <textarea cols="30" rows="5" class="form-control input-sm" id="wysiwg_full" name="preventivo_valutazione" tabindex="44" placeholder="Note generali..."></textarea>
                                                </div>

                                                </div>



                                                <div class="col-lg-6" id="storicoAzioni">

                                                    <div class="form-group">
                                                        <label for="reg_input">Prossime azioni</label>
                                                            <select id="newtStep" class="form-control">
                                                                <option disabled value="" selected="">Scegli la prossima azione per questo preventivo</option>
                                                                <option value="Recall">Recall</option>
                                                                <option value="Richiedere documenti">Richiedere documenti</option>
                                                                <option value="Richiedere pagamento">Richiedere pagamento</option>
                                                                <option value="Richiedere documenti/dati personali">Richiedere documenti/dati personali</option>
                                                            </select>
                                                    </div>

                                                    <div class="form-group">
                                                        <a href="#" class="btn btn-success btn-md applyStatus" onclick="addStatus()">
                                                          <span class="glyphicon glyphicon-plus"></span> Applica
                                                        </a>
                                                    </div>

                                                    <div class="form-group">
                                                        <!-- Modal -->
                                                        <div id="notaStato" class="modal fade" role="dialog">
                                                          <div class="modal-dialog">

                                                            <!-- Modal content-->
                                                            <div class="modal-content">
                                                              <div class="modal-header">
                                                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                                <h4 class="modal-title">Imposta la scadenza</h4>
                                                              </div>
                                                              <div class="modal-body">
                                                                <div class="form-group"><label class="col-md-4 control-label">Scadenza impegno</label></div>
                                                                <div class="form-group"><input type="date" id="scadenza" class="data form-control input-md"  placeholder="imposta la scadenza"></div>
                                                                <div class="form-group"><label class="col-md-4 control-label" >Note generiche?</label></div>
                                                                <div class="form-group"><input type="text" id="nota"  class="form-control input-md" placeholder="opzionale, aggiungi qui la tua nota"></div>
                                                              </div>
                                                              <div class="modal-footer">
                                                                <button type="button" class="btn btn-default" data-dismiss="modal" onclick="addStatusStep2()">Applica e chiudi</button>
                                                              </div>
                                                            </div>

                                                          </div>
                                                        </div>
                                                        <script type="text/javascript">

                                                             function goDest(){
                                                                $('[href="#destinazioni"]').click();
                                                            }

                                                            function goReq(){
                                                                $('[href="#richiesta"]').click();
                                                            }

                                                        </script>

                                                        <label for="reg_input">Storico azioni</label>
                                                        <listaStati>
                                                            @if(!$preventivoAzioni)
                                                               <p class="defaultMsg">Non sono state inserite azioni per questo preventivo</p>
                                                            @endif
                                                            @foreach($preventivoAzioni as $azione)
                                                                <div class="azioneContainer">
                                                                    <div class="liStati col-md-8">
                                                                       <p>Scadenza prevista: {{$azione->scadenza}}</p>
                                                                       <p>l'utente <strong>{{$azione->username}}</strong> il <strong>{{$azione->timer}}</strong> ha aggiornato lo stato in: <br><strong>{{$azione->azione}}</strong></p>

                                                                       @if($azione->nota)
                                                                        <p>Con la seguente nota: {{$azione->nota}}</p>
                                                                       @endif
                                                                       <p></p>
                                                                    </div>
                                                                    <div class="col-md-4">
                                                                    <input type="hidden" name="preventivoAzioni_azione_id[]" value="{{$azione->id}}">
                                                                    <input type="hidden" name="preventivoAzioni_eseguito_edit[]" value="0">
                                                                       @if($azione->eseguito)
                                                                            <input type="checkbox" onclick="foo(this)" checked>Eseguito?
                                                                        @else
                                                                            <input type="checkbox" onclick="foo(this)" >Eseguito?
                                                                       @endif<br><br>
                                                                    <a href="#" class="btn btn-xs btn-danger news-zama" style="margin-left: 10px;" data-id="{{$azione->id}}" data-action="container" data-table="preventivoAzioni" data-parent=".azioneContainer" onclick="cleanDeleteRecord(this)"><i class="glyphicon glyphicon-minus"></i>&nbsp;Elimina</a>
                                                                    </div>
                                                                </div>

                                                            @endforeach
                                                        </listaStati>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>

                                        <div id="partecipanti" class="tab-pane fade">
                                            <div class="row">
                                                <div class="col-lg-12">
                                                    <div class="form-group">
                                                        <table id="partab" style="display: none;">
                                                            <thead>
                                                                <th style="display: none;"></th>
                                                                <th>Partecipanti</th>
                                                                <th>Num</th>
                                                                <th>Età?</th>
                                                                <th>Azioni</th>
                                                            </thead>

                                                            <tbody>
                                                                @foreach($preventivoPartecipanti as $partecipante)

                                                                <tr>
                                                                    <td style="display: none;">{{ $partecipante->id }} <input type="hidden" name="preventivoPartecipanti_id[]" value="{{ $partecipante->id }}"></td>

                                                                    <td>
                                                                        <select name='preventivoPartecipanti_tipo_edit[]' skip>
                                                                            <option value='adulti' <?php if ($partecipante->tipo=="adulti") {
    echo "selected";
}?>>adulti</option>
                                                                            <option value='ragazzi' <?php if ($partecipante->tipo=="ragazzi") {
    echo "selected";
}?>>ragazzi</option>
                                                                            <option value='bambini' <?php if ($partecipante->tipo=="bambini") {
    echo "selected";
}?>>bambini</option>
                                                                            <option value='neonati' <?php if ($partecipante->tipo=="neonati") {
    echo "selected";
}?>>neonati</option>
                                                                        </select>
                                                                    </td>
                                                                    <td><input type='text' name='preventivoPartecipanti_numero_edit[]' placeholder='numero di partecipanti' value="{{ $partecipante->numero }}"></td>
                                                                    <td><input type='text' name='preventivoPartecipanti_eta_edit[]' placeholder='eta' value="{{ $partecipante->eta }}"></td>
                                                                    <td><a href="#" class="btn btn-xs btn-danger news-zama" style="margin-left: 10px;" onclick="deleteRecord(this,['preventivoPartecipanti'])"><i class="glyphicon glyphicon-minus"></i>&nbsp;Elimina</a></td>
                                                                </tr>
                                                                @endforeach
                                                            </tbody>
                                                        </table>

                                                    </div>
                                                    <div>

                                                    </div>
                                                </div>

                                                <div class="col-lg-12">
                                                    <div class="form-group">

                                                        <a href="#" class="btn btn-success btn-md applyStatus" onclick="addPeople()">
                                                          <span class="glyphicon glyphicon-plus"></span> Aggiungi un partecipante
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div id="destinazioni" class="tab-pane fade">

                                            <div class="row">
                                                <div class="col-lg-12">
                                                    <div class="form-group col-lg-6">
                                                        <table id="partab2">
                                                            <thead>
                                                                <th style="display: none;"></th>
                                                                <th style="padding-bottom: 10px;">Destinazione</th>
                                                                <th style="padding-bottom: 10px;">E' visibile?</th>
                                                                <th style="padding-left: 10px; padding-bottom: 10px;"></th>
                                                            </thead>

                                                            <tbody>
                                                                @foreach($destinazioni as $destinazione)

                                                                <tr>
                                                                    <td style="display: none;">{{ $destinazione->id }} <input type="hidden" value="{{ $destinazione->id }}"></td>

                                                                    <td>
                                                                        {{ $destinazione->destinazione }}
                                                                    </td>

                                                                    <td style="text-align: center;">
                                                                    <?php if ($destinazione->visibile) {
    echo '<input type="checkbox" id="dest_visibile" checked>';
} else {
    echo '<input type="checkbox" id="dest_visibile">';
}
                                                                    ?>

                                                                    </td>

                                                                    <td><a href="#" class="btn btn-xs btn-danger news-zama" style="margin-left: 10px;"  data-action="container" data-table="destinazioni" data-parent="tr" data-id="{{ $destinazione->id }}" onclick="cleanDeleteRecord(this)"><i class="glyphicon glyphicon-minus"></i>&nbsp;Elimina</a>
                                                                    </td>
                                                                </tr>
                                                                @endforeach
                                                            </tbody>
                                                        </table>

                                                    </div>

                                                        <div class="form-group col-lg-6">
                                                            <label for="reg_input">Aggiungi una destinazione</label>
                                                            <input type="text" name="dest_label" class="form-control" tabindex="2" style="text-transform: uppercase;" placeholder="Destinazione">
                                                            <p>

                                                            Clicca
                                                            <a href="#" class="" onclick="goReq()">qui</a>
                                                            per tornare indietro
                                                            </p>
                                                            <p><a href="#" class="btn btn-primary btn-md" onclick="addAjaxFieldWrapper()">Aggiungi</a> </p>
                                                        </div>
                                                        <div id="no_reload" style="display: none;">
                                                            La destinazione è stata aggiunta, si prega di ricarica la pagina per caricare i nuovi risultati sul menu destinazioni
                                                        </div>
                                                    <div>

                                                    </div>
                                                </div>

                                            </div>
                                        </div>


                        <div id="preview" class="tab-pane fade">
	                        <div id="" class="col-sm-12" style="margin-left: 70px;margin-bottom: 20px;">
                                <button type="button" class="pull-left btn btn-lg btn-success btn-block" style="width: 150px!important;z-index:10000;" onclick="sendHtml()">Stampa <i class="fa fa-print fa-2x"></i></button>
	                        </div>

	                        <div id="pdfhtml" class="col-sm-12">
	                            <div id="output" class="pdfpage">
	                                @include('layouts.pdf_layout')
	                            </div>
	                        </div>


	                        <div id="hiddenoutput"></div>


                        </div>



                        <div id="coupon" class="tab-pane fade">
                            <style type="text/css">
                                #coupon tbody tr{
                                    border-bottom: 1px solid gray;
                                }
                            </style>
                            @if(empty($preventivo[0]->riferitoA))
                                <p><b>Attenzione!</b> Per utilizzare questa sezione scegliere un privato a cui associare la richiesta di preventivo e salvare la pagina per applicare le modifiche</p>
                            @endif

                            <legend>Gift Card</legend>
                            @if(empty($giftCard[0]))
                                <p><b>Non</b> sono presenti gift card assegnate a questo utente</p>
                            @endif
                            <table>
                            <thead>
                                <th>Riepilogo</th>
                                <th>Azioni</th>
                            </thead>  
                            <tbody>
                                 @foreach($giftCard as $card)
                                    <tr>
                                        <td> 
                                            <a href="{{route('card edit',['id' => $card->id])}}" target="blank"> Carta: {{$card->code}} </a><br>
                                            Creata il {{$card->created_at}}  <br>
                                            Scadenza {{$card->deadline}}  <br>
                                            Stato :<b>
                                            @if($card->deadline>date("Y-m-d"))
                                                Attiva
                                            @else
                                                Scaduta
                                            @endif 
                                            </b> <br>
                                            Credito speso : {{$card->spesa}} / {{$card->value}} <br>
                                        </td>
                                         @if($card->deadline>date("Y-m-d"))
                                            <td>
                                                @if($card->spesa == 0)
                                                    Questa offerta non è ancora stata utilizzata,puoi scegliere un importo da scalare al totale del preventivo fino ad un massimo di {{$card->value}} o utilizzare l'opzione sotto per applicarla in automatico <br>
                                                    <input type="number" class="form-control importo" name="importo" class="importo" min="0" max="{{$card->value - $card->spesa}}" placeholder="importo">
                                                    <input type="checkbox" id="applica" class="applicaOffertaAlPreventivo" onclick="toggleCardValue(this)" data-amount="{{$card->value - $card->spesa}}"> Applica per intero l'offerta al preventivo
                                                @else
                                                    @if($card->spesa < $card->value && $card->spesa>0)
                                                        Questa offerta è stata utilizzata parzialmente,puoi scegliere un importo da scalare al totale del preventivo fino ad un massimo di {{$card->value}} o utilizzare l'opzione sotto per applicarle il residuo in automatico<br>
                                                        <input type="number" class="form-control importo" name="importo" min="0" max="{{$card->value - $card->spesa}}" placeholder="importo">
                                                        <input type="checkbox" id="applica" class="applicaOffertaAlPreventivo" onclick="toggleCardValue(this)" data-amount="{{$card->value - $card->spesa}}"> Applica per intero l'offerta al preventivo
                                                    @endif
                                                    @if($card->value - $card->spesa == 0)
                                                        Questa offerta è ancora stata pienamente utilizzata<br>
                                                        <input type="checkbox" id="applica" disabled=""> Applica per intero l'offerta al preventivo
                                                    @endif
                                                @endif
                                            </td>
                                        @else
                                        <td>
                                            Questa offerta è scaduta e non può essere applicata al preventivo
                                        </td>
                                        @endif

                                    </tr>
                                @endforeach
                            </tbody>
                            </table>

                            <script type="text/javascript">
                                function toggleCardValue(el){
                                    var amount=0;
                                    if(el.checked)
                                        amount = Number(el.getAttribute("data-amount"));
                                    el.parentElement.querySelector(".importo").value=amount;
                                }
                            </script>

                            <legend>Coupon</legend>
                            @if(empty($coupons[0]))
                                <p><b>Non</b> sono presenti coupon assegnate a questo utente</p>
                            @endif

                            <table>
                            <thead>
                                <th>Riepilogo</th>
                                <th>Azioni</th>
                            </thead>  
                            <tbody>
                                 @foreach($coupons as $coupon)
                                    <tr>
                                        <td> 
                                            <a href="{{route('coupon edit',['id' => $coupon->id])}}" target="blank"> Carta: {{$coupon->code}} </a><br>
                                            Creato il {{$coupon->created_at}}  <br>
                                            Scadenza {{$coupon->deadline}}  <br>
                                            Stato :<b>
                                            @if($coupon->deadline>date("Y-m-d"))
                                                Attiva
                                            @else
                                                Scaduta
                                            @endif 
                                            </b> <br>
                                            Credito speso : {{$coupon->spesa}} / {{$coupon->value}} <br>
                                        </td>
                                         @if($coupon->deadline>date("Y-m-d"))
                                            <td>
                                                @if($coupon->spesa == 0)
                                                    Questa offerta non è ancora stata utilizzata,puoi scegliere un importo da scalare al totale del preventivo fino ad un massimo di {{$coupon->value}} o utilizzare l'opzione sotto per applicarla in automatico <br>
                                                    <input type="number" class="form-control importo" name="importo" class="importo" min="0" max="{{$coupon->value - $coupon->spesa}}" placeholder="importo">
                                                    <input type="checkbox" id="applica" class="applicaOffertaAlPreventivo" onclick="toggleCardValue(this)" data-amount="{{$coupon->value - $coupon->spesa}}"> Applica per intero l'offerta al preventivo
                                                @else
                                                    @if($coupon->spesa < $coupon->value && $coupon->spesa>0)
                                                        Questa offerta è stata utilizzata parzialmente,puoi scegliere un importo da scalare al totale del preventivo fino ad un massimo di {{$coupon->value}} o utilizzare l'opzione sotto per applicarle il residuo in automatico<br>
                                                        <input type="number" class="form-control importo" name="importo" min="0" max="{{$coupon->value - $coupon->spesa}}" placeholder="importo">
                                                        <input type="checkbox" id="applica" class="applicaOffertaAlPreventivo" onclick="toggleCardValue(this)" data-amount="{{$coupon->value - $coupon->spesa}}"> Applica per intero l'offerta al preventivo
                                                    @endif
                                                    @if($coupon->value - $coupon->spesa == 0)
                                                        Questa offerta è ancora stata pienamente utilizzata<br>
                                                        <input type="checkbox" id="applica" disabled=""> Applica per intero l'offerta al preventivo
                                                    @endif
                                                @endif
                                            </td>
                                        @else
                                        <td>
                                            Questa offerta è scaduta e non può essere applicata al preventivo
                                        </td>
                                        @endif

                                    </tr>
                                @endforeach
                            </tbody>
                            </table>


                        </div>


                                        </div>
                                    </div>
                                </div>
                            </div>

                            </div>
                            <button class="btn btn-lg btn-success btn-block">Aggiorna</button>
                        </div>

                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
<div id="loading-page">
  <div id="container_load">
    <h1 ><i class="fa fa-print fa-2x"></i> &nbsp;&nbsp;<i class="fa fa-refresh fa-spin fa-2x"></i> &nbsp;&nbsp;<i class="fa fa-file fa-2x"></i></h1>
    <h2 style="color:#f27b17">Stampa in corso...</h2>
    <p>Attendi qualche secondo su questa pagina per ottenere il pdf.</p>

  </div>
</div>
@endsection
@section('footer-plugin')
    <script src="{{ asset('/js/html2pdf.bundle.min.js') }}"></script>
    <script src="{{ asset('/lib/bootstrap-datepicker/js/bootstrap-datepicker.js') }}"></script>
  <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
  <!-- wysiwg editor -->
    <script src="{{ asset('/lib/ckeditor/ckeditor.js') }}"></script>
    <script src="{{ asset('/lib/ckeditor/adapters/jquery.js') }}"></script>
    <!-- wizard functions -->
    <script src="{{ asset('/js/apps/tisa_wysiwg.js') }}"></script>

<script type="text/javascript">
var privati= JSON.parse('<?php echo addslashes(json_encode($privati)) ;?>');

function sendHtml(){

  $(".prew_dest").slice(1).parents("td").remove();
  var name=''
  name += $("#prew_nr").val()+'_';
  name +=$("#prew_cst").val()+'_';
  name += $("#prew_dest").val();

    $("page_footer:last").replaceWith($("page_footer:first").clone());

  var serialized_output = ''
    var s = new XMLSerializer();
    var el = document.getElementsByClassName("page_pdf");
  for (let i=0; el[i]; i++){
      let page = el[i].cloneNode(true)
      $('input', page).each(function(){
        let input = $(this)
        let val = input.val() && input.val().trim()!='' ?  input.val() : '_  _ _ _ _ _ _'
        input.replaceWith('<p class="input">'+ val+'</p>')
      })
      serialized_output +=  s.serializeToString(page);
  }
  debugger;

  $('#loading-page').css('display','block')
    $.post("/admin/preventivo/topdf/{{@$id}}",{_token: "{{ csrf_token() }}", html: serialized_output, name: name},function( data ) {
      console.log( data );
    $('#loading-page').css('display','none')
    window.open('/admin/preventivo/getpdf/'+data.pdf_name,'_blank');
    },'json').fail((err)=>{
        debugger;
    $('#loading-page').css('display','none')
    alert('Errore durante il caricamento del file. Probailmente le note eccedono il limite')
  })
}




function addAjaxFieldWrapper(){
    var value=$('[name="dest_label"]').val();
    if(value=="")
    {
        alert("Compila il campo destinazione per aggiungere una nuova destinazione");
        return;
    }
    addAjaxField('destinazioni',{tag:value,destinazione:value,visibile:1},(res)=>{
      jQuery('#partab2').prepend('<tr>\<td style="display: none;">373 <input type="hidden" value="373" tabindex="21" old="true" style="text-transform: uppercase;"></td>\
        <td>'+value+'</td> \
        <td style="text-align: center;"><input type="checkbox" id="dest_visibile" checked="" tabindex="22" old="true" style="text-transform: uppercase"></td> \
        <td><a href="#" class="btn btn-xs btn-danger news-zama" style="margin-left: 10px;" data-action="container" data-id="'+res['last_id']+'" data-table="destinazioni" data-parent="tr" data-id="373" onclick="cleanDeleteRecord(this)"><i class="glyphicon glyphicon-minus"></i>&nbsp;Elimina</a></td>\
        </tr>')
    });
}

function addUserLine(value,text){
    var newValue=jQuery('#userAssigned').val()+","+value;
    jQuery('#userAssigned').val(newValue);

    var newText=jQuery('#userAssignedText').text()+text+"\n";
    jQuery('#userAssignedText').text(newText);
    jQuery('#userAssignedText').val(newText);
    jQuery('#category_id').trigger("change");
    jQuery('#userAssigned').trigger("change");

    jQuery('#task_note').trigger("change");
    jQuery('#task_deadline').trigger("change");
}

    function addUser(el){
        var value=jQuery(el).find('option:selected').val();
        var text=jQuery(el).find('option:selected').text();
        addUserLine(value,text);
    }

    function applyRep(el){
        var list=jQuery(el).find('option:selected').data().list.split(",");
           jQuery("[name='task_fcfs']").val("1");
            list.map(function(currentValue){
                var value=currentValue;
                var text=jQuery("#user_id option[value='"+value+"']:first").text();
                addUserLine(value,text);
            })
    }

function pdf_preview_get_value_from_form_el(){
    var list=[];
    $("#output input").each(function(){
        var item={};
            item.id='id=\"'+$(this).attr("id")+'\"';
            item.value=$(this).val();
            list.push(item);
    })
    return list;
}

function editViewLoadDataInForm(data,fieldPrefix="") {
    var id=data[0].id;
    var keys=Object.keys(data[0]);
    var href=$("#qui").attr("href");
    $("#qui").attr("href",href.replace("#",id));
    var href=$("#editScheda a").attr("href");
    $("#editScheda a").attr("href",href+id);
    for(var i=0;i<keys.length;i++){

        if($("[name='preventivo_"+keys[i]+"']:first").length)
            var el=$("[name='"+fieldPrefix+keys[i]+"']:first");
        else
   	        continue;

        var type=el[0].tagName;
        var value=data[0][keys[i]];

        switch(type){
            case 'SELECT':
            $(el).find("option[value='"+value+"']").prop("selected",true);
            break;
            case 'INPUT':
             if ($(el).attr('type') == 'date') {
               if (value)
               {
                 let t = value.split('/')
                 $(el).val(t[1]+'/'+t[0]+'/'+t[2])
               }
             }
            case 'TEXTAREA':
            if(keys[i]=="riferitoA"){
                for(var j=0;j<privati.length;j++){
                    if(privati[j].value==value){
                        $("#preventivo_riferitoA").val(privati[j].label);
                        $("[name='preventivo_riferitoA']").val(value);
                        $("[name='preventivo_riferitoA']").removeAttr("old");
                        break;
                    }
                }
            }
            else{
	            $(el).val(value)
            }
            break;
            case 'CHECKBOX':
            $(el).val(value);
            break;
        }

    }

    if($("[name='"+fieldPrefix+"id']:first").length==0){
        var idFiled="<input type='hidden' name='"+fieldPrefix+"id' value='"+data[0].id+"'>";
        $("form:first").append(idFiled);
    }
    $("#prew_dal").val(fdate($("#preventivo_dal").val()));
    $("#prew_al").val(fdate($("#preventivo_al").val()));
    $("#prew_dest").val($("#status option:selected").text());
    $("#prew_dest").val($("#status option:selected").text());
    $("#prew_nr").val(data[0].id);
    $("#prew_cst").val($("#preventivo_riferitoA").val());
    $("#prew_notes").val(data[0].valutazione);
    $("#prew_kind").val(data[0].TipologiaDiViaggio);
    $("#prew_price").val(format_to_eur_js($('[name="preventivo_prezzoTotalePreventivo"]').val()));
    if(data[0].scadenza!=null)
        $("#prew_deadline").val(data[0].scadenza.format_to_ita());
    $("#prew_tratt").val(data[0].trattamento);
    $("#prew_sistemaz").val(data[0].sistemazione);

    compute_notes_page();

}

    function compute_notes_page(){
        //blocco-note

        var page= $(".page_pdf");
    console.log(page);

    let new_page = page.clone() ;
    let new_body = $(".pdf_body", new_page);
    new_body.html($('#wysiwg_full').val());
    $("#output").append(new_page);

    }

    var action ="{{ $action }}";

    if(action=="edit")
    {
        var azioni=JSON.parse('<?php echo addslashes(json_encode($preventivoAzioni))?>');
        var data=JSON.parse('<?php echo addslashes(json_encode($preventivo))?>');
        console.log(data)
        if (data && data.length > 0){
          editViewLoadDataInForm(data,"preventivo_");
        }
    }
    else
        $('[href="#taskpreventivo"]').hide()

    function addPeople(){
        $("#partab").show();
        var l=$("#partab th").length/2;
        var tr="<tr>";

            tr+="<td><select name='preventivoPartecipanti_tipo[]' >"+
            "<option value='adulti'>adulti</option>"+
            "<option value='ragazzi'>ragazzi</option>"+
            "<option value='bambini'>bambini</option>"+
            "<option value='neonati'>neonati</option>"+
            "</select></td>";
            tr+="<td><input type='text' name='preventivoPartecipanti_numero[]' placeholder='numero di partecipanti'></td>";
            tr+="<td><input type='text' name='preventivoPartecipanti_eta[]'placeholder='eta'></td>";

        tr+='<td><a href="#" class="btn btn-xs btn-danger news-zama" style="margin-left: 10px;" onclick="deleteRecord(this)"><i class="glyphicon glyphicon-minus"></i>&nbsp;Elimina</a></td>';
        tr+="</tr>";
        $("#partab tbody").append(tr);
    }

    $(document).ready(function(){
          if (location.hash =="#destinazioni"){
            console.log($('[href="'+location.hash+'"]'))
            $('[href="'+location.hash+'"]').click()
          }


            $( "#preventivo_riferitoA" ).autocomplete({
              source: privati,
                select: function (a, b) {
                    a.preventDefault();
                    $('#preventivo_riferitoA').val(b.item.label);
                    $('[name="preventivo_riferitoA"]').val(b.item.value);
                    $('[name="preventivo_riferitoA"]').val(b.item.value);
                    $('[name="preventivo_riferitoA"]').removeAttr("old");
                },
                response: function(event, ui) {
                     // ui.content is the array that's about to be sent to the response callback.
                     if (ui.content.length === 0) {
                         $("#privati-message").text("Il cliente inserito non è presente nel elenco privati");
                     } else {
                         $("#privati-message").empty();
                     }
                 }
            });

        // The "change" event is fired whenever a change is made in the editor.
        CKEDITOR.instances.wysiwg_full.on( 'change', function( evt ) {
            $('[name="preventivo_valutazione"]').val(evt.editor.getData())
            $('[name="preventivo_valutazione"]').attr("dirty",true);
           $('[name="preventivo_valutazione"]').removeAttr("old");
        });


        if($("#partab tbody tr").length)
            $("#partab").show();

        $("select").attr("old",true);
        $("input[name!='_token'][name!='prev_id']").attr("old",true);

        $("select,input,textarea").change(function(){
            $(this).attr("dirty",true);
            $(this).removeAttr("old");
        });

        var tmp=window.location.href.split("/");
        if(tmp.includes("edit")){
            tmp=tmp[tmp.length-1];
            jQuery("[ name='prev_id']").val(tmp);
            jQuery("[ name='prev_id']").removeAttr("old");
        }
        else
            jQuery("[ name='prev_id']").remove();

        $('input').css('text-transform','uppercase');

        $("form").on('submit', function() {
            $("#richiesta [old]").remove();

            $(this).find('[type="checkbox"]').each(function () {
                $(this).attr('checked', true).val(0);
            });
        })
    })

    var name="test";

    function addStatus(){
        if($("#newtStep option:selected").val()=="")
            return;
        $("#notaStato input").val("");
        $("#notaStato").modal();
    }

//      var sentences=$('[name="preventivo_valutazione"]').val().split("\n");
//      var pages=get_text_paged_for_max_length(sentences,950);

    /* da una stringa di testo ritorna un array di max_char_length stringhe di testo */
    function get_text_paged_for_max_length(sentences,max_char_length){
        var global_length=0;
        var pages=[];

        var html="";
        for(var i=0;i<sentences.length;i++){
            global_length+=sentences[i].length;
            html+=sentences[i];
            if(global_length>max_char_length){
                pages.push(html);
                html="";
                global_length=0;
            }
        }
        if(global_length==0)
            pages.push(html);
        return pages;
    }

    function get_text_paged_for_max_lines(sentences,max_lines){
        var MAX_LEN_PHRASE = 50
        var global_length=0;
        var pages=[];
        var html="";
        var lines = 0
        //sentences = sentences.join(' <br> ')
        //sentences = sentences.split(' ')

        for(var i=0;i<sentences.length;i++){
            lines++

            html += sentences[i]
            /*global_length += sentences[i].length

            if (global_length >= MAX_LEN_PHRASE){
                html+= sentences[i]+'<br>'
                lines++
                global_length=0;
            }else{
                html+= sentences[i]+' '
            }*/


            if(lines >= max_lines){
                pages.push(html);
                html="";
                global_length=0;
                lines = 0;
            }
        }
        if(lines > 0){
                pages.push(html);
                html="";
                global_length=0;
                lines = 0;
            }
        /*if(global_length==0)
            pages.push(html);*/
        return pages;
    }


    function addStatusStep2(){

        $(".defaultMsg").remove();
        var time=new Date();
            time=time.getDate()+"/"+time.getMonth()+"/"+time.getFullYear();
        var status=$("#newtStep option:selected").val();

            if($('#scadenza').val()!="")
                var scadenza="<p>Scadenza prevista: "+$('#scadenza').val()+"</p>";
            else
                var scadenza="<p>non è prevista una scadenza</p>";

            if($('#nota').val()!="")
                var nota="<p>Con la seguente nota: "+$('#nota').val()+"</p>";
            else
                var nota="";

        var row=scadenza+"<p>l'utente <strong>"+name+"</strong> il <strong>"+time+"</strong> ha aggiornato lo stato in: <br><strong>"+status+"</strong>"+nota+"</p>";

        $("listaStati").prepend("<div class='liStati col-md-8'>"+row+"</div><div class='col-md-4'><input type='hidden' name='preventivoAzioni_eseguito[] value='0'><input type='checkbox' onclick='foo(this)' >Eseguito?</div>");
        $("listaStati").append("<input type='hidden' id='preventivoAzioni_azione[]' name='preventivoAzioni_azione[]' value='"+status+"'>");
        $("listaStati").append("<input type='hidden' id='preventivoAzioni_nota[]' name='preventivoAzioni_nota[]' value='"+$('#nota').val()+"'>");
        $("listaStati").append("<input type='hidden' id='preventivoAzioni_scadenza[]' name='preventivoAzioni_scadenza[]' value='"+$('#scadenza').val()+"'>");

    }

    function foo(el){
        if($(el).prop("checked"))
            $(el).prev("[type='hidden']").val(1);
        else
            $(el).prev("[type='hidden']").val(0);

        $(el).parents("div").find("input").removeAttr("old").attr("dirty",true);
    }

    window['table']="preventivo";
    window['table']="tasks";
    window['isntable']="li";

    function addUserLine(value,text){
        var newValue=jQuery('#userAssigned').val()+","+value;
        jQuery('#userAssigned').val(newValue);

        var newText=jQuery('#userAssignedText').text()+text+"\n";
        jQuery('#userAssignedText').text(newText);
    }

    function addUser(el){
        var value=jQuery(el).find('option:selected').val();
        var text=jQuery(el).find('option:selected').text();
        addUserLine(value,text);
    }

    function applyRep(el){
        var list=jQuery(el).find('option:selected').data().list.split(",");
           jQuery("[name='task_fcfs']").val("1");
            list.map(function(currentValue){
                var value=currentValue;
                var text=jQuery("#user_id option[value='"+value+"']:first").text();
                addUserLine(value,text);
            })
    }
    console.error("admin/preventivo1");

    function br2nl(varTest) {
        return varTest.replace(/<br\s*\/?>/ig, "\r");
    }

    function fdate (text_date){
      return text_date.split('-').reverse().join('-')
    }
</script>

<style type="text/css">
        th{ text-rendering: center;    width: 200px; }

        table#partab2 {
            width: 100%;
            display:block;
        }
        #partab2 thead {
            display: inline-block;
            width: 100%;
            height: 20px;
        }
        #partab2 th{
            text-align: center;
        }
        #partab2 tbody {
            height: 200px;
            display: inline-block;
            width: 500px;
            overflow: auto;
        }

        #loading-page{
          display: none;
          position: fixed;
          top: 0;
          right: 0;
          left: 0;
          bottom: 0;
          height: 100%;
          width: 100%;
          z-index: 100000;
          background-color: rgba(0, 0, 0, 0.2);
        }
        #container_load{
          padding: 2em;
          border-radius: 10px;
          position: absolute;
          margin: auto;
          top: 0;
          right: 0;
          left: 0;
          bottom: 0;
          color: white;
          height: 50%;
          width: 50%;
          box-shadow: 0 19px 19px 0 rgba(0,0,0,.3), 0 15px 6px 0 rgba(0,0,0,.22);
          text-align: center;
          background-color: #3c7fb8;
        }
</style>
@endsection
