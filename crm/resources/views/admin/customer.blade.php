@extends('layouts.master')
@section('header-page-personalization')
    <style>
        .twitter-typeahead, .tt-hint, .tt-input, .tt-menu { width: 100%;  }
        .tt-menu {
            background: #ffffff;
        }
        .applyStatus{
            float: right;
        }
        [name="notaStato"]{
            width: 90%;
            height: 150px;
        }
        .spacer{
            visibility: hidden;
            height: 50px;
        }
        listapreferite li{
          margin: 10px;
        }
        .destinazioniSalvate{
            margin-top: 20px;
        }
        listapreferite li{
          margin: 10px;
        }
        deleter{
          float: right;
        }
        .form-group{
          padding: 15px;
        }
    </style>
@endsection
@section('content')
    <div class="page_content page_customers">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">


                <!-- Modal -->
                <div id="addDestination" class="modal fade" role="dialog">

                  <div class="modal-dialog">

                    <!-- Modal content-->
                    <div class="modal-content">
                      <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Desideri aggiungere una nota per questo contatto?</h4>
                      </div>
                      <div class="modal-body">
                        <form class="form-horizontal">
                        <fieldset>

                        <div class="form-group">
                          <div class="col-md-12" style="padding:15px;">
                          <input id="dsnm" type="text" placeholder="Nome destinazione" class="form-control input-md">
                          </div>
                        </div>

                        <div class="form-group">
                          <div class="col-md-12" style="text-align: center;">
                            <button id="button1id" type="button" onclick="addDestination()" name="button1id" class="btn btn-success">Seleziona</button>
                          </div>
                        </div>
                        </fieldset>
                        </form>
                          </div>
                        </div>

                      </div>
                    </div>
                <!-- Modal -->
                <div id="infocitta" class="modal fade" role="dialog">

                  <div class="modal-dialog">

                    <!-- Modal content-->
                    <div class="modal-content">
                      <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Desideri aggiungere una nota per questo contatto?</h4>
                      </div>
                      <div class="modal-body">
                                                        <p> Per la compilazione dei dati anagrafici è attiva la compilazione automatica.<br>
                                                            Le città con le iniziali corrispondenti a quanto digitato sono suggerite per compilare i campi cap, provincia regione e stato<br>
                                                            La compilazione automatica impara quando sono inserite nuove città, italiane ed estere. Per assicurarsi che la memorizzazione vada a buon fine i campi città, cap, provincia, regione e stato devono essere compilati
                                                        </p>
                      </div>
                          <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Chiudi</button>
                          </div>
                        </div>

                      </div>
                    </div>

                <!-- Modal -->
                <div id="addDestination2" class="modal fade" role="dialog">

                  <div class="modal-dialog">

                    <!-- Modal content-->
                    <div class="modal-content">
                      <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Desideri aggiungere una nota per questo contatto?</h4>
                      </div>
                      <div class="modal-body">
                        <form class="form-horizontal">
                        <fieldset>

                        <div class="form-group">
                          <div class="col-md-12" style="padding:20px;">
                          <input id="dsnm" type="text" placeholder="Nome destinazione" class="form-control input-md">
                          </div>
                        </div>

                        <div class="form-group">
                          <div class="col-md-12" style="text-align: center;">
                            <button id="button1id" type="button" onclick="addDestination2()" name="button1id" class="btn btn-success">Salva</button>
                          </div>
                        </div>

                        </fieldset>
                        </form>

                          </div>

                        </div>

                      </div>
                    </div>

                <!-- Modal -->
                <div id="addDestination3" class="modal fade" role="dialog">

                  <div class="modal-dialog">

                    <!-- Modal content-->
                    <div class="modal-content">
                      <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Desideri aggiungere una nota per questo contatto?</h4>
                      </div>
                      <div class="modal-body">
                        <form class="form-horizontal">
                        <fieldset>

                        <div class="form-group">

                          <div class="col-md-12" style="padding:20px;">
                          <input id="dsnm" type="text" placeholder="Nome destinazione" class="form-control input-md">

                          </div>
                        </div>


                        <div class="form-group">
                          <div class="col-md-12" style="text-align: center;">
                            <button id="button1id" type="button" onclick="addDestination3()" name="button1id" class="btn btn-success">Salva</button>
                          </div>
                        </div>

                        </fieldset>
                        </form>

                          </div>

                        </div>

                      </div>
                    </div>

                    <form id="login_form" role="form" method="POST" action="{{ route('customer.store') }}" class="customForm">
                    {{ csrf_field() }}
                    <?php $name=Auth::user()->name." ".Auth::user()->surname;?>
                        <div class="panel panel-default">
                            <div class="panel-body">
                                <div class="heading_a"><legend><span class="big-title">Aggiungi Nuovo Privato</span></legend></div>
                                <div class="tabbable tabs-left">
                                    <ul class="nav nav-tabs" id="tabs_c">
                                        <li class="active"><a data-toggle="tab" href="#info">Info</a></li>
                                        <li><a data-toggle="tab" href="#profilo">Profilo</a></li>
                                        <li><a data-toggle="tab" href="#categoria">Categoria</a></li>
                                        <li><a data-toggle="tab" href="#infoviaggi">Info Viaggi</a></li>
                                        <li><a data-toggle="tab" href="#qasuend">Info Suend</a></li>
                                        <li><a data-toggle="tab" href="#qaviaggio">Valutazione Viaggio</a></li>
                                        <li><a data-toggle="tab" href="#relazione">Stato relazione</a></li>
                                        <li><a data-toggle="tab" href="#coupon">Gift Card / Coupon</a></li>
                                    </ul>
                                    @if(session()->has('message'))
                                        <div class="alert alert-success">
                                            {{ session()->get('message') }}
                                        </div>
                                    @endif
                                    @if(session()->has('error'))
                                        <div class="alert alert-danger">
                                            {{ session()->get('error') }}
                                        </div>
                                    @endif
                                    <div class="tab-content" id="tabs_content_c">
                                        <div id="info" class="tab-pane fade in active">
                                            <div class="row">
                                                <div class="col-lg-12">

                                                    <div class="form-group col-md-6" style="display: none;">
                                                        <label for="reg_input">Codice</label>
                                                        <input type="text" id="codice" name="codice" class="form-control"
                                                               tabindex="1" disabled>
                                                    </div>

                                                    <div class="form-group col-md-6">
                                                        <label for="reg_input">Nome</label>
                                                        <input type="text" id="nome" name="nome" class="form-control" tabindex="2">
                                                    </div>

                                                    <div class="form-group col-md-6">
                                                        <label for="reg_input">Cognome</label>
                                                        <input type="text" id="cognome" name="cognome" class="form-control"
                                                               tabindex="1">
                                                    </div>
                                                    <div class="form-group col-md-6">
                                                        <label for="reg_input">Codice Fiscale</label>
                                                        <input type="text" id="info_codice_fiscale" name="info_codice_fiscale"
                                                               class="form-control" tabindex="15" onfocusout="validateFs(this)">

                                                    </div>

                                                    <div class="form-group col-md-6">
                                                        <label for="reg_input">Data di Nascita</label>
                                                        <input type="date" id="info_data_nascita" name="info_data_nascita"
                                                               class="form-control" tabindex="3">
                                                    </div>

                                                    <div class="form-group col-md-6">
                                                        <label for="reg_input">Sesso</label>
                                                        <select id="info_sesso" name="info_sesso" class="form-control"
                                                                tabindex="4">
                                                            <option value="MASCHIO">MASCHIO</option>
                                                            <option value="FEMMINA">FEMMINA</option>
                                                        </select>
                                                    </div>


                                                    <div class="form-group col-md-6">
                                                        <label for="reg_input">Indirizzo</label>
                                                        <input type="text" id="info_indirizzo" name="info_indirizzo"
                                                               class="form-control" tabindex="5">
                                                    </div>
                                                    <div class="spacer"></div>
                                                    <div class="form-group col-md-6">
                                                      <label class="col-md-12 control-label" for="radios" style="padding-left: 0px;">Residenza Italiana?</label>
                                                      <div class="col-md-12" style="padding-left: 0px;">
                                                        <label class="radio-inline" for="radios-0">
                                                          <input type="radio" name="residenza" id="residenza" value="0" checked="checked" onclick="setAutocompile(0)">
                                                          Si
                                                        </label>
                                                        <label class="radio-inline" for="radios-1">
                                                          <input type="radio" name="residenza" id="residenza" value="1" onclick="setAutocompile(1)">
                                                          No
                                                        </label>
                                                      </div>
                                                    </div>
                                                    <div class="form-group col-md-6">
                                                        <label for="reg_input" style="clear: both;">Citta'
                                                        <i class="fa fa-question-circle"  data-toggle="modal" data-target="#infocitta" style="cursor: pointer;margin-left: 10px;font-size: 15px;" ></i></label>
                                                        <input type="text" id="citta" name="citta" class="form-control" tabindex="6" placeholder="Inserisci citta" style="text-transform: uppercase;">
                                                    </div>
                                                    <div class="form-group col-md-6">
                                                        <label for="reg_input">Cap</label>
                                                        <input type="text" id="cap" name="cap" class="form-control" tabindex="7">

                                                    </div>
                                                    <div class="form-group col-md-6">
                                                        <label for="reg_input">Provincia</label>
                                                        <input type="text" id="provincia" name="provincia" class="form-control" tabindex="8">
                                                    </div>
                                                    <div class="form-group col-md-6">
                                                        <label for="reg_input">Regione</label>
                                                        <input type="text" id="regione" name="regione" class="form-control" tabindex="9">

                                                    </div>
                                                    <div class="form-group col-md-6">
                                                        <label for="reg_input">Stato</label>
                                                        <input type="text" id="stato" name="stato" class="form-control"
                                                               tabindex="10">
                                                    </div>
                                                    <div class="spacer"></div>

                                                    <div class="col-md-6 form-group" style="padding-left: 15px;">
                                                        <div class="form-group col-md-2" style="padding-left: 0px;">
                                                            <label for="reg_input">Prefisso</label>
                                                            <input type="text" id="pref1" name="pref1" class="form-control" tabindex="11" placeholder="+39">
                                                        </div>

                                                        <div class="form-group col-md-10" style="padding-left: 0px;">
                                                            <label for="reg_input">Telefono Cellulare</label>
                                                            <input type="text" id="tel1" name="tel1" class="form-control" tabindex="11">
                                                        </div>
                                                    </div>

                                                    <div class="col-md-6 form-group" style="padding-left: 15px;">
                                                        <div class="form-group col-md-2" style="padding-left: 0px;">
                                                            <label for="reg_input">Prefisso</label>
                                                            <input type="text" id="pref2" name="pref2" class="form-control" tabindex="12" value="036">
                                                        </div>

                                                        <div class="form-group col-md-10" style="padding-left: 0px;">
                                                            <label for="reg_input">Telefono Fisso</label>
                                                            <input type="text" id="tel2" name="tel2" class="form-control" tabindex="12">
                                                        </div>
                                                    </div>

                                                    <div class="form-group col-md-6">
                                                        <label for="reg_input" style="clear: both;">Email Principale</label>
                                                        <input type="text" id="email1" name="email1" class="form-control"
                                                               tabindex="13">
                                                    </div>
                                                    <div class="form-group col-md-6">
                                                        <label for="reg_input" style="clear: both;">Email Secondaria</label>
                                                        <!--input type="text" id="info_mail2" name="info_mail2" class="form-control" tabindex="14"-->
                                                        <textarea type="text" id="email2" name="email2" class="form-control" tabindex="14"></textarea>
                                                    </div>
                                                </div>
                                        </div>

                                        </div>
                                        <div id="categoria"  class="tab-pane fade">
                                          <div class="row">
                                              <div class="col-lg-8">
                                                  <div class="form-group">
                                                      <label for="reg_input">Categoria</label>
                                                       <select name="categoria" class="form-control">
                                                           <option disabled="" selected="">Seleziona la Categoria</option>
                                                           @foreach (App\Category_Customers::all() as $cat)
                                                           <option value="{{$cat->id}}">{{$cat->name}}</option>
                                                          @endforeach
                                                       </select>
                                                  </div>
                                                  <!--a href="{{url('admin/clienti/categories')}}" class="btn btn-info">MODIFICA LE CATEGORIE</a-->
                                              </div>
                                            </div>
                                        </div>
                                        <div id="infoviaggi" class="tab-pane fade">
                                            <div class="row">
                                                <div class="col-lg-8">
                                                    <div class="form-group">
                                                        <label for="reg_input">Vacanze/Anno</label>
                                                         <select id="infoviaggi_vacanzeanno" name="infoviaggi_vacanzeanno" class="form-control">
                                                             <option disabled="" selected="">Scegli la frequenza</option>
                                                             <option value="1 / anno">1 / anno</option>
                                                             <option value="1-2 / anno">1-2 / anno</option>
                                                             <option value="3-4 / anno">3-4 / anno</option>
                                                             <option value="più di 5">più di 5</option>
                                                         </select>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="reg_input">Preferenza Viaggi</label>
                                                        <select id="infoviaggi_preferenze" name="infoviaggi_preferenze" class="form-control">
                                                            <option disabled="" selected="">Scegli la preferenza</option>
                                                            <option value="Tour di gruppo">Tour di gruppo</option>
                                                            <option value="Tour individuali">Tour individuali</option>
                                                            <option value="Vacanza gruppo">Vacanza gruppo</option>
                                                            <option value="Vacanza individuale">Vacanza individuale</option>
                                                            <option value="Solo volo">Solo volo</option>
                                                        </select>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="reg_input">Durata Media</label>
                                                        <select id="infoviaggi_duratamedia" name="infoviaggi_duratamedia" class="form-control">
                                                            <option disabled="" selected="">Durata media notti</option>
                                                            <option value="1-3 notti">1-3 notti</option>
                                                            <option value="3-7 notti">3-7 notti</option>
                                                            <option value="7-10 notti">7-10 notti</option>
                                                            <option value="10-15 notti">10-15 notti</option>
                                                            <option value="più di 15 notti">più di 15 notti</option>
                                                        </select>
                                                    </div>
                                                    <?php
                                                        $destinazioni=DB::select('SELECT id,UPPER(destinazione) AS label FROM destinazioni WHERE visibile=1 ORDER BY destinazione ASC');
                                                    ?>

                                                    <div class="form-group col-md-12">
                                                                <div class="ui-filter-container-pref col-md-12">
                                                                    <input type="text" id="prossime" placeholder="Cerca una destinazione" class="form-control input-md ui-autocomplete-input" autocomplete="off">
                                                                    <input type="hidden" name="destinazioniprossime[]">
                                                                </div>
                                                                <div class="ui-filter-container-pref col-md-12">
                                                                    <input type="text" id="preferite" placeholder="Cerca una destinazione" class="form-control input-md ui-autocomplete-input" autocomplete="off">
                                                                    <input type="hidden" name="destinazionipreferite[]">
                                                                </div>
                                                                <div class="ui-filter-container-pref col-md-12">
                                                                    <input type="text" id="passate" placeholder="Cerca una destinazione" class="form-control input-md ui-autocomplete-input" autocomplete="off">
                                                                    <input type="hidden" name="destinazionipassate[]">
                                                                </div>
                                                    </div>

                                                    <div class="form-group" style="display: none;">
                                                        <label for="reg_input">Fonti</label>
                                                        <input type="text" id="infoviaggi_fonti" name="infoviaggi_fonti" class="form-control">
                                                    </div>


                                                </div>
                                                <div class="col-lg-4">
                                                    <label style="margin-left: 40px;">Mesi Preferiti</label>
                                                    <div class="col-lg-3">
                                                        <div class="checkbox">
                                                            <label>
                                                                <input type="checkbox" name="infoviaggi_gennaio" value="1"  tabindex="20">
                                                                Gennaio
                                                            </label>
                                                        </div>
                                                        <div class="checkbox">
                                                            <label>
                                                                <input type="checkbox" name="infoviaggi_febbraio" value="1"  tabindex="21">
                                                                Febbraio
                                                            </label>
                                                        </div>
                                                        <div class="checkbox">
                                                            <label>
                                                                <input type="checkbox" name="infoviaggi_marzo" value="1"  tabindex="22">
                                                                Marzo
                                                            </label>
                                                        </div>
                                                        <div class="checkbox">
                                                            <label>
                                                                <input type="checkbox" name="infoviaggi_aprile" value="1"  tabindex="23">
                                                                Aprile
                                                            </label>
                                                        </div>
                                                        <div class="checkbox">
                                                            <label>
                                                                <input type="checkbox" name="infoviaggi_maggio" value="1"  tabindex="24">
                                                                Maggio
                                                            </label>
                                                        </div>
                                                        <div class="checkbox">
                                                            <label>
                                                                <input type="checkbox" name="infoviaggi_giugno" value="1"  tabindex="25">
                                                                Giugno
                                                            </label>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-3">

                                                        <div class="checkbox">
                                                            <label>
                                                                <input type="checkbox" name="infoviaggi_luglio" value="1"   tabindex="26">
                                                                Luglio
                                                            </label>
                                                        </div>
                                                        <div class="checkbox">
                                                            <label>
                                                                <input type="checkbox" name="infoviaggi_agosto" value="1"   tabindex="27">
                                                                Agosto
                                                            </label>
                                                        </div>
                                                        <div class="checkbox">
                                                            <label>
                                                                <input type="checkbox" name="infoviaggi_settembre" value="1"   tabindex="28">
                                                                Settembre
                                                            </label>
                                                        </div>
                                                        <div class="checkbox">
                                                            <label>
                                                                <input type="checkbox" name="infoviaggi_ottobre" value="1"   tabindex="29">
                                                                Ottobre
                                                            </label>
                                                        </div>
                                                        <div class="checkbox">
                                                            <label>
                                                                <input type="checkbox" name="infoviaggi_novembre" value="1"   tabindex="30">
                                                                Novembre
                                                            </label>
                                                        </div>
                                                        <div class="checkbox">
                                                            <label>
                                                                <input type="checkbox" name="infoviaggi_dicembre" value="1"   tabindex="31">
                                                                Dicembre
                                                            </label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>


                                            <div class="row">
                                                <div class="col-lg-8">
                                                    <div class="col-lg-4">
                                                        <label>Tematiche</label>
                                                        <div class="checkbox">
                                                            <label>
                                                                <input type="checkbox" name="infoviaggi_archeologia" value="1"   tabindex="32">
                                                                Archeologia
                                                            </label>
                                                        </div>
                                                        <div class="checkbox">
                                                            <label>
                                                                <input type="checkbox" name="infoviaggi_avventura" value="1"   tabindex="33">
                                                                Avventura
                                                            </label>
                                                        </div>
                                                        <div class="checkbox">
                                                            <label>
                                                                <input type="checkbox" name="infoviaggi_cultura" value="1"   tabindex="34">
                                                                Cultura
                                                            </label>
                                                        </div>
                                                        <div class="checkbox">
                                                            <label>
                                                                <input type="checkbox" name="infoviaggi_ecoturismo" value="1"  tabindex="35">
                                                                Ecoturismo
                                                            </label>
                                                        </div>
                                                        <div class="checkbox">
                                                            <label>
                                                                <input type="checkbox" name="infoviaggi_altro" value="1" tabindex="43">
                                                                Altro
                                                            </label>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-4">
                                                        <div class="checkbox">
                                                            <label>
                                                                <input type="checkbox" name="infoviaggi_enogastronomia" value="1"   tabindex="36">
                                                                Enogastronomia
                                                            </label>
                                                        </div>
                                                        <div class="checkbox">
                                                            <label>
                                                                <input type="checkbox" name="infoviaggi_fotografico" value="1"   tabindex="37">
                                                                Fotografico
                                                            </label>
                                                        </div>
                                                        <div class="checkbox">
                                                            <label>
                                                                <input type="checkbox" name="infoviaggi_marerelax" value="1"  tabindex="38">
                                                                Mare e relax
                                                            </label>
                                                        </div>
                                                        <div class="checkbox">
                                                            <label>
                                                                <input type="checkbox" name="infoviaggi_montagna" value="1"  tabindex="39">
                                                                Montagna
                                                            </label>
                                                        </div>

                                                    </div>
                                                    <div class="col-lg-4">
                                                        <div class="checkbox">
                                                            <label>
                                                                <input type="checkbox" name="infoviaggi_religioso" value="1"   tabindex="40">
                                                                Religioso
                                                            </label>
                                                        </div>
                                                        <div class="checkbox">
                                                            <label>
                                                                <input type="checkbox" name="infoviaggi_sportivo" value="1"   tabindex="41">
                                                                Sportivo
                                                            </label>
                                                        </div>
                                                        <div class="checkbox">
                                                            <label>
                                                                <input type="checkbox" name="infoviaggi_storicopolitico" value="1" tabindex="42">
                                                                Storico Politico
                                                            </label>
                                                        </div>
                                                        <div class="checkbox">
                                                            <label>
                                                                <input type="checkbox" name="infoviaggi_solo_volo" value="1" >
                                                                Lavoro
                                                            </label>
                                                        </div>
                                                    </div>

                                                </div>
                                                <div class="col-lg-4">
                                                    <textarea cols="30" rows="5" class="form-control input-sm" name="infoviaggi_tematiche_altro" tabindex="44"
                                                              placeholder="Altre tematiche..."></textarea>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-lg-8">
                                                    <label>Mood Viaggio</label>
                                                    <div class="col-lg-4">

                                                        <div class="checkbox"><label>
                                                           <input type="checkbox" name="infoviaggi_mood[]" value="0">Avventura</label>
                                                        </div>
                                                        <div class="checkbox"><label>
                                                           <input type="checkbox" name="infoviaggi_mood[]" value="1">Cultura e tradizione</label>
                                                        </div>
                                                        <div class="checkbox"><label>
                                                           <input type="checkbox" name="infoviaggi_mood[]" value="2">Sport e benessere</label>
                                                        </div>
                                                        <div class="checkbox"><label>
                                                           <input type="checkbox" name="infoviaggi_mood[]" value="3">Viaggio di nozze</label>
                                                        </div>
                                                    </div>
                                                     <div class="col-lg-4">
                                                        <div class="checkbox"><label>
                                                           <input type="checkbox" name="infoviaggi_mood[]" value="4">Viaggio con i bambini</label>
                                                        </div>
                                                        <div class="checkbox"><label>
                                                           <input type="checkbox" name="infoviaggi_mood[]" value="5">Viaggio da solo</label>
                                                        </div>
                                                        <div class="checkbox"><label>
                                                           <input type="checkbox" name="infoviaggi_mood[]" value="6">Romantico</label>
                                                        </div>
                                                        <div class="checkbox"><label>
                                                           <input type="checkbox" name="infoviaggi_mood[]" value="7">Fashion</label>
                                                        </div>
                                                    </div>
                                                     <div class="col-lg-4">
                                                        <div class="checkbox"><label>
                                                           <input type="checkbox" name="infoviaggi_mood[]" value="8">Gourmet</label>
                                                        </div>
                                                        <div class="checkbox"><label>
                                                           <input type="checkbox" name="infoviaggi_mood[]" value="9">Relax</label>
                                                        </div>
                                                        <div class="checkbox"><label>
                                                           <input type="checkbox" name="infoviaggi_mood[]" value="10">Luxury</label>
                                                        </div>
                                                        <div class="checkbox"><label>
                                                           <input type="checkbox" name="infoviaggi_mood[]" value="11">Esplorazione</label>
                                                        </div>
                                                        <div class="checkbox"><label>
                                                           <input type="checkbox" name="infoviaggi_mood[]" value="12"> Divertimento</label>
                                                        </div>
                                                    </div>
                                                    </div>
                                                </div>
                                            </div>

                                        <div id="qasuend" class="tab-pane fade">
                                            <div class="row">

                                                <div class="col-lg-12">
                                                    <div class="form-group">
                                                        <label for="reg_input">Conoscenza Suend</label>
                                                        <input type="text" id="qasuend_conoscenza" name="qasuend_conoscenza"   tabindex="47"
                                                               class="form-control">
                                                    </div>
                                                </div>

                                                <div class="col-lg-12">
                                                    <div class="form-group">
                                                        <label for="reg_input">Altre motivazioni conoscenza
                                                            Suend</label>
                                                        <input type="text" id="qasuend_altremotivazione" name="qasuend_altremotivazione"   tabindex="48"
                                                               class="form-control">
                                                    </div>
                                                </div>

                                            </div>
                                            <div class="row">
                                                <div class="col-lg-4">
                                                    <div class="form-group inlineCheckbox">
                                                        <label for="reg_input" style="text-align: center;">Ha viaggiato con noi?</label>
                                                        <input type="checkbox" class="input-sm"   tabindex="49"
                                                               name="qasuend_giaviaggiato" id="qasuend_giaviaggiato" value="true">
                                                    </div>
                                                </div>
                                                <div class="col-lg-4">
                                                    <div class="form-group">
                                                        <label for="reg_input">Quante Volte</label>
                                                        <input type="text" id="qasuend_quantiviaggi" name="qasuend_quantiviaggi"   tabindex="50"
                                                               class="form-control"
                                                               tabindex="0">
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-lg-4">
                                                        <div class="form-group inlineCheckbox">
                                                            <label for="reg_input" style="text-align: center;">Conosce
                                                                Sito</label>
                                                            <input type="checkbox" class="input-sm"   tabindex="52"
                                                                   name="qasuend_conoscesito" id="qasuend_conoscesito" value="true">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-8">
                                                    <div class="form-group">
                                                        <label for="reg_input"></label>
                                                        <input type="text" id="qasuend_doveviaggi" name="qasuend_doveviaggi"   tabindex="51"
                                                               class="form-control"
                                                               tabindex="0">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-12" id="bonus">
                                              <legend> Storico buoni</legend>
                                              <p>E' neccessario salvare la pagina per utilizzare questa funzionalità</p>
                                            </div>

                                            <div class="row">
                                                <div class="col-lg-12">
                                                    <div class="form-group">
                                                        <label for="reg_input">Note</label>
                                                        <textarea name="qasuend_note" id="qasuend_note" cols="10" rows="3"   tabindex="53"
                                                                  class="form-control"></textarea>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div id="qaviaggio" class="tab-pane fade">
                                            <div class="row">
                                                <div class="col-lg-12">
                                                    <div class="form-group">
                                                        <label for="reg_input">Soddisfazione Generale</label>
                                                        <select name="qaviaggio_soddisfazione" id="qaviaggio_soddisfazione" class="form-control">
                                                            <option value="eccellente">eccellente</option>
                                                            <option value="buono">buono</option>
                                                            <option value="male">male</option>
                                                            <option value="N/A" selected>N/A</option>
                                                        </select>
                                                    </div>

                                                    <div class="form-group">
                                                        <label for="reg_input">Data Partenza</label>
                                                        <input class="form-control" type="date"
                                                               name="qaviaggio_datapartenza" id="qaviaggio_datapartenza"   tabindex="55"
                                                               placeholder="gg/mm/aaaa">
                                                    </div>

                                                    <?php $preventivi=DB::table("preventivo")->where("assegnato","=","0")->where("statoPreventivo","=","preventivo confermato")->get();?>
                                                    <div class="form-group">
                                                        <label for="reg_input">Collega a un preventivo confermato</label>
                                                        <select name="qaviaggio_preventivo" class="form-control">
                                                          <option selected="" disabled="">Scegli un preventivo</option>
                                                           @foreach($preventivi as $preventivo)
                                                           <option value="{{$preventivo->id}}" data-dest="{{$preventivo->labeldest}}">{{$preventivo->labeldest}}</option>
                                                           @endforeach
                                                        </select>
                                                    </div>

                                                    <div class="form-group">
                                                        <label for="reg_input">Destinazione</label>
                                                        <input type="text" id="qaviaggio_destinazione" name="qaviaggio_destinazione"   tabindex="56"
                                                               class="form-control">
                                                    </div>
                                                    <div class="form-group">
                                                      <label for="reg_input">Soddisfazione servizi Suend</label>
                                                            <select name="qaviaggio_valutazione" id="qaviaggio_valutazione" class="form-control">
                                                            <option value="eccellente">eccellente</option>
                                                            <option value="ottimo">ottimo</option>
                                                            <option value="buono">buono</option>
                                                            <option value="discreto">discreto</option>
                                                            <option value="sufficiente">sufficiente</option>
                                                            <option value="mediocre">mediocre</option>
                                                            <option value="pessimo">pessimo</option>
                                                        </select>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="reg_input">Ultima Rilevazione</label>
                                                        <input class="form-control" type="date"
                                                               name="qaviaggio_ultimarilevazione"
                                                               id="qaviaggio_ultimarilevazione"

                                                               value="{{ isset($qaviaggio->ultima_rilevazione) ? $qaviaggio->ultima_rilevazione : ''  }}"
                                                               placeholder="gg/mm/aaaa">
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="reg_input">Commenti</label>
                                                        <textarea cols="30" name="qaviaggio_storico" id="qaviaggio_storico" rows="3"   tabindex="57"
                                                                  class="form-control input-sm"></textarea>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div id="profilo" class="tab-pane fade">

                                            <div class="row">
                                                <div class="col-md-8">

                                                <div class="form-group">
                                                    <label>Luogo di nascita</label>
                                                    <input type="text" id="luogonascita" name="luogonascita"   tabindex="58" class="form-control">
                                                </div>

                                                    <div class="form-group">
                                                        <label>Professione</label>
                                                        <input type="text" id="profilo_professione" name="profilo_professione"   tabindex="58"
                                                               class="form-control">
                                                    </div>
                                                    <div class="form-group">
                                                        <label>Hobby 1</label>
                                                        <input type="text" id="href="#profilo"_hobby1" name="profilo_hobby1"   tabindex="59"
                                                               class="form-control">
                                                    </div>
                                                    <div class="form-group">
                                                        <label>Hobby 2</label>
                                                        <input type="text" id="profilo_hobby2" name="profilo_hobby2"   tabindex="60"
                                                               class="form-control">
                                                    </div>
                                                    <div class="form-group">
                                                        <label>Numero Carta Identita</label>
                                                        <input class="form-control " type="text"
                                                               name="profilo_cartaidentita" id="profilo_cartaidentita" tabindex="61">
                                                    </div>
                                                    <div class="form-group">
                                                        <label>Scadenza Carta identita'</label>
                                                        <input class="form-control " type="date"
                                                               name="profilo_datascadenzaidentita" id="profilo_datascadenzaidentita" tabindex="62"
                                                               placeholder="gg/mm/aaaa">
                                                    </div>
                                                    <div class="form-group">
                                                        <label>Passaporto</label>
                                                        <input class="form-control " type="text"
                                                               name="profilo_passaporto" id="profilo_passaporto" tabindex="63">
                                                    </div>
                                                    <div class="form-group">
                                                        <label>Scadenza Passaporto</label>
                                                        <input class="form-control " type="date"
                                                               name="profilo_datascadenzapassaporto" id="profilo_datascadenzapassaporto" tabindex="64"
                                                               placeholder="gg/mm/aaaa">
                                                    </div>
                                                    <div class="form-group">
                                                        <div class="checkbox">
                                                            <label>
                                                                <input type="checkbox" name="profilo_newsletter" tabindex="65" value="true">
                                                                Newsletter
                                                            </label>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <div class="checkbox">
                                                            <label>
                                                                <input type="checkbox" name="profilo_clientediretto" tabindex="66" value="true">
                                                                Cliente diretto
                                                            </label>
                                                        </div>
                                                    </div>

                                                </div>
                                                <div class="col-md-4">
                                                    <br>
                                                    <div class="form-group">
                                                        <label>Frequent Flyer</label>
                                                        <textarea cols="30" rows="20" class="form-control input-sm" name="profilo_frequentflyer" tabindex="67"
                                                                  placeholder=""></textarea>
                                                    </div>
                                                </div>

                                            </div>
                                            <div class="row">
                                                <div class="col-md-8">
                                                    <label>Formazione Nucleo Familiare</label>
                                                    <br>
                                                    <div class="col-md-2">

                                                        <label>Adulti</label>
                                                        <input type="text" id="profilo_adulti" name="profilo_adulti" tabindex="68"
                                                               class="form-control">
                                                    </div>
                                                    <div class="col-md-2">
                                                        <label>Bambini</label>
                                                        <input type="text" id="profilo_bambini" name="profilo_bambini" tabindex="69"
                                                               class="form-control">
                                                    </div>
                                                    <div class="col-md-2">
                                                        <label>Neonati</label>
                                                        <input type="text" id="profilo_neonati" name="profilo_neonati" tabindex="70"
                                                               class="form-control">
                                                    </div>
                                                    <div class="col-md-2" style="display: none;">

                                                        <label>Figli</label>
                                                        <input type="text" id="profilo_figli" name="profilo_figli" tabindex="71"
                                                               class="form-control">
                                                    </div>
                                                    <div class="col-md-2">
                                                        <label>Anziani</label>
                                                        <input type="text" id="profilo_anziani" name="profilo_anziani" tabindex="72"
                                                               class="form-control">
                                                    </div>
                                                    <div class="col-md-2">
                                                        <label>Animali</label>
                                                        <input type="text" id="profilo_animali" name="profilo_animali" tabindex="73"
                                                               class="form-control">
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label>Note</label>
                                                        <textarea style="height: 300px;" cols="30" rows="5" class="form-control input-sm" name="profilo_note"  tabindex="74"
                                                                  placeholder=""></textarea>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-8">
                                                    <div class="col-md-2">
                                                        <div class="form-group">
                                                            <div class="checkbox">
                                                                <label>
                                                                    <input type="checkbox" value="true" name="profilo_iscrittoclub"  tabindex="75">
                                                                    Iscritto Club
                                                                </label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-10">
                                                        <div class="form-group">
                                                            <label>Codice Club</label>
                                                            <input type="text" id="profilo_codiceclub" name="profilo_codiceclub"  tabindex="76"
                                                                   class="form-control">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div id="relazione" class="tab-pane fade">
                                                <div id="storicoContatti">

                                                    <div class="form-group">
                                                        <label for="reg_input">Aggiorna relazione</label>
                                                            <select type="text" id="status" name="status" class="form-control">
                                                                <option selected disabled value="">Scegli uno step della relazione</option>
                                                                <option>contattato - Sms </option>
                                                                <option>contattato - Mail </option>
                                                                <option>contattato - Recall  </option>
                                                                <option>contattato - Visita Diretta  </option>
                                                                <option>Da risentire</option>
                                                                <option>Interessato</option>
                                                                <option>Non interessato</option>
                                                                <option>Cliente suend</option>
                                                            </select>
                                                    </div>

                                                    <div class="form-group">
                                                        <a href="#" class="btn btn-success btn-md applyStatus" onclick="addStatus()">
                                                          <span class="glyphicon glyphicon-plus"></span> Applica
                                                        </a>
                                                    </div>
                                                    <div class="form-group">


                                                        <!-- Modal -->
                                                        <div id="notaStato" class="modal fade" role="dialog">
                                                          <div class="modal-dialog">

                                                            <!-- Modal content-->
                                                            <div class="modal-content">
                                                              <div class="modal-header">
                                                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                                <h4 class="modal-title">Desideri aggiungere una nota per questo contatto?</h4>
                                                              </div>
                                                              <div class="modal-body">
                                                                <p><input type="text" name="notaStato" placeholder="aggiungi qui la tua nota"></p>
                                                              </div>
                                                              <div class="modal-footer">
                                                                <button type="button" class="btn btn-default" data-dismiss="modal" onclick="addStatusStep2()">Applica e chiudi</button>
                                                              </div>
                                                            </div>

                                                          </div>
                                                        </div>

                                                        <label for="reg_input">Stato relazione</label>
                                                        <listaStati>
                                                            <p class="defaultMsg">Non sono stati inseriti dati su questo cliente</p>
                                                        </listaStati>
                                                    </div>
                                                </div>
                                        </div>

                                        <div id="coupon" class="tab-pane fade">
                                                <p><b>Attenzione!</b> Per utilizzare questa sezione scegliere un privato a cui associare la richiesta di preventivo e salvare la pagina per applicare le modifiche</p>
                                        </div>
                                        
                                    </div>
                                </div>
                            </div>
                            <button class="btn btn-lg btn-success btn-block confirmsave">Aggiungi Cliente</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

@endsection
@section('footer-plugin')

  <script src="{{ asset('/lib/bootstrap-datepicker/js/bootstrap-datepicker.js') }}"></script>
  <script src="{{ asset('/lib/mydestination.js') }}"></script>
  <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>


    <script type="text/javascript">
        var passate=JSON.parse('<?php echo json_encode($destinazioni);?>');

            function setAutocompile(kind){
              $("#citta").typeahead("destroy");

              if(kind==0)
                var path = "{{ route('autocomplete') }}";
              else
                var path = "{{ route('autocomplete_ext') }}";

              var engine = new Bloodhound({
                  datumTokenizer: Bloodhound.tokenizers.whitespace('username'),
                  queryTokenizer: Bloodhound.tokenizers.whitespace,
                  remote:{
                      url: path + '?term=%QUERY',
                      wildcard: '%QUERY'
                  }
              });

              engine.initialize();

              $("#citta").typeahead({
                  hint: true,
                  highlight: true,
                  minLength: 2
              }, {
                  source: engine.ttAdapter(),
                  limit:1000,
                  name: 'comune',
                  displayKey: 'comune',
                  templates: {
                      empty: [
                          '<div class="empty-message">Nessun risultato</div>'
                      ].join('\n'),

                      suggestion: function (data) {
                          return '<div class="citta-search-result"><h4>'+ data.comune +'</h4></div>'
                      }
                  },

              });
            }

        $(document).ready(function () {
          setAutocompile(0);

            var lastAvaibleId=Number('<?php echo $lastId?>');

            $('#citta').bind('typeahead:selected', function(obj, datum, name) {

                var cap=datum.cap.split("-");
                if(cap.length>1)
                    cap=cap[0];
                else
                    cap=datum.cap;
                $("#provincia").val(datum.provincia);
                $("#cap").val(cap);
                $("#regione").val(datum.regione);
                $("#stato").val(datum.stato);
                $("#codice").val(datum.sigla+"-"+lastAvaibleId);
            });

            $("#info-citta2").bind('typeahead:selected', function(obj, datum, name) {

                $("#info-provincia2").val(datum.provincia);
                $("#info-cap2").val(datum.cap);
                $("#info-regione2").val(datum.regione);
                $("#info-stato2").val(datum.stato);
            });


            $('input').css('text-transform','uppercase');
            $('[name="qaviaggio_preventivo"]').change(function(){
                var data=$(this).find("option:selected").data();
                $('[name="qaviaggio_destinazione"]').val(data.dest);
            })
        })

        var name="<?php echo $name?>";

        function addStatus(){

            if($("#status option:selected").val()=="")
                return;
            $("#notaStato input").val("");
            $("#notaStato").modal();
        }

        function addStatusStep2(){
                $(".defaultMsg").remove();
                var time=new Date();
                    time=time.getDate()+"/"+(time.getMonth()+1)+"/"+time.getFullYear();
                var status=$("#status option:selected").val();
                    if($('[name="notaStato"]').val()!="")
                        var nota="<p>Con la seguente nota: "+$('[name="notaStato"]').val()+"</p>";
                    else
                        var nota="";
                var row="<p>l'utente <strong>"+name+"</strong> il <strong>"+time+"</strong> ha aggiornato lo stati in: <br><strong>"+status+"</strong>"+nota+"</p>";
                $("listaStati").prepend(row);
                $("listaStati").append("<input type='hidden' id='statiStato[]' name='statiStato[]' value='"+status+"'>");
                $("listaStati").append("<input type='hidden' id='statiNota[]' name='statiNota[]' value='"+$('[name="notaStato"]').val()+"'>");
            }

            function validateFs(el){
                if(el.value.length<16)
                    alert("Inserisci un codice fiscale valido");
            }




  var destinazioniPreferite=JSON.parse("<?php echo addslashes(json_encode($destinazioniPreferite)) ?>");
  var destinazioniPassate=JSON.parse("<?php echo addslashes(json_encode($destinazioniPassate)) ?>");
  var destinazioniProssime=JSON.parse("<?php echo addslashes(json_encode($destinazioniProssime)) ?>");
  var destinazioniDove=JSON.parse("<?php echo addslashes(json_encode($destinazioniDove)) ?>");


  var md1=new mydestination("#preferite",'passate',
    {
      load_source: destinazioniPreferite,
      variable_name: 'passate',
      resultcontainer:'res_destinazionipreferite',
      hiddenname:' destinazionipreferite[]',
      table_name: 'destinazioniPreferite',
      label: 'Destinazioni preferite'
    }
  );

  var md2=new mydestination("#passate",'passate',
    {
      load_source: destinazioniPassate,
      variable_name: 'passate',
      resultcontainer:'res_destinazionipassate',
      hiddenname:' destinazionipassate[]',
      table_name: 'destinazioniPassate',
      label: 'Destinazioni passate'
    }
  );

  var md3=new mydestination("#prossime",'passate',
    {
      load_source: destinazioniProssime,
      variable_name: 'passate',
      resultcontainer:'res_destinazioniprossime',
      hiddenname:' destinazioniprossime[]',
      table_name: 'destinazioniProssime',
      label: 'Destinazioni prossime'
    }
  );

  var md4=new mydestination("#qasuend_doveviaggi",'passate',
    {
      load_source: destinazioniDove,
      variable_name: 'passate',
      table_name: 'destinazioniDove',
      resultcontainer:'res_destinazioniDove',
      hiddenname:' destinazioniDove[]',
      label: 'Dove'
    }
  );

function just_remove_container(el){
  var data=$(el).data();
  $(el).parents(data.parent).hide(1000);
  $(el).parents(data.parent).remove();
    $.post(apiUrl+'api.php',data,function(response){
  });

}

  console.error("admin/customer");
</script>


@endsection
