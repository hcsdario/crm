@extends('layouts.master')
@section('header-page-personalization')
    <!-- page specific stylesheets -->
    <!-- nvd3 charts -->
    <link rel="stylesheet" href="{{ asset('/lib/novus-nvd3/nv.d3.min.css') }}">
    <!-- owl carousel -->
    <link rel="stylesheet" href="{{ asset('/lib/owl-carousel/owl.carousel.css') }}">

    <!-- main stylesheet -->
    <link href="{{ asset('/css/style.css" rel="stylesheet') }}" media="screen">

    <!-- google webfonts -->
    <link href='https://fonts.googleapis.com/css?family=Source+Sans+Pro:400&amp;subset=latin-ext,latin' rel='stylesheet' type='text/css'>

    <!-- moment.js (date library) -->
    <script src="{{ asset('/lib/moment-js/moment.min.js') }}"></script>

@endsection
@section('content')
<?php  $userid=Auth::user()->id; ?>

    <div class="page_content page_dashboard">
    <div class="container-fluid">
                    @if(session()->has('success'))
                        <div class="alert alert-success">
                            {{ session()->get('success') }}
                        </div>
                    @endif
                    @if(session()->has('error'))
                        <div class="alert alert-danger">
                            {{ session()->get('error') }}
                        </div>
                    @endif



                <!-- inizo dashboard -->

                <div>
                    <div class="row">
                       <div class="col-lg-12">
                            <?php
                                $html="";
                                $breaking=DB::table("news")->where("breaking","1")->select(["news_title","news_body"])->get();
                                $i=1;
                                foreach ($breaking as $bnews){
                                    $html.="<strong> #".$i." News: </strong>". strip_tags($bnews->news_title)." ";
                                    $i++;
                                }
                                if(!empty($html)){
                            ?>
                                <div id="div" class="alert alert-warning alert-dismissible" style="display: none;" >
                                    <span>
                                        <p id="par" style="font-family:sans-serif;position: absolute;">
                                         <?php echo $html ?></p>
                                        <a id="close_brk_news" onclick="remove_breaking()" href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                    </span>
                                </div>
                                <?php } ?>
                        </div>
                    </div>


                    <div class="row">
                       <div class="col-lg-4">
                          <div class="boxusers">
                             <h2>CLIENTI PRIVATI</h2>
                             <div class="iconbox1">
                                <i class="fa fa-users"></i>
                             </div>
                             <div class="optionbox optionbox1">
                                <div><i class="fa fa-plus-circle"></i><a href="/admin/cliente"><span>NUOVO CLIENTE</span></a></div>
                                <div><i class="fa fa-list-ul"></i><a href="{{ route('clienti_admin') }}"><span>LISTA CLIENTI</span></a></div>
                                <div><i class="fa fa-search"></i><a href="{{ route('search_privato') }}"><span>CERCA CLIENTE</span></a></div>
                             </div>
                             <div class="optionbox optionbox2">
                                <div><i class="fa fa-plus-circle"></i><a href="{{ url('/admin/preventivo') }}"><span>NUOVO PREVENTIVO</span></a></div>
                                <div><i class="fa fa-list-ul"></i><a href="/admin/preventivi?view=all"><span>LISTA PREVENTIVI</span></a></div>
                                <div><i class="fa fa-search"></i><a href="/admin/preventiviSearch"><span>CERCA PREVENTIVO</span></a></div>
                             </div>
                             <div style="clear:both;"></div>
                          </div>
                       </div>
                       <div class="col-lg-4">
                          <div class="boxbusiness">
                             <h2>CLIENTI BUSINESS</h2>
                             <div class="iconbox">
                                <i class="fa fa-briefcase"></i>
                             </div>
                             <div class="optionbox optionbox3">
                                <div><i class="fa fa-plus-circle"></i><a href="/user/business/0"><span>NUOVO CLIENTE BUSINESS</span></a></div>
                                <div><i class="fa fa-list-ul"></i><a href="{{ route('clienti_admin_ext') }}"><span>LISTA CLIENTI BUSINESS</span></a></div>
                                <div><i class="fa fa-search"></i><a href="{{ route('search_privato_ext') }}"><span>CERCA CLIENTE BUSINESS</span></a></div>
                             </div>
                             <div style="clear:both;"></div>
                          </div>
                       </div>
                       <div class="col-lg-4">
                          <div class="boxagency">
                             <h2>AGENZIE DI VIAGGIO</h2>
                             <div class="iconbox1">
                                <i class="fa fa-plane" style="margin-left: 10px;"></i>
                             </div>
                             <div class="optionbox optionbox1">
                                <div><i class="fa fa-plus-circle"></i><a href="/user/new"><span>NUOVA AGENZIA DI VIAGGIO</span></a></div>
                                <div><i class="fa fa-list-ul"></i><a href="/admin/adv"><span>LISTA AGENZIA DI VIAGGIO</span></a></div>
                                <div><i class="fa fa-search"></i><a href="/admin/cerca_adv"><span>CERCA AGENZIA DI VIAGGIO</span></a></div>
                             </div>
                             <div class="optionbox optionbox2">
                                <div><i class="fa fa-plus-circle"></i><a href="/user/new?adv=ext"><span>NUOVA ADV ESTERA</span></a></div>
                                <div><i class="fa fa-list-ul"></i><a href="/admin/adv_ext"><span>LISTA ADV ESTERE</span></a></div>
                                <div><i class="fa fa-search"></i><a href="/admin/cerca_advext"><span>CERCA ADV ESTERA</span></a></div>
                             </div>
                             <div style="clear:both;"></div>
                          </div>
                       </div>
                    </div>
                    <div class="row ">
                    <!--  inizio sezione task -->
                       <div class="col-lg-6 div_task">
                        <div class="todo_date">
                             <h4 class="big-title">{{ $task_total }} Task Pendenti</h4>
                        </div>
                          <div class="todo_section panel panel-default scollable_dashboard_task">
                            @foreach($tasks as $task)
                             <ul class="todo_list_wrapper">
                                <li data-task-title="{{ $task->task_note}}" data-task-label="{{ $task->task_note}}" data-task-date="{{ $task->created_at}}">
                                    @if($task->user_id)
                                        <span class="label color_e pull-right">{{ \App\Http\Controllers\CategoryController::getUserData($task->user_id) }}</span>
                                    @endif

                                    <span class="label color_{{ \App\Http\Controllers\CategoryController::showCategoryColor($task->category_id) }} pull-right">{{ \App\Http\Controllers\CategoryController::showCategoryName($task->category_id)}}</span>

                                    @if($task->preventivo>0)
                                        <span class="label color_d pull-right">PREVENTIVO</span>
                                    @endif

                                    <span>Task creato da: <strong class="nome-utente-task"> {{\App\Http\Controllers\DashboardController::whoHasCreatedTask($task->user_assigned_id)}} </strong></span>

                                    <h4 class="todo_title">

                                        <a href="#todo_task_modal_{{ $task->id }}"><i class="glyphicon glyphicon-search" class="disabled"></i> {{ $task->task_note}}</a>

                                    </h4>
                                    <span> Dal <strong>{{\Carbon\Carbon::parse($task->created_at)->format('d-m-Y') }}</strong></span>
                                    <span>al <strong> {{\Carbon\Carbon::parse($task->task_deadline)->format('d-m-Y') }}</strong></span>
                                    <br>
                                </li>
                             </ul>


                            <!-- Modal -->
                            <div id="breaking_news_modal" class="modal fade" role="dialog">
                              <div class="modal-dialog">

                                <!-- Modal content-->
                                <div class="modal-content">
                                  <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                    <h4 class="modal-title">Breaking News</h4>
                                  </div>
                                  <div class="modal-body">
                                  <ul>

                                  </ul>
                                  </div>
                                  <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                  </div>
                                </div>

                              </div>
                            </div>


                            <div class="modal fade taskpopup" id="todo_task_modal_{{ $task->id }}" data-id="{{ $task->id }}">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <form  id="login_form" role="form" method="POST" action="{{ route('send-mail') }}">
                                            {{ csrf_field() }}
                                            <input type="hidden" name="task_id" value="{{ $task->id}}">
                                            <input type="hidden" name="task_action" value="">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                                <h4 class="modal-title">Dettagli task <reparto class="reparto-warning"> supervisionato da:  <supervisor></supervisor> </reparto> </h4>

                                            </div>
                                            <div class="modal-body">
                                                <div class="form-group">
                                                    <div class="row">
                                                        <div class="col-sm-12">
                                                            <h5 class="taskNote">{{ $task->task_note}}</h5>
                                                        </div>
                                                        <notelist class="col-sm-12">

                                                        <p>Note:</p>
                                                        <ul>
                                                            <li class="default">
                                                                <p>Nessuna nota</p>
                                                            </li>
                                                        </ul>
                                                         </notelist>
                                                        <noteform class="col-sm-12">
                                                                <div>

                                                                <h4>
                                                                    <a id="addnote" href="#" class="addnote btn btn-xs btn-primary news-zama" onclick="addnote(this,{{ $task->id}})">
                                                                        <i class="glyphicon glyphicon-plus"></i>&nbsp;Aggiungi nota
                                                                    </a>
                                                                    <a style="display: none;" id="savenote" class="savenote btn btn-success news-zama" href="#" onclick="savenote(this,{{ $task->id}})">
                                                                        <i class="glyphicon glyphicon-check"></i>&nbsp;Salva nota
                                                                    </a>
                                                                    <!-- glyphicon-floppy-disk -->
                                                                </h4>
                                                                </div>

                                                        </noteform>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                </div>
                                            </div>
                                            <div class="modal-footer">
                                            <reparto class="reparto-warning">Attenzione: solo il supervisore <supervisor></supervisor> può chiudere questo task<br></reparto>
                                            <button type="button" class="btn btn-success closeAsSupervisor" onclick="requestConfirm(this,'Confermi che il compito di questo task è stato svolto?')">Segnala il task come concluso</button>

                                                @if(Auth::user()->admin == 1)
                                                   <button type="button" class="btn btn-danger" onclick="requestConfirm(this)">Rifiuta task</button>
                                                @endif
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        @endforeach




                          </div>
                       </div>
<div class="row news_section">
    <div class="col-lg-6 div_task">
       <div class="todo_date news_todo_date">
          <h4 class="big-title">News</h4>
       </div>
       <div class="todo_section panel panel-default scollable_dashboard_task">

                        <table class="table table-bordered" id="news_table">
                            <thead>
                               <tr>
                                  <th>Titolo</th>
                                  <th>Autore</th>
                                  <th width="10%">Azioni</th>
                               </tr>
                            </thead>
                            <tbody>
                               @foreach($news as $new)
                               <?php
                                  if(!empty($new->evidenzadeadline))
                                  {
                                      $news_deadline=new Datetime($new->evidenzadeadline);
                                      $today=new Datetime();
                                      if($news_deadline<$today)
                                          $new->evidenza=0;
                                  }

                                  if($new->userid == '-3'){
                                      $a=unserialize($new->news_title);
                                      $b=unserialize($new->news_body);

                                      $head="";
                                      $body="";

                                      if(isset($a['h1']))
                                          $head.=$a['h1']." compie gli anni oggi!";

                                      if(isset($b['b1']))
                                          $body.=" <span class='glyphicon glyphicon-earphone'></span> ".$b['b1']." ";
                                      if(isset($b['b2']))
                                          $body.=" <span class='glyphicon  glyphicon-phone-alt'></span>".$b['b2']." ";
                                      if(isset($b['b3']))
                                          $body.=" <span class='glyphicon  glyphicon-envelope'></span> ".$b['b3']." ";
                                      $body.="";
                                      $new->news_title=$head;
                                      $new->news_body=$body;
                                  }



                                  ?>
                               <tr>
                                  <td>
                                     @if($new->evidenza == '1')
                                     <span class="label color_h pull-left newsitem">Evidenza</span>
                                     @endif
                                     {{ $new->news_title }}
                                  </td>
                                  <td> <span class="label color_e pull-right">{{$new->username}}</span> </td>
                                  <td>
                                     @if($new->userid == '-3')
                                     <a href="#" class="btn btn-xs news-zama" style="background: #e67e22;color:white" data-toggle="modal" data-target="#news-modal-{{ $new->id }}">
                                      <i class="glyphicon glyphicon-earphone" class="disabled"></i>&nbsp;Contattalo
                                     </a>
                                     @else
                                     <a href="#" class="btn btn-xs btn-primary news-zama" data-toggle="modal" data-target="#news-modal-{{ $new->id }}">
                                      <i class="glyphicon glyphicon-search" class="disabled"></i>&nbsp;Visualizza
                                     </a>
                                     @endif

                                     <div class="modal fade newspopup" id="news-modal-{{ $new->id }}" tabIndex="-1" style="display: none;">
                                        <div class="modal-dialog">
                                           <div class="modal-content">
                                              <div class="modal-header">
                                                 @if($new->evidenza == '1')
                                                 @if($new->userid == '-3')
                                                 <p><img src="../img/cake.png" alt="Compleanno" width="30" height="30"> <strong>Contatta il festeggiato</strong></p>
                                                 @else
                                                 <span class="label color_h pull-left"> News in Evidenza</span>&nbsp;&nbsp;
                                                 <h4 class="modal-title2">
                                                 Titolo: {{ htmlspecialchars_decode ($new->news_title) }}
                                                 <h4>
                                                 <h6>
                                                    Creata il: {{\Carbon\Carbon::parse($new->created_at)->format('d-m-yy') }}
                                                    alle ore:{{\Carbon\Carbon::parse($new->created_at)->format('H:i:s') }}
                                                 </h6>
                                                 @endif
                                                 @endif
                                              </div>
                                              <div class="modal-body">
                                                 {!!   htmlspecialchars_decode ($new->news_body)  !!}
                                              </div>
                                              <div class="modal-footer">
                                                 <button type="button" class="btn btn-danger" data-dismiss="modal" aria-hidden="true">Chiudi</button>
                                              </div>
                                           </div>
                                        </div>
                                     </div>
                                  </td>
                               </tr>

                               @endforeach
                            </tbody>
                        </table>
       </div>
    </div>

                    </div>
                </div>



                    <!-- fine dashboard -->








    </div>
    </div>

@endsection
@section('footer-plugin')

    <!-- textarea autosize -->
    <script src="{{ asset('/lib/autosize/jquery.autosize.min.js') }}"></script>
    <script src="{{ asset('/lib/d3/d3.min.js') }}"></script>
    <script src="{{ asset('/lib/novus-nvd3/nv.d3.min.js') }}"></script>
    <!-- flot charts-->
    <script src="{{ asset('/lib/flot/jquery.flot.min.js') }}"></script>
    <script src="{{ asset('/lib/flot/jquery.flot.pie.min.js') }}"></script>
    <script src="{{ asset('/lib/flot/jquery.flot.resize.min.js') }}"></script>
    <script src="{{ asset('/lib/flot/jquery.flot.tooltip.min.js') }}"></script>
    <!-- clndr -->
    <script src="{{ asset('/lib/underscore-js/underscore-min.js') }}"></script>
    <script src="{{ asset('/lib/CLNDR/src/clndr.js') }}"></script>
    <!-- easy pie chart -->
    <script src="{{ asset('/lib/easy-pie-chart/dist/jquery.easypiechart.min.js') }}"></script>
    <!-- owl carousel -->
    <script src="{{ asset('/lib/owl-carousel/owl.carousel.min.js') }}"></script>

    <!-- dashboard functions -->
    <script src="{{ asset('/js/apps/tisa_dashboard.js') }}"></script>
    <!-- datatables -->
    <script src="{{ asset('/lib/DataTables/media/js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('/lib/DataTables/media/js/dataTables.bootstrap.js') }}"></script>
    <script src="{{ asset('/lib/DataTables/extensions/TableTools/js/dataTables.tableTools.min.js') }}"></script>
    <script src="{{ asset('/lib/DataTables/extensions/Scroller/js/dataTables.scroller.min.js') }}"></script>

    <!-- datatables functions -->
    <script src="{{ asset('/js/apps/tisa_datatables.js') }}"></script>
    <!-- todo functions -->
    <script src="{{ asset('/js/apps/tisa_todo.js') }}"></script>

<script type="text/javascript">
    console.error("admin/new home ");


    window['table']="tasks";
    window['isntable']="li";
    var userid="<?php echo $userid;?>";

    function remove_breaking(){
        $("#div").slideUp(1000);
    }


    function deletenote(id,el){
        window['tmp']=el
        $.post("../api.php",{qr:'delnote',id:id},function(response){
            $(window['tmp']).parents("li").remove();
        })
    }

    var savenoteflag=false;
    function savenote(el,taskid){
        var value=$(el).parents(".modal-body").find("textarea").val();
        var note=$(el).parents(".modal-body").find(".taskNote").text();
        var title=$(el).parents(".modal-body").find(".taskNote").text();
        if(value==""){
            $(el).parents("noteform").html("");
            return;
        }
        if(savenoteflag)
            return;
        savenote=true;
        window['el']=el;

        $.post("../api.php",{qr:'addnote',taskid:taskid,text:value,note:note,title:title},function(response){
            window.location.reload(true);
        })
    }

    function addnote(el,id){
        if($(".noteformcontainer:visible").length)
            return;
        var html=
            '<div class="form-group noteformcontainer">'+
                '<div class="">'+
                    '<textarea id="textinput" name="textinput" type="text" placeholder="Nota di testo" style="width:300px;height:100px;" class="form-control"></textarea>'+
                '<span class="help-block">'+'</span>  '+
            '</div>';
        $(el).parents("noteform").prepend(html);
        $("#savenote").attr("data-id",id);
        $(el).parents("noteform").find(".addnote").toggle();
        $(el).parents("noteform").find(".savenote").toggle();
    }

    var breaking= JSON.parse("<?php echo addslashes(json_encode($breaking)); ?>");
    $(document).ready(function () {

        $("#div").click(function(){
            $("#breaking_news_modal ul").html("");
            for(var i=0;i<breaking.length;i++){
                var el="<div class='br1'><div class='br2'>news n. #"+(i+1)+" "+breaking[i].news_title+"</div><div class='br3'>"+breaking[i].news_body+"</div>";
                var li="<li>"+el+"</li>";
                $("#breaking_news_modal ul").append(li);

            }
            $("#breaking_news_modal").modal();

        })

        var l=$('.todo_list_wrapper').length;
        $('#side_nav .label.label-danger').text(l);
        $(".todo_title a").on('click', function () {

            var href=$(this).attr("href");
            var id=href.replace("#todo_task_modal_","");
            window['popup']=href;
            $(window['popup']+" reparto").hide();
            $.post("../api.php",{qr:'loadnote',id:id},function(response){
                response=JSON.parse(response);

                if(response.length==0)
                    return;
                  debugger;
                var el=window['popup'];
                $(el+" notelist ul").html("");

                for(var i=0;i<response.data.length;i++){
                    var pre=response.data[i].username+" / alle ore : "+response.data[i].timer+" ha aggiunto una nota"+'<i class="fa fa-times" style="margin-left:15px;color:#9F2F24" onclick="deletenote('+response.data[i].id+',this)" aria-hidden="true"></i>';

                    if($(el).find("notelist ul li.default"))
                        $(el).find("notelist ul li.default").remove();

                    if(userid==response.data[i].userid)
                       $(el).find("notelist ul").append("<li><p>"+pre+"</p>"+response.data[i].note+"</li>");
                    else
                       $(el).find("notelist ul").append("<li><p>"+pre+"</p>"+response.data[i].note+"</li>");
                }
                /* task di gruppo */
                if(response.info.length==0)
                  return;

                $(el+" .modal-footer *").hide();
                if(userid==response.info[0].task_supervisor)
                {

                    $(window['popup']+" reparto").show();
                    $(window['popup']+" supervisor").show();
                    $(window['popup']+" .closeAsSupervisor").show();
                }
                if(userid!=response.info[0].task_supervisor){
                  $(window['popup']+" supervisor").show();
                  $(window['popup']+" .reparto-warning").show();
                  $(window['popup']+" .closeAsSupervisor").hide();
                }

                $(window['popup']+" supervisor").text(response.info[0].supervisor);
            })
        })

        $("#div").slideDown(1000);


	var par = document.getElementById('par');
	var div = document.getElementById('div');
  var w = getComputedStyle(div).width;
	var parw = $("#par").width();
	var s = 0;
    var el = $("#par")
    var wdt = $("#div").width()+el.width();
    movefunction()

    function movefunction() {
        let ml = parseInt(el.css('margin-left'));
        if (ml<0 && Math.abs(ml) > parw){
          el.css('margin-left',w);
        }else{
          el.css('margin-left', ml-3+'px')
        }
        setTimeout(movefunction,50)
	   }

    })


</script>
@endsection
