@extends('layouts.master')
@section('header-page-personalization')
    <!-- page specific stylesheets -->
<?php 
ini_set('get_max_size' , '20M');
?>
    <link href="{{ asset('/css/style.css') }}" rel="stylesheet" media="screen">

    <!-- google webfonts -->
    <link href='https://fonts.googleapis.com/css?family=Source+Sans+Pro:400&amp;subset=latin-ext,latin' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="{{ asset('/lib/DataTables/media/css/jquery.dataTables.min.css') }}">

    <style>
        #customers_table thead,
        #customers_table th {text-align: center;}
        #customers_table tr {text-align: center;}

        #customers_adv_table thead,
        #customers_adv_table th {text-align: center;}
        #customers_adv_table tr {text-align: center;}

        .hideBecauseDatatablesSucks{
        }

        .search{
            text-align: center;
        }

        #dateRangeForm .form-control-feedback {
            top: 0;
            right: -15px;
        }
    </style>
@endsection
@section('content')

<?php 

$nome=DB::select("SELECT DISTINCT nome as label,nome as value FROM `customers` WHERE nome!='' AND adv=1");
$societa=DB::select("SELECT DISTINCT societa as label,societa as value FROM `customers` WHERE societa!='' AND adv=1");
$destinazione=DB::select("
    SELECT DISTINCT destinazioni_vendute1 as label,destinazioni_vendute1 as value FROM `info_adv` WHERE destinazioni_vendute1!=''
    UNION
    SELECT DISTINCT destinazioni_vendute2 as label,destinazioni_vendute2 as value FROM `info_adv` WHERE destinazioni_vendute2!=''
    UNION
    SELECT DISTINCT destinazioni_vendute3 as label,destinazioni_vendute3 as value FROM `info_adv` WHERE destinazioni_vendute3!=''
    UNION
    SELECT DISTINCT destinazioni_vendute4 as label,destinazioni_vendute4 as value FROM `info_adv` WHERE destinazioni_vendute4!=''
    UNION
    SELECT DISTINCT destinazioni_vendute5 as label,destinazioni_vendute5 as value FROM `info_adv` WHERE destinazioni_vendute5!=''
    UNION
    SELECT DISTINCT destinazioni_vendute6 as label,destinazioni_vendute6 as value FROM `info_adv` WHERE destinazioni_vendute6!=''
    UNION
    SELECT DISTINCT destinazioni_vendute7 as label,destinazioni_vendute7 as value FROM `info_adv` WHERE destinazioni_vendute7!=''
    UNION
    SELECT DISTINCT destinazioni_vendute8 as label,destinazioni_vendute8 as value FROM `info_adv` WHERE destinazioni_vendute8!=''
    ");


?>
    <div class="page_content page_advSearch">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                        <div class="panel panel-default">
                            <div class="panel-body">

                            <form class="form-horizontal" id="search_privati"  method="get" action="../admin/adv">
                            <fieldset>

                            <legend><span class="big-title">Cerca adv</span></legend>
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label class="col-md-4 control-label" for="textinput">Nome</label>  
                                    <div class="col-md-4">
                                        <input id="rag" name="customerswjknome" type="text" placeholder="Ragione sociale" class="form-control input-md">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-4 control-label" for="textinput">Società</label>  
                                    <div class="col-md-4">
                                        <input id="iva" name="customerswjksocieta" type="text" placeholder="Partita iva" class="form-control input-md">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-4 control-label" for="textinput">Destinazione viaggi </label>  
                                    <div class="col-md-4">
                                        <input id="res" name="infoadvwjkdestinazione" type="text" class="form-control input-md">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-4 control-label" for="textinput">Tipologia </label>  
                                    <div class="col-md-4">
                                        <input id="res" name="customerswjkitaliana_o_estera" type="radio" value="0" class="input-md">Italiana <br>
                                        <input id="res" name="customerswjkitaliana_o_estera" type="radio" value="1" class="input-md">Straniera
                                    </div>
                                </div>
                            </div>


                              
  
                            <div class="form-group search" >
                              <div class="col-md-8">
                                <button id="button1id" name="button1id" class="btn btn-info">Cerca</button>
                              </div>
                            </div>

                            </fieldset>
                            </form>

                                </div>
                            </div>
                        </div>

                </div>
            </div>
        </div>
    </div>

  

@endsection
@section('footer-plugin')
    <!-- datatables -->
    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.18/pdfmake.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.2.4/js/dataTables.buttons.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.2.4/js/buttons.bootstrap.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.2.4/js/buttons.print.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.2.4/js/buttons.html5.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <script src="//cdn.datatables.net/buttons/1.4.2/js/buttons.colVis.min.js"></script>
    <script src="{{ asset('/lib/DataTables/extensions/Scroller/js/dataTables.scroller.min.js') }}"></script>
    <script src="https://crm.suend.it/js/tinynav.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.perfect-scrollbar/0.8.1/js/perfect-scrollbar.jquery.min.js"></script>
    <script src="{{ asset('/js/apps/tisa_datatables.js') }}"></script>
    <script src="{{ asset('/lib/moment-js/moment.min.js') }}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.2.0/jquery-confirm.min.js"></script>
    <script src="{{ asset('/lib/jquery.confirm/jquery-confirm.min.js') }}"></script>

    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/css/datepicker.min.css" />
    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/css/datepicker3.min.css" />
    <script src="{{ asset('/crm/resources/assets/js/bootstrap_datepicker.js') }}"></script>

  <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
  <link rel="stylesheet" href="/resources/demos/style.css">
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

    <script>

    var availableTags1= JSON.parse("<?php echo addslashes(json_encode($nome)) ;?>");
    var availableTags2= JSON.parse('<?php echo addslashes(json_encode($societa)) ;?>');
    var availableTags3= JSON.parse('<?php echo addslashes(json_encode($destinazione)) ;?>');

        $(document).ready(function(){
            $( "#rag" ).autocomplete({
              source: availableTags1
            });   
            $( "#iva" ).autocomplete({
              source: availableTags2
            });   
            $( "#res" ).autocomplete({
              source: availableTags3
            });            
        })
    console.error("admin/adv-search");
    </script>
@endsection
