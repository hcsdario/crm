@extends('layouts.master')
@section('header-page-personalization')
    <style>
        .twitter-typeahead, .tt-hint, .tt-input, .tt-menu { width: 100%;  }
        .tt-menu {
            background: #ffffff;
        }
        [readonly]{
          background-color: white; 
          opacity: 1;         
        }
        templatebuoni{
            display: none;
        }
    </style>

@endsection
@section('content')
<?php 
$name=Auth::user()->name." ".Auth::user()->surname; 
ini_set("log_errors", 1);
error_reporting(E_ALL);
ini_set("error_log", getcwd()."/crm/php-error.log");
ini_set("display_errors", "On");
?>
    <div class="page_content page_customer page_edit_customer">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <form id="login_form" role="form" method="post" action="../admin/cliente/{{$customer->id}}" class="customForm">
                        {{ csrf_field() }}
                        {{ method_field('PUT') }}

                        <div class="panel panel-default">
                            <div class="panel-body">
                                <div class="heading_a">Suend CRM</div>
                                <div class="tabbable tabs-left">
                                    <ul class="nav nav-tabs" id="tabs_c">
                                        <li class="active"><a data-toggle="tab" href="#info">Info</a></li>
                                        <li><a data-toggle="tab" href="#profilo">Profilo</a></li>
                                        <li><a data-toggle="tab" href="#infoviaggi">Info Viaggi</a></li>
                                        <li><a data-toggle="tab" href="#qasuend">Info Suend</a></li>
                                        <li><a data-toggle="tab" href="#qaviaggio">Valutazione Viaggio</a></li>
                                    </ul>
                                    @if(session()->has('message'))
                                        <div class="alert alert-success">
                                            {{ session()->get('message') }}
                                        </div>
                                    @endif
                                    @if(session()->has('error'))
                                        <div class="alert alert-danger">
                                            {{ session()->get('error') }}
                                        </div>
                                    @endif
                                    <div class="tab-content" id="tabs_content_c">
                                        <div id="info" class="tab-pane fade in active">
                                            <div class="row">
                                                <div class="col-lg-6">
                                                    <div class="form-group">
                                                        <label for="reg_input">Cognome</label>
                                                        <input type="text" id="cognome" name="cognome"
                                                               class="form-control"
                                                               value="{{ isset($customer->cognome) ? $customer->cognome : ''  }}"
                                                               tabindex="3">
                                                    </div>

                                                    <div class="form-group">
                                                        <label for="reg_input">Nome</label>
                                                        <input type="text" id="nome" name="nome" class="form-control"
                                                               tabindex="2"
                                                               value="{{ isset($customer->nome) ? $customer->nome : ''  }}">
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="reg_input">Data di Nascita</label>
                                                        <input type="date" id="info_data_nascita"

                                                               value="<?php  
                                                               if(isset($info->data_nascita)){
                                                                  $info->data_nascita=new Datetime($info->data_nascita);
                                                                  echo $info->data_nascita->format('Y/m/d');
                                                                }?>"
                                                               name="info_data_nascita"
                                                               class="form-control" tabindex="15">
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="reg_input">Sesso</label>
                                                        <select id="info_sesso" name="info_sesso" class="form-control"
                                                                tabindex="16">
                                                            <option value="{{ isset($info->sesso) ? $info->sesso : '' }}">
                                                                {{ isset($info->sesso) ? $info->sesso : ''}}
                                                            </option>
                                                            <option value="MASCHIO">MASCHIO</option>
                                                            <option value="FEMMINA">FEMMINA</option>
                                                        </select>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="reg_input">Indirizzo</label>
                                                        <input type="text" id="info_indirizzo" name="info_indirizzo"

                                                               value="{{ isset($info->indirizzo) ? $info->indirizzo : '' }}"
                                                               class="form-control" tabindex="11">
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="reg_input">Citta'</label>
                                                        <input type="text" id="citta" name="citta" class="form-control"
                                                               tabindex="4"
                                                               value="{{ isset($customer->citta) ? $customer->citta : ''  }}">
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="reg_input">Cap</label>
                                                        <input type="text" id="cap" name="cap" class="form-control"
                                                               tabindex="7"
                                                               value="{{ isset($customer->cap) ? $customer->cap : ''  }}">
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="reg_input">Provincia</label>
                                                        <input type="text" id="provincia" name="provincia"
                                                               class="form-control"
                                                               value="{{ isset($customer->provincia) ? $customer->provincia : ''  }}"
                                                               tabindex="5">
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="reg_input">Regione</label>
                                                        <input type="text" id="regione" name="regione"
                                                               class="form-control"
                                                               value="{{ isset($customer->regione) ? $customer->regione : ''  }}"
                                                               tabindex="6">
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="reg_input">Stato</label>
                                                        <input type="text" id="stato" name="stato" class="form-control"
                                                               tabindex="8"
                                                               value="{{ isset($customer->stato) ? $customer->stato : ''  }}">
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="reg_input">Telefono Principale</label>
                                                        <input type="text" id="tel1" name="tel1" class="form-control"
                                                               tabindex="9"
                                                               value="{{ isset($customer->tel1) ? $customer->tel1 : ''  }}">
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="reg_input">Telefono Secondario</label>
                                                        <input type="text" id="tel2" name="tel2"

                                                               value="{{ isset($customer->tel2) ? $customer->tel2 : ''  }}"
                                                               class="form-control"
                                                               tabindex="22">
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="reg_input">Email Principale</label>
                                                        <input type="text" id="email1" name="email1"
                                                               class="form-control"
                                                               value="{{ isset($customer->email1) ? $customer->email1 : ''  }}">
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="reg_input">Email Secondaria</label>
                                                        <input type="text" id="email2" name="email2"
                                                               value="{{ isset($customer->email2) ? $customer->email2 : ''  }}"
                                                               class="form-control"
                                                               tabindex="22">
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="reg_input">Codice Fiscale</label>
                                                        <input type="text" id="info_codice_fiscale"

                                                               value="{{ isset($info->codice_fiscale) ? $info->codice_fiscale : '' }}"
                                                               name="info_codice_fiscale"
                                                               class="form-control" tabindex="14">
                                                    </div>
                                                </div>
                                                <div class="col-lg-6" id="storicoContatti">
                                                    
                                                    <div class="form-group">
                                                        <label for="reg_input">Aggiorna relazione</label>
                                                        <select type="text" id="status" name="status" class="form-control">
                                                            <option selected disabled value="">Scegli uno step della relazione</option> 
                                                            <option>contattato - Sms </option>
                                                            <option>contattato - Mail </option>
                                                            <option>contattato - Recall  </option>
                                                            <option>contattato - Visita Diretta  </option>
                                                            <option>Da risentire</option>
                                                            <option>Interessato</option>
                                                            <option>Non interessato</option>
                                                            <option>Cliente suend</option>
                                                        </select>                                                        
                                                    </div>  

                                                    <div class="form-group">
                                                      <a href="#" class="btn btn-success btn-md applyStatus" onclick="addStatus()">
                                                      <span class="glyphicon glyphicon-plus"></span> Applica
                                                    </a>
                                                    </div>     
                                                    <div class="form-group">


                                                        <!-- Modal -->
                                                        <div id="notaStato" class="modal fade" role="dialog">
                                                          <div class="modal-dialog">

                                                            <!-- Modal content-->
                                                            <div class="modal-content">
                                                              <div class="modal-header">
                                                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                                <h4 class="modal-title">Desideri aggiungere una nota per questo contatto?</h4>
                                                              </div>
                                                              <div class="modal-body">
                                                                <p><input type="text" name="notaStato" placeholder="aggiungi qui la tua nota"></p>
                                                              </div>
                                                              <div class="modal-footer">
                                                                <button type="button" class="btn btn-default" data-dismiss="modal" onclick="addStatusStep2()">Applica e chiudi</button>
                                                              </div>
                                                            </div>

                                                          </div>
                                                        </div>


                                                        <label for="reg_input">Stato relazione</label>
                                                        <listaStati>
                                                            @foreach($customersRelations as $relations)
                                                            <p>l'utente <strong>
                                                              {{ $relations->name }} {{ $relations->surname }}
                                                            </strong> il 
                                                            <strong> {{ $relations->timer }} </strong> 
                                                            ha aggiornato lo stati in: <br> {{ $relations->status }} 
                                                            @if($relations->nota)
                                                              <p>Con la seguente nota: </p>
                                                              <p> <strong> {{ $relations->nota }} </strong></p>
                                                            @endif
                                                          @if(!$customersRelations)   
                                                            <p class="defaultMsg">Non sono stati inseriti dati su questo cliente</p>
                                                          @endif
                                                          @endforeach
                                                        </listaStati>
                                                    </div>                                                
                                                </div> 
                                            </div>
                                        </div>
                                        <div id="infoviaggi" class="tab-pane fade">
                                            <div class="row">
                                                <div class="col-lg-8">
                                                    <div class="form-group">
                                                        <label for="reg_input">Vacanze/Anno</label>
                                                        <input type="text" id="infoviaggi_vacanzeanno"
                                                               name="infoviaggi_vacanzeanno"

                                                               value="{{ isset($infoviaggi->vacanze_anno) ? $infoviaggi->vacanze_anno : ''  }}"
                                                               class="form-control">
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="reg_input">Preferenza Viaggi</label>
                                                        <input type="text" id="infoviaggi_preferenze"
                                                               name="infoviaggi_preferenze"

                                                               value="{{ isset($infoviaggi->preferenza_viaggi) ? $infoviaggi->preferenza_viaggi : ''  }}"
                                                               class="form-control">
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="reg_input">Durata Media</label>
                                                        <input type="text" id="infoviaggi_duratamedia"
                                                               name="infoviaggi_duratamedia"

                                                               value="{{ isset($infoviaggi->durata_media) ? $infoviaggi->durata_media : ''  }}"
                                                               class="form-control">
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="reg_input">Prossimi Paesi</label>
                                                        <input type="text" id="infoviaggi_prossimipaesi"
                                                               name="infoviaggi_prossimipaesi"

                                                               value="{{ isset($infoviaggi->prossimi_paesi) ? $infoviaggi->prossimi_paesi : ''  }}"
                                                               class="form-control">
                                                    </div>
                                                    <div class="form-group" style="display: none;">
                                                        <label for="reg_input">Fonti</label>
                                                        <input type="text" id="infoviaggi_fonti"
                                                               name="infoviaggi_fonti"
                                                               value="{{ isset($infoviaggi->fonti) ? $infoviaggi->fonti : ''  }}"

                                                               class="form-control">
                                                    </div>
                                                </div>
                                                <div class="col-lg-4">
                                                    <div class="col-lg-2">
                                                        <label>Mesi Preferiti</label>
                                                        <div class="checkbox">
                                                            <label>
                                                                <input type="checkbox" name="infoviaggi_gennaio"
                                                                       @if(isset($infoviaggi->gennaio) and $infoviaggi->gennaio == 1)
                                                                       checked
                                                                       @endif
                                                                       value="1">
                                                                Gennaio
                                                            </label>
                                                        </div>
                                                        <div class="checkbox">
                                                            <label>
                                                                <input type="checkbox" name="infoviaggi_febbraio"
                                                                       @if(isset($infoviaggi->febbraio) and $infoviaggi->febbraio == 1)
                                                                       checked
                                                                       @endif
                                                                       value="1">
                                                                Febbraio
                                                            </label>
                                                        </div>
                                                        <div class="checkbox">
                                                            <label>
                                                                <input type="checkbox" name="infoviaggi_marzo"
                                                                       @if(isset($infoviaggi->marzo) and $infoviaggi->marzo == 1)
                                                                       checked
                                                                       @endif
                                                                       value="1">
                                                                Marzo
                                                            </label>
                                                        </div>
                                                        <div class="checkbox">
                                                            <label>
                                                                <input type="checkbox" name="infoviaggi_aprile"
                                                                       @if(isset($infoviaggi->aprile) and $infoviaggi->aprile == 1)
                                                                       checked
                                                                       @endif
                                                                       value="1">
                                                                Aprile
                                                            </label>
                                                        </div>
                                                        <div class="checkbox">
                                                            <label>
                                                                <input type="checkbox" name="infoviaggi_maggio"
                                                                       @if(isset($infoviaggi->maggio) and $infoviaggi->maggio == 1)
                                                                       checked
                                                                       @endif
                                                                       value="1">
                                                                Maggio
                                                            </label>
                                                        </div>
                                                        <div class="checkbox">
                                                            <label>
                                                                <input type="checkbox" name="infoviaggi_giugno"
                                                                       @if(isset($infoviaggi->giugno) and $infoviaggi->giugno == 1)
                                                                       checked
                                                                       @endif
                                                                       value="1">
                                                                Giugno
                                                            </label>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-2">

                                                        <div class="checkbox">
                                                            <label>
                                                                <input type="checkbox" name="infoviaggi_luglio"
                                                                       @if(isset($infoviaggi->luglio) and $infoviaggi->luglio == 1)
                                                                       checked
                                                                       @endif
                                                                       value="1">
                                                                Luglio
                                                            </label>
                                                        </div>
                                                        <div class="checkbox">
                                                            <label>
                                                                <input type="checkbox" name="infoviaggi_agosto"
                                                                       @if(isset($infoviaggi->agosto) and $infoviaggi->agosto == 1)
                                                                       checked
                                                                       @endif
                                                                       value="1">
                                                                Agosto
                                                            </label>
                                                        </div>
                                                        <div class="checkbox">
                                                            <label>
                                                                <input type="checkbox" name="infoviaggi_settembre"
                                                                       @if(isset($infoviaggi->settembre) and $infoviaggi->settembre == 1)
                                                                       checked
                                                                       @endif
                                                                       value="1">
                                                                Settembre
                                                            </label>
                                                        </div>
                                                        <div class="checkbox">
                                                            <label>
                                                                <input type="checkbox" name="infoviaggi_ottobre"
                                                                       @if(isset($infoviaggi->ottobre) and $infoviaggi->ottobre == 1)
                                                                       checked
                                                                       @endif
                                                                       value="1">
                                                                Ottobre
                                                            </label>
                                                        </div>
                                                        <div class="checkbox">
                                                            <label>
                                                                <input type="checkbox" name="infoviaggi_novembre"
                                                                       @if(isset($infoviaggi->novembre) and $infoviaggi->novembre == 1)
                                                                       checked
                                                                       @endif
                                                                       value="1">
                                                                Novembre
                                                            </label>
                                                        </div>
                                                        <div class="checkbox">
                                                            <label>
                                                                <input type="checkbox" name="infoviaggi_dicembre"
                                                                       @if(isset($infoviaggi->dicembre) and $infoviaggi->dicembre == 1)
                                                                       checked
                                                                       @endif
                                                                       value="1">
                                                                Dicembre
                                                            </label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-lg-8">
                                                    <div class="col-lg-4">
                                                        <label>Tematiche</label>
                                                        <div class="checkbox">
                                                            <label>
                                                                <input type="checkbox" name="infoviaggi_archeologia"
                                                                       @if(isset($infoviaggi->archeologia) and $infoviaggi->archeologia == 1)
                                                                       checked
                                                                       @endif
                                                                       value="1">
                                                                Archeologia
                                                            </label>
                                                        </div>
                                                        <div class="checkbox">
                                                            <label>
                                                                <input type="checkbox" name="infoviaggi_avventura"
                                                                       @if(isset($infoviaggi->avventura) and $infoviaggi->avventura == 1)
                                                                       checked
                                                                       @endif
                                                                       value="1">
                                                                Avventura
                                                            </label>
                                                        </div>
                                                        <div class="checkbox">
                                                            <label>
                                                                <input type="checkbox" name="infoviaggi_cultura"
                                                                       @if(isset($infoviaggi->cultura) and $infoviaggi->cultura == 1)
                                                                       checked
                                                                       @endif
                                                                       value="1">
                                                                Cultura
                                                            </label>
                                                        </div>
                                                        <div class="checkbox">
                                                            <label>
                                                                <input type="checkbox" name="infoviaggi_ecoturismo"
                                                                       @if(isset($infoviaggi->ecoturismo) and $infoviaggi->ecoturismo == 1)
                                                                       checked
                                                                       @endif
                                                                       value="1">
                                                                Ecoturismo
                                                            </label>
                                                        </div>

                                                    </div>
                                                    <div class="col-lg-4">
                                                        <div class="checkbox">
                                                            <label>
                                                                <input type="checkbox"
                                                                       name="infoviaggi_enogastronomia"
                                                                       @if(isset($infoviaggi->enograstronomia) and $infoviaggi->enogastronomia == 1)
                                                                       checked
                                                                       @endif
                                                                       value="1">
                                                                Enogastronomia
                                                            </label>
                                                        </div>
                                                        <div class="checkbox">
                                                            <label>
                                                                <input type="checkbox" name="infoviaggi_fotografico"
                                                                       @if(isset($infoviaggi->fotografico) and $infoviaggi->fotografico == 1)
                                                                       checked
                                                                       @endif
                                                                       value="1">
                                                                Fotografico
                                                            </label>
                                                        </div>
                                                        <div class="checkbox">
                                                            <label>
                                                                <input type="checkbox" name="infoviaggi_marerelax"
                                                                       @if(isset($infoviaggi->mare_relax) and $infoviaggi->mare_relax == 1)
                                                                       checked
                                                                       @endif
                                                                       value="1">
                                                                Mare e relax
                                                            </label>
                                                        </div>
                                                        <div class="checkbox">
                                                            <label>
                                                                <input type="checkbox" name="infoviaggi_montagna"
                                                                       @if(isset($infoviaggi->montagna) and $infoviaggi->montagna == 1)
                                                                       checked
                                                                       @endif
                                                                       value="1">
                                                                Montagna
                                                            </label>
                                                        </div>

                                                    </div>
                                                    <div class="col-lg-4">
                                                        <div class="checkbox">
                                                            <label>
                                                                <input type="checkbox" name="infoviaggi_religioso"
                                                                       @if(isset($infoviaggi->religioso) and $infoviaggi->religioso == 1)
                                                                       checked
                                                                       @endif
                                                                       value="1">
                                                                Religioso
                                                            </label>
                                                        </div>
                                                        <div class="checkbox">
                                                            <label>
                                                                <input type="checkbox" name="infoviaggi_sportivo"
                                                                       @if(isset($infoviaggi->sportivo) and $infoviaggi->sportivo == 1)
                                                                       checked
                                                                       @endif
                                                                       value="1">
                                                                Sportivo
                                                            </label>
                                                        </div>
                                                        <div class="checkbox">
                                                            <label>
                                                                <input type="checkbox"
                                                                       name="infoviaggi_storicopolitico"
                                                                       @if(isset($infoviaggi->storico_politico) and $infoviaggi->storico_politico == 1)
                                                                       checked
                                                                       @endif
                                                                       value="1">
                                                                Storico Politico
                                                            </label>
                                                        </div>
                                                        <div class="checkbox">
                                                            <label>
                                                                <input type="checkbox" name="infoviaggi_altro"
                                                                       @if(isset($infoviaggi->altro) and $infoviaggi->altro == 1)
                                                                       checked
                                                                       @endif
                                                                       value="1">
                                                                Altro
                                                            </label>
                                                        </div>

                                                    </div>
                                                </div>
                                                <div class="col-lg-4">
                                                    <textarea cols="30" rows="5" class="form-control input-sm"
                                                              name="infoviaggi_tematiche_altro"
                                                              placeholder="Altre tematiche...">
                                                         {{ isset($infoviaggi->note_tematiche) ? $infoviaggi->note_tematiche : ''  }}

                                                    </textarea>
                                                </div>


                                            </div>

                                            <div class="row">
                                                <div class="col-lg-8">
                                                    <label>Mood Viaggio</label>
                                                    <div class="col-lg-4">
                                                    <?php 
                                                      $mood=explode(",", $infoviaggi->infoviaggi_mood);
                                                      function isMood($array,$value){
                                                        if(in_array($value, $array))
                                                          echo "checked";
                                                      }

                                                    ?>
                                                    <div class="checkbox"><label>
                                                       <input type="checkbox" name="infoviaggi_mood[]" value="0" <?php isMood($mood,0) ?> >Avventura</label>
                                                    </div>
                                                    <div class="checkbox"><label>
                                                       <input type="checkbox" name="infoviaggi_mood[]" value="1" <?php isMood($mood,1) ?> >Cultura e tradizione</label>
                                                    </div>
                                                    <div class="checkbox"><label>
                                                       <input type="checkbox" name="infoviaggi_mood[]" value="2" <?php isMood($mood,2) ?> >Sport e benessere</label>
                                                    </div>
                                                    <div class="checkbox"><label>
                                                       <input type="checkbox" name="infoviaggi_mood[]" value="3" <?php isMood($mood,3) ?> >Viaggio di nozze</label>
                                                    </div>
                                                  </div>
                                                   <div class="col-lg-4">
                                                    <div class="checkbox"><label>
                                                       <input type="checkbox" name="infoviaggi_mood[]" value="4" <?php isMood($mood,4) ?> >Viaggio con i bambini</label>
                                                    </div>
                                                    <div class="checkbox"><label>
                                                       <input type="checkbox" name="infoviaggi_mood[]" value="5" <?php isMood($mood,5) ?> >Viaggio da solo</label>
                                                    </div>
                                                    <div class="checkbox"><label>
                                                       <input type="checkbox" name="infoviaggi_mood[]" value="6" <?php isMood($mood,6) ?> >Romantico</label>
                                                    </div>
                                                    <div class="checkbox"><label>
                                                       <input type="checkbox" name="infoviaggi_mood[]" value="7" <?php isMood($mood,7) ?> >Fashion</label>
                                                    </div>
                                                  </div>
                                                   <div class="col-lg-4">
                                                    <div class="checkbox"><label>
                                                       <input type="checkbox" name="infoviaggi_mood[]" value="8" <?php isMood($mood,8) ?> >Gourmet</label>
                                                    </div>
                                                    <div class="checkbox"><label>
                                                       <input type="checkbox" name="infoviaggi_mood[]" value="9" <?php isMood($mood,9) ?> >Relax</label>
                                                    </div>
                                                    <div class="checkbox"><label>
                                                       <input type="checkbox" name="infoviaggi_mood[]" value="10" <?php isMood($mood,10) ?> >Luxury</label>
                                                    </div>
                                                    <div class="checkbox"><label>
                                                       <input type="checkbox" name="infoviaggi_mood[]" value="11" <?php isMood($mood,11) ?> >Esplorazione</label>
                                                    </div>
                                                    <div class="checkbox"><label>
                                                       <input type="checkbox" name="infoviaggi_mood[]" value="12" <?php isMood($mood,12) ?> > Divertimento</label>
                                                    </div>
                                                  </div>
                                              </div>
                                            </div>

                                            <?php 
                                              $destinazioni=DB::table('destinazioni')->where("visibile","=","1")->select(["id","tag","destinazione"])->orderBy('destinazione', 'asc')->get();
                                              $destinazioniPassate=DB::table('destinazioniPassate')->where("customerid","=",$customer->id)->get();
                                              $destinazioniPreferite=DB::table('destinazioniPreferite')->where("customerid","=",$customer->id)->get();
                                            ?>

                                              <div class="row">
                                                  <div class="col-lg-8">

                                          <div class="form-group">
                                          <div class="fkacc"><label class="col-md-12 control-label" for="textinput">Destinazioni Preferite</label>  
                                                <a class="accordion"><i class="fa fa-arrows-v" aria-hidden="true"></i>Scegli</a>
                                              <div class="panel well">
                                                  @foreach($destinazioni as $destinazione)
                                                      <div class="checkbox col-lg-4"> 
                                                          <input type="checkbox" name="destinazionipreferite[]" value="{{ $destinazione->id }}">{{ $destinazione->destinazione }}
                                                      </div>
                                                   @endforeach
                                              </div>
                                            </div>
                                          </div>  

                                          <div class="form-group">
                                          <div class="fkacc"><label class="col-md-12 control-label" for="textinput">Destinazioni Passate</label>  
                                                <a class="accordion"><i class="fa fa-arrows-v" aria-hidden="true"></i>Scegli</a>
                                              <div class="panel well">
                                                  @foreach($destinazioni as $destinazione)
                                                      <div class="checkbox col-lg-4"> 
                                                          <input type="checkbox" name="destinazionipassate[]" value="{{ $destinazione->id }}">{{ $destinazione->destinazione }}
                                                      </div>
                                                   @endforeach
                                              </div>
                                            </div>
                                          </div>   

                                                    </div>
                                              </div>  
                                        </div>

                                        <div id="qasuend" class="tab-pane fade">
                                            <div class="row">

                                                <div class="col-lg-12">
                                                    <div class="form-group">
                                                        <label for="reg_input">Conoscenza Suend</label>
                                                        <input type="text" id="qasuend_conoscenza"
                                                               name="qasuend_conoscenza"

                                                               value="{{ isset($qasuend->conoscenza_suend) ? $qasuend->conoscenza_suend : ''  }}"
                                                               class="form-control">
                                                    </div>
                                                </div>

                                                <div class="col-lg-12">
                                                    <div class="form-group">
                                                        <label for="reg_input">Altre motivazioni conoscenza
                                                            Suend</label>
                                                        <input type="text" id="qasuend_altremotivazione"
                                                               name="qasuend_altremotivazione"

                                                               value="{{ isset($qasuend->note_conoscenza_suend) ? $qasuend->note_conoscenza_suend : ''  }}"
                                                               class="form-control">
                                                    </div>
                                                </div>
                                                <div class="col-lg-12" style="display: none;">
                                                    <div class="form-group">
                                                        <label for="reg_input">Codice Cliente</label>
                                                        <input type="text" id="qasuend_codicecliente"
                                                               name="qasuend_codicecliente"
                                                               class="form-control"

                                                               value="{{ isset($qasuend->codice_cliente) ? $qasuend->codice_cliente : ''  }}"
                                                               tabindex="0">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-lg-2">
                                                    <div class="form-group">
                                                        <label for="reg_input" style="text-align: center;">Già
                                                            viaggiato
                                                            con noi</label>
                                                        <input type="checkbox" class="form-control input-sm"
                                                               name="qasuend_giaviaggiato" id="qasuend_giaviaggiato"
                                                               @if(isset($qasuend->gia_viaggiato) and $qasuend->gia_viaggiato == 1)
                                                               checked
                                                               @endif
                                                               value="true">
                                                    </div>
                                                </div>
                                                <div class="col-lg-2">
                                                    <div class="form-group">
                                                        <label for="reg_input">Quante Volte</label>
                                                        <input type="text" id="qasuend_quantiviaggi"
                                                               name="qasuend_quantiviaggi"

                                                               value="{{ isset($qasuend->quante_volte) ? $qasuend->quante_volte : ''  }}"
                                                               class="form-control"
                                                               tabindex="0">
                                                    </div>
                                                </div>
                                                <div class="col-lg-8">
                                                    <div class="form-group">
                                                        <label for="reg_input">Dove</label>
                                                        <input type="text" id="qasuend_doveviaggi"
                                                               name="qasuend_doveviaggi"

                                                               value="{{ isset($qasuend->luogo_viaggio) ? $qasuend->luogo_viaggio : ''  }}"
                                                               class="form-control"
                                                               tabindex="0">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-lg-2">
                                                    <div class="form-group">
                                                        <label for="reg_input" style="text-align: center;">Conosce
                                                            Sito</label>
                                                        <input type="checkbox" class="form-control input-sm"
                                                               name="qasuend_conoscesito" id="qasuend_conoscesito"
                                                               @if(isset($qasuend->conosce_sito) and $qasuend->conosce_sito == 1)
                                                               checked
                                                               @endif
                                                               value="true">
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-lg-12" id="bonus">
                                              <legend> Storico buoni</legend>
                                              <p>
                                                <a href="#" class="btn btn-success btn-md" onclick="addBonus(this)">
                                                  <span class="glyphicon glyphicon-plus"></span> Aggiungi buono
                                                </a>
                                              </p>   
                                              <listabuoni>
                                                    <?php 
                                                    //print_r($bonusList);
                                                    ?>
                                                    @foreach($bonusList as $bonus)
                                                    <div class="col-lg-12 bonus oldbonus">
                                                      <p>Creato il <strong>{{$bonus->timer}}</strong></p>
                                                        <div class="form-group col-lg-2">
                                                            <label for="reg_input">Numero buono</label>
                                                            <input type="text" data-bonus="new" class="form-control" tabindex="52" name="qasuend_numero_buono" id="qasuend_numero_buono" placeholder="identificativo buono" value="{{$bonus->bonusId}}" data-id="{{$bonus->Value}}">
                                                        </div>
                                                        <div class="form-group col-lg-2">
                                                            <label for="reg_input">Tipo buono</label>
                                                            <input type="text" data-bonus="new" class="form-control" tabindex="52" name="qasuend_tipo_buono" id="qasuend_tipo_buono" placeholder="tipologia buono" value="{{$bonus->Kind}}" data-id="{{$bonus->Value}}">
                                                        </div>
                                                        <div class="form-group col-lg-2">
                                                            <label for="reg_input">Importo buono</label>
                                                            <input type="text" data-bonus="new" class="form-control" tabindex="52" name="qasuend_importo_buono" id="qasuend_importo_buono" placeholder="importo" value="{{$bonus->Value}}" data-id="{{$bonus->Value}}">
                                                        </div>
                                                        <div class="form-group col-lg-2">
                                                            <label for="reg_input">Azioni</label>
                                                            <a href="#" class="btn btn-danger btn-md"  onclick="jsRequestConfirm(deleteBonus,[{{$bonus->id}},this])">
                                                              <span class="glyphicon glyphicon-trash"></span> Rimuovi 
                                                            </a>
                                                        </div>
                                                    </div>                                                       
                                                    @endforeach
                                              </listabuoni>            

                                                <templatebuoni>
                                                    <div class="col-lg-12 bonus newbonus">
                                                        <div class="form-group col-lg-2">
                                                            <label for="reg_input">Numero buono</label>
                                                            <input type="text" data-bonus="new" class="form-control" tabindex="52" name="qasuend_numero_buono" id="qasuend_numero_buono" placeholder="identificativo buono">
                                                        </div>
                                                        <div class="form-group col-lg-2">
                                                            <label for="reg_input">Tipo buono</label>
                                                            <input type="text" data-bonus="new" class="form-control" tabindex="52" name="qasuend_tipo_buono" id="qasuend_tipo_buono" placeholder="tipologia buono">
                                                        </div>
                                                        <div class="form-group col-lg-2">
                                                            <label for="reg_input">Importo buono</label>
                                                            <input type="text" data-bonus="new" class="form-control" tabindex="52" name="qasuend_importo_buono" id="qasuend_importo_buono" placeholder="importo">
                                                        </div>
                                                        <div class="form-group col-lg-2">
                                                            <label for="reg_input">Azioni</label>
                                                            <a href="#" class="btn btn-danger btn-md" onclick="jsRequestConfirm(deleteBonus,['',this])">
                                                              <span class="glyphicon glyphicon-trash" ></span> Rimuovi 
                                                            </a>
                                                        </div>
                                                    </div>                                                    
                                                </templatebuoni>
                                            </div>

                                            <div class="row">
                                                <div class="col-lg-12">
                                                    <div class="form-group">
                                                        <label for="reg_input">Note</label>
                                                        <textarea name="qasuend_note" id="qasuend_note" cols="10"
                                                                  rows="3"
                                                                  class="form-control">
                                                         {{ isset($qasuend->note_qa_suend) ? $qasuend->note_qa_suend : ''  }}
                                                        </textarea>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div id="qaviaggio" class="tab-pane fade">
                                            <div class="row">
                                                <div class="col-lg-12">
                                                    <div class="form-group">
                                                        <label for="reg_input">Soddisfazione Generale</label>
                                                        <select name="qaviaggio_soddisfazione" id="qaviaggio_soddisfazione" class="form-control">
                                                            <option value="eccellente" {{ isset($qaviaggio->choices) && $qaviaggio->choices  == 'eccellente' ? 'selected' : '' }}>eccellente</option>
                                                            <option value="buono" {{ isset($qaviaggio->choices) && $qaviaggio->choices  == 'buono' ? 'selected' : '' }}>buono</option>
                                                            <option value="male" {{ isset($qaviaggio->choices) && $qaviaggio->choices  == 'male' ? 'selected' : '' }}>male</option>
                                                            <option value="N/A"  {{ isset($qaviaggio->choices) && $qaviaggio->choices == 'N/A' ? 'selected' : '' }}>N/A</option>
                                                        </select>                                                               
                                                    </div>
                                                    <?php $preventivi=DB::table("preventivo")->where("assegnato","=","0")->get();?>
                                                    <div class="form-group">
                                                        <label for="reg_input">Collega a un preventivo confermato</label>
                                                        <select name="qaviaggio_preventivo" class="form-control">
                                                          <option selected="" disabled="">Scegli un preventivo</option>
                                                           @foreach($preventivi as $preventivo)
                                                           <option value="{{$preventivo->id}}" data-dest="{{$preventivo->labeldest}}">{{$preventivo->labeldest}}</option>
                                                           @endforeach
                                                        </select>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="reg_input">Data Partenza</label>
                                                        <input class="form-control ts_datepicker" type="date"
                                                               name="qaviaggio_datapartenza"
                                                               id="qaviaggio_datapartenza"
                                                               value="{{ isset($qaviaggio->data_partenza) ? $qaviaggio->data_partenza : ''  }}"
                                                               placeholder="gg/mm/aaaa">
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="reg_input">Destinazione</label>
                                                        <input type="text" id="qaviaggio_destinazione"
                                                               name="qaviaggio_destinazione"

                                                               value="{{ isset($qaviaggio->destinazione) ? $qaviaggio->destinazione : ''  }}"
                                                               class="form-control">
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="reg_input">Soddisfazione servizi Suend</label>
                                                        <select name="qaviaggio_valutazione" id="qaviaggio_valutazione" class="form-control" {{ $qaviaggio->valutazione}}>
                                                            <option value="eccellente" {{ $qaviaggio->valutazione=='eccellente' ? 'selected' : ''  }} >eccellente</option>
                                                            <option value="buono" {{ $qaviaggio->valutazione=='buono' ? 'selected' : ''  }}>buono</option>
                                                            <option value="male" {{ $qaviaggio->valutazione=='male' ? 'selected' : ''  }}>male</option>
                                                            <option value="N/A" {{ $qaviaggio->valutazione=='' ? 'selected' : ''  }}>N/A</option>
                                                        </select>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="reg_input">Ultima Rilevazione</label>
                                                        <input class="form-control ts_datepicker" type="date"
                                                               name="qaviaggio_ultimarilevazione"
                                                               id="qaviaggio_ultimarilevazione"

                                                               value="{{ isset($qaviaggio->ultima_rilevazione) ? $qaviaggio->ultima_rilevazione : ''  }}"
                                                               placeholder="gg/mm/aaaa">
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="reg_input">Commenti</label>
                                                        <textarea cols="30" name="qaviaggio_storico"
                                                                  id="qaviaggio_storico" rows="3"
                                                                  class="form-control input-sm">
                                                            {{ isset($qaviaggio->storico) ? $qaviaggio->storico : ''  }}

                                                        </textarea>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div id="profilo" class="tab-pane fade">
                                            <div class="row">
                                                <div class="col-md-4" style="display: none;">

                                                    <label>Data di nascita</label>
                                                    <input class="form-control ts_datepicker" type="text"

                                                           value="{{ isset($profilo->data_nascita) ? $profilo->data_nascita : ''  }}"
                                                           name="profilo_datanascita" id="profilo_datanascita">
                                                </div>
                                                <div class="col-md-8">
                                                    <div class="form-group">
                                                        <label>Luogo di nascita</label>
                                                        <input type="text" id="profilo_luogonascita"
                                                               name="profilo_luogonascita"
                                                               value="{{ isset($profilo->luogonascita) ? $profilo->luogonascita : ''  }}"

                                                               class="form-control">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-8">
                                                    <div class="form-group">
                                                        <label>Professione</label>
                                                        <input type="text" id="profilo_professione"
                                                               name="profilo_professione"
                                                               value="{{ isset($profilo->professione) ? $profilo->professione : ''  }}"

                                                               class="form-control">
                                                    </div>
                                                    <div class="form-group">
                                                        <label>Hobby 1</label>
                                                        <input type="text" id="profilo_hobby1" name="profilo_hobby1"
                                                               value="{{ isset($profilo->hobby1) ? $profilo->hobby1 : ''  }}"
                                                               class="form-control">
                                                    </div>
                                                    <div class="form-group">
                                                        <label>Hobby 2</label>
                                                        <input type="text" id="profilo_hobby2" name="profilo_hobby2"
                                                               value="{{ isset($profilo->hobby2) ? $profilo->hobby2 : ''  }}"

                                                               class="form-control">
                                                    </div>
                                                    <div class="form-group">
                                                        <label>Numero Carta Identita</label>
                                                        <input class="form-control ts_datepicker" type="text"
                                                               name="profilo_cartaidentita" id="profilo_cartaidentita"
                                                               value="{{ isset($profilo->cartaidentita) ? $profilo->cartaidentita : ''  }}"

                                                               placeholder="gg/mm/aaaa">
                                                    </div>
                                                    <div class="form-group">
                                                        <label>Scadenza Carta identita'</label>
                                                        <input class="form-control ts_datepicker" type="date"
                                                               name="profilo_datascadenzaidentita"
                                                               id="profilo_datascadenzaidentita"
                                                               value="{{ isset($profilo->scadenza_passaporto) ? $profilo->scadenza_passaporto : ''  }}"

                                                               placeholder="gg/mm/aaaa">
                                                    </div>
                                                    <div class="form-group">
                                                        <label>Numero Passaporto</label>
                                                        <input class="form-control ts_datepicker" type="text"
                                                               name="profilo_passaporto" id="profilo_passaporto"
                                                               value="{{ isset($profilo->passaporto) ? $profilo->passaporto : ''  }}"

                                                               placeholder="gg/mm/aaaa">
                                                    </div>
                                                    <div class="form-group">
                                                        <label>Scadenza passaporto</label>
                                                        <input class="form-control ts_datepicker" type="date"
                                                               name="profilo_datascadenzapassaporto"
                                                               id="profilo_datascadenzapassaporto"
                                                               value="{{ isset($profilo->scadenza_passaporto) ? $profilo->scadenza_passaporto : ''  }}"

                                                               placeholder="gg/mm/aaaa">
                                                    </div>
                                                    <div class="form-group">
                                                        <div class="checkbox">
                                                            <label>
                                                                <input type="checkbox" name="profilo_newsletter"
                                                                       @if(isset($profilo->newsletter) and $profilo->newsletter == 1)
                                                                       checked
                                                                       @endif
                                                                       value="true">
                                                                Newsletter
                                                            </label>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <div class="checkbox">
                                                            <label>
                                                                <input type="checkbox" name="profilo_clientediretto"
                                                                       @if(isset($profilo->cliente_diretto) and $profilo->cliente_diretto == 1)
                                                                       checked
                                                                       @endif
                                                                       value="true">
                                                                Cliente diretto
                                                            </label>
                                                        </div>
                                                    </div>

                                                </div>
                                                <div class="col-md-4">
                                                    <br>
                                                    <div class="form-group">
                                                        <label>Frequent Flyer</label>
                                                        <textarea cols="30" rows="20" class="form-control input-sm"
                                                                  name="profilo_frequentflyer"
                                                                  placeholder="">
                                                        {{ isset($profilo->frequent_flyer) ? $profilo->frequent_flyer : ''  }}
                                                        </textarea>
                                                    </div>
                                                </div>

                                            </div>
                                            <div class="row">
                                                <div class="col-md-8">
                                                    <div class="col-md-2">

                                                        <label>Adulti</label>
                                                        <input type="text" id="profilo_adulti" name="profilo_adulti"
                                                               value="{{ isset($profilo->adulti) ? $profilo->adulti : ''  }}"

                                                               class="form-control">
                                                    </div>
                                                    <div class="col-md-2">
                                                        <label>Bambini</label>
                                                        <input type="text" id="profilo_bambini"
                                                               name="profilo_bambini"
                                                               value="{{ isset($profilo->bambini) ? $profilo->bambini : ''  }}"

                                                               class="form-control">
                                                    </div>
                                                    <div class="col-md-2">
                                                        <label>Neonati</label>
                                                        <input type="text" id="profilo_neonati"
                                                               name="profilo_neonati"
                                                               value="{{ isset($profilo->neonati) ? $profilo->neonati : ''  }}"

                                                               class="form-control">
                                                    </div>
                                                    <div class="col-md-2">

                                                        <label>Figli</label>
                                                        <input type="text" id="profilo_figli" name="profilo_figli"
                                                               value="{{ isset($profilo->figli) ? $profilo->figli : ''  }}"

                                                               class="form-control">
                                                    </div>
                                                    <div class="col-md-2">
                                                        <label>Anziani</label>
                                                        <input type="text" id="profilo_anziani"
                                                               name="profilo_anziani"
                                                               value="{{ isset($profilo->anziani) ? $profilo->anziani : ''  }}"
                                                               class="form-control">
                                                    </div>
                                                    <div class="col-md-2">
                                                        <label>Animali</label>
                                                        <input type="text" id="profilo_animali"
                                                               name="profilo_animali"

                                                               value="{{ isset($profilo->animali) ? $profilo->animali : ''  }}"
                                                               class="form-control">
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label>Note</label>
                                                        <textarea cols="30" rows="5" class="form-control input-sm"
                                                                  name="profilo_note"
                                                                  placeholder="">
                                                           {{ isset($profilo->note) ? $profilo->note : ' '  }}
                                                        </textarea>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-8">
                                                    <div class="col-md-2">
                                                        <div class="form-group">
                                                            <div class="checkbox">
                                                                <label>
                                                                    <input type="checkbox" value="true"
                                                                           @if(isset($profilo->iscritto_club) and $profilo->iscritto_club == 1)
                                                                           checked
                                                                           @endif
                                                                           name="profilo_iscrittoclub">
                                                                    Iscritto Club
                                                                </label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-10">
                                                        <div class="form-group">
                                                            <label>Codice Club</label>
                                                            <input type="text" id="profilo_codiceclub"
                                                                   name="profilo_codiceclub"
                                                                   value="{{ isset($profilo->codice_club) ? $profilo->codice_club : ' '  }}"
                                                                   class="form-control">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>


                        <button class="btn btn-lg btn-success btn-block">Modifica Cliente</button>
                    </form>

                </div>
            </div>
        </div>
    </div>



@endsection
@section('footer-plugin')
    <!-- datatables -->
    <script src="{{ asset('/lib/DataTables/media/js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('/lib/DataTables/media/js/dataTables.bootstrap.js') }}"></script>
    <script src="{{ asset('/lib/DataTables/extensions/TableTools/js/dataTables.tableTools.min.js') }}"></script>
    <script src="{{ asset('/lib/DataTables/extensions/Scroller/js/dataTables.scroller.min.js') }}"></script>

    <!-- datatables functions -->
    <script src="{{ asset('/js/apps/tisa_datatables.js') }}"></script>
    <script>
    var customerId=<?php echo $customer->id ?>; 
        $('#travels_table').DataTable({
            processing: true,
            serverSide: true,
            ajax: '{!! route('travels_data', ['customer_id' => $customer->id ]) !!}',
            columns: [
                {data: 'id'},
                {data: 'choices'},
                {data: 'data_partenza'},
                {data: 'ultima_rilevazione'},
                {data: 'destinazione'},
                {data: 'Visualizza', name: 'azioni', orderable: false, searchable: false}
            ]
        });
    </script>
    <script type="text/javascript">

    function deleteDestPref(el){
      
      debugger;
        var id=$(el).attr("id");

        $.ajax({
          type: 'POST',
          url: apiUrl+'api.php',
          data: {qr:'removerow',id:id,table:'destinazioniPreferite'},
          el:el,
          success: function(res){
            
          },
          async:false
        });            
    }

    function deleteDestPass(el){
      debugger;

        var id=$(el).attr("id");

        $.ajax({
          type: 'POST',
          url: apiUrl+'api.php',
          data: {qr:'removerow',id:id,table:'destinazioniPassate'},
          el:el,
          success: function(res){
            
          },
          async:false
        });   
    }

        $(document).ready(function () {

            $(".fkacc .accordion").click(function(){
                $(this).parent(".fkacc").find(".panel").toggle(600);
            })

            var destinazioniPassate=JSON.parse('<?php echo json_encode($destinazioniPassate)?>');
            var destinazioniPreferite=JSON.parse('<?php echo json_encode($destinazioniPreferite)?>');


            for(var i=0;i<destinazioniPassate.length;i++){
              $("[name='destinazionipreferite[]'][value='"+destinazioniPassate[i].iddest+"']").prop("checked",true);
              $("[name='destinazionipreferite[]'][value='"+destinazioniPassate[i].iddest+"']").attr("id",destinazioniPassate[i].id);
            }
           
            for(var i=0;i<destinazioniPreferite.length;i++){
              $("[name='destinazionipassate[][]'][value='"+destinazioniPreferite[i].iddest+"']").prop("checked",true);
              $("[name='destinazionipassate[][]'][value='"+destinazioniPassate[i].iddest+"']").attr("id",destinazioniPassate[i].id);
            }

             $("[name='destinazionipreferite[]']:checked").click(function(){
              jsRequestConfirm(deleteDestPref,[$(this),'destinazioniPreferite']);
            });

             $("[name='destinazionipassate[]']:checked").click(function(){
              jsRequestConfirm(deleteDestPass,[$(this),'destinazioniPassate']);
            });
            
            $("#preventivo").change(function(){
            })
            if(window.location.search.includes("view")){
              debugger;
              $("input,select,textarea").attr("readonly","true");
              $("input[type='checkbox']").attr("disabled","true");
              $(".btn.btn-lg.btn-success.btn-block").hide();
            }
            
//            ResetForm();
//            ResetFormInfo();

            function ResetForm() {
                $("#provincia").val('');
                $("#cap").val('');
                $("#regione").val('');
                $("#stato").val('');
            }


            function ResetFormInfo() {
                $("#info-provincia2").val('');
                $("#info-cap2").val('');
                $("#info-regione2").val('');
                $("#info-stato2").val('');
            }



            $("#citta").focus(function() {
                ResetForm();
            });

            $("#info-citta2").focus(function() {
                ResetFormInfo();
            });


            var path = "{{ route('autocomplete') }}";
            var engine = new Bloodhound({
                datumTokenizer: Bloodhound.tokenizers.whitespace('username'),
                queryTokenizer: Bloodhound.tokenizers.whitespace,
                remote:{
                    url: path + '?term=%QUERY',
                    wildcard: '%QUERY'
                }
            });

            engine.initialize();

            $("#citta").typeahead({
                hint: true,
                highlight: true,
                minLength: 2
            }, {
                source: engine.ttAdapter(),
                // This will be appended to "tt-dataset-" to form the class name of the suggestion menu.
                name: 'comune',
                // the key from the array we want to display (name,id,email,etc...)
                displayKey: 'comune',
                templates: {
                    empty: [
                        '<div class="empty-message">Nessun risultato</div>'
                    ].join('\n'),

                    suggestion: function (data) {
                        return '<div class="citta-search-result"><h4>'+ data.comune +'</h4></div>'
                    }
                },

            });
            $('#citta').bind('typeahead:selected', function(obj, datum, name) {

                $("#provincia").val(datum.provincia);
                $("#cap").val(datum.cap);
                $("#regione").val(datum.regione);
                console.log((datum.cap).length);

                $("#stato").val(datum.stato);
            });



            $("#info-citta2").typeahead({
                hint: true,
                highlight: true,
                minLength: 2
            }, {
                source: engine.ttAdapter(),
                // This will be appended to "tt-dataset-" to form the class name of the suggestion menu.
                name: 'comune',
                // the key from the array we want to display (name,id,email,etc...)
                displayKey: 'comune',
                templates: {
                    empty: [
                        '<div class="empty-message">Nessun risultato</div>'
                    ].join('\n'),

                    suggestion: function (data) {
                        return '<div class="citta2-search-result"><h4>'+ data.comune +'</h4></div>'
                    }
                },

            });

            $("#info-citta2").bind('typeahead:selected', function(obj, datum, name) {

                $("#info-provincia2").val(datum.provincia);
                $("#info-cap2").val(datum.cap);
                $("#info-regione2").val(datum.regione);
                $("#info-stato2").val(datum.stato);
            });
        });
           $(document).ready(function(){
            $('input').css('text-transform','uppercase');
        }) 
    console.log("edit customer");
    </script>

<script src="{{ asset('/lib/utils.js') }}"></script>
<script type="text/javascript">
function addBonus(el){
   $("listabuoni").prepend($("templatebuoni").html());
}
function deleteBonus(id,el){
  debugger;

    if(id==""){
      jQuery(el).parents(".bonus").remove();
      return;
    }
    $.ajax({
      type: 'POST',
      url: apiUrl+'api.php',
      data: {qr:'removebonus',id:id},
      el:el,
      success: function(res){
        jQuery(this.el).parents(".bonus").remove();
      },
      async:false
    });    
}



$("form").on("submit",function(){
  
    var data=htmlToStruct("listabuoni:eq(0) .newbonus");
    $("#bonus").remove();
    $.ajax({
      type: 'POST',
      url: apiUrl+'api.php',
      data: {qr:'addbonus',customerId:customerId,data:data},
      success: function(res){
        debugger;
      },
      async:false
    });

});

$(document).ready(function(){
    $("oldbonus input").change(function(){
        $(this).parents(".oldbonus").removeClass("oldbonus").addClass("newbonus");
    })


})

var name="<?php echo $name?>";
function addStatus(){

          if($("#status option:selected").val()=="")
            return;
          $("#notaStato input").val("");
          $("#notaStato").modal();
}

function addStatusStep2(){
          $(".defaultMsg").remove();
          var time=new Date();
            time=time.getDate()+"/"+time.getMonth()+"/"+time.getFullYear();
          var status=$("#status option:selected").val();
            if($('[name="notaStato"]').val()!="")
              var nota="<p>Con la seguente nota: "+$('[name="notaStato"]').val()+"</p>";
            else
              var nota="";
          var row="<p>l'utente <strong>"+name+"</strong> il <strong>"+time+"</strong> ha aggiornato lo stati in: <br><strong>"+status+"</strong>"+nota+"</p>";
          $("listaStati").prepend(row);
          $("listaStati").append("<input type='hidden' id='statiStato[]' name='statiStato[]' value='"+status+"'>");
          $("listaStati").append("<input type='hidden' id='statiNota[]' name='statiNota[]' value='"+$('[name="notaStato"]').val()+"'>");
}
       // alert("edit-customer");

</script>
@endsection
