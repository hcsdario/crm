@extends('layouts.master')
@section('header-page-personalization')
    <style>
        #travels_table thead,
        #travels_table th {
            text-align: center;
        }

        #travels_table tr {
            text-align: center;
        }

        #files_table thead,
        #files_table th {
            text-align: center;
        }

        #files_table tr {
            text-align: center;
        }
    </style>
@endsection
@section('content')
    <div class="page_content">
        <div class="container-fluid">



            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-body">
                            <div class="heading_a">Suend CRM</div>
                            <div class="tabbable tabs-left">
                                <ul class="nav nav-tabs" id="tabs_c">
                                    <li class="active"><a data-toggle="tab" href="#info">Info</a></li>
                                    <li><a data-toggle="tab" href="#profilo">Profilo</a></li>
                                    <li><a data-toggle="tab" href="#infoviaggi">Info Viaggi</a></li>
                                    <li><a data-toggle="tab" href="#qasuend">Info Suend</a></li>
                                    <li><a data-toggle="tab" href="#qaviaggio">Valutazione Viaggio</a></li>
                                </ul>
                                <div class="tab-content" id="tabs_content_c">
                                    <div id="info" class="tab-pane fade in active">
                                        <div class="row">
                                            <div class="col-lg-8">
                                                <div class="form-group">
                                                    <label for="reg_input">Cognome</label>
                                                    <h5>{{ isset($customer->cognome) ? $customer->cognome : ' ' }}</h5>
                                                </div>
                                                <div class="form-group">
                                                    <label for="reg_input">Nome</label>
                                                    <h5>{{ isset($customer->nome) ? $customer->nome : ' ' }}</h5>
                                                </div>


                                                <div class="form-group">
                                                    <label for="reg_input">Data di Nascita</label>
                                                    {{ isset($info->data_nascita) ? $info->data_nascita : ' '  }}
                                                </div>
                                                <div class="form-group">
                                                    <label for="reg_input">Sesso</label>
                                                    {{ isset($info->sesso) ? $info->sesso : ' '  }}
                                                </div>
                                                <div class="form-group">
                                                    <label for="reg_input">Indirizzo</label>
                                                    {{ isset($info->indirizzo) ? $info->indirizzo : ' '  }}
                                                </div>

                                                <div class="form-group">
                                                    <label for="reg_input">Citta'</label>
                                                    <h5>  {{ isset($customer->citta) ? $customer->citta : ' '  }}</h5>
                                                </div>
                                                <div class="form-group">
                                                    <label for="reg_input">Cap</label>
                                                    <h5>  {{ isset($customer->cap) ? $customer->cap : ' '  }}</h5>
                                                </div>

                                                <div class="form-group">
                                                    <label for="reg_input">Provincia</label>
                                                    <h5>  {{ isset($customer->provincia) ? $customer->provincia : ' '  }}</h5>
                                                </div>
                                                <div class="form-group">
                                                    <label for="reg_input">Regione</label>
                                                    <h5>  {{ isset($customer->regione) ? $customer->regione : ' '  }}</h5>
                                                </div>
                                                <div class="form-group">
                                                    <label for="reg_input">Stato</label>
                                                    <h5>  {{ isset($customer->stato) ? $customer->stato : ' '  }}</h5>
                                                </div>

                                                <div class="form-group">
                                                    <label for="reg_input">Telefono Principale</label>
                                                    <h5> {{ isset($customer->tel1) ? $customer->tel1 : ' '  }}</h5>
                                                </div>
                                                <div class="form-group">
                                                    <label for="reg_input">Telefono Secondario</label>
                                                    {{ isset($info->tel2) ? $info->tel2 : ' '  }}
                                                </div>
                                                <div class="form-group">
                                                    <label for="reg_input">Email Principale</label>
                                                    <h5> {{ isset($customer->email1) ? $customer->email1 : ' '  }}</h5>
                                                </div>
                                                <div class="form-group">
                                                    <label for="reg_input">Email Secondaria</label>
                                                    <h5> {{ isset($info->email2) ? $info->email2 : ' '  }}</h5>
                                                </div>
                                                <div class="form-group">
                                                    <label for="reg_input">Codice Fiscale</label>
                                                    {{ isset($info->codice_fiscale) ? $info->codice_fiscale : ' '  }}
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                    <div id="infoviaggi" class="tab-pane fade">
                                        <div class="row">
                                            <div class="col-lg-8">
                                                <div class="form-group">
                                                    <label for="reg_input">Vacanze/Anno</label>
                                                    {{ isset($infoviaggi->vacanze_anno) ? $infoviaggi->vacanze_anno : ' '  }}
                                                </div>
                                                <div class="form-group">
                                                    <label for="reg_input">Preferenza Viaggi</label>
                                                    {{ isset($infoviaggi->preferenza_viaggi) ? $infoviaggi->preferenza_viaggi : ' '  }}
                                                </div>
                                                <div class="form-group">
                                                    <label for="reg_input">Durata Media</label>
                                                    {{ isset($infoviaggi->durata_media) ? $infoviaggi->durata_media : ' '  }}
                                                </div>
                                                <div class="form-group">
                                                    <label for="reg_input">Prossimi Paesi</label>
                                                    {{ isset($infoviaggi->prossimi_paesi) ? $infoviaggi->prossimi_paesi : ' '  }}
                                                </div>
                                                <div class="form-group">
                                                    <label for="reg_input">Fonti</label>
                                                    {{ isset($infoviaggi->fonti) ? $infoviaggi->fonti : ' '  }}
                                                </div>
                                            </div>
                                            <div class="col-lg-4">
                                                <div class="col-lg-2">
                                                    <label>Mesi Preferiti</label>

                                                    <div class="checkbox">
                                                        <label>

                                                            @if(isset($infoviaggi) && $infoviaggi->gennaio == 1)
                                                                <input type="checkbox" name="infoviaggi_gennaio"
                                                                       value="1" checked disabled>
                                                            @else
                                                                <input type="checkbox" name="infoviaggi_gennaio"
                                                                       value="1" disabled>
                                                            @endif
                                                            Gennaio
                                                        </label>
                                                    </div>
                                                    <div class="checkbox">
                                                        <label>
                                                            @if(isset($infoviaggi) && $infoviaggi->febbraio == 1)
                                                                <input type="checkbox" name="infoviaggi_febbraio"
                                                                       value="1" checked disabled>
                                                            @else
                                                                <input type="checkbox" name="infoviaggi_febbraio"
                                                                       value="1" disabled>
                                                            @endif
                                                            Febbraio
                                                        </label>
                                                    </div>
                                                    <div class="checkbox">
                                                        <label>
                                                            @if(isset($infoviaggi) && $infoviaggi->marzo == 1)
                                                                <input type="checkbox" name="infoviaggi_marzo" value="1"
                                                                       checked disabled>
                                                            @else
                                                                <input type="checkbox" name="infoviaggi_marzo" value="1"
                                                                       disabled>
                                                            @endif
                                                            Marzo
                                                        </label>
                                                    </div>
                                                    <div class="checkbox">
                                                        <label>
                                                            @if(isset($infoviaggi) &&  $infoviaggi->aprile == 1)
                                                                <input type="checkbox" name="infoviaggi_aprile"
                                                                       value="1" checked disabled>
                                                            @else
                                                                <input type="checkbox" name="infoviaggi_aprile"
                                                                       value="1" disabled>
                                                            @endif
                                                            Aprile
                                                        </label>
                                                    </div>
                                                    <div class="checkbox">
                                                        <label>
                                                            @if(isset($infoviaggi) &&  $infoviaggi->maggio == 1)
                                                                <input type="checkbox" name="infoviaggi_maggio"
                                                                       value="1" checked disabled>
                                                            @else
                                                                <input type="checkbox" name="infoviaggi_maggio"
                                                                       value="1" disabled>
                                                            @endif
                                                            Maggio
                                                        </label>
                                                    </div>
                                                    <div class="checkbox">
                                                        <label>
                                                            @if(isset($infoviaggi) &&  $infoviaggi->giugno == 1)
                                                                <input type="checkbox" name="infoviaggi_giugno"
                                                                       value="1" checked disabled>
                                                            @else
                                                                <input type="checkbox" name="infoviaggi_giugno"
                                                                       value="1" disabled>
                                                            @endif
                                                            Giugno
                                                        </label>
                                                    </div>
                                                </div>
                                                <div class="col-lg-2">

                                                    <div class="checkbox">
                                                        <label>
                                                            @if(isset($infoviaggi) &&  $infoviaggi->luglio == 1)
                                                                <input type="checkbox" name="infoviaggi_luglio"
                                                                       value="1" checked disabled>
                                                            @else
                                                                <input type="checkbox" name="infoviaggi_luglio"
                                                                       value="1" disabled>
                                                            @endif
                                                            Luglio
                                                        </label>
                                                    </div>
                                                    <div class="checkbox">
                                                        <label>
                                                            @if(isset($infoviaggi) &&  $infoviaggi->agosto == 1)
                                                                <input type="checkbox" name="infoviaggi_agosto"
                                                                       value="1" checked disabled>
                                                            @else
                                                                <input type="checkbox" name="infoviaggi_agosto"
                                                                       value="1" disabled>
                                                            @endif
                                                            Agosto
                                                        </label>
                                                    </div>
                                                    <div class="checkbox">
                                                        <label>
                                                            @if(isset($infoviaggi) && $infoviaggi->settembre == 1)
                                                                <input type="checkbox" name="infoviaggi_settembre"
                                                                       value="1" checked disabled>
                                                            @else
                                                                <input type="checkbox" name="infoviaggi_settembre"
                                                                       value="1" disabled>
                                                            @endif
                                                            Settembre
                                                        </label>
                                                    </div>
                                                    <div class="checkbox">
                                                        <label>
                                                            @if(isset($infoviaggi) &&  $infoviaggi->ottobre == 1)
                                                                <input type="checkbox" name="infoviaggi_ottobre"
                                                                       value="1" checked disabled>
                                                            @else
                                                                <input type="checkbox" name="infoviaggi_ottobre"
                                                                       value="1" disabled>
                                                            @endif
                                                            Ottobre
                                                        </label>
                                                    </div>
                                                    <div class="checkbox">
                                                        <label>
                                                            @if(isset($infoviaggi) &&  $infoviaggi->novembre == 1)
                                                                <input type="checkbox" name="infoviaggi_novembre"
                                                                       value="1" checked disabled>
                                                            @else
                                                                <input type="checkbox" name="infoviaggi_novembre"
                                                                       value="1" disabled>
                                                            @endif
                                                            Novembre
                                                        </label>
                                                    </div>
                                                    <div class="checkbox">
                                                        <label>
                                                            @if(isset($infoviaggi) &&  $infoviaggi->dicembre == 1)
                                                                <input type="checkbox" name="infoviaggi_dicembre"
                                                                       value="1" checked disabled>
                                                            @else
                                                                <input type="checkbox" name="infoviaggi_dicembre"
                                                                       value="1" disabled>
                                                            @endif
                                                            Dicembre
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-lg-8">
                                                <div class="col-lg-4">
                                                    <label>Tematiche</label>
                                                    <div class="checkbox">
                                                        <label>
                                                            @if(isset($infoviaggi) &&  $infoviaggi->archeologia == 1)
                                                                <input type="checkbox" checked disabled>
                                                            @else
                                                                <input type="checkbox" disabled>
                                                            @endif
                                                            Archeologia
                                                        </label>
                                                    </div>
                                                    <div class="checkbox">
                                                        <label>
                                                            @if(isset($infoviaggi) &&  $infoviaggi->avventura == 1)
                                                                <input type="checkbox" checked disabled>
                                                            @else
                                                                <input type="checkbox" disabled>
                                                            @endif
                                                            Avventura
                                                        </label>
                                                    </div>
                                                    <div class="checkbox">
                                                        <label>
                                                            @if(isset($infoviaggi) &&  $infoviaggi->cultura == 1)
                                                                <input type="checkbox" checked disabled>
                                                            @else
                                                                <input type="checkbox" disabled>
                                                            @endif
                                                            Cultura
                                                        </label>
                                                    </div>
                                                    <div class="checkbox">
                                                        <label>
                                                            @if(isset($infoviaggi) &&  $infoviaggi->ecoturismo == 1)
                                                                <input type="checkbox" checked disabled>
                                                            @else
                                                                <input type="checkbox" disabled>
                                                            @endif
                                                            Ecoturismo
                                                        </label>
                                                    </div>

                                                </div>
                                                <div class="col-lg-4">
                                                    <div class="checkbox">
                                                        <label>
                                                            @if(isset($infoviaggi) &&  $infoviaggi->enogastronomia == 1)
                                                                <input type="checkbox" checked disabled>
                                                            @else
                                                                <input type="checkbox" disabled>
                                                            @endif
                                                            Enogastronomia
                                                        </label>
                                                    </div>
                                                    <div class="checkbox">
                                                        <label>
                                                            @if(isset($infoviaggi) &&  $infoviaggi->fotografico == 1)
                                                                <input type="checkbox" checked disabled>
                                                            @else
                                                                <input type="checkbox" disabled>
                                                            @endif
                                                            Fotografico
                                                        </label>
                                                    </div>
                                                    <div class="checkbox">
                                                        <label>
                                                            @if(isset($infoviaggi) &&  $infoviaggi->mare_relax == 1)
                                                                <input type="checkbox" checked disabled>
                                                            @else
                                                                <input type="checkbox" disabled>
                                                            @endif
                                                            Mare e relax
                                                        </label>
                                                    </div>
                                                    <div class="checkbox">
                                                        <label>
                                                            @if(isset($infoviaggi) &&  $infoviaggi->montagna == 1)
                                                                <input type="checkbox" checked disabled>
                                                            @else
                                                                <input type="checkbox" disabled>
                                                            @endif
                                                            Montagna
                                                        </label>
                                                    </div>

                                                </div>
                                                <div class="col-lg-4">
                                                    <div class="checkbox">
                                                        <label>
                                                            @if(isset($infoviaggi) &&  $infoviaggi->religioso == 1)
                                                                <input type="checkbox" checked disabled>
                                                            @else
                                                                <input type="checkbox" disabled>
                                                            @endif
                                                            Religioso
                                                        </label>
                                                    </div>
                                                    <div class="checkbox">
                                                        <label>
                                                            @if(isset($infoviaggi) &&  $infoviaggi->sportivo == 1)
                                                                <input type="checkbox" checked disabled>
                                                            @else
                                                                <input type="checkbox" disabled>
                                                            @endif
                                                            Sportivo
                                                        </label>
                                                    </div>
                                                    <div class="checkbox">
                                                        <label>
                                                            @if(isset($infoviaggi) &&  $infoviaggi->storico_politico == 1)
                                                                <input type="checkbox" checked disabled>
                                                            @else
                                                                <input type="checkbox" disabled>
                                                            @endif
                                                            Storico Politico
                                                        </label>
                                                    </div>
                                                    <div class="checkbox">
                                                        <label>
                                                            @if(isset($infoviaggi) &&  $infoviaggi->altro == 1)
                                                                <input type="checkbox" checked disabled>
                                                            @else
                                                                <input type="checkbox" disabled>
                                                            @endif
                                                            Altro
                                                        </label>
                                                    </div>

                                                </div>
                                            </div>
                                            <div class="col-lg-4">
                                                {{ isset($infoviaggi->note_tematiche) ? $infoviaggi->note_tematiche : ' '  }}
                                            </div>


                                        </div>
                                        <div class="row">
                                            <div class="col-lg-8">
                                                <div class="form-group">
                                                    <label for="reg_input">Destinazioni Preferite</label>
                                                    {{ isset($infoviaggi->destinazioni_preferite) ? $infoviaggi->destinazioni_preferite : ' '  }}

                                                </div>
                                                <div class="form-group">
                                                    <label for="reg_input">Destinazioni Passate</label>
                                                    {{ isset($infoviaggi->destinazioni_passate) ? $infoviaggi->destinazioni_passate : ' '  }}
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div id="qasuend" class="tab-pane fade">
                                        <div class="row">

                                            <div class="col-lg-12">
                                                <div class="form-group">
                                                    <label for="reg_input">Conoscenza Suend</label>
                                                    {{ isset($qasuend->conoscenza_suend) ? $qasuend->conoscenza_suend : ' '  }}
                                                </div>
                                            </div>

                                            <div class="col-lg-12">
                                                <div class="form-group">
                                                    <label for="reg_input">Altre motivazioni conoscenza
                                                        Suend</label>
                                                    {{ isset($qasuend->note_conoscenza_suend) ? $qasuend->note_conoscenza_suend : ' '  }}
                                                </div>
                                            </div>
                                            <div class="col-lg-12">
                                                <div class="form-group">
                                                    <label for="reg_input">Codice Cliente</label>
                                                    {{ isset($qasuend->codice_cliente) ? $qasuend->codice_cliente : ' '  }}
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-lg-2">
                                                <div class="form-group">
                                                    <label for="reg_input" style="text-align: center;">Già viaggiato
                                                        con noi</label>
                                                    @if(isset($qasuend) &&  $qasuend->gia_viaggiato == 1)
                                                        <input type="checkbox" class="form-control input-sm" checked
                                                               disabled>
                                                    @else
                                                        <input type="checkbox" class="form-control input-sm" disabled>
                                                    @endif

                                                </div>
                                            </div>
                                            <div class="col-lg-2">
                                                <div class="form-group">
                                                    <label for="reg_input">Quante Volte</label>

                                                    {{ isset($qasuend->quante_volte) ? $qasuend->quante_volte : ''  }}
                                                </div>
                                            </div>
                                            <div class="col-lg-8">
                                                <div class="form-group">
                                                    <label for="reg_input">Dove</label>
                                                    {{ isset($qasuend->luogo_viaggio) ? $qasuend->luogo_viaggio : ''  }}
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-lg-2">
                                                <div class="form-group">
                                                    <label for="reg_input" style="text-align: center;">Conosce
                                                        Sito</label>
                                                    @if(isset($qasuend) &&  $qasuend->conosce_sito == 1)
                                                        <input type="checkbox" class="form-control input-sm" checked
                                                               disabled>
                                                    @else
                                                        <input type="checkbox" class="form-control input-sm" disabled>
                                                    @endif

                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-lg-12">
                                                <div class="form-group">
                                                    <label for="reg_input">Note</label>

                                                    {{ isset($qasuend->note_qa_suend) ? $qasuend->note_qa_suend : ''  }}
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div id="qaviaggio" class="tab-pane fade">
                                        <div class="row">
                                            <div class="col-lg-12">
                                                <div class="form-group">
                                                    <label for="reg_input">Soddisfazione Generale</label>
                                                    {{ isset($qatravel->choices) ? $qatravel->choices : ''  }}


                                                </div>
                                                <div class="form-group">
                                                    <label for="reg_input">Data Partenza</label>
                                                    {{ isset($qatravel->data_partenza) ? $qatravel->data_partenza : ''  }}
                                                </div>
                                                <div class="form-group">
                                                    <label for="reg_input">Ultima Rilevazione</label>
                                                    {{ isset($qatravel->ultima_rilevazione) ? $qatravel->ultima_rilevazione : ''  }}
                                                </div>
                                                <div class="form-group">
                                                    <label for="reg_input">Destinazione</label>
                                                    {{ isset($qatravel->destinazione) ? $qatravel->destinazione : ''  }}
                                                </div>
                                                <div class="form-group">
                                                    <label for="reg_input">Storico</label>
                                                    {{ isset($qatravel->storico) ? $qatravel->storico : ''  }}
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div id="profilo" class="tab-pane fade">
                                        <div class="row">
                                            <div class="col-md-4">

                                                <label>Data di nascita</label>

                                                {{ isset($profilo->data_nascita) ? $profilo->data_nascita : ''  }}
                                            </div>
                                            <div class="col-md-8">
                                                <div class="form-group">
                                                    <label>Luogo di nascita</label>
                                                    {{ isset($profilo->luogonascita) ? $profilo->luogonascita : ''  }}
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-8">
                                                <div class="form-group">
                                                    <label>Professione</label>
                                                    {{ isset($profilo->professione) ? $profilo->professione : ''  }}
                                                </div>
                                                <div class="form-group">
                                                    <label>Hobby 1</label>
                                                    {{ isset($profilo->hobby1) ? $profilo->hobby1 : ''  }}
                                                </div>
                                                <div class="form-group">
                                                    <label>Hobby 2</label>
                                                    {{ isset($profilo->hobby2) ? $profilo->hobby2 : ''  }}
                                                </div>
                                                <div class="form-group">
                                                    <label>Data Scadenza</label>
                                                    {{ isset($profilo->scadenza) ? $profilo->scadenza : ''  }}
                                                </div>
                                                <div class="form-group">
                                                    <label>Numero Carta Identita</label>
                                                    {{ isset($profilo->cartaidentita) ? $profilo->cartaidentita : ''  }}
                                                </div>
                                                <div class="form-group">
                                                    <label>Scadenza Carta identita'</label>
                                                    {{ isset($profilo->scadenza_cartaidentita) ? $profilo->scadenza_cartaidentita : ''  }}
                                                </div>
                                                <div class="form-group">
                                                    <div class="checkbox">
                                                        <label>
                                                            @if(isset($profilo) &&  $profilo->newsletter == 1)
                                                                <input type="checkbox" checked disabled>
                                                            @else
                                                                <input type="checkbox" disabled>
                                                            @endif
                                                            Newsletter
                                                        </label>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <div class="checkbox">
                                                        <label>
                                                            @if(isset($profilo) &&  $profilo->cliente_diretto == 1)
                                                                <input type="checkbox" checked disabled>
                                                            @else
                                                                <input type="checkbox" disabled>
                                                            @endif
                                                            Cliente diretto
                                                        </label>
                                                    </div>
                                                </div>

                                            </div>
                                            <div class="col-md-4">
                                                <br>
                                                <div class="form-group">
                                                    <label>Frequent Flyer</label>
                                                    {{ isset($profilo->frequent_flyer) ? $profilo->frequent_flyer : ''  }}
                                                </div>
                                            </div>

                                        </div>
                                        <div class="row">
                                            <div class="col-md-8">
                                                <div class="col-md-2">

                                                    <label>Adulti</label>
                                                    {{ isset($profilo->adulti) ? $profilo->adulti : ''  }}
                                                </div>
                                                <div class="col-md-2">
                                                    <label>Bambini</label>
                                                    {{ isset($profilo->bambini) ? $profilo->bambini : ''  }}
                                                </div>
                                                <div class="col-md-2">
                                                    <label>Neonati</label>
                                                    {{ isset($profilo->neonati) ? $profilo->neonati : ''  }}
                                                </div>
                                                <div class="col-md-2">

                                                    <label>Figli</label>
                                                    {{ isset($profilo->figli) ? $profilo->figli : ''  }}
                                                </div>
                                                <div class="col-md-2">
                                                    <label>Anziani</label>
                                                    {{ isset($profilo->anziani) ? $profilo->anziani : ''  }}
                                                </div>
                                                <div class="col-md-2">
                                                    <label>Animali</label>
                                                    {{ isset($profilo->animali) ? $profilo->animali : ''  }}
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label>Note</label>
                                                    {{ isset($profilo->note) ? $profilo->note : ''  }}
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-8">
                                                <div class="col-md-2">
                                                    <div class="form-group">
                                                        <div class="checkbox">
                                                            <label>
                                                                @if(isset($profilo) &&  $profilo->iscritto_club == 1)
                                                                    <input type="checkbox" checked disabled>
                                                                @else
                                                                    <input type="checkbox" disabled>
                                                                @endif
                                                                Iscritto Club
                                                            </label>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-10">
                                                    <div class="form-group">
                                                        <label>Codice Club</label>
                                                        {{ isset($profilo->codice_club) ? $profilo->codice_club : ''  }}
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>

                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-body">

                            <div class="panel panel-default">

                                <div class="panel-body">
                                    <fieldset>
                                        <legend><span>Files Associati</span></legend>
                                    </fieldset>


                                    <div class="row">
                                        <div class="col-lg-12">
                                            <table class="table table-bordered" id="files_table">
                                                <thead>
                                                <tr>
                                                    <th>Id</th>
                                                    <th>Nome File</th>
                                                    <th>Data Caricamento</th>
                                                    <th>Azioni</th>
                                                </tr>
                                                </thead>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <button class="btn btn-lg btn-success btn-block" data-toggle="modal"
                            data-target="#modal_upload_file">Aggiungi File Pdf
                    </button>
                    <div class="modal fade" id="modal_add_travel">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-body">
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                                        &times;
                                    </button>
                                    <div class="panel panel-default">
                                        <div class="panel-body">
                                            <fieldset>
                                                <legend><span></span></legend>
                                            </fieldset>
                                            <div class="row">
                                                <div class="col-lg-12">
                                                    <div class="form-group">
                                                        <label for="reg_input">Soddisfazione generale</label>
                                                        <input type="text" id="soddisfazione" name="soddisfazione"
                                                               class="form-control" tabindex="1" required>
                                                    </div>

                                                    <div class="form-group">
                                                        <label for="reg_input">Data Partenza</label>
                                                        <input type="text" id="data_partenz" name="data_partenza"
                                                               class="form-control" tabindex="2" required>
                                                    </div>

                                                    <div class="form-group">
                                                        <label for="reg_input">Ultima rilevazione</label>
                                                        <input type="text" id="ultima_rilevazione"
                                                               name="ultima_rilevazione" class="form-control"
                                                               tabindex="3" required>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="reg_input">Destinazionee</label>
                                                        <input type="text" id="destinazione" name="destinazione"
                                                               class="form-control" tabindex="4" required>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="reg_input">Storico</label>
                                                        <input type="textaere" id="storico" name="storico"
                                                               class="form-control" tabindex="5" required>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="modal_add_travel">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-body">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h3>Inserisci Viaggio</h3>
                    @if(session()->has('message'))
                        <div class="alert alert-success">
                            {{ session()->get('message') }}
                        </div>
                    @endif
                    @if(session()->has('error'))
                        <div class="alert alert-error">
                            {{ session()->get('error') }}
                        </div>
                    @endif
                    <form id="login_form" role="form" method="POST" action="{{ route('travel.store') }}">
                        {{ csrf_field() }}
                        <div class="panel panel-default">
                            <div class="panel-body">
                                <fieldset>
                                    <legend><span></span></legend>
                                </fieldset>
                                <input type="hidden" name="customer_id" value=" {{$customer->id}}">

                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="form-group">
                                            <label for="reg_input">Soddisfazione generale</label>
                                            <input type="text" id="soddisfazione" name="soddisfazione"
                                                   class="form-control" tabindex="1" required>
                                        </div>

                                        <div class="form-group">
                                            <label for="reg_input">Data Partenza</label>
                                            <input type="text" id="data_partenz" name="data_partenza"
                                                   class="form-control" tabindex="2" required>
                                        </div>

                                        <div class="form-group">
                                            <label for="reg_input">Ultima rilevazione</label>
                                            <input type="text" id="ultima_rilevazione" name="ultima_rilevazione"
                                                   class="form-control" tabindex="3" required>
                                        </div>
                                        <div class="form-group">
                                            <label for="reg_input">Destinazionee</label>
                                            <input type="text" id="destinazione" name="destinazione"
                                                   class="form-control" tabindex="4" required>
                                        </div>


                                        <div class="form-group">
                                            <label for="reg_input">Storico</label>
                                            <input type="textaere" id="storico" name="storico" class="form-control"
                                                   tabindex="5" required>
                                        </div>


                                        <button class="btn btn-lg btn-success btn-block">Aggiungi Cliente</button>
                                    </div>

                                </div>
                            </div>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="modal_upload_file">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-body">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h3>Carica Files</h3>
                    @if(session()->has('message'))
                        <div class="alert alert-success">
                            {{ session()->get('message') }}
                        </div>
                    @endif
                    @if(session()->has('error'))
                        <div class="alert alert-error">
                            {{ session()->get('error') }}
                        </div>
                    @endif
                    <form id="files_form" role="form" method="POST" action="{{ route('file.store') }}"
                          enctype="multipart/form-data">
                        {{ csrf_field() }}
                        <div class="panel panel-default">
                            <div class="panel-body">

                                <input type="hidden" name="customer_id" value=" {{$customer->id}}">

                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="form-group">
                                            <label for="reg_input">Carica del File</label>
                                            <input type="file" name="file_upload" id="file_upload"/>
                                        </div>

                                    </div>
                                    <button class="btn btn-lg btn-success btn-block">Upload File</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>



@endsection
@section('footer-plugin')
    <!-- datatables -->
    <script src="{{ asset('/lib/DataTables/media/js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('/lib/DataTables/media/js/dataTables.bootstrap.js') }}"></script>
    <script src="{{ asset('/lib/DataTables/extensions/TableTools/js/dataTables.tableTools.min.js') }}"></script>
    <script src="{{ asset('/lib/DataTables/extensions/Scroller/js/dataTables.scroller.min.js') }}"></script>

    <!-- datatables functions -->
    <script src="{{ asset('/js/apps/tisa_datatables.js') }}"></script>
    <script>

        $('#travels_table').DataTable({
            processing: true,
            serverSide: true,
            ajax: '{!! route('travels_data', ['customer_id' => $customer->id ]) !!}',
            columns: [
                {data: 'id'},
                {data: 'choices'},
                {data: 'data_partenza'},
                {data: 'ultima_rilevazione'},
                {data: 'destinazione'},
                {data: 'azioni', orderable: false, searchable: false}
            ]
        });
        $('#files_table').DataTable({
            processing: true,
            serverSide: true,
            ajax: '{!! route('files_data', ['customer_id' => $customer->id ]) !!}',
            columns: [
                {data: 'id'},
                {data: 'file_name'},
                {data: 'created_at'},
                {data: 'azioni', orderable: false, searchable: false}
            ]
        });
    </script>

@endsection
