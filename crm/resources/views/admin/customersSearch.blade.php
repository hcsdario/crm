@extends('layouts.master')
@section('header-page-personalization')
    <!-- page specific stylesheets -->
<?php ini_set('get_max_size' , '20M'); ?>
    <link href="{{ asset('/css/style.css') }}" rel="stylesheet" media="screen">

    <!-- google webfonts -->
    <link href='https://fonts.googleapis.com/css?family=Source+Sans+Pro:400&amp;subset=latin-ext,latin' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="{{ asset('/lib/DataTables/media/css/jquery.dataTables.min.css') }}">

    <style>
        #customers_table thead,
        #customers_table th {text-align: center;}
        #customers_table tr {text-align: center;}

        #customers_adv_table thead,
        #customers_adv_table th {text-align: center;}
        #customers_adv_table tr {text-align: center;}

        .hideBecauseDatatablesSucks{
        }

        .search{
            text-align: center;
        }

        #dateRangeForm .form-control-feedback {
            top: 0;
            right: -15px;
        }
        .text-sm-left{
            text-align: left!important;
        }
        .fkacc .panel{
            display: none;         
        }

        form {
            margin-top: 20px;
        }
        h1{
            text-align: center;
        }
        .vh{
        	visibility: hidden;
        }
        .sectionLeft{

        }
        .sectionRight{

        }
        .sectionRight label{
            text-align: left!important;
        }
    </style>
@endsection
@section('content')

<?php 

$destinazioni_preferite=DB::select("SELECT DISTINCT destinazioni_preferite as destinazioni FROM `infoviaggi` WHERE destinazioni_preferite!='' order by destinazioni_preferite asc");
$destinazioni_passate=DB::select("SELECT DISTINCT destinazioni_passate as destinazioni FROM `infoviaggi` WHERE destinazioni_passate!='' order by destinazioni_passate asc");
$provincie=DB::select("SELECT DISTINCT provincia FROM `customers` WHERE provincia!='' order by provincia asc");

$nomi=DB::select("SELECT DISTINCT concat(ifnull(nome,''),' ',ifnull(cognome,'')) as value,concat(ifnull(nome,''),' ',ifnull(cognome,'')) as label FROM `customers` WHERE concat(ifnull(nome,''),' ',ifnull(cognome,''))!=' ' order by label asc");

?>
    <div class="page_content page_search_customer">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-body">
                            <h4><legend><span class="big-title">Cerca privato</span></legend></h4>

                            <form class="form-horizontal accordion" id="search_privati"  method="get" action="../admin/clienti">
                            <fieldset>
                        
                        <div class="panel-group" id="accordion">
                        <div class="col-lg-6 sectionLeft">
                            <legend>Dati anagrafici</legend>

                            <div class="form-group">
                              <label class="col-md-4 control-label" for="textinput">Nome,Cognome</label>  
                              <div class="col-md-4">
                                <input id="textinput" name="customerswjknome" type="text" placeholder="Nome" class="form-control input-md">
                              </div>
                            </div>


                            <div class="form-group">
                                <label class="col-md-4 control-label" for="textinput">Residenza</label>  
	                            <div class="fkacc">
                                <a class="accordion"><i class="fa fa-plus" aria-hidden="true"></i>Scegli</a>
                                    <div class="panel well accordion-contenuto">
                                        <?php 
                                            foreach ($provincie as $provincia) { ?>
                                                <div class="checkbox col-lg-4"> 
                                                    <input type="checkbox" name="customerswjkprovincia[]" value="{{ $provincia->provincia }}">{{ $provincia->provincia }}
                                                </div>
                                            <?php } ?>
                                    </div>
                            	</div>
                            </div>

                            <div class="form-group">
                              <label class="col-md-4 control-label" for="textinput">Data di nascita</label>  
                              <div class="col-md-4">

                                <div class=" date">
                                    <div class="input-group input-append date" id="dateRangePicker">
                                        <input id="infoviaggiwjkdal" name="customerswjkdata_nascita" type="text" placeholder="Data di nascita" class="form-control bdate" name="date" />
                                        <span class="input-group-addon add-on"><span class="glyphicon glyphicon-calendar"></span></span>
                                    </div>
                                </div>

                              </div>
                            </div>



                            <div class="form-group">
                              <label class="col-md-4 control-label" for="textinput">Data ultimo contatto</label>  
                              <div class="col-md-4">
                                <div class=" date">
                                    <div class="input-group input-append date" id="dateRangePicker">
                                        <input id="customerswjkal" name="customerswjkal" type="text"  placeholder="dal" class="form-control bdate" name="date" />
                                        <span class="input-group-addon add-on"><span class="glyphicon glyphicon-calendar"></span></span>
                                    </div>
                                </div>
                                <div class=" date">
                                    <div class="input-group input-append date" id="dateRangePicker">
                                        <input id="customerswjkal" name="customerswjkal" type="text"  placeholder="al" class="form-control bdate" name="date" />
                                        <span class="input-group-addon add-on"><span class="glyphicon glyphicon-calendar"></span></span>
                                    </div>
                                </div>
                              </div>
                            </div>
                        </div>


                        <div class="col-lg-6 sectionRight">
                             <legend><span class="big-title">Informazioni su viaggi precedenti</span></legend>


                            <div class="form-group">
                              <label class="col-md-4 control-label" for="textinput">Data ultimo viaggio</label>  
                              <div class="">
                                <div class="col-md-4 date">
                                    <div class="input-group input-append date" id="dateRangePicker">
                                        <input id="infoviaggiwjkdal" name="infoviaggiwjkdal" placeholder="dal" type="text" class="form-control bdate" name="date" />
                                        <span class="input-group-addon add-on"><span class="glyphicon glyphicon-calendar"></span></span>
                                    </div>
                                </div>

                              </div>
                            </div>
                            <div class="form-group">
                              <label class="col-md-4 control-label vh" for="textinput">Data ultimo viaggio</label>  
                                <div class="date col-md-4">
                                    <div class="input-group input-append date" id="dateRangePicker">
                                        <input id="infoviaggiwjkal" name="infoviaggiwjkal"  placeholder="al" type="text" class="form-control bdate" name="date" />
                                        <span class="input-group-addon add-on"><span class="glyphicon glyphicon-calendar"></span></span>
                                    </div>
                                </div>
                            </div>

											<div class="form-group">
					                            <div class="">
						                            <div class="fkacc"><label class="col-md-4 control-label" for="textinput">Periodo viaggio</label>  
                                                    <a class="accordion"><i class="fa fa-plus" aria-hidden="true"></i> Scegli</a>

                                                    <div class="panel well accordion-contenuto">
                                                    <div class="col-lg-12">

                                                        <div class="checkbox col-lg-4">
                                                            <label>
                                                                <input type="checkbox" name="infoviaggiwjkgennaio" value="1" tabindex="20" style="text-transform: uppercase;">
                                                                Gennaio
                                                            </label>
                                                        </div>
                                                        <div class="checkbox col-lg-4">
                                                            <label>
                                                                <input type="checkbox" name="infoviaggiwjkfebbraio" value="1" tabindex="21" style="text-transform: uppercase;">
                                                                Febbraio
                                                            </label>
                                                        </div>
                                                        <div class="checkbox col-lg-4">
                                                            <label>
                                                                <input type="checkbox" name="infoviaggiwjkmarzo" value="1" tabindex="22" style="text-transform: uppercase;">
                                                                Marzo
                                                            </label>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-12">
                                                        <div class="checkbox col-lg-4">
                                                            <label>
                                                                <input type="checkbox" name="infoviaggiwjkaprile" value="1" tabindex="23" style="text-transform: uppercase;">
                                                                Aprile
                                                            </label>
                                                        </div>
                                                        <div class="checkbox col-lg-4">
                                                            <label>
                                                                <input type="checkbox" name="infoviaggiwjkmaggio" value="1" tabindex="24" style="text-transform: uppercase;">
                                                                Maggio
                                                            </label>
                                                        </div>
                                                        <div class="checkbox col-lg-4">
                                                            <label>
                                                                <input type="checkbox" name="infoviaggiwjkgiugno" value="1" tabindex="25" style="text-transform: uppercase;">
                                                                Giugno
                                                            </label>
                                                        </div>
                                                    </div>

                                                    <div class="col-lg-12">
                                                        <div class="checkbox col-lg-4">
                                                            <label>
                                                                <input type="checkbox" name="infoviaggiwjkluglio" value="1" tabindex="26" style="text-transform: uppercase;">
                                                                Luglio
                                                            </label>
                                                        </div>
                                                        <div class="checkbox col-lg-4">
                                                            <label>
                                                                <input type="checkbox" name="infoviaggiwjkagosto" value="1" tabindex="27" style="text-transform: uppercase;">
                                                                Agosto
                                                            </label>
                                                        </div>
                                                        <div class="checkbox col-lg-4">
                                                            <label>
                                                                <input type="checkbox" name="infoviaggiwjksettembre" value="1" tabindex="28" style="text-transform: uppercase;">
                                                                Settembre
                                                            </label>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-12">
                                                        <div class="checkbox col-lg-4">
                                                            <label>
                                                                <input type="checkbox" name="infoviaggiwjkottobre" value="1" tabindex="29" style="text-transform: uppercase;">
                                                                Ottobre
                                                            </label>
                                                        </div>
                                                        <div class="checkbox col-lg-4">
                                                            <label>
                                                                <input type="checkbox" name="infoviaggiwjknovembre" value="1" tabindex="30" style="text-transform: uppercase;">
                                                                Novembre
                                                            </label>
                                                        </div>
                                                        <div class="checkbox col-lg-4">
                                                            <label>
                                                                <input type="checkbox" name="infoviaggiwjkdicembre" value="1" tabindex="31" style="text-transform: uppercase;">
                                                                Dicembre
                                                            </label>
                                                    </div>
                                                    </div>

                                                    </div>
                                                </div>
                                            </div>
                                        </div>

										<div class="form-group">
					                        <div class="">
						                        <div class="fkacc"><label class="col-md-4 control-label" for="textinput">Tematica viaggio</label>  
                                                    <a class="accordion"><i class="fa fa-plus" aria-hidden="true"></i>Scegli</a>
                                                    <div class="panel well accordion-contenuto">

                                                    <div class="col-lg-12">
                                                        <div class="checkbox col-lg-4">
                                                            <label>
                                                                <input type="checkbox" name="infoviaggiwjkarcheologia" value="1" tabindex="32" style="text-transform: uppercase;">
                                                                Archeologia
                                                            </label>
                                                        </div>
                                                        <div class="checkbox col-lg-4">
                                                            <label>
                                                                <input type="checkbox" name="infoviaggiwjkavventura" value="1" tabindex="33" style="text-transform: uppercase;">
                                                                Avventura
                                                            </label>
                                                        </div>
                                                        <div class="checkbox col-lg-4">
                                                            <label>
                                                                <input type="checkbox" name="infoviaggiwjkcultura" value="1" tabindex="34" style="text-transform: uppercase;">
                                                                Cultura
                                                            </label>
                                                        </div>
                                                        <div class="checkbox col-lg-4">
                                                            <label>
                                                                <input type="checkbox" name="infoviaggiwjkecoturismo" value="1" tabindex="35" style="text-transform: uppercase;">
                                                                Ecoturismo
                                                            </label>
                                                        </div>

                                                    </div>
                                                    <div class="col-lg-12">
                                                        <div class="checkbox col-lg-4">
                                                            <label>
                                                                <input type="checkbox" name="infoviaggiwjkenogastronomia" value="1" tabindex="36" style="text-transform: uppercase;">
                                                                Enogastronomia
                                                            </label>
                                                        </div>
                                                        <div class="checkbox col-lg-4">
                                                            <label>
                                                                <input type="checkbox" name="infoviaggiwjkfotografico" value="1" tabindex="37" style="text-transform: uppercase;">
                                                                Fotografico
                                                            </label>
                                                        </div>
                                                        <div class="checkbox col-lg-4">
                                                            <label>
                                                                <input type="checkbox" name="infoviaggiwjkmare_relax" value="1" tabindex="38" style="text-transform: uppercase;">
                                                                Mare e relax
                                                            </label>
                                                        </div>
                                                        <div class="checkbox col-lg-4">
                                                            <label>
                                                                <input type="checkbox" name="infoviaggiwjkmontagna" value="1" tabindex="39" style="text-transform: uppercase;">
                                                                Montagna
                                                            </label>
                                                        </div>

                                                    </div>
                                                    <div class="col-lg-12">
                                                        <div class="checkbox col-lg-4">
                                                            <label>
                                                                <input type="checkbox" name="infoviaggiwjkreligioso" value="1" tabindex="40" style="text-transform: uppercase;">
                                                                Religioso
                                                            </label>
                                                        </div>
                                                        <div class="checkbox col-lg-4">
                                                            <label>
                                                                <input type="checkbox" name="infoviaggiwjksportivo" value="1" tabindex="41" style="text-transform: uppercase;">
                                                                Sportivo
                                                            </label>
                                                        </div>
                                                        <div class="checkbox col-lg-4">
                                                            <label>
                                                                <input type="checkbox" name="infoviaggiwjkstoricopolitico" value="1" tabindex="42" style="text-transform: uppercase;">
                                                                Storico Politico
                                                            </label>
                                                        </div>
                                                        <div class="checkbox col-lg-4">
                                                            <label>
                                                                <input type="checkbox" name="infoviaggiwjkaltro" value="1" tabindex="43" style="text-transform: uppercase;">
                                                                Altro
                                                            </label>
                                                        </div>

                                                    </div>


                                                    </div>
                                                </div>
                                            </div>
                                            </div>

							<div class="form-group">
								<div class="">
									<div class="fkacc">

                                    <label class="col-md-4 control-label" for="textinput">Scegli una/le destinazioni precedenti da considerare</label>  

                                    <input style="max-width: 250px;margin-left: 80px;" id="filtra_destinazione_precedente" onkeyup="filtraprecedente(this)" type="text" placeholder="Cerca destinazione" class="form-control input-md" tabindex="2">
                                                    
										<a class="accordion"><i class="fa fa-plus" aria-hidden="true"></i></a>
                                <div class="panel well col-md-12 accordion-contenuto">
                                  <?php 
                                    foreach ($destinazioni_preferite as $value) { ?>
                                        <div class="checkbox col-lg-4"> 
                                            <input type="checkbox" name="infoviaggiwjkdestinazioni_preferite[]" value="{{ $value->destinazioni }}" data-label="{{ $value->destinazioni }}">{{ $value->destinazioni }}
                                        </div>
                                    <?php } ?>
                                </div>
                            		</div>
                            	</div>
                            </div>

							<div class="form-group">
								<div class="">
									<div class="fkacc"><label class="col-md-4 control-label" for="textinput">Scegli una/le destinazioni preferite da considerare</label>

                                    <input style="max-width: 250px;margin-left: 80px;" id="filtra_destinazione_precedente" onkeyup="filtraprecedente(this)" type="text" placeholder="Cerca destinazione" class="form-control input-md" tabindex="2">  


										<a class="accordion"><i class="fa fa-plus" aria-hidden="true"></i></a>
                                <div class="panel well col-md-12 accordion-contenuto">
                                  <?php 
                                    foreach ($destinazioni_passate as $value) { ?>
                                        <div class="checkbox col-lg-4"> <input type="checkbox" name="infoviaggiwjkdestinazioni_passate[]" value="{{ $value->destinazioni }}">{{ $value->destinazioni }}</div>
                                    <?php } ?>
                                </div>
                            		</div>
                            	</div>
                            </div>

  
                        </div>



                            <div class="form-group search" >
                              <div class="col-md-12">
                                <button id="button1id" name="button1id" class="btn btn-info btn btn-lg btn-success btn-block" style="margin-top: 20px!important;">Cerca</button>
                              </div>
                            </div>

                            </fieldset>
                            </form>

                                </div>
                            </div>
                        </div>

                </div>
            </div>
        </div>
    </div>


@endsection
@section('footer-plugin')
    <!-- datatables -->
    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.18/pdfmake.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.2.4/js/dataTables.buttons.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.2.4/js/buttons.bootstrap.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.2.4/js/buttons.print.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.2.4/js/buttons.html5.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <script src="//cdn.datatables.net/buttons/1.4.2/js/buttons.colVis.min.js"></script>
    <script src="{{ asset('/lib/DataTables/extensions/Scroller/js/dataTables.scroller.min.js') }}"></script>
    <script src="https://crm.suend.it/js/tinynav.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.perfect-scrollbar/0.8.1/js/perfect-scrollbar.jquery.min.js"></script>
    <script src="{{ asset('/js/apps/tisa_datatables.js') }}"></script>
    <script src="{{ asset('/lib/moment-js/moment.min.js') }}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.2.0/jquery-confirm.min.js"></script>
    <script src="{{ asset('/lib/jquery.confirm/jquery-confirm.min.js') }}"></script>

    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/css/datepicker.min.css" />
    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/css/datepicker3.min.css" />
    <script src="{{ asset('/crm/resources/assets/js/bootstrap_datepicker.js') }}"></script>


  <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>  
    <script type="text/javascript">

        function filtraprecedente(el){
                var value=$(el).val();
                if(value.length<3){
                    if(value=="")
                        $(el).parents(".fkacc").find('.checkbox').show();
                    return;
                }
                value=value.toUpperCase();

                $(el).parents(".fkacc").find('.checkbox').hide();
                $(el).parents(".fkacc").find('[type="checkbox"]').each(function(){
                    var data=$(this).data();
                    data.label=""+data.label+"";
                    data.label=data.label.toUpperCase();
                    if(data.label.includes(value))
                        $(this).parents(".checkbox").show();
                });
                $(".checkbox input:checked").parent().show();
                if($(el).parents(".fkacc").find('.accordion i').hasClass("fa-plus"))
                    $(el).parents(".fkacc").find('.accordion i').click();

        }    

    
    var availableTags1= JSON.parse("<?php echo addslashes(json_encode($nomi)) ;?>");

        $(document).ready(function(){

        $(".accordion-contenuto [type='checkbox']").change(function(){
            if($(this).parents("#filtra_destinazione_precedente").val()!="" && $(this).prop("checked"))
            {
                $(".checkbox input:checked").parents(".fkacc").find("#filtra_destinazione_precedente").val("");
                filtraprecedente($(".checkbox input:checked").parents(".fkacc").find("#filtra_destinazione_precedente"));
            }
        })

        $( '[name="customerswjknome"]' ).autocomplete({
            source: availableTags1
        });  

            
            $('.bdate').datepicker({
              altFormat: "yy-mm-dd"
            });            
        })

    </script>

@endsection
