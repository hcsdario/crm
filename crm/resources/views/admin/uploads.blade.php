@extends('layouts.master')
@section('header-page-personalization')
    <style>
        .twitter-typeahead, .tt-hint, .tt-input, .tt-menu { width: 100%;  }
        .tt-menu {
            background: #ffffff;
        }
        note{
        	color: white;
		    font-weight: 900;
        }
        textarea{
            overflow: hidden;
        }
        td.fa-file-text-container{
            text-align: center;
        }
        .fa-file-text:hover{
            color: rgb(66, 139, 202);
        }
        .fa-file-text{
            cursor: pointer;
            font-size: 23px;
        }
        th{
            text-align: center;            
        }

        .label{
            font-size: 12px;
            padding-left: 0px!important;
            text-align: left;
            display: block;            
        }

    </style>

            @endsection
            @section('content')
                <div class="page_content page_uploads">

                    <div class="container-fluid">

             @if(session()->has('message'))
                <div class="alert alert-success">
                    {{ session()->get('message') }}
                </div>
            @endif
            @if(session()->has('error'))
                <div class="alert alert-danger">
                    {{ session()->get('error') }}
                </div>
            @endif

            <div class="row page_uploads">
                <div class="col-md-12 form-upload" style="text-align: center;">
                    <div class="panel panel-default">
                            <div class="panel-body">
                <h1><legend><span class="big-title">Upload Allegati</span></legend></h1>
                <?php 
                	$isAdmin=false;
               		$adminStyle=' style="display: none" ';
                	if(Auth::user()->admin == 1) 
                		$isAdmin=true;
                	if($isAdmin)
                		$adminStyle="";

                ?>
                <form  action="{{ route('uploadsStore') }}" method="post" class="form" novalidate="novalidate" files="true" enctype="multipart/form-data">
                 {{ csrf_field() }}
                
					<div class="form-group col-md-12">
					<label for="reg_input">Zona News - Scegli la zona in cui sarà visualizzata la News</label><br>
						<select id="zone" name="zone" class="form-control">
							<?php 
								$zone=DB::select("SELECT * FROM `newsZone`");
								for($i=0;$i<count($zone);$i++){?>
								<option value="{{$zone[$i]->zone}}" <?php if($i==0) echo "selected"; ?>
								>{{$zone[$i]->zone}}</option>
							<?php } ?>
						</select>
					</div>

                    <div class="form-group col-md-12">
                      <label class="col-md-4 control-label" for="textinput">Nome/Etichetta del file?</label>  <br>
                      <input name="label" type="text" class="form-control input-md" required="">
                      <span class="help-block">Solo per riconoscere il file</span>  
                     
                    </div>  
                    
                    <div class="form-group col-md-12">
                      <label class="col-md-4 control-label" for="textarea">Descrizione/note (opzionale)</label><br>
                                       
                        <textarea rows="3" class="form-control" name="descr"></textarea>
                    </div>   

					<div class="form-group col-md-12">
					 <label for="reg_input">Scegli un allegato</label>			<br>		
					  <input type="file" name="files" id="files">
                   	 <input type="hidden" name="userid" value="{{Auth::user()->id}}">
                    </div>
                    <button class="btn btn-info btn-sm"><i class="fa fa-upload"></i> <note>&nbsp;Carica</note></button>
                </form>
                  
                   </div>
                   </div> 
                </div>

            </div>

            <div class="row">
        <div class="container-fluid">
            <div class="row">

            <!-- Modal -->
            <div id="notes" class="modal fade" role="dialog">
              <div class="modal-dialog">

                <!-- Modal content-->
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title"><strong>File:</strong><label></label></h4>
                  </div>
                  <div class="modal-body">
                    <p><strong>Note:</strong><descr></descr></p>
                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                  </div>
                </div>

              </div>
            </div>

                <div class="col-lg-12">
                        <div class="panel panel-default">
                            <div class="panel-body">
                                <div class="row">
                                    <div class="col-lg-12">
                                    <h1><legend><span class="big-title">Allegati</span></legend></h1>
                                            <table class="table table-bordered" id="customers_table" >
                                                <thead>
                                                <tr>
                                                    <th>Id</th>
                                                    <th>File</th>
                                                    <th>Caricato il</th>
                                                    <th>Proprietario</th>
                                                    <th>Etichetta</th>
                                                    <th>Descrizione</th>
                                                    <th>Zona</th>
                                                    <th <?php echo $adminStyle ?> >Azione</th>
                                                </tr>
                                                </thead>
                                                <?php 
                                                if(Auth::user()->id!=1)
                                                {
                                                    $sql="SELECT uploads.id,uploads.label,uploads.descr,uploads.files,uploads.created_at,uploads.dir, users.username, users.surname FROM `uploads` INNER JOIN users ON uploads.userid=users.id WHERE uploads.dir in(SELECT newsUserZone.userZone FROM newsUserZone WHERE newsUserZone.userid=".Auth::user()->id.") OR uploads.dir='Tutte'";
                                                    $row=DB::select($sql);
                                                }
                                                else
                                                $row=DB::select("SELECT uploads.id,uploads.files,uploads.created_at,uploads.dir, users.username, users.surname FROM `uploads` INNER JOIN users ON uploads.userid=users.id");
                                                for($i=0;$i<count($row);$i++){ ?>
                                                <tr>
                                                    <td>{{$row[$i]->id}}</td>
                                                    <td><a href="../crm/upload/{{$row[$i]->dir}}/{{$row[$i]->files}}" style="margin-left: 10px;" >{{$row[$i]->files}}</a></td>
                                                    <td>{{$row[$i]->created_at}}</td>
                                                    <td>{{$row[$i]->username}} / {{$row[$i]->surname}}</td>
                                                    <td>{{$row[$i]->label}}</td>
                                                    <td class="fa-file-text-container">
                                                        <i class="fa fa-file-text" data-label="{{$row[$i]->label}}" data-text="{{$row[$i]->descr}}" onclick="showtext(this)"></i>
                                                        <i class="fa fa-trash" data-label="{{$row[$i]->label}}" data-text="{{$row[$i]->descr}}" onclick="showtext(this)"></i>
                                                    </td>
                                                    <td>{{$row[$i]->dir}}</td>
                                                    <td <?php echo $adminStyle ?>><a href="#" class="btn btn-xs btn-danger news-zama" style="margin-left: 10px;" onclick="deleteRecord(this)"><i class="glyphicon glyphicon-minus"></i>&nbsp;Elimina</a></td>
                                                </tr>
                                                <?php } ?>
                                            </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                </div>
            </div>
    </div>
                    
                </div>

            </div>
        </div>

    </div>
    <script type="text/javascript">
        window['table']="uploads";
        function showtext(el) {
            debugger;
            var data=$(el).data();
            var label=data.label;
            var descr;data.descr
            $("#notes label").html(label);
            var descr;
            $("#notes descr").html(descr);
            $("#notes").modal();
        }
    </script>
@endsection
@section('footer-plugin')

@endsection