@extends('layouts.master')
@section('header-page-personalization')
    <!-- page specific stylesheets -->

    <!-- datatables -->
    <link rel="stylesheet" href="{{ asset('/lib/DataTables/media/css/jquery.dataTables.min.css') }}">
    <link rel="stylesheet" href="{{ asset('/lib/DataTables/extensions/TableTools/css/dataTables.tableTools.min.css') }}">
    <link rel="stylesheet" href="{{ asset('/lib/DataTables/extensions/Scroller/css/dataTables.scroller.min.css') }}">

    <!-- main stylesheet -->
    <link href="{{ asset('/css/style.css') }}" rel="stylesheet" media="screen">

    <!-- google webfonts -->
    <link href='http://fonts.googleapis.com/css?family=Source+Sans+Pro:400&amp;subset=latin-ext,latin' rel='stylesheet' type='text/css'>


    <!-- moment.js (date library) -->
    <script src="{{ asset('/lib/moment-js/moment.min.js') }}"></script>
    <style>
        #customers_table thead,
        #customers_table th {text-align: center;}
        #customers_table tr {text-align: center;}

        #customers_adv_table thead,
        #customers_adv_table th {text-align: center;}
        #customers_adv_table tr {text-align: center;}
        [readonly]{
          background-color: white;
          opacity: 1;
        }

    </style>
@endsection
@section('content')

    <div class="page_content page_advs">
        <div class="container-fluid">
                    <div class="parentPdfTemplate" style="display: none;">
                        <div class="pdfTemplate">
                            <div class="cardContainer" style="border: 1px solid;width: fit-content;border-radius: 12px;padding: 5px;">
                                <table>
                                <thead>
                                    <th style="width: 150px;max-width: 150px;word-break: break-word;"></th>
                                    <th style="width: 150px;max-width: 150px;word-break: break-word;"></th>
                                </thead>
                                <tbody>
                                    <tr style="text-align: center;">
                                      <td style="width: 150px;max-width: 150px;word-break: break-word;"><b>Codice carta VIP</b></td>
                                      <td style="width: 150px;max-width: 150px;word-break: break-word;" id="code">000047#2018</td>
                                    </tr>
                                    <tr style="text-align: center;">
                                      <td style="width: 150px;max-width: 150px;word-break: break-word;"><b>Intestatario</b></td>
                                      <td style="width: 150px;max-width: 150px;word-break: break-word;" id="name">Mario Rossi</td>
                                    </tr>
                                    <tr style="text-align: center;">
                                      <td style="width: 150px;max-width: 150px;word-break: break-word;"><b>Importo</b></td>
                                      <td style="width: 150px;max-width: 150px;word-break: break-word;" id="value">35,00</td>
                                    </tr>
                                    <tr style="text-align: center;">
                                      <td style="width: 150px;max-width: 150px;word-break: break-word;"><b>Scadenza</b></td>
                                      <td style="width: 150px;max-width: 150px;word-break: break-word;" id="deadline">17/01/2019</td>
                                    </tr>
                                </tbody>
                                </table>
                            </div>
                        </div>
                    </div>

                    <page class="page_pdf" style=" display: none;" backtop="50mm" backbottom="50mm" backleft="10mm" backright="10mm">
                        <page_header>
                        </page_header>
                        <div id="outputContainer">
                            <table><thead><th></th><th></th></thead><tbody id="output"></tbody></table>
                        </div>
                        <page_footer class="page_footer" style="height: 200px; position: relative;z-index-1; background: white">
                        </page_footer>
                    </page>

                    

            <div class="row">

                <div class="col-lg-12">
                    <div class="panel panel-default">

                        <div class="panel-body">
                            <fieldset>
                                <legend><span class="big-title">Elenco Gift Card </span></legend>
                            </fieldset>


                            <div class="row">
                                <div class="col-lg-12">
                                    <table class="table table-bordered table-striped" id="customers_adv_table" >
                                                <thead>
                                                    <th style="display: none;">id</th>
                                                    <th style="width: 10px;"></th>
                                                    <th>Cod</th>
                                                    <th>Azienda</th>
                                                    <th>Beneficiario</th>
                                                    <th style="width: 400px;">Descrizione carta</th>
                                                    <th>Importo</th>
                                                    <th>Prezzo</th>
                                                    <th>Tipologia</th>
                                                    <th>Saldata</th>
                                                    <th>Scadenza</th>
                                                    <th>Azioni</th>
                                                </thead>
                                                <tbody>

                                                @foreach($cards as $card)
                                                    <tr data-code="{{ $card->code }}"  data-name="Non assegnata"  data-value="{{ $card->value }}"  data-deadline="{{ $card->deadline }}" >
                                                        <td style="display: none;">{{ $card->id }}</td>
                                                        <td class="center"><input type="checkbox" class="selector" name="selector" value="{{ $card->id }}"></td>
                                                        <td>{{ $card->code }}</td>
                                                        <td> 
                                                        
                                                            @if($card->azienda!="Non assegnato")
                                                              <a href="/suendcrm/user/business/{{$card->id_azienda}}"  target="blank">{{ $card->azienda }} </a>
                                                            @else
                                                                {{ $card->azienda }} 
                                                            @endif

                                                        </td>
                                                        <td> 

                                                            @if($card->privato!="Non assegnato")
                                                              <a href="/suendcrm/admin/cliente/{{$card->id_privato}}"  target="blank">{{ $card->privato }} </a>
                                                            @else
                                                                {{ $card->privato }} 
                                                            @endif

                                                        </td>
                                                        <td>{{ $card->label }}</td>
                                                        <td>{{ $card->value }}</td>
                                                        <td>{{ $card->price }}</td>
                                                        <td>{{ $card->kind }}</td>
                                                        
                                                        @if($card->payed)
                                                         <td>Si</td>
                                                        @else
                                                         <td>No</td>
                                                        @endif

                                                        <td>{{ $card->deadline }}</td>

                                                        <td>
                                                            <a href="../admin/card/edit/{{ $card->id }}?action=view" class="btn btn-xs btn-info">
                                                            <i class="glyphicon glyphicon-search" data-toggle="tooltip" title="Visualizza"></i></a>
                                                            <a href="../admin/card/edit/{{ $card->id }}" class="btn btn-xs btn-secondary"><i class="glyphicon glyphicon-edit" data-toggle="tooltip" title="Modifica"></i></a>
                                                            <a href="#" class="btn btn-xs btn-danger news-zama" style="margin-left: 10px;" onclick="deleteRecord(this,['card',{{ $card->id }}])"><i class="glyphicon glyphicon-trash" data-toggle="tooltip" title="Elimina"></i></a>
                                                        </td>
                                                    </tr>
                                                     @endforeach
                                                </tbody>
                                    </table>
                                </div>
                            </div>
                            <button class="btn btn-lg btn-success btn-block" ><a href="card/new" style="color:#fff;text-decoration: none;">Aggiungi una gift card</a></button>



                            <!-- Modal -->
                            <div id="popupRipetizioni" class="modal fade" role="dialog">
                              <div class="modal-dialog">

                                <!-- Modal content-->
                                <div class="modal-content">
                                  <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                    <h4 class="modal-title">Numero copie</h4>
                                  </div>
                                  <div class="modal-body">
                                    <p>Quante copie desideri effettuare di ciascuna tessera?</p>
                                    <input type="number" id="ripetizioni" class="form-control" value="1">
                                  </div>
                                  <div class="modal-footer">
                                    <button type="button" class="btn btn-default" onclick="copyCard()" data-dismiss="modal">Duplica</button>
                                  </div>
                                </div>

                              </div>
                            </div>

                            <!-- Modal -->
                            <div id="popupRipetizioniPdf" class="modal fade" role="dialog">
                              <div class="modal-dialog">

                                <!-- Modal content-->
                                <div class="modal-content">
                                  <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                    <h4 class="modal-title">Numero copie</h4>
                                  </div>
                                  <div class="modal-body">
                                    <p>Quante copie desideri effettuare di ciascuna tessera?</p>
                                    <input type="number" id="ripetizioni" class="form-control" value="1">
                                  </div>
                                  <div class="modal-footer">
                                    <button type="button" class="btn btn-default" onclick="copyCardPdf()" data-dismiss="modal">Duplica</button>
                                  </div>
                                </div>

                              </div>
                            </div>


                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>



@endsection
@section('footer-plugin')
    <!-- datatables -->
    <script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.18/pdfmake.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.2.4/js/dataTables.buttons.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.2.4/js/buttons.bootstrap.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.2.4/js/buttons.print.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.2.4/js/buttons.html5.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <script src="//cdn.datatables.net/buttons/1.4.2/js/buttons.colVis.min.js"></script>
    <script src="{{ asset('/lib/DataTables/extensions/Scroller/js/dataTables.scroller.min.js') }}"></script>
    <script src="http://tinynav.com/tinynav.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.perfect-scrollbar/0.8.1/js/perfect-scrollbar.jquery.min.js"></script>
    <script src="{{ asset('/js/apps/tisa_datatables.js') }}"></script>
    <script src="{{ asset('/lib/moment-js/moment.min.js') }}"></script>

    <script src="{{ asset('/lib/DataTables/media/js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('/lib/DataTables/media/js/dataTables.bootstrap.js') }}"></script>
    <script src="{{ asset('/lib/DataTables/extensions/TableTools/js/dataTables.tableTools.min.js') }}"></script>
    <script src="{{ asset('/lib/DataTables/extensions/Scroller/js/dataTables.scroller.min.js') }}"></script>
    <script src="{{ asset('/js/apps/tisa_datatables.js') }}"></script>

    <script>

        if(is_tablet()||is_tablet_pro())
            var tablet_hide_col={
/*                    "targets": [0,3,5,9,11,12],
                    "visible": false,*/
                };
        else
            var tablet_hide_col={
/*                    "targets": [0,3,4,9,11,12],
                    "visible": false,*/
                };

function copyCard(){
    $('#popupRipetizioni').on('hidden.bs.modal', function () {
        var ripetizioni=$("#popupRipetizioni #ripetizioni").val();

        if(isNaN(ripetizioni) || ripetizioni<1)
            return;

        var id=[];
        
        $(".selector:checked").each(function(){
            id.push($(this).val());
        })

        var token=$('[name="_token"]').val();
        $(".btn.btn-default:eq(1) span").replaceWith('<i class="fa fa-spinner fa-spin"></i>')
         $.post("cards/duplify",{'_token':token,ripetizioni:ripetizioni,id:id},function(response){
            window.location.reload(true);
        })
    })
}

function copyCardPdf(){
    $('#popupRipetizioniPdf').on('hidden.bs.modal', function () {
        var ripetizioni=$("#popupRipetizioniPdf #ripetizioni").val();

        if(isNaN(ripetizioni) || ripetizioni<1)
            return;

        var tdContainer=[];

        $(".selector:checked").each(function(){

            var template=$(".parentPdfTemplate").clone();
            var data=$(this).parents("tr").data();
            $("#output").append(template);
            $("#output .pdfTemplate:last #code").text(data.code);
            $("#output .pdfTemplate:last #name").text(data.name);
            $("#output .pdfTemplate:last #value").text(data.value);
            $("#output .pdfTemplate:last #deadline").text(data.deadline);
            var card=$("#output .parentPdfTemplate:last").html();

            tdContainer.push(card);


            for(var i=0;i<ripetizioni-1;i++)
                tdContainer.push(card);
        
            $("#output").html("");
        })

        var tdIndex=0;

        for(var i=0;i<tdContainer.length;i++){
            if(tdIndex==0)
                $("#output").append("<tr class='cardRow'></tr>");
                $("#output tr.cardRow:last").append("<td>"+tdContainer[i]+"</td>");
                tdIndex++;
                if(tdIndex==2)
                    tdIndex=0;
        }
        
        if(tdContainer.length%2!=0)
            $("#output tr.cardRow:last").append("<td></td>");


        $(".btn.btn-default:eq(0) span").replaceWith('<i class="fa fa-spinner fa-spin"></i>')
        $(".page_pdf").show();
        $(".parentPdfTemplate").show();
        var html=$("#outputContainer").html()
        $(".page_pdf").hide();
        $(".parentPdfTemplate").hide();
        $("#output").html("");
        $.post("/suendcrm/admin/preventivo/topdf/0",{_token: "{{ csrf_token() }}", html: html, name: 'giftCard'},function( data ) {

            $(".btn.btn-default:eq(0) span").replaceWith('<span>PDF</span>')
            data=JSON.parse(data);
            window.open('/suendcrm/admin/preventivo/getpdf/'+data.pdf_name,'_blank');
        });


    })
}

function on_confirm_get_pdf(){

    if($(".selector:checked").length==0){
        alert("Si prega di selezionare almeno una card prima di proseguire con la duplicazione");
        return;
    }

    $.confirm({
        title: 'Attenzione',
        content: 'Confermi di voler generare il pdf per le '+$(".selector:checked").length+' card selezionate?',
        confirmButton: 'Sì',
        cancelButton: 'No',
        confirmButtonClass: 'btn-warning',
        cancelButtonClass: 'btn btn-success',
        confirm: function () {
            $("#popupRipetizioniPdf #ripetizioni").val(1);
            $("#popupRipetizioniPdf").modal();
        }
    });

}
function on_confirm_copy_selected(){

    if($(".selector:checked").length==0){
        alert("Si prega di selezionare almeno una card prima di proseguire con la generazione del pdf");
        return;
    }

    $.confirm({
        title: 'Attenzione',
        content: 'Confermi di voler generare il pdf con le '+$(".selector:checked").length+' card selezionate?',
        confirmButton: 'Sì',
        cancelButton: 'No',
        confirmButtonClass: 'btn-warning',
        cancelButtonClass: 'btn btn-success',
        confirm: function () {
            $("#popupRipetizioni #ripetizioni").val(1);
            $("#popupRipetizioni").modal();
        }
    });

}



        $('#customers_adv_table').DataTable({
            oLanguage: {sSearch: "Filtra: "},
            responsive: true,
            pageLength:my_preference,
            dom: 'Bfrtip',
            buttons: [
                "excelHtml5",
                {
                    text: 'Duplica',
                    action: function ( e, dt, node, config ) {
                        on_confirm_copy_selected();
                    }
                },
                {
                    text: 'PDF',
                    action: function ( e, dt, node, config ) {
                        on_confirm_get_pdf();
                    }
                }


            ],
           "columnDefs": [
                tablet_hide_col
            ]
        });

        var my_preference=localStorage.getItem('page_length_preference');
        if(my_preference==null)
            my_preference=10;

    function add_page(el){
        localStorage.setItem('page_length_preference',el.value);
        window.location.reload(true);
    }

    function get_page_length(my_preference){
        var page_content=
        '<div class="dataTables_length" id="example_length" style="float: left;"><label>'+
        'Showing <select onchange="add_page(this)" name="example_length" aria-controls="example" class="">';
        var dim=[10,25,50,100,250,500,1000,2000];

        for(var i =0;i<dim.length;i++){
            if(my_preference==dim[i])
                page_content+='<option value="'+dim[i]+'" selected>'+dim[i]+'</option>'
            else
                page_content+='<option value="'+dim[i]+'">'+dim[i]+'</option>'
        }

        page_content+='</select> entries </label></div>'  ;
        return page_content;
    }

    $(document).ready(function(){
        var pl=get_page_length(my_preference);
        $(".btn-default.buttons-excel.buttons-html5").after(pl);
    });

    window['table']="cards";
    console.error("admin/adv");
    </script>
@endsection
