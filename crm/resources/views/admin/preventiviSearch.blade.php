@extends('layouts.master')
@section('header-page-personalization')
    <!-- page specific stylesheets -->
<?php ini_set('get_max_size' , '20M');?>

    <link href="{{ asset('/css/style.css') }}" rel="stylesheet" media="screen">

    <link href='https://fonts.googleapis.com/css?family=Source+Sans+Pro:400&amp;subset=latin-ext,latin' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="{{ asset('/lib/DataTables/media/css/jquery.dataTables.min.css') }}">

    <style>
        #customers_table thead,
        #customers_table th {text-align: center;}
        #customers_table tr {text-align: center;}

        #customers_adv_table thead,
        #customers_adv_table th {text-align: center;}
        #customers_adv_table tr {text-align: center;}

        .hideBecauseDatatablesSucks{
        }

        .search{
            text-align: center;
        }

        #dateRangeForm .form-control-feedback {
            top: 0;
            right: -15px;
        }
    </style>
@endsection
@section('content')

<?php 
    $stato=DB::select("SELECT DISTINCT statoPreventivo as label,statoPreventivo as value FROM `preventivo` WHERE statoPreventivo!=''");
    $destinazione=DB::select("SELECT DISTINCT labeldest as label,labeldest as value FROM `preventivo` WHERE labeldest!=''");
    $editore=DB::select("SELECT DISTINCT username as label,users.id as value FROM users inner join `preventivo` on preventivo.authorId = users.id");
    $clienti=DB::select("SELECT DISTINCT concat( IFNULL(customers.cognome,'') ,' ',IFNULL(customers.nome,'') ) as label, customers.id as value FROM customers inner join `preventivo` on preventivo.riferitoA = customers.id");
?>
    <div class="page_content page_preventivoSearch">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                        <div class="panel panel-default">
                            <div class="panel-body">

                            <form class="form-horizontal"  method="get" action="/admin/preventivi">
                            <fieldset>

                            <legend><span class="big-title">Ricerca preventivo</span></legend>

                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label class="col-md-4 control-label" for="textinput">Stato</label>  
                                    <input type="hidden" name="view" value="search">
                                    <div class="col-md-4">
                                        <input id="stato" name="preventivowjkstatoPreventivo" type="text" placeholder="Stato del preventivo" class="form-control input-md">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-4 control-label" for="textinput">Destinazione</label>  
                                    <div class="col-md-4">
                                        <input id="destinazione" name="preventivowjklabeldest" type="text" placeholder="Destinazione" class="form-control input-md">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-4 control-label" for="textinput">Editore </label>  
                                    <div class="col-md-4">
                                        <input id="editore" name="preventivowjkauthorId" type="text" class="form-control input-md">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-4 control-label" for="textinput">Cliente </label>  
                                    <div class="col-md-4">
                                        <input id="cliente" type="text" class="form-control input-md">
                                        <input name="customerswjkid" type="hidden" class="form-control input-md">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-md-4 control-label" for="textinput">Dal </label>  
                                    <div class="col-md-4">
                                    <input type="date" id="preventivowjkdal" name="preventivowjkdal" class="form-control">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-4 control-label" for="textinput">Al </label>  
                                    <div class="col-md-4">
                                        <input type="date" id="preventivowjkal" name="preventivowjkal" class="form-control">
                                    </div>
                                </div>
                            </div>


                              
  
                            <div class="form-group search" >
                              <div class="col-md-8">
                                <button id="button1id" name="button1id" class="btn btn-info">Cerca</button>
                              </div>
                            </div>

                            </fieldset>
                            </form>

                                </div>
                            </div>
                        </div>

                </div>
            </div>
        </div>
    </div>

  

@endsection
@section('footer-plugin')
    <!-- datatables -->
    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.18/pdfmake.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.2.4/js/dataTables.buttons.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.2.4/js/buttons.bootstrap.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.2.4/js/buttons.print.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.2.4/js/buttons.html5.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <script src="//cdn.datatables.net/buttons/1.4.2/js/buttons.colVis.min.js"></script>
    <script src="{{ asset('/lib/DataTables/extensions/Scroller/js/dataTables.scroller.min.js') }}"></script>
    <script src="https://crm.suend.it/js/tinynav.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.perfect-scrollbar/0.8.1/js/perfect-scrollbar.jquery.min.js"></script>
    <script src="{{ asset('/js/apps/tisa_datatables.js') }}"></script>
    <script src="{{ asset('/lib/moment-js/moment.min.js') }}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.2.0/jquery-confirm.min.js"></script>
    <script src="{{ asset('/lib/jquery.confirm/jquery-confirm.min.js') }}"></script>

    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/css/datepicker.min.css" />
    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/css/datepicker3.min.css" />
    <script src="{{ asset('/crm/resources/assets/js/bootstrap_datepicker.js') }}"></script>

  <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
  <link rel="stylesheet" href="/resources/demos/style.css">
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

    <script>

    var availableTags1= JSON.parse("<?php echo addslashes(json_encode($stato)) ;?>");
    var availableTags2= JSON.parse('<?php echo addslashes(json_encode($destinazione)) ;?>');
    var availableTags3= JSON.parse('<?php echo addslashes(json_encode($editore)) ;?>');
    var availableTags4= JSON.parse('<?php echo addslashes(json_encode($clienti)) ;?>');

    $(document).ready(function(){
        $( "#stato" ).autocomplete({
            source: availableTags1
        });   
        $( "#destinazione" ).autocomplete({
          source: availableTags2
        });   
        $( "#editore" ).autocomplete({
            source: availableTags3
        });  

        $( "#cliente" ).autocomplete({
            source: availableTags4,
            select: function (a, b) {
                a.preventDefault();
                $('#cliente').val(b.item.label);
                $('[name="customerswjkid"]').val(b.item.value);
            }
            });   

    })

    console.error("admin/preventivoSearch");
    
    </script>
@endsection
