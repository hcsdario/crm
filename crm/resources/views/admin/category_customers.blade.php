@extends('layouts.master')



@section('header-page-personalization')
    <!-- page specific stylesheets -->

    <!-- datatables -->
    <link rel="stylesheet" href="{{ asset('/lib/DataTables/media/css/jquery.dataTables.min.css') }}">
    <link rel="stylesheet" href="{{ asset('/lib/DataTables/extensions/TableTools/css/dataTables.tableTools.min.css') }}">
    <link rel="stylesheet" href="{{ asset('/lib/DataTables/extensions/Scroller/css/dataTables.scroller.min.css') }}">

    <!-- main stylesheet -->
    <link href="{{ asset('/css/style.css') }}" rel="stylesheet" media="screen">

    <!-- google webfonts -->
    <link href='http://fonts.googleapis.com/css?family=Source+Sans+Pro:400&amp;subset=latin-ext,latin' rel='stylesheet' type='text/css'>


    <!-- moment.js (date library) -->
    <script src="{{ asset('/lib/moment-js/moment.min.js') }}"></script>
    <style>
        #customers_table thead,
        #customers_table th {text-align: center;}
        #customers_table tr {text-align: center;}

        #customers_adv_table thead,
        #customers_adv_table th {text-align: center;}
        #customers_adv_table tr {text-align: center;}
        [readonly]{
          background-color: white;
          opacity: 1;
        }

    </style>
@endsection
@section('content')
    <div class="page_content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <form id="login_form" role="form" method="POST" action="{{ route('category_customers.store') }}" class="customForm">
                        {{ csrf_field() }}
                        <div class="panel panel-default">

                            <div class="panel-body">
                                <fieldset>
                                    <legend><span>Categorie Clienti</span></legend>
                                    <table class="table table-bordered table-striped" id="customers_adv_table" >
                                        <thead>
                                            
                                            <th>id</th>
                                            <th>Categoria</th>
                                            <th>Azioni</th>
                                        </thead>
                                        <tbody>


                                             @foreach($categorie as $categoria)
                                                    <tr>
                                                        <td>{{ $categoria->id }}</td>
                                                        <td class="center">{{ $categoria->name }}</td>
                                                        <td>
                                                            <a href="#" class="btn btn-xs btn-danger news-zama" style="margin-left: 10px;" onclick="deleteRecord(this,['categories_customers',{{ $categoria->id }}])"><i class="glyphicon glyphicon-trash" data-toggle="tooltip" title="Elimina"></i></a>
                                                         </td>
                                                    </tr>
                                            @endforeach
                                            
                                        </tbody>
                                </fieldset>
                                </fieldset>
                                @if(session()->has('message'))
                                    <div class="alert alert-success">
                                        {{ session()->get('message') }}
                                    </div>
                                @endif
                                @if(session()->has('error'))
                                    <div class="alert alert-danger">
                                        {{ session()->get('error') }}
                                    </div>
                                @endif

                                <div class="row">
                                    <div class="col-lg-12">
                                      <input type="hidden" id="id" name="id" class="form-control" tabindex="1" value="{{@$category->id}}">
                                        <div class="form-group">
                                            <label for="reg_input">nome</label>
                                            <input type="text" id="categoria" name="name" class="form-control" tabindex="1" value="{{@$category->name}}">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        @if (@$category)
                          <button class="btn btn-lg btn-success btn-block">Modifica Categoria</button>
                        @else
                          <button class="btn btn-lg btn-success btn-block">Aggiungi Categoria</button>
                        @endif
                    </form>
                </div>
            </div>
        </div>
    </div>




@endsection
@section('footer-plugin')
    <!-- datatables -->
    <script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.18/pdfmake.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.2.4/js/dataTables.buttons.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.2.4/js/buttons.bootstrap.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.2.4/js/buttons.print.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.2.4/js/buttons.html5.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <script src="//cdn.datatables.net/buttons/1.4.2/js/buttons.colVis.min.js"></script>
    <script src="{{ asset('/lib/DataTables/extensions/Scroller/js/dataTables.scroller.min.js') }}"></script>
    <script src="http://tinynav.com/tinynav.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.perfect-scrollbar/0.8.1/js/perfect-scrollbar.jquery.min.js"></script>
    <script src="{{ asset('/js/apps/tisa_datatables.js') }}"></script>
    <script src="{{ asset('/lib/moment-js/moment.min.js') }}"></script>

    <script src="{{ asset('/lib/DataTables/media/js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('/lib/DataTables/media/js/dataTables.bootstrap.js') }}"></script>
    <script src="{{ asset('/lib/DataTables/extensions/TableTools/js/dataTables.tableTools.min.js') }}"></script>
    <script src="{{ asset('/lib/DataTables/extensions/Scroller/js/dataTables.scroller.min.js') }}"></script>
    <script src="{{ asset('/js/apps/tisa_datatables.js') }}"></script>

<script type="text/javascript">
    $('#customers_adv_table').DataTable();
</script>


@endsection
