@extends('layouts.master')
@section('header-page-personalization')
    <style>
        .twitter-typeahead, .tt-hint, .tt-input, .tt-menu { width: 100%;  }
        .tt-menu {
            background: #ffffff;
        }

    </style>
@endsection
@section('content')
    <div class="page_content">

        <div class="container-fluid">
                           @if(session()->has('message'))
                                <div class="alert alert-success">
                                    {{ session()->get('message') }}
                                </div>
                            @endif         
            <div class="row">
                <div class="col-lg-12">
                  <!-- * -->
                  <div class="panel panel-default">
                            <div class="panel-body">
                              <!--* -->
                  <form id="register_form" role="form" method="POST" action="{{ route('newUser.store') }}">
                      {{ csrf_field() }}
                      <h1 class="login_heading"><legend><span class="big-title">Crea un Nuovo utente </span></legend></h1>
                      <div class="form-group">
                          <label for="register_username">Nome</label>


                          <input type="text" class="form-control input-lg" name="name" id="name" value="{{ old('name') }}" required
                                 autofocus>
                          @if ($errors->has('name'))
                              <span class="help-block">
                                  <strong>{{ $errors->first('name') }}</strong>
                              </span>
                          @endif
                      </div>
                      <div class="form-group">
                          <label for="register_surname">Cognome</label>
                          <input type="text" class="form-control input-lg" id="surname" name="surname" value="{{ old('surname') }}" required>
                          @if ($errors->has('surname'))
                              <span class="help-block">
                              <strong>{{ $errors->first('surname') }}</strong>
                          </span>
                          @endif
                      </div>
                      <div class="form-group">
                          <label for="register_surname">Username</label>
                          <input type="text" class="form-control input-lg" id="username" name="username" value="{{ old('username') }}"
                                 required>
                          @if ($errors->has('username'))
                              <span class="help-block">
                              <strong>{{ $errors->first('username') }}</strong>
                          </span>
                          @endif
                      </div>
                      <div class="form-group">
                          <label for="register_email">Email</label>
                          <input type="text" class="form-control input-lg" id="email" name="email" value="{{ old('email') }}"
                                 required>
                          @if ($errors->has('email'))
                              <span class="help-block">
                                  <strong>{{ $errors->first('email') }}</strong>
                              </span>
                          @endif
                      </div>


                      <div class="form-group">
                          <label for="register_password">Password</label>
                          <input type="password" class="form-control input-lg" name="password" id="password" required>
                          @if ($errors->has('password'))
                              <span class="help-block">
                              <strong>{{ $errors->first('password') }}</strong>
                          </span>
                          @endif
                      </div>

                      <div class="form-group">
                          <label for="register_password">Conferma Password</label>

                          <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
                      </div>
                      <div class="form-group" style="display: none;">
                          <label for="register_password">Amministratore</label>
                          <input type="hidde" class="form-control input-sm" name="admin" id="admin" value="0">
                          @if ($errors->has('admin'))
                              <span class="help-block">
                              <strong>{{ $errors->first('admin') }}</strong>
                          </span>
                          @endif
                      </div>

                      <div class="submit_section">
                          <button type="submit" class="btn btn-lg btn-success btn-block">Salva</button>

                      </div>
                  </form>
                </div>
                </div>
                </div>

            </div>
        </div>
        <br>
<?php 
  $IsSuperAdmin=true;
  $idutente=Auth::user()->id;
  if($idutente!=1)
    $IsSuperAdmin=false;
?>
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                        <div class="panel panel-default">

                            <div class="panel-body">
                                <fieldset>
                                    <legend><span class="big-title">Utenti della piattaforma</span></legend>
                                    <p>Assegna le zone agli utenti della piattaforma</p>
                                </fieldset>
                                <div class="row">
                                    <div class="col-lg-12">
                                            <table class="table table-bordered" id="user_table" >
                                                <thead>
                                                <tr>
                                                    <th>Id</th>
                                                    <th>Cognome</th>
                                                    <th>Nome</th>
                                                    <th>Username</th>
                                                    <th>Email</th>
                                                    <th>Amministratore?</th>
                                                    <th>Zona</th>
                                                   <?php if($IsSuperAdmin){ ?>
                                                      <th>Permessi di visualizzazione</th>
                                                      <th>Elimina</th>
                                                    <?php } ?>
                                                    <th>Azione</th>
                                                </tr>
                                                </thead>
                                                <tbody>
<?php 
  if(Auth::user()->id==1)
    $sql = "SELECT users.id id,name,surname,username,email,admin,userZone, users.permessi FROM `users` LEFT JOIN newsUserZone ON users.id=userid";
  else
    $sql="SELECT users.id id,name,surname,username,email,admin,COALESCE(userZone,'Tutte le zone') userZone FROM `users` LEFT JOIN newsUserZone ON users.id=userid WHERE userZone IS NULL OR userZone in (SELECT userZone FROM newsUserZone WHERE userid='$idutente')";
     $query = DB::select($sql);
  
  $zone=DB::select("SELECT * FROM `newsZone`");
  for($i=0;$i<count($query);$i++){?>
<tr>
  <td>{{$query[$i]->id}}</td>
  <td>{{$query[$i]->surname}}</td>
  <td>{{$query[$i]->name}}</td>
  <td>{{$query[$i]->username}}</td>
  <td>{{$query[$i]->email}}</td>
   <td>
    <?php if($IsSuperAdmin){ ?>
      <select name="admin" class="form-control" onchange="updateAdmin(this)">
        <option value="1" <?php if($query[$i]->admin==1) echo "selected"; ?>>Si </option>
        <option value="0" <?php if($query[$i]->admin==0) echo "selected"; ?>> No </option>
      </select>
    <?php }else{ ?>
      <?php if($query[$i]->admin==1) echo "Si"; ?>
      <?php if($query[$i]->admin==0) echo "No"; ?>
    <?php } ?>
  </td>
  <td>




<?php if(empty($query[$i]->userZone)) { ?>
      <select name="zone" class="form-control" onchange="updateZone(this)">
        <option disabled selected>Scegli una zona</option>
        <?php 

         for($j=0;$j<count($zone);$j++){?>
        <option value="{{$zone[$j]->zone}}">{{$zone[$j]->zone}}</option>
        <?php } ?>
      </select>

    <?php }else{ ?>

      <select name="zone" class="form-control" onchange="updateZone(this)">
        <?php 
        for($j=0;$j<count($zone);$j++){?>
        <option value="{{$zone[$j]->zone}}"<?php 
        if($zone[$j]->zone==$query[$i]->userZone)
          echo "selected";?>
        >{{$zone[$j]->zone}}</option>
        <?php } ?>
      </select>
    <?php } ?>
    
  </td>

  <?php if($IsSuperAdmin){ ?>
    <td>
      <form>
        <fieldset>
          <div class="some-class">
            <input type="radio" onclick="changePermissionLevel({{$query[$i]->id}},1)" class="radio" name="Permessi" value="1" id="y" <?php if($query[$i]->permessi=="1") echo 'checked="checked"';?> />
            <label for="y">Puo visualizzare tutto</label>

            <input type="radio" onclick="changePermissionLevel({{$query[$i]->id}},0)" style="clear:both;" class="radio" name="Permessi" value="0" <?php if($query[$i]->permessi=="0") echo 'checked="checked"';?> id="z" />
            <label for="z">Puo visualizzare solo ciò che inserisce</label>
          </div>
        </fieldset>
      </form>
    </td>
   <td><a href="#" class="btn btn-xs btn-danger news-zama" style="margin-left: 10px;" onclick="deleteRecord(this)"><i class="glyphicon glyphicon-minus" class="disabled"></i>&nbsp;Elimina</a></td>
  <?php } ?>
  <td><a href="../admin/newUser/{{$query[$i]->id}}" class="btn btn-xs btn-secondary"><i class="glyphicon glyphicon-edit"></i>&nbsp;Modifica</a></td>
</tr>
<?php } ?>
                                                </tbody>
                                            </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                </div>
            </div>
        </div>

    </div>
@endsection
@section('footer-plugin')
<script type="text/javascript">
window['table']="users";

function changePermissionLevel(userid,lv) {
  debugger;

  $.ajax({
        url: '../api.php',     
        type: "POST",
        data: { 
            qr:'changePermissionLevel',
            userid:userid,
            lv:lv,
          },
      success: function(res){
          debugger;
    },
    error: function(){
          debugger;
    }
    });  
}

function updateZone(el) {

var userid=$(el).parents('tr').find('td:eq(0)').text().trim();
var userZone=$(el).find('option:selected').val();

$.ajax({
      url: '../api.php',     
      type: "POST",
      data: { 
          qr:'updateZone',
          userid:userid,
          userZone:userZone,
        },
    success: function(res){
      
  console.log(res);
  },
  error: function(){
      
    console.log("ajax error ");
  }
  });  
}
function updateAdmin(el) {

var userid=$(el).parents('tr').find('td:eq(0)').text().trim();
var status=$(el).find('option:selected').val();

$.ajax({
      url: '../api.php',     
      type: "POST",
      data: { 
          qr:'updateAdmin',
          userid:userid,
          status:status,
        },
    success: function(res){
      
  console.log(res);
  },
  error: function(){
      
    console.log("ajax error ");
  }
  });  
}

</script>
@endsection