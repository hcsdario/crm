@extends('layouts.master')
@section('header-page-personalization')
    <!-- page specific stylesheets -->
<?php
ini_set('get_max_size' , '20M');
?>
    <link href="{{ asset('/css/style.css') }}" rel="stylesheet" media="screen">

    <!-- google webfonts -->
    <link href='https://fonts.googleapis.com/css?family=Source+Sans+Pro:400&amp;subset=latin-ext,latin' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="{{ asset('/lib/DataTables/media/css/jquery.dataTables.min.css') }}">

    <style>
        #customers_table thead,
        #customers_table th {text-align: center;}
        #customers_table tr {text-align: center;}

        #customers_adv_table thead,
        #customers_adv_table th {text-align: center;}
        #customers_adv_table tr {text-align: center;}
        .hideBecauseDatatablesSucks{
          /*  display: none;*/
        }
    </style>
@endsection
@section('content')

    <div class="page_content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">

                        <div class="panel panel-default">
                            <div class="panel-body">
                                <fieldset>
                                    <legend><span class="big-title">Elenco privati</span></legend>
                                </fieldset>
                                <div class="row">
                                            <table class="table table-bordered privati table-striped" id="customers_table" data-tablename="customers">
                                                <thead>
                                                    <th>Id</th>
                                                    <th>Codice</th>
                                                    <th>Societa</th>
                                                    <th>Cognome</th>
                                                    <th>Nome</th>
                                                    <th>Categoria</th>
                                                    <th>Data di nascita</th>
                                                    <th>Citta'</th>
                                                    <th>Provincia</th>
                                                    <th>Cap</th>
                                                    <th>Regione</th>
                                                    <th>Stato</th>
                                                    <th>Telefono principale</th>
                                                    <th>Contatto telefonico</th>
                                                    <th>Contatto telefonico</th>
                                                    <th>Email</th>
                                                    <th>Indirizzo</th>
                                                    <th>status</th>
                                                    <th>Ultimo aggiornamento</th>
                                                    <th>Professione</th>
                                                    <th>Hobby1</th>
                                                    <th>Hobby2</th>
                                                    <th>Scadenza passaporto</th>
                                                    <th>Passaporto</th>
                                                    <th>Carta d'identita</th>
                                                    <th>Scadenza carta identita</th>
                                                    <th>Frequent flyer</th>
                                                    <th>Newsletter</th>
                                                    <th>Cliente diretto</th>
                                                    <th>Adulti</th>
                                                    <th>Bambini</th>
                                                    <th>Neonati</th>
                                                    <th>Figli</th>
                                                    <th>Anziani</th>
                                                    <th>Animali</th>
                                                    <th>Iscritto_club</th>
                                                    <th>Codice club</th>
                                                    <th>note</th>
                                                    <th>Inserito da</th>
                                                    <th>Azioni</th>
                                                </thead>
                                                <tbody>
                                                @foreach($privati as $privato)
                                                    <tr>
                                                      @php  $cat = App\Category_Customers::find($privato->cat_id);
                                                      @endphp
                                                        <td>{{ $privato->id }}</td>
                                                        <td>{{ $privato->codice }}</td>
                                                        <td>{{ $privato->societa }}</td>
                                                        <td>{{ $privato->cognome }}</td>
                                                        <td>{{ $privato->nome }}</td>
                                                        <td>{{@$cat? $cat->name : 'Nessuna' }}</td>
                                                        <td>{{ $privato->data_nascita }}</td>
                                                        <td>{{ $privato->citta }}</td>
                                                        <td>{{ $privato->provincia }}</td>
                                                        <td>{{ $privato->cap }}</td>
                                                        <td>{{ $privato->regione }}</td>
                                                        <td>{{ $privato->stato }}</td>
                                                        <td>{{ $privato->tel1 }}</td>
                                                        <td>{{ $privato->tel2 }}</td>
                                                        <td>{{ $privato->tel3 }}</td>
                                                        <td>{{ $privato->email1 }}</td>
                                                        <td>{{ $privato->indirizzo2 }}</td>
                                                        <td>{{ $privato->status }}</td>
                                                        <td>{{ $privato->updated_at }}</td>
                                                        <td>{{ $privato->professione }}</td>
                                                        <td>{{ $privato->hobby1 }}</td>
                                                        <td>{{ $privato->hobby2 }}</td>
                                                        <td>{{ $privato->scadenza_passaporto }}</td>
                                                        <td>{{ $privato->passaporto }}</td>
                                                        <td>{{ $privato->carteidentita }}</td>
                                                        <td>{{ $privato->scadenza_cartaidentita }}</td>
                                                        <td>{{ $privato->frequent_flyer }}</td>
                                                        <td>{{ $privato->newsletter }}</td>
                                                        <td>{{ $privato->cliente_diretto }}</td>
                                                        <td>{{ $privato->adulti }}</td>
                                                        <td>{{ $privato->bambini }}</td>
                                                        <td>{{ $privato->neonati }}</td>
                                                        <td>{{ $privato->figli }}</td>
                                                        <td>{{ $privato->anziani }}</td>
                                                        <td>{{ $privato->animali }}</td>
                                                        <td>{{ $privato->iscritto_club }}</td>
                                                        <td>{{ $privato->codice_club }}</td>
                                                        <td>{{ $privato->note }}</td>
                                                        <td>{{ $privato->insertBy }}</td>
                                                        <td>
                                                            <a href="../admin/cliente/{{ $privato->id }}?action=view" class="btn btn-xs btn-info"><i class="glyphicon glyphicon-search" data-toggle="tooltip" title="Visualizza"></i></a>
                                                            <a href="../admin/cliente/{{ $privato->id }}" class="btn btn-xs btn-secondary"><i class="glyphicon glyphicon-edit" data-toggle="tooltip" title="Modifica"></i></a>
                                                            <a href="#" class="btn btn-xs btn-danger news-zama" style="margin-left: 10px;" onclick="deleteRecord(this,['customers',{{$privato->id}}])"><i class="glyphicon glyphicon-trash" data-toggle="tooltip" title="Elimina"></i></a>
                                                        </td>
                                                    </tr>
                                                     @endforeach
                                                </tbody>
                                            </table>
                                    </div>
                                    <div class="addNew">
                                        <button class="btn btn-lg btn-success btn-block" onclick="window.location='{{ url("/admin/cliente") }}'">Aggiungi un nuovo privato</button>
                                    </div>
                                </div>
                            </div>
                        </div>

                </div>
            </div>
        </div>
    </div>



@endsection
@section('footer-plugin')
    <!-- datatables -->
    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.18/pdfmake.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.2.4/js/dataTables.buttons.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.2.4/js/buttons.bootstrap.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.2.4/js/buttons.print.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.2.4/js/buttons.html5.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <script src="//cdn.datatables.net/buttons/1.4.2/js/buttons.colVis.min.js"></script>
    <script src="{{ asset('/lib/DataTables/extensions/Scroller/js/dataTables.scroller.min.js') }}"></script>
    <script src="https://crm.suend.it/js/tinynav.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.perfect-scrollbar/0.8.1/js/perfect-scrollbar.jquery.min.js"></script>
    <script src="{{ asset('/js/apps/tisa_datatables.js') }}"></script>
    <script src="{{ asset('/lib/moment-js/moment.min.js') }}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.2.0/jquery-confirm.min.js"></script>
    <script src="{{ asset('/lib/jquery.confirm/jquery-confirm.min.js') }}"></script>


    <script src="{{ asset('/lib/DataTables/media/js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('/lib/DataTables/media/js/dataTables.bootstrap.js') }}"></script>
    <script src="{{ asset('/lib/DataTables/extensions/TableTools/js/dataTables.tableTools.min.js') }}"></script>
    <script src="{{ asset('/lib/DataTables/extensions/Scroller/js/dataTables.scroller.min.js') }}"></script>
    <script src="{{ asset('/js/apps/tisa_datatables.js') }}"></script>

    <script>

        var my_preference=localStorage.getItem('page_length_preference');
        if(my_preference==null)
            my_preference=10;

        var dtable = $('#customers_table').DataTable({
            responsive: true,
            pageLength:my_preference,
            oLanguage: {sSearch: "Filtra: "},
            dom: 'Bfrtip',
            buttons: [
                "excelHtml5"
            ],
            "columnDefs": [
                {
                    "targets": [0,2, 7,8,9,10,12,13,14,15,16,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36 ],
                    "visible": false,
                }
            ]
        });

    window['table']="dataInTable";
    console.error("admin/customers");



    function add_page(el){
        localStorage.setItem('page_length_preference',el.value);
        window.location.reload(true);
    }

    function get_page_length(my_preference){
        var page_content=
        '<div class="dataTables_length" id="example_length" style="float: left;"><label>'+
        'Showing <select onchange="add_page(this)" name="example_length" aria-controls="example" class="">';
        var dim=[10,25,50,100,250,500,1000,2000];

        for(var i =0;i<dim.length;i++){
            if(my_preference==dim[i])
                page_content+='<option value="'+dim[i]+'" selected>'+dim[i]+'</option>'
            else
                page_content+='<option value="'+dim[i]+'">'+dim[i]+'</option>'
        }

        page_content+='</select> entries </label></div>'  ;
        return page_content;
    }

    $(document).ready(function(){
        var pl=get_page_length(my_preference);
        $(".btn-default.buttons-excel.buttons-html5").after(pl);
    });

    </script>
@endsection
