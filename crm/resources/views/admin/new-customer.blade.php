@extends('layouts.master')
@section('header-page-personalization')
@endsection
@section('content')
    <div class="page_content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <form id="login_form" role="form" method="POST" action="{{ route('customer.store') }}" class="customForm">
                        {{ csrf_field() }}
                        <div class="panel panel-default">

                            <div class="panel-body">
                                <fieldset>
                                    <legend><span>Clienti</span></legend>
                                </fieldset>
                                @if(session()->has('message'))
                                    <div class="alert alert-success">
                                        {{ session()->get('message') }}
                                    </div>
                                @endif
                                @if(session()->has('error'))
                                    <div class="alert alert-danger">
                                        {{ session()->get('error') }}
                                    </div>
                                @endif

                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="form-group">
                                            <label for="reg_input">Societa</label>
                                            <input type="text" id="societa" name="societa" class="form-control" tabindex="1" >
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="form-group">
                                            <label for="reg_input">Cognome</label>
                                            <input type="text" id="cognome" name="cognome" class="form-control" tabindex="3" >
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label for="reg_input">Nome</label>
                                            <input type="text" id="nome" name="nome" class="form-control" tabindex="2" >
                                        </div>
                                        <div class="form-group">
                                            <label for="reg_input">Regione</label>
                                            <select id="regione" name="regione" class="form-control">
                                                @foreach ($regioni as $regione)
                                                    <option value="{{ $regione->id_regione }}">{{ $regione->regione }}</option>
                                                @endforeach
                                            </select>

                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label for="reg_input">Provincia</label>
                                            <select id="provincia" name="provincia" class="form-control">
                                                <option value=""></option>
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label for="reg_input">Cap</label>
                                            <select id="cap" name="cap" class="form-control">
                                                <option value=""></option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label for="reg_input">Citta'</label>
                                            <select id="citta" name="citta" class="form-control">
                                                <option value=""></option>
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label for="reg_input">Stato</label>
                                            <input type="text" id="stato" name="stato" class="form-control" tabindex="8" >
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label for="reg_input">Telefono Principale</label>
                                            <input type="text" id="tel1" name="tel1" class="form-control" tabindex="9" >
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label for="reg_input">Email Principale</label>
                                            <input type="text" id="email1" name="email1" class="form-control" tabindex="10" >
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-body">
                                <fieldset>
                                    <legend><span>Info</span></legend>
                                </fieldset>
                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label for="reg_input">Indirizzo</label>
                                            <input type="text" id="indirizzo" name="indirizzo" class="form-control" tabindex="11" >
                                        </div>
                                        <div class="form-group">
                                            <label for="reg_input">Sito Web</label>
                                            <input type="text" id="web" name="cognome" class="form-control" tabindex="13" >
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label for="reg_input">Partita Iva</label>
                                            <input type="text" id="piva" name="piva" class="form-control" tabindex="12" >
                                        </div>
                                        <div class="form-group">
                                            <label for="reg_input">Codice Fiscale</label>
                                            <input type="text" id="codice_fiscale" name="codice_fiscale" class="form-control" tabindex="14" >
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label for="reg_input">Data di Nascita</label>
                                            <input type="text" id="data_nascita" name="data_nascita" class="form-control" tabindex="15" >
                                        </div>

                                    </div>

                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label for="reg_input">Sesso</label>
                                            <select id="sesso" name="sesso" class="form-control" tabindex="16" >
                                                <option value="MASCHIO">MASCHIO</option>
                                                <option value="FEMMINA">FEMMINA</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label for="reg_input">Telefono2</label>
                                            <input type="text" id="tel2" name="tel2" class="form-control" tabindex="22" >
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label for="reg_input">Email2</label>
                                            <textarea type="text" id="email2" name="email2" class="form-control" tabindex="21"></textarea>

                                        </div>
                                    </div>
                                    <div class="col-lg-12">
                                        <div class="form-group">
                                            <label for="privacy" style="text-align: center;">Accettazione Privacy</label>
                                            <input type="checkbox" class="form-control input-sm" name="privacy" id="privacy" >
                                        </div>
                                    </div>
                                    <div class="col-lg-12">
                                        <div class="form-group">
                                            <label for="privacy_commerciali" style="text-align: center;">Accettazione Privacy Commerciale</label>
                                            <input type="checkbox" class="form-control input-sm" name="privacy_commerciali" id="privacy_commerciali" >
                                        </div>
                                    </div>
                                    <div class="col-lg-12">
                                        <div class="form-group">
                                            <label for="privacy_profilazione" style="text-align: center;">Accettazione Privacy Profilazione</label>
                                            <input type="checkbox" class="form-control input-sm" name="privacy_profilazione" id="privacy_profilazione" >
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                        <button class="btn btn-lg btn-success btn-block">Aggiungi Cliente</button>
                    </form>
                </div>
            </div>
        </div>
    </div>

@endsection
@section('footer-plugin')

    <script type="text/javascript">
        $(document).ready(function()
        {
            // get the province by id regione
            $("#regione").change(function()
            {
                var id=$(this).val();
                console.log(id);
                $('#citta').empty();
                $('#cap').empty();
                $('#provincia').empty();
                $('#stato').val('');

                if (id == "99") {
                    $('#citta').empty();
                    $('#cap').empty();
                    $('#stato').val('STATO ESTERO');
                    provincia =  '<option value="STATO ESTERO">STATO ESTERO</option>';
                    citta = '<option value="STATO ESTERO">STATO ESTERO</option>';
                    cap = '<option value="STATO ESTERO">STATO ESTERO</option>';
                    $('#citta').append(citta);
                    $('#cap').append(cap);
                    $('#provincia').append(provincia);

                } else {
                    var url = '/provincia/'+id;
                    $.ajax
                    ({
                        type: "GET",
                        cache: false,
                        url : url,

                        success: function(data)
                        {

                            var provincia = '';
                            console.log(data);
                            $('#provincia').empty();
                            $.each(data, function( k, v ) {

                                console.log(v.provincia);
                                provincia = '<option value="'+v.sigla+'">'+v.provincia+'</option>';
                                console.log(provincia);
                                $('#provincia').append(provincia);
                            });

                        },

                    });

                }

            });

            $("#provincia").change(function()
            {
                var provincia=$(this).val();
                console.log(provincia);
                var url = '/citta/'+provincia;
                console.log(url);
                $.ajax
                ({
                    type: "GET",
                    cache: false,
                    url : url,
                    success: function(data)
                    {
                        var citta = '';
                        var cap = '';
                        console.log(data);
                        $('#citta').empty();
                        $('#cap').empty();
                        $.each(data, function( k, v ) {
                            //console.log(v.provincia);
                            console.log(provincia);
                            if (v.cap === "00000") {
                                $('#stato').val('STATO ESTERO');
                                citta = '<option value="STATO ESTERO">STATO ESTERO</option>';
                                cap = '<option value="STATO ESTERO">STATO ESTERO</option>';
                            } else {
                                citta = '<option value="'+v.comune+'">'+v.comune+'</option>';
                                cap = '<option value="'+v.cap+'">'+v.cap+'</option>';
                                $('#stato').val('ITALIA');
                            }
                            $('#citta').append(citta);
                            $('#cap').append(cap);
                        });

                    },

                });

            });

        });
        $(document).ready(function(){
            $('input').css('text-transform','uppercase');
        })
            console.error("new customer");
    </script>
@endsection
