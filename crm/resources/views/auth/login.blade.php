<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Login Page</title>
    <meta name="viewport" content="initial-scale=1.0,maximum-scale=1.0,user-scalable=no">

    <!-- bootstrap framework -->
    <link href="{{ asset('/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet">
    <!-- google webfonts -->
    <link href='https://fonts.googleapis.com/css?family=Source+Sans+Pro:400&amp;subset=latin-ext,latin' rel='stylesheet' type='text/css'>

    <link href="{{ asset('/css/login.css') }}" rel="stylesheet">

</head>
<body>
<div class="login_container">

    <form  id="login_form" role="form" method="POST" action="{{ route('login') }}">
        {{ csrf_field() }}
        <h1 class="login_heading">Accedi <span>/ <a href="{{ url('register') }}" class="open_register_form">Registrati</a></span></h1>
        <div class="form-group">
            <label for="login_username">Nome Utente</label>
            <input type="text" class="form-control input-lg" name="username" id="username"  value="{{ old('username') }}" required autofocus>
            @if ($errors->has('username'))
                <span class="help-block">
                    <strong>{{ $errors->first('username') }}</strong>
                </span>
            @endif
        </div>
        <div class="form-group">
            <label for="login_password">Password</label>
            <input type="password" class="form-control input-lg" name="password" id="password" required>
            @if ($errors->has('password'))
                <span class="help-block">
                    <strong>{{ $errors->first('password') }}</strong>
                </span>
            @endif
            <span class="help-block"><a href="#">Password Dimenticata?</a></span>
        </div>
        <div class="submit_section">
            <button class="btn btn-lg btn-success btn-block">Continua</button>
        </div>
    </form>
<p>
</p>
</div>


<!-- jQuery -->
<script src="{{ asset('/js/jquery.min.js') }}"></script>
<!-- bootstrap js plugins -->
<script src="{{ asset('bootstrap/js/bootstrap.min.js') }}"></script>
<script>
    $(function() {
        // switch forms
   /*     $('.open_register_form').click(function(e) {
            e.preventDefault();
            $('#login_form').removeClass().addClass('animated fadeOutDown');
            setTimeout(function() {
                $('#login_form').removeClass().hide();
                $('#register_form').show().addClass('animated fadeInUp');
            }, 700);
        })
        $('.open_login_form').click(function(e) {
            e.preventDefault();
            $('#register_form').removeClass().addClass('animated fadeOutDown');
            setTimeout(function() {
                $('#register_form').removeClass().hide();
                $('#login_form').show().addClass('animated fadeInUp');
            }, 700);
        })*/
    })
</script>
</body>
</html>