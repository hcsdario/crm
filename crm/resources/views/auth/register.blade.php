<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Login Page</title>
    <meta name="viewport" content="initial-scale=1.0,maximum-scale=1.0,user-scalable=no">

    <!-- bootstrap framework -->
    <link href="{{ asset('/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet">
    <!-- google webfonts -->
    <link href='https://fonts.googleapis.com/css?family=Source+Sans+Pro:400&amp;subset=latin-ext,latin' rel='stylesheet'
          type='text/css'>

    <link href="{{ asset('/css/login.css') }}" rel="stylesheet">

</head>
<body>
<div class="login_container">


    <form id="register_form" role="form" method="POST" action="{{ route('newUser.store') }}">
        {{ csrf_field() }}
        <h1 class="login_heading">Register <span>/ <a href="{{ url('login') }}" class="open_login_form">login</a></span></h1>
        <div class="form-group">
            <label for="register_username">Nome</label>
            <input type="text" class="form-control input-lg" name="name" id="name" value="{{ old('name') }}" required
                   autofocus>
            @if ($errors->has('name'))
                <span class="help-block">
                    <strong>{{ $errors->first('name') }}</strong>
                </span>
            @endif
        </div>
        <div class="form-group">
            <label for="register_surname">Cognome</label>
            <input type="text" class="form-control input-lg" id="surname" name="surname" value="{{ old('surname') }}" required>
            @if ($errors->has('surname'))
                <span class="help-block">
                <strong>{{ $errors->first('surname') }}</strong>
            </span>
            @endif
        </div>
        <div class="form-group">
            <label for="register_surname">Username</label>
            <input type="text" class="form-control input-lg" id="username" name="username" value="{{ old('username') }}"
                   required>
            @if ($errors->has('username'))
                <span class="help-block">
                <strong>{{ $errors->first('username') }}</strong>
            </span>
            @endif
        </div>
        <div class="form-group">
            <label for="register_email">Email</label>
            <input type="text" class="form-control input-lg" id="email" name="email" value="{{ old('email') }}"
                   required>
            @if ($errors->has('email'))
                <span class="help-block">
                    <strong>{{ $errors->first('email') }}</strong>
                </span>
            @endif
        </div>


        <div class="form-group">
            <label for="register_password">Password</label>
            <input type="password" class="form-control input-lg" name="password" id="password" required>
            @if ($errors->has('password'))
                <span class="help-block">
                <strong>{{ $errors->first('password') }}</strong>
            </span>
            @endif
        </div>

        <div class="form-group">
            <label for="register_password">Conferma Password</label>

            <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
        </div>
<!--         <div class="form-group">
            <label for="register_password">Amministratore</label>
            <input type="checkbox" class="form-control input-sm" name="admin" id="admin" >
            @if ($errors->has('admin'))
                <span class="help-block">
                <strong>{{ $errors->first('admin') }}</strong>
            </span>
            @endif
        </div> -->

        <div class="submit_section">
            <button type="submit" class="btn btn-lg btn-success btn-block">Continue</button>

        </div>
    </form>

</div>

<!-- jQuery -->
<script src="{{ asset('/js/jquery.min.js') }}"></script>
<!-- bootstrap js plugins -->
<script src="{{ asset('assets/bootstrap/js/bootstrap.min.js') }}"></script>
<script>
    $(function () {
        // switch forms
      /*  $('.open_register_form').click(function (e) {
            e.preventDefault();
            $('#login_form').removeClass().addClass('animated fadeOutDown');
            setTimeout(function () {
                $('#login_form').removeClass().hide();
                $('#register_form').show().addClass('animated fadeInUp');
            }, 700);
        })
        $('.open_login_form').click(function (e) {
            e.preventDefault();
            $('#register_form').removeClass().addClass('animated fadeOutDown');
            setTimeout(function () {
                $('#register_form').removeClass().hide();
                $('#login_form').show().addClass('animated fadeInUp');
            }, 700);
        })*/
    })
</script>
</body>
</html>
