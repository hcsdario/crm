<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Suend CRM</title>
    <meta name="viewport" content="initial-scale=1.0,maximum-scale=1.0,user-scalable=no">

    <link rel="shortcut icon" href="favicon.ico" />

    <!-- bootstrap framework -->
    <link href="{{ asset('/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet" media="screen">

    <!-- custom icons -->
    <!-- font awesome icons -->
    <link href="{{ asset('/icons/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet" media="screen">
    <!-- ionicons -->
    <link href="{{ asset('/icons/ionicons/css/ionicons.min.css') }}" rel="stylesheet" media="screen">
    <!-- flags -->
    <link rel="stylesheet" href="{{ asset('/icons/flags/flags.css') }}">


    <!-- main stylesheet -->
    <link href="{{ asset('/css/style.css') }}" rel="stylesheet" media="screen">
    <link href="{{ asset('/lib/confirm/confirm.css') }}" rel="stylesheet">

    <!-- google webfonts -->
    <link href='https://fonts.googleapis.com/css?family=Source+Sans+Pro:400&amp;subset=latin-ext,latin' rel='stylesheet' type='text/css'>

    <!-- moment.js (date library) -->
    <script src="{{ asset('/lib/moment-js/moment.min.js') }}"></script>
    <script src="{{ asset('/lib/utils.js') }}"></script>


    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.2.0/jquery-confirm.min.css">
    <link rel="stylesheet" href="{{ asset('/lib/jquery.confirm/jquery-confirm.min.css') }}">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Lato" rel="stylesheet">
    <!-- jQuery -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <style type="text/css">
        .page_preventivo input:not([type]):disabled,.page_preventivo .form-control[disabled],.page_preventivo .form-control[readonly],.page_preventivo fieldset[disabled] .form-control{
            background: transparent;
        }
        .page_tasks .radio input[type=radio],.page_tasks  .radio-inline input[type=radio],.page_tasks  .checkbox input[type=checkbox],.page_tasks  .checkbox-inline input[type=checkbox]{
            margin-left: 0px!important;
        }

        .accordion-contenuto{
            overflow: scroll!important;
            height: 120px;
        }

		.pdfpage{
			width: 721px!important; height: 960px!important;
		}
		.pdf_header{
			height:210px !important; display:block!important; float: none!important;
		}
		.page_footer{
			height:200px !important; width:723px!important;display:block!important; float: none!important; margin-bottom:20px;
		}.block1{
			height: auto!important; width:225px!important;
		}.block2{
			width:50px!important;
		}
		@media print
		{
		.block1,.block2 {page-break-before:always}
		}
		#blocco-note{
			text-align: left;
			max-width:500px!important
		}
        @media screen and (orientation: portrait) {
            body{
                -webkit-transform: rotate(90deg);
                -moz-transform: rotate(90deg);
                -o-transform: rotate(90deg);
                -ms-transform: rotate(90deg);
                transform: rotate(90deg);
            }
        }
	    @media screen and (orientation: landscape) {


    	    .alert-warning {
			    width: 70%!important;
			    z-index: 1000;
			}
			.scollable_dashboard_task{
	            max-height: 300px!important;
	            overflow-y:  scroll;
	        }
	        .scollable_dashboard_news{
	            max-height: 255px!important;
	            overflow-y:  scroll;
	        }
			.col-lg-4{
				width:33.3%;
				float:left;
				font-size:11px!important;
			}

			.col-lg-4 h2{
				font-size:20px!important;
			}

			.col-lg-4{
				font-size:10px!important;
			}
			.col-lg-4:nth-of-type(1) .optionbox1, .col-lg-4:nth-of-type(1) .optionbox2{
				padding-left:0px!important;
			}

			.col-lg-6{
				width:50%;
				float:left;
			}

			.div_task{
				margin-top:0px!important;
			}

			.div_news{
				margin-top:0px!important;
			}


	    }

		@media screen and (orientation: portrait) {
		    .alert-warning {
		    	width: 50%!important;
		    	z-index: 1000;
			}
			.div_task {
				margin-top:0px!important;
			}

			.col-lg-4:nth-of-type(2), .col-lg-4:nth-of-type(3){
				width:50%;
				float:left;
			}

			.optionbox span{
				font-size:13px!important;
			}

		}

        @font-face {
           font-family: 'Lato';
              src: url('https://fonts.googleapis.com/css?family=Lato');
              src: url('https://fonts.googleapis.com/css?family=Lato') format('truetype');
           font-weight: normal;
           font-style: normal;
        }


        @font-face {
           font-family: 'OpenSans';
              src: url('https://fonts.googleapis.com/css?family=Open+Sans');
              src: url('https://fonts.googleapis.com/css?family=Open+Sans') format('truetype');
           font-weight: normal;
           font-style: normal;
        }

        @font-face {
           font-family: 'Playfair';
              src: url('/crm/resources/assets/font/PlayfairDisplay-BlackItalic.ttf');
              src: url('/crm/resources/assets/font/PlayfairDisplay-BlackItalic.ttf') format('truetype');
           font-weight: normal;
           font-style: normal;
        }
        #div{
            width: 900px;
            height: 40px;
            margin: auto;
            border: 1px solid rgb(219, 226, 239)!important;
            border-radius: 5px;
            padding: 5px;
            position: relative;
            overflow: hidden;
            top: 20px;
            font-size: 22px;
            border: none;
        }
        #div .spann{
            position: relative;
        }
        #close_brk_news{

        }
        .table.dataTable thead .sorting, .table.dataTable thead .sorting_asc, .table.dataTable thead .sorting_desc,table.dataTable thead  th:last-child {
            background: #61b1c1!important;
        }
        .jconfirm-box{
            margin-top: 50px!important;
        }

        .inlineCheckbox{
            position: relative;
        }
        .inlineCheckbox label{
            position: absolute;
            top: 0px;
            left: 15px;
        }
        .inlineCheckbox input[type='checkbox']{
            position: absolute;
            top: 0px;
            left: 0px;
            height: 13px;
        }

        .accordion:hover{
            background: rgba(66, 139, 202, 0.2);
        }

        .fa-plus, .fa-minus{
            margin: 10px;
        }
        .customForm label{
                height: 30px;
        }
        .fkacc .panel{
            display: none;
        }
        .fa-arrows-v{
            margin: 10px;
        }
    #side_nav .dropdown-submenu{
        position:relative;
    }
    #side_nav .dropdown-submenu > .dropdown-menu
    {
        top:0;
        left:100%;
        margin-top:-6px;
        margin-left:-1px;
        -webkit-border-radius:0 6px 6px 6px;
        -moz-border-radius:0 6px 6px 6px;
        border-radius:0 6px 6px 6px;
    }
    #side_nav .dropdown-submenu:hover > .dropdown-menu{
        display:block;
    }
    #breaking_news_modal ul{

    }
    #breaking_news_modal li{
        font-family: Lato;
        margin-top: 20px;
    }
    #breaking_news_modal .br1{

    }
    #breaking_news_modal .br2{
        font-size: 20px;
        font-weight: bolder;
    }
    #breaking_news_modal .br3{

    }
    .page_dashboard .todo_section {
	  	    margin: 0px!important;
		    padding: 20px!important;
		    margin-top: 0px;
            min-height: 300px;
	}

    #side_nav .dropdown-submenu > a:after{
        display:block;
        content:" ";
        float:right;
        width:0;
        height:0;
        border-color:transparent;
        border-style:solid;
        border-width:5px 0 5px 5px;
        border-left-color:#cccccc;
        margin-top:5px;
        margin-right:-10px;
    }

    #side_nav .dropdown-submenu:hover > a:after{
        border-left-color:#ffffff;
    }

    #side_nav .dropdown-submenu .pull-left{
        float:none;
    }

    #side_nav .dropdown-submenu.pull-left > .dropdown-menu{
        left:-100%;
        margin-left:10px;
        -webkit-border-radius:6px 0 6px 6px;
        -moz-border-radius:6px 0 6px 6px;
        border-radius:6px 0 6px 6px;
    }

    #side_nav .root:hover > .dropdown-menu{
        display: block;
    }
    #side_nav ul.dropdown-menu li{
        height: 40px;
    }
    .checklist{
        width: 30px;
    }
    .tt-dataset.tt-dataset-comune{
        max-height: 150px;
        overflow: scroll;
    }
    .btn.btn-lg.btn-success.btn-block{
      width: 20%!important;
      text-align: center!important;
      margin: auto!important;
      color: white!important;
    }

    .tt-dataset{
      background: white!important;
    }

    .mydestination input{
        width: 200px;
    }

  @font-face {
           font-family: 'Lato';
              src: url('https://fonts.googleapis.com/css?family=Lato');
              src: url('https://fonts.googleapis.com/css?family=Lato') format('truetype');
           font-weight: normal;
           font-style: normal;
        }


        @font-face {
           font-family: 'OpenSans';
              src: url('https://fonts.googleapis.com/css?family=Open+Sans');
              src: url('https://fonts.googleapis.com/css?family=Open+Sans') format('truetype');
           font-weight: normal;
           font-style: normal;
        }

        @font-face {
           font-family: 'Playfair';
              src: url('/crm/resources/assets/font/PlayfairDisplay-BlackItalic.ttf');
              src: url('/crm/resources/assets/font/PlayfairDisplay-BlackItalic.ttf') format('truetype');
           font-weight: normal;
           font-style: normal;
        }

        .jconfirm-box{
            margin-top: 50px!important;
        }

        .inlineCheckbox{
            position: relative;
        }
        .inlineCheckbox label{
            position: absolute;
            top: 0px;
            left: 15px;
        }
        .inlineCheckbox input[type='checkbox']{
            position: absolute;
            top: 0px;
            left: 0px;
            height: 13px;
        }

        .accordion:hover{
            background: rgba(66, 139, 202, 0.2);
        }

        .fa-plus, .fa-minus{
            margin: 10px;
        }
        .customForm label{
                height: 30px;
        }
        .fkacc .panel{
            display: none;
        }
        .fa-arrows-v{
            margin: 10px;
        }
    #side_nav .dropdown-submenu{
        position:relative;
    }
    #side_nav .dropdown-submenu > .dropdown-menu
    {
        top:0;
        left:100%;
        margin-top:-6px;
        margin-left:-1px;
        -webkit-border-radius:0 6px 6px 6px;
        -moz-border-radius:0 6px 6px 6px;
        border-radius:0 6px 6px 6px;
    }
    #side_nav .dropdown-submenu:hover > .dropdown-menu{
        display:block;
    }

    #side_nav .dropdown-submenu > a:after{
        display:block;
        content:" ";
        float:right;
        width:0;
        height:0;
        border-color:transparent;
        border-style:solid;
        border-width:5px 0 5px 5px;
        border-left-color:#cccccc;
        margin-top:5px;
        margin-right:-10px;
    }

    #side_nav .dropdown-submenu:hover > a:after{
        border-left-color:#ffffff;
    }

    #side_nav .dropdown-submenu .pull-left{
        float:none;
    }

    #side_nav .dropdown-submenu.pull-left > .dropdown-menu{
        left:-100%;
        margin-left:10px;
        -webkit-border-radius:6px 0 6px 6px;
        -moz-border-radius:6px 0 6px 6px;
        border-radius:6px 0 6px 6px;
    }

    #side_nav .root:hover > .dropdown-menu{
        display: block;
    }
    #side_nav ul.dropdown-menu li{
        height: 40px;
    }
    .checklist{
        width: 30px;
    }
    .tt-dataset.tt-dataset-comune{
        max-height: 150px;
        overflow: scroll;
    }
    .btn.btn-lg.btn-success.btn-block{
      width: 20%!important;
      text-align: center!important;
      margin: auto!important;
      color: white!important;
    }

    .tt-dataset{
      background: white!important;
    }
        .col-lg-4 i{
            font-size: 4em;
            color: white;
        }

        .div_news .panel{
            border: 2px solid #ffcf00;
            background: #ffcf0024;
        }

        .div_news table, .div_news td, .div_news th{
            border: 1px solid #ffcf00!important;
        }

        #news_table{
            margin-bottom: 0px;
            background:rgba(255, 255, 255, 0.6);
        }

        .todo_section{
            border: 2px solid #f5983d;
            background: rgba(242, 123, 6, 0.1);
            padding-bottom: 6px;
        }


        .todo_date{
            background-color: rgba(242, 123, 6, 0.78);
        }

        .iconbox1{
            width: 20%;
            float: left;
            position: absolute;
            top: 10px;
            left: 30px;
            display:none;
        }

        .iconbox{
            width: 20%;
            float: left;
            padding-top: 40px;
        }

        .optionbox1{
            width:50%!important;
            float:left;
            padding-left: 20px;
            padding-top: 20px;
        }

        .optionbox2{
            width:50%!important;
            float:left;
            padding-left: 20px;
            padding-top: 20px;
        }

        .optionbox3{
            width:80%!important;
            float:left;
            padding-left: 20px;
            padding-top: 20px;
        }

        .optionbox{
            width:75%;
            float:left;
        }

        .optionbox div{
            padding-bottom:10px;
            cursor: pointer;
        }

        .optionbox span:hover{
            color:grey;
            transition:all 0.5s;

        }

        .optionbox i{
            font-size:2em;
        }

        .optionbox span{
            color:white;
            font-weight: bold;
            position: relative;
            bottom: 5px;
            left: 5px;
        }

        .col-lg-4 .boxagency h2{
            font-family: "Open Sans"!important;
            color: #ffffff;
            text-align: center;
            margin-top: 0px;
            font-weight: bold;
            font-size: 25px;
            background-color: #3d9740;
        }

        .col-lg-4 .boxusers h2{
            font-family: "Open Sans"!important;
            color: #ffffff;
            text-align: center;
            margin-top: 0px;
            font-weight: bold;
            font-size: 25px;
            background-color: #3d9794;
        }

        .col-lg-4 .boxbusiness h2{
            font-family: "Open Sans"!important;
            color: #ffffff;
            text-align: center;
            margin-top: 0px;
            font-weight: bold;
            font-size: 25px;
            background-color: #37658d;
        }

        .col-lg-4 h2{
            font-family: "Open Sans"!important;
            color: #4b4b4b;
            text-align: center;
            margin-top: 0px;
            font-weight: bold;
            font-size: 25px;
        }

        .boxusers{
                background-color: #4fbeba;
                padding: 20px;
                box-shadow: 1px 1px 10px #666;
                transition:all 1s;
        }

        .boxusers:hover{
                background-color: #4fbe96;
                padding: 20px;
                box-shadow: 1px 1px 4px #666;
                transition:all 0.2s;
        }

        .boxusers:hover i{
                transition:all 0.2s;
        }

        .boxbusiness{
                background-color: #4279a9;;
                padding: 20px;
                box-shadow: 1px 1px 10px #666;
        }

        .boxbusiness:hover{
                background-color: #4295a9;
                padding: 20px;
                box-shadow: 1px 1px 4px #666;
                transition:all 0.2s;
        }

        .boxbusiness:hover i{
                transition:all 0.2s;
        }

        .boxagency{
                background-color: #4fbe53;
                padding: 20px;
                box-shadow: 1px 1px 10px #666;
        }

        .boxagency:hover{
                background-color: #7ebe4f;
                padding: 20px;
                box-shadow: 1px 1px 4px #666;
                transition:all 0.2s;
        }

        .boxagency:hover i{
                transition:all 0.2s;
        }

        .col-lg-6{
            margin-top:50px;
        }

        .col-lg-4{
            margin-top:20px;
        }

        .tooltitle small{
            font-family: "Open Sans"!important;
            text-align: center;
            background: transparent;
            -webkit-background-clip: text;
            color: white;
        }

        .tooltitle{
            font-family: "Open Sans"!important;
            text-align: center;
            margin-top: 00px;
            font-weight: bold;
            font-size: 25px;
            display: block;
            background: transparent;
            -webkit-background-clip: text;
            height: 25px;
            padding-top: 0;
            top: 5px!important;
            position: relative;
            color: white;
            margin-bottom: 0px;
            text-align: left;
            margin-left: 200px;
		}

        .scollable_dashboard_task{
            max-height: 400px;
            overflow-y:  scroll;
        }
        .scollable_dashboard_news{
            max-height: 350px;
            overflow-y:  scroll;
        }
        .boxbusiness a,.boxusers a{
            color: white;
        }
        .alert.alert-warning{
            background-color: transparent!important;
            color: black!important;
        }

        table.dataTable th{
            max-width: 125px;
        }
        table.dataTable {
            border-collapse: collapse!important;
            width: 100%!important;
        }

        #news_table thead{
          display: none;
        }

        #news_table{
          background: transparent;
        }
        #news_table td{
          border: 0px;
          background: transparent;
        }


        .news_todo_date{
            background:rgba(255, 235, 59, 0.33);
            border-color: rgba(255, 255, 255, 0.6);
        }
        secret{
            display: none;
        }
        .news_todo_date{
            background: rgba(241, 222, 54, 0.58);

        }
        .news_section .todo_section{
            border-color: rgba(241, 222, 54, 0.58);
        }
</style>

</head>
<body class="theme_a"">
<!-- top bar -->
<header class="navbar navbar-fixed-top" role="banner" style="background-color: #f27b06">

        <div class="container-fluid" id="container-fluid">
            <div class="navbar-header">
                <a href="{{ route('dashboard_admin') }}" class="navbar-brand">
                    <img src="{{ asset('/img/logho-suend.png') }}">
                </a>
            </div>
            <div>
			  <h4 class="tooltitle">SUEND TOOLS <small>versione 1.0</small></h4>
            </div>
            <div style="position: absolute;top: 0px;right: 10px;">

            <ul class="nav navbar-nav navbar-right">
                <li class="user_menu">
                    <a href="/admin/settings" style="" target="_blank">
                    Manuale di utilizzo
                    </a>
                </li>
                <li class="user_menu">
                    <a href="http://mezzoclick.suend.it/Account/Login.Aspx?rEturnUrL=%2fdefault.aspx" style="" target="_blank">
                    Comunicazioni
                    </a>
                </li>
                <li>
                    <a href="{{ route('logout') }}" onclick="event.preventDefault();document.getElementById('logout-form').submit();">Logout</a>
                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        {{ csrf_field() }}
                    </form>
                </li>
            </ul>
            </div>
        </div>
</header>

<!-- main content -->
<div id="main_wrapper">
    @yield('content')
    </div>

    <!-- side navigation -->
    @if(Auth::user()->admin == 1)
        <nav id="side_nav">
            <ul>

                <li>
                    <a href="{{ route('dashboard_admin') }}"><span class="ion-speedometer"></span> <span class="nav_title">Dashboard</span></a>
                </li>
                <li>
                    <a href="{{ route('tasks_admin') }}">
                        <span class="label label-danger"><?php if(isset($myPendingTask )) echo $myPendingTask ; ?></span>
                        <span class="ion-clipboard enlightIcon"></span>
                        <span class="nav_title">Tasks</span>
                    </a>

                </li>
                <li>
                    <a href="{{ route('news_admin') }}"><span class="ion-ios7-chatbubble enlightIcon"></span><span class="nav_title">News</span></a>
                </li>
                <li class="liDivisor"><hr></li>
                <li>
                    <li class="dropdown-submenu"> <a class="tasto-menu-crm" tabindex="-1" href="#"><a class="tasto-menu-crm" href="#"><span class="fa fa-users"></span><span class="nav_title">CRM</span></a></a>
                                <ul class="dropdown-menu">
                                      <li class="dropdown-submenu"> <a href="#">Privati</a>
                                          <ul class="dropdown-menu">
											<li><a href="{{ route('search_privato') }}">Cerca privato</a></li>
                                            <li><a href="{{ route('clienti_admin') }}">Elenca Privati</a></li>
                                            <li><a href="/admin/cliente">Aggiungi nuovo privato</a></li>
                                            <li><a href="/admin/preventivi?view=pv">Preventivi</a></li>
                                            <li><a href="/admin/clienti/categories?view=pv">Categorie</a></li>
                                          </ul>
                                      </li>
                                      <li class="dropdown-submenu"> <a href="#">Aziende</a>
                                          <ul class="dropdown-menu">
	                                        <li><a href="{{ route('search_privato_ext') }}">Cerca azienda</a></li>
                                            <li><a href="{{ route('clienti_admin_ext') }}">Elenca aziende</a></li>
                                            <li><a href="/user/business/0">Nuova azienda</a></li>
                                          </ul>
                                      </li>
                                      <li class="dropdown-submenu"> <a href="#">ADV Italiane</a>
                                          <ul class="dropdown-menu">
	                                        <li><a href="{{ route('search_adv') }}">Cerca adv italiana</a></li>
                                            <li><a href="{{ route('adv_admin') }}">Elenca adv italiana</a></li>
                                            <li><a href="/user/new">Nuova adv italiana</a></li>
                                          </ul>
                                      </li>
                                      <li class="dropdown-submenu"> <a href="#">ADV Estere</a>
                                          <ul class="dropdown-menu">
                                            <li><a href="{{ route('search_adv_ext') }}">Cerca adv estera</a></li>
                                            <li><a href="{{ route('adv_admin_ext') }}">Elenca adv estera</a></li>
                                            <li><a href="/user/new?adv=ext">Nuova adv estera</a></li>
                                          </ul>
                                      </li>

                                      <li class="dropdown-submenu"> <a href="#">Preventivi</a>
                                          <ul class="dropdown-menu">
	                                        <li><a href="/admin/preventiviSearch">Cerca preventivo</a></li>
                                            <li><a href="/admin/preventivi?view=all">Elenca preventivi</a></li>
                                            <li><a href="{{ url('/admin/preventivo') }}">Nuovo preventivo</a></li>
                                          </ul>
                                      </li>
                                </ul>
                    </li>
                </li>
                <li>
                    <li class="dropdown-submenu"> 
                        <a class="tasto-menu-gift" href="#">
                            <a class="tasto-menu-submenu" href="#">
                             <span class="fa fa-ticket"></span><span class="nav_title">Card & Coupon</span>
                            </a>
                        </a>
                                
                            <ul class="dropdown-menu">

                                  <li class="dropdown-submenu"> <a href="#">Gift Card</a>
                                      <ul class="dropdown-menu">
                                        <li><a href="{{ route('cards') }}">Elenca gift card</a></li>
                                        <li><a href="{{ route('cards new') }}">Nuova gift card</a></li>
                                      </ul>
                                  </li>

                                  <li class="dropdown-submenu"> <a href="#">Coupon</a>
                                      <ul class="dropdown-menu">
                                           <li><a href="{{ route('coupon') }}">Elenca coupon</a></li>
                                           <li><a href="{{ route('coupon new') }}">Nuovo coupon</a></li>
                                      </ul>
                                  </li>
                            </ul>
                    </li>
                </li>
                <li>
                    <a href="{{ route('new_user') }}"><span class="ion-android-add-contact"></span> <span class="nav_title marg01">Utenti</span></a>
                </li>
                <li>
                    <a href="{{ route('reparti') }}"><span class="fa fa-sitemap"></span> <span class="nav_title marg01">Reparti Utenti</span></a>
                </li>
                <li>
                    <a href="http://www.hcsmail.it/login"><span class="ion-email"></span> <span class="nav_title">Mails</span></a>
                </li>
                <li>
                    <a href="{{ route('uploads') }}"><span class="fa fa-download"></span> <span class="nav_title">Uploads</span></a>
                </li>

            </ul>
        </nav>
    @else
        <nav id="side_nav">
            <ul>
                <li>
                    <a href="{{ route('dashboard_user') }}"><span class="ion-speedometer"></span> <span class="nav_title">Dashboard</span></a>
                </li>
                <li>
                    <a href="{{ route('tasks_user') }}">
                        <span class="label label-danger">{{ $task_open }}</span>
                        <span class="ion-clipboard enlightIcon"></span>
                        <span class="nav_title">Tasks</span>
                    </a>

                </li>
                <li>
                    <a href="{{ route('news_user') }}"><span class="ion-ios7-chatbubble enlightIcon"></span> <span class="nav_title ">News</span></a>
                </li>
                <li class="liDivisor"><hr></li>
                <li>
                    <li class="dropdown-submenu"> <a class="tasto-menu-crm"tabindex="-1" href="#"><a class="tasto-menu-crm" href="#"><span class="fa fa-users"></span><span class="nav_title">CRM</span></a></a>
                                <ul class="dropdown-menu">
                                      <li class="dropdown-submenu"> <a href="#">Privati</a>
                                          <ul class="dropdown-menu">
                                            <li><a href="{{ route('search_privato') }}">Cerca privato</a></li>
                                            <li><a href="{{ route('clienti_admin') }}">Elenca Privati</a></li>
                                            <li><a href="/admin/cliente">Aggiungi nuovo privato</a></li>
                                            <li><a href="/admin/preventivi?view=pv">Preventivi</a></li>
                                          </ul>
                                      </li>
                                      <li class="dropdown-submenu"> <a href="#">Aziende</a>
                                          <ul class="dropdown-menu">
                                            <li><a href="{{ route('search_privato_ext') }}">Cerca azienda</a></li>
                                            <li><a href="{{ route('clienti_admin_ext') }}">Elenca aziende</a></li>
                                            <li><a href="/user/business/0">Nuova azienda</a></li>
                                          </ul>
                                      </li>
                                      <li class="dropdown-submenu"> <a href="#">ADV Italiane</a>
                                          <ul class="dropdown-menu">
                                            <li><a href="{{ route('search_adv') }}">Cerca adv italiana</a></li>
                                            <li><a href="{{ route('adv_admin') }}">Elenca adv italiana</a></li>
                                            <li><a href="/user/new">Nuova italiana</a></li>
                                          </ul>
                                      </li>
                                      <li class="dropdown-submenu"> <a href="#">ADV Estere</a>
                                          <ul class="dropdown-menu">
                                            <li><a href="{{ route('search_adv_ext') }}">Cerca adv estera</a></li>
                                            <li><a href="{{ route('adv_admin_ext') }}">Elenca adv estera</a></li>
                                            <li><a href="/user/new?adv=ext">Nuovo adv estera</a></li>
                                          </ul>
                                      </li>

                                      <li class="dropdown-submenu"> <a href="#">Preventivi</a>
                                          <ul class="dropdown-menu">
                                            <li><a href="/admin/preventiviSearch">Cerca preventivo</a></li>
                                            <li><a href="/admin/preventivi?view=all">Elenca preventivi</a></li>
                                            <li><a href="{{ url("/admin/preventivo") }}">Nuovo preventivo</a></li>
                                          </ul>
                                      </li>
                                </ul>
                    </li>
                </li>




                <li>
                    <a href="http://www.hcsmail.it/login"><span class="ion-email"></span> <span class="nav_title">Mails</span></a>
                </li>


                <li>
                    <a href="{{ route('uploads') }}"><span class="fa fa-download"></span> <span class="nav_title">Uploads</span></a>
                </li>
            </ul>
        </nav>
    @endif


    <!-- right slidebar -->
    <div id="slidebar">
        <div id="slidebar_content">
            <div class="input-group">
                <input type="text" class="form-control input-sm" placeholder="Search...">
                <span class="input-group-btn">
                        <button class="btn btn-default btn-sm" type="button"><i class="fa fa-search"></i></button>
                    </span>
            </div>
            <hr>

            <div class="sepH_a">
                <div class="progress">
                    <div style="width: 60%;" role="progressbar" class="progress-bar">
                        60%
                    </div>
                </div>
                <span class="help-block">CPU Usage</span>
            </div>
            <div class="sepH_a">
                <div class="progress">
                    <div style="width: 28%;" class="progress-bar progress-bar-success">
                        28%
                    </div>
                </div>
                <span class="help-block">Disk Usage</span>
            </div>
            <div class="progress">
                <div style="width: 82%;" class="progress-bar progress-bar-danger">
                    0.2GB/20GB
                </div>
            </div>
            <span class="help-block">Monthly Transfer</span>
            <hr>



            <form>
                <div class="form-group">
                    <input type="text" class="input-sm form-control" placeholder="Tilte...">
                </div>
                <div class="form-group">
                    <textarea cols="30" rows="3" class="form-control input-sm" placeholder="Message..."></textarea>
                </div>
                <button type="button" class="btn btn-default btn-sm">Send message</button>
            </form>

            <hr>
            <div class="sepH_a">
                <span class="label label-info">Reminder</span>
            </div>
        </div>
    </div>




    <!-- easing -->
    <script src="{{ asset('/js/jquery.easing.1.3.min.js') }}"></script>
    <!-- bootstrap js plugins -->
    <script src="{{ asset('/bootstrap/js/bootstrap.min.js') }}"></script>
    <!-- top dropdown navigation -->
    <script src="{{ asset('/js/tinynav.js') }}"></script>
    <!-- perfect scrollbar -->
    <script src="{{ asset('/lib/perfect-scrollbar/min/perfect-scrollbar-0.4.8.with-mousewheel.min.js') }}"></script>

    <!-- common functions -->
    <script src="{{ asset('/js/tisa_common.js') }}"></script>

    <!-- style switcher -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/typeahead.js/0.11.1/typeahead.bundle.min.js"></script>
    <script src="{{ asset('/lib/jquery.confirm/jquery-confirm.min.js') }}"></script>
    <script type="text/javascript">

        var p=0;

        function logout_force_open(el){
            if(el.className.includes("open"))
                return;
            document.getElementById("li-logout").classList.add("open");
        }

        $(document).ready(function(){
            $("form [type='checkbox']").click(function(){
                $(this).trigger("change");
            })
        })

        $(document).ready(function(){
            $('form input, form select, form textarea').each(function(){
                p+=1;
                $(this).attr("tabindex",p);
            })
        })

        $(document).ready(function(){
            $("table:eq(0) th:last").removeClass("sorting");
            $(".fkacc .accordion").click(function(){
                $(this).parent(".fkacc").find(".panel").toggle(600);
                if($(this).find(".fa").hasClass("fa-plus"))
                    $(this).find(".fa").removeClass("fa-plus").addClass("fa-minus");
                else
                    $(this).find(".fa").removeClass("fa-minus").addClass("fa-plus");
            })
        })

        $(document).ready(function(){
            $('[data-toggle="tooltip"]').tooltip();
        });
    </script>
    @yield('footer-plugin')

    </body>
</html>
