<page class="page_pdf" backtop="50mm" backbottom="50mm" backleft="10mm" backright="10mm">
<style>
    .ft_tab tr,.ft_tab td{
      max-height: 30px;
    }
    table,
    th,
    td {
        #border: 1px solid black;
        border-collapse: collapse;
    }
    th,
    td {
        padding: 5px;
        padding-left: 30px;
        padding-right: 30px;
        text-align: left;
    }
    table{
      margin-left:50px;
    }
    page_footer{
      background-image: url('https://crm.suend.it/crm/resources/assets/img/sfondo.jpg');
    }
    .pdf_body{
      width: 700px;
      #text-align: left;
    }
    .label{
      font-family: 'Arial', sans-serif;
      font-weight: bold;
      font-size: 20px;
      text-align: center;
      color: black;
    }
    .label-big{
      font-family: 'Arial', sans-serif;
      font-weight: bold;
      font-size: 25px;
      text-align: center;
      color: black;
    }
    .input{
      text-transform: uppercase;
      font-family: helvetica;
      text-align: center;
      font-size: 14px;
      font-weight: normal;
      padding: 5px;
      margin-top: 8px;
      min-height: 20px;
      min-width:  50px;
    }
    #ft_tab p{
      font-weight: bold;
      text-align: left;
    }
    #ft_tab td{
        padding-left: 0px;
        padding-right: 0px;
    }


</style>
    <page_header>
        <span style="display:none">[[page_cu]]/[[page_nb]]</span>
        <table style="height:150px; margin-left:5px;">
            <tr>
                <td style="border-right: solid 20px #84C9C1;text-align: center;border-right: solid 20px #84C9C1;font-weight: bold;font-family: 'Arial', sans-serif;font-size: 22px;line-height: 21px;">
                  <img src="https://crm.suend.it/img/logho-suend.png" style="margin-left: 40px;" >
                  Suend Viaggi
                </td>
                <td>
                <p style="font-weight: bold;font-family: times; font-size: 22px;">
                  Destinazione richiesta
                <input disabled id="prew_dest" class="prew_dest" old="true">
                </p>
                </td>
            </tr>
            <tr>
                <td rowspan="1">
                </td>
                <td>
                </td>
            </tr>
        </table>


    </page_header>
    <page_footer class="page_footer" style="height: 200px; position: relative;z-index-1; background: white">
      <img src="https://crm.suend.it/crm/resources/assets/img/sfondo.jpg" style="height: 200px;width: 700px;z-index:-1;margin-left:30px"/>
        <table id="ft_tab" style="position:absolute; top:50px;" style="border:solid 1px black; text-align:left">
          <tr>
            <td style="width: 3px; vertical-align:bottom">
              <img src="https://crm.suend.it/crm/resources/assets/img/icona_telefono.png"/>
            </td>
            <td>
              <input class="form-control" type="text" id="suend_phone" old="true" placeholder="+39 342 8627114" value="+39 0294437016">
            </td>
          </tr>
          <tr>
            <td style="width: 3px; vertical-align:bottom">
              <img src="https://crm.suend.it/crm/resources/assets/img/icona_mail.png"/>
            </td>
            <td>
              <input class="form-control" type="text" id="suend_email" old="true" placeholder="booking@suend.it" value="booking@suend.it">
            </td>
          </tr>
          <tr >
            <td style="width: 3px; vertical-align:bottom">
                <img src="https://crm.suend.it/crm/resources/assets/img/icona_sito.png"/>
            </td>
            <td>
                <input class="form-control" disabled="" type="text" id="suend_site" old="true" placeholder="www.suend.it" value="www.suend.it">
            </td>
          </tr>
        </table>
    </page_footer>
    <span class="pdf_body" style="width: 650px;word-wrap: break-word;">
        <table style="width: 100%; height:150px">
          <tr>
            <td>
              <p class="label">Preventivo NR</p>
              <input disabled type="text" old="true" class="form-control" id="prew_nr" >
            </td>
            <td>
              <p class="label">Cliente richiedente</p>
              <input disabled type="text" old="true" class="form-control" id="prew_cst" >
            </td>
          </tr>
        </table>
        <table style="width: 100%; margin-top:20px; height:400px;">
          <tr>
            <td>
              <p class="label"><b>Date del viaggio -</b> dal</p>
              <input disabled type="text" old="true" class="form-control" id="prew_dal" >
            </td>
            <td>
              <p class="label">al</p>
              <input disabled type="text" old="true" class="form-control" id="prew_al" >
            </td>
          </tr>
          <tr>
            <td style="border-right: dashed 2px grey;">
              <p class="label">Tipologia di viaggio</label>
              <input disabled type="text" old="true" class="form-control" id="prew_kind">
            </td>
            <td>
              <p class="label">Referente</label>
              <input type="text" old="true" class="form-control" id="prew_ref">
            </td>
          </tr>
          <tr>
            <td style="border-right: dashed 2px grey;">
              <p class="label" >Sistemazione</label>
              <input disabled type="text" old="true" class="form-control" id="prew_sistemaz">
            </td>
            <td>
              <p class="label">Scadenza preventivo</label>
              <input disabled type="text" old="true" class="form-control"  id="prew_deadline">
            </td>
          </tr>
          <tr>
            <td style="border-right: dashed 2px grey;">
              <p class="label" >Trattamento</label>
                <input disabled type="text" old="true" class="form-control" id="prew_tratt">
            </td>
            <td>
              <p class="label-big">Prezzo Totale</label>
              <input disabled type="text" old="true" class="form-control" id="prew_price">
            </td>
          </tr>
        </table>
  </span>


</page>
