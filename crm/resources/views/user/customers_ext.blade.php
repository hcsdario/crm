@extends('layouts.master')
@section('header-page-personalization')
    <!-- page specific stylesheets -->
<?php 
ini_set('get_max_size' , '20M');
?>
    <link href="{{ asset('/css/style.css') }}" rel="stylesheet" media="screen">

    <!-- google webfonts -->
    <link href='https://fonts.googleapis.com/css?family=Source+Sans+Pro:400&amp;subset=latin-ext,latin' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="{{ asset('/lib/DataTables/media/css/jquery.dataTables.min.css') }}">
    
    <style>
        #customers_table thead,
        #customers_table th {text-align: center;}
        #customers_table tr {text-align: center;}

        #customers_adv_table thead,
        #customers_adv_table th {text-align: center;}
        #customers_adv_table tr {text-align: center;}
        .hideBecauseDatatablesSucks{
          /*  display: none;*/
        }
    </style>
@endsection
@section('content')

    <div class="page_content">
        <div class="container-fluid">
            <div class="row">

                        <div class="panel panel-default">
                            <button class="btn btn-lg btn-success btn-block" onclick="window.location='{{ url("/user/business") }}/0'">Aggiungi una nuova Azienda</button>
                            <div class="panel-body">
                                <fieldset>
                                    <legend><span>Elenco Aziende</span></legend>
                                </fieldset>
                                <div class="row">
                                    <div class="col-lg-12">
                                            <table class="table table-bordered aziende" id="customers_table_ext"  data-tablename="business">
                                                <thead>
                                                    <th class="hideBecauseDatatablesSucks">Id</th>
                                                    <th>Codice</th>
                                                    <th>Nome</th>
                                                    <th>Cognome</th>
                                                    <th>Fiscale iva</th>
                                                    <th>Ragione</th>
                                                    <th>Data nascita</th>
                                                    <th>Sesso</th>
                                                    <th>Indirizzo</th>
                                                    <th class="hideBecauseDatatablesSucks">Cap</th>
                                                    <th class="hideBecauseDatatablesSucks">Citta</th>
                                                    <th class="hideBecauseDatatablesSucks">Provincia</th>
                                                    <th class="hideBecauseDatatablesSucks">Regione</th>
                                                    <th>Stato</th>
                                                    <th>Fisso</th>
                                                    <th class="hideBecauseDatatablesSucks">Cell</th>
                                                    <th>Mail</th>
                                                    <th class="hideBecauseDatatablesSucks">Fax</th>
                                                    <th class="hideBecauseDatatablesSucks">www</th>
                                                    <th class="hideBecauseDatatablesSucks">Settore</th>
                                                    <th class="hideBecauseDatatablesSucks">Addetti</th>
                                                    <th class="hideBecauseDatatablesSucks">Range info</th>
                                                    <th class="hideBecauseDatatablesSucks">Fatturato</th>
                                                    <th class="hideBecauseDatatablesSucks">Inizio attività</th>
                                                    <th class="hideBecauseDatatablesSucks">Referente</th>
                                                    <th class="hideBecauseDatatablesSucks">Note</th>
                                                    <th class="hideBecauseDatatablesSucks">Stato relazione</th>
                                                    <th class="hideBecauseDatatablesSucks">Ultimo aggiornamento</th>
                                                    <th class="hideBecauseDatatablesSucks">Luogo di nascita</th>
                                                    <th class="hideBecauseDatatablesSucks">Professione</th>
                                                    <th class="hideBecauseDatatablesSucks">Hobby1</th>
                                                    <th class="hideBecauseDatatablesSucks">Hobby2</th>
                                                    <th class="hideBecauseDatatablesSucks">Scadenza passaporto</th>
                                                    <th class="hideBecauseDatatablesSucks">Passaporto</th>
                                                    <th class="hideBecauseDatatablesSucks">Carta d'identita</th>
                                                    <th class="hideBecauseDatatablesSucks">Scadenza carta identita</th>
                                                    <th class="hideBecauseDatatablesSucks">Frequent flyer</th>
                                                    <th class="hideBecauseDatatablesSucks">Newsletter</th>
                                                    <th class="hideBecauseDatatablesSucks">Cliente diretto</th>
                                                    <th class="hideBecauseDatatablesSucks">Adulti</th>
                                                    <th class="hideBecauseDatatablesSucks">Bambini</th>
                                                    <th class="hideBecauseDatatablesSucks">Neonati</th>
                                                    <th class="hideBecauseDatatablesSucks">Figli</th>
                                                    <th class="hideBecauseDatatablesSucks">Anziani</th>
                                                    <th class="hideBecauseDatatablesSucks">Animali</th>
                                                    <th class="hideBecauseDatatablesSucks">Iscritto club</th>
                                                    <th class="hideBecauseDatatablesSucks">Codice club</th>
                                                    <th class="hideBecauseDatatablesSucks">Note</th>     
                                                    <th>Azioni</th>                                           
                                                </thead>
                                                <tbody>

                                         @foreach($aziende as $azienda)
                                         <tr>
                                            <td class="hideBecauseDatatablesSucks">{{ $azienda->id }}</td>
                                            <td>{{ $azienda->codice }}</td>
                                            <td>{{ $azienda->nome }}</td>
                                            <td>{{ $azienda->cognome }}</td>
                                            <td>{{ $azienda->fiscale_iva }}</td>
                                            <td>{{ $azienda->ragione }}</td>
                                            <td class="hideBecauseDatatablesSucks">{{ $azienda->data_nascita }}</td>
                                            <td class="hideBecauseDatatablesSucks">{{ $azienda->sesso }}</td>
                                            <td>{{ $azienda->indirizzo }}</td>
                                            <td>{{ $azienda->cap }}</td>
                                            <td>{{ $azienda->citta }}</td>
                                            <td class="hideBecauseDatatablesSucks">{{ $azienda->provincia }}</td>
                                            <td class="hideBecauseDatatablesSucks">{{ $azienda->regione }}</td>
                                            <td class="hideBecauseDatatablesSucks">{{ $azienda->stato }}</td>
                                            <td class="hideBecauseDatatablesSucks">{{ $azienda->fisso }}</td>
                                            <td>{{ $azienda->cell }}</td>
                                            <td>{{ $azienda->mail }}</td>
                                            <td class="hideBecauseDatatablesSucks">{{ $azienda->fax }}</td>
                                            <td class="hideBecauseDatatablesSucks">{{ $azienda->www }}</td>
                                            <td class="hideBecauseDatatablesSucks">{{ $azienda->settore }}</td>
                                            <td class="hideBecauseDatatablesSucks">{{ $azienda->addetti }}</td>
                                            <td class="hideBecauseDatatablesSucks">{{ $azienda->range_info }}</td>
                                            <td class="hideBecauseDatatablesSucks">{{ $azienda->fatturato }}</td>
                                            <td class="hideBecauseDatatablesSucks">{{ $azienda->inizio_attività }}</td>
                                            <td class="hideBecauseDatatablesSucks">{{ $azienda->referente }}</td>
                                            <td class="hideBecauseDatatablesSucks">{{ $azienda->note }}</td>
                                            <td class="hideBecauseDatatablesSucks">{{ $azienda->status }}</td>
                                            <td class="hideBecauseDatatablesSucks">{{ $azienda->updated_at }}</td>
                                            <td class="hideBecauseDatatablesSucks">{{ $azienda->luogonascita }}</td>
                                            <td class="hideBecauseDatatablesSucks">{{ $azienda->professione }}</td>
                                            <td class="hideBecauseDatatablesSucks">{{ $azienda->hobby1 }}</td>
                                            <td class="hideBecauseDatatablesSucks">{{ $azienda->hobby2 }}</td>
                                            <td class="hideBecauseDatatablesSucks">{{ $azienda->scadenza_passaporto }}</td>
                                            <td class="hideBecauseDatatablesSucks">{{ $azienda->passaporto }}</td>
                                            <td class="hideBecauseDatatablesSucks">{{ $azienda->carteidentita }}</td>
                                            <td class="hideBecauseDatatablesSucks">{{ $azienda->scadenza_cartaidentita }}</td>
                                            <td class="hideBecauseDatatablesSucks">{{ $azienda->frequent_flyer }}</td>
                                            <td class="hideBecauseDatatablesSucks">{{ $azienda->newsletter }}</td>
                                            <td class="hideBecauseDatatablesSucks">{{ $azienda->cliente_diretto }}</td>
                                            <td class="hideBecauseDatatablesSucks">{{ $azienda->adulti }}</td>
                                            <td class="hideBecauseDatatablesSucks">{{ $azienda->bambini }}</td>
                                            <td class="hideBecauseDatatablesSucks">{{ $azienda->neonati }}</td>
                                            <td class="hideBecauseDatatablesSucks">{{ $azienda->figli }}</td>
                                            <td class="hideBecauseDatatablesSucks">{{ $azienda->anziani }}</td>
                                            <td class="hideBecauseDatatablesSucks">{{ $azienda->animali }}</td>
                                            <td class="hideBecauseDatatablesSucks">{{ $azienda->iscritto_club }}</td>
                                            <td class="hideBecauseDatatablesSucks">{{ $azienda->codice_club }}</td>
                                            <td class="hideBecauseDatatablesSucks">{{ $azienda->note }}</td>
                                            <td>
                                                <a href="../user/business/{{ $azienda->id }}" class="btn btn-xs btn-secondary"><i class="glyphicon glyphicon-edit"></i>&nbsp;Modifica</a>
                                                <a href="#" class="btn btn-xs btn-danger news-zama" style="margin-left: 10px;" onclick="deleteRecord(this)"><i class="glyphicon glyphicon-minus"></i>&nbsp;Elimina</a>
                                            </td>
                                         </tr>
                                        @endforeach       
                                                </tbody>
                                            </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                </div>
            </div>
        </div>
    </div>

  

@endsection
@section('footer-plugin')
    <!-- datatables -->
    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.18/pdfmake.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.2.4/js/dataTables.buttons.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.2.4/js/buttons.bootstrap.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.2.4/js/buttons.print.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.2.4/js/buttons.html5.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <script src="//cdn.datatables.net/buttons/1.4.2/js/buttons.colVis.min.js"></script>
    <script src="{{ asset('/lib/DataTables/extensions/Scroller/js/dataTables.scroller.min.js') }}"></script>
    <script src="https://crm.suend.it/js/tinynav.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.perfect-scrollbar/0.8.1/js/perfect-scrollbar.jquery.min.js"></script>
    <script src="{{ asset('/js/apps/tisa_datatables.js') }}"></script>
    <script src="{{ asset('/lib/moment-js/moment.min.js') }}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.2.0/jquery-confirm.min.js"></script>
    <script src="{{ asset('/lib/jquery.confirm/jquery-confirm.min.js') }}"></script>

    <script>

        $('#customers_table_ext').DataTable({
            oLanguage: {sSearch: "Filtra: "},
            dom: 'Bfrtip',
            buttons: [
                { extend: 'colvis', text: 'Mostra/Nascondi colonne' }, 
                { extend: 'print', text: 'Stampa' },
                "excelHtml5",
                "pdfHtml5"
            ],
            "columnDefs": [
                {
                    "targets": [ 17 ,
                                18 ,
                                19 ,
                                20 ,
                                21 ,
                                22 ,
                                23 ,
                                24 ,
                                25 ,
                                26 ,
                                28 ,
                                29 ,
                                30 ,
                                31 ,
                                32 ,
                                33 ,
                                34 ,
                                35 ,
                                36 ,
                                37 ,
                                38 ,
                                39 ,
                                40 ,
                                41 ,
                                42 ,
                                43 ,
                                44 ,
                                45 ,
                                46 ,
                                47 ,
                                 ],
                    "visible": false,
                }
            ]
        });
        $(document).ready(function () {
//            $(".hideBecauseDatatablesSucks").removeClass("")
        })
    window['table']="dataInTable";
    </script>
@endsection
