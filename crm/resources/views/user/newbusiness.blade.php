@extends('layouts.master')
@section('header-page-personalization')




<?php
    $lastId=DB::select("select max(id) +1 as id from business");
    $lastId=$lastId[0]->id;
?>
    <style>
        .twitter-typeahead, .tt-hint, .tt-input, .tt-menu { width: 100%;  }
        .tt-menu {
            background: #ffffff;
        }
        [readonly]{
          background-color: white;
          opacity: 1;
        }
    </style>
@endsection
@section('content')
<?php

    $load=1;
    if(!isset($business)||isset($business)&&empty($business[0]))
        $load=0;
    else
        $business[0]->note= str_replace("\"", "", $business[0]->note) ;

function myjsonencode($item){
    echo "[{";
    foreach ($item as $key => $value) {
        $item->{$key}=str_replace("'", "", $item->{$key});
        $item->{$key}=str_replace("\\\\", "", $item->{$key});
        $item->{$key}=str_replace("\n", "", $item->{$key});
        $item->{$key}=str_replace("\r", "", $item->{$key});
        echo "\"".$key."\":";
        echo '"'.$item->{$key}.'",';
    }
    echo "}]";
}

?>

<?php $name=Auth::user()->name." ".Auth::user()->surname;?>

    <div class="page_content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <form id="login_form" role="form" method="POST" action="{{ route('business.store') }}" class="customForm">
                    {{ csrf_field() }}

                        <div class="panel panel-default">
                            <div class="panel-body">
                                <div class="heading_a"><legend><span class="big-title">Profilo Azienda</span></legend></div>
                                <div class="tabbable tabs-left">
                                    <ul class="nav nav-tabs" id="tabs_c">
                                        <li class="active"><a data-toggle="tab" href="#info">Info/Azienda</a></li>
                                        <li><a data-toggle="tab" href="#relazione">Stato relazione</a></li>

                                        <?php  if(Auth::user()->admin){ ?>
                                            <li class="tasklink" style="display: none;"><a id="qui" target="_blank" href="/admin/tasks?action=linkto&ent=business&n=#">Tasks</a></li>
                                        <?php  }else{ ?>
                                            <li class="tasklink" style="display: none;"><a id="qui" target="_blank" href="/user/tasks?action=linkto&ent=business&n=#">Tasks</a></li>
                                        <?php  } ?>
                                        <li><a data-toggle="tab" href="#coupon">Gift Card / Coupon </a></li>
                                        <li class="editScheda" style="display: none;">
                                            <a href="#"><i class="glyphicon glyphicon-edit"></i>&nbsp;Modifica</a>
                                        </li>
                                    </ul>
                                    @if(session()->has('message'))
                                        <div class="alert alert-success">
                                            {{ session()->get('message') }}
                                        </div>
                                    @endif
                                    @if(session()->has('error'))
                                        <div class="alert alert-danger">
                                            {{ session()->get('error') }}
                                        </div>
                                    @endif
                                    <div class="tab-content" id="tabs_content_c">
                                        <div id="info" class="tab-pane fade in active">
                                            <div class="row">
                                                <div class="col-lg-6">

                                                    <input type="hidden" id="id" name="id">
                                                    <div class="form-group" style="display: none;">
                                                        <label for="reg_input">Codice</label>
                                                        <input type="text" id="codice" name="codice" class="form-control"
                                                               tabindex="1">
                                                    </div>

                                                    <div class="form-group">
                                                        <label for="reg_input">Azienda</label>
                                                        <input type="text" id="azienda" name="azienda"
                                                               class="form-control" tabindex="15">
                                                    </div>

                                                    <div class="form-group">
                                                        <label for="reg_input">Ragione sociale</label>
                                                        <input type="text" id="ragione" name="ragione"
                                                               class="form-control" tabindex="15">
                                                    </div>

                                                    <div class="form-group">
                                                        <label for="reg_input">Indirizzo</label>
                                                        <input type="text" id="indirizzo" name="indirizzo"
                                                               class="form-control" tabindex="5">
                                                    </div>


                                                    <div class="form-group">
                                                        <label for="reg_input">Citta'</label>
                                                        <input type="text" id="citta" name="citta" class="form-control" tabindex="6" placeholder="Inserisci citta">
                                                    </div>

                                                    <div class="form-group">
                                                        <label for="reg_input">Cap</label>
                                                        <input type="text" id="cap" name="cap" class="form-control" tabindex="7" >

                                                    </div>

                                                    <div class="form-group">
                                                        <label for="reg_input">Provincia</label>
                                                        <input type="text" id="provincia" name="provincia" class="form-control" tabindex="8">
                                                    </div>

                                                    <div class="form-group">
                                                        <label for="reg_input">Fisso</label>
                                                        <input type="text" id="fisso" name="fisso" class="form-control" tabindex="11">
                                                    </div>

                                                    <div class="form-group">
                                                        <label for="reg_input">Fax</label>
                                                        <input type="text" id="fax" name="fax" class="form-control"
                                                               tabindex="13">
                                                    </div>

                                                    <div class="form-group">
                                                        <label for="reg_input">www</label>
                                                        <input type="text" id="www" name="www" class="form-control"
                                                               tabindex="13">
                                                    </div>


                                                    <div class="form-group">
                                                        <label for="reg_input">Codice Fiscale/ Partita iva</label>
                                                        <input type="text" id="fiscale_iva" name="fiscale_iva" onfocusout="validateFs(this)"
                                                               class="form-control" tabindex="15">
                                                    </div>


                                                    <div class="form-group" style="display: none;">
                                                        <label for="reg_input">Nome</label>
                                                        <input type="text" id="nome" name="nome" class="form-control" tabindex="2">
                                                    </div>

                                                    <div class="form-group" style="display: none;">
                                                        <label for="reg_input">Cognome</label>
                                                        <input type="text" id="cognome" name="cognome" class="form-control"
                                                               tabindex="1">
                                                    </div>


                                                    <div class="form-group" style="display: none;">
                                                        <label for="reg_input">Data di Nascita</label>
                                                        <input type="date" id="data_nascita" name="data_nascita" placeholder="31/12/2018"
                                                               class="form-control ts_datepicker" tabindex="3">
                                                    </div>

                                                    <div class="form-group" style="display: none;">
                                                        <label for="reg_input">Sesso</label>
                                                        <select id="sesso" name="sesso" class="form-control"
                                                                tabindex="4">
                                                            <option value="MASCHIO">MASCHIO</option>
                                                            <option value="FEMMINA">FEMMINA</option>
                                                        </select>
                                                    </div>


                                                    <div class="form-group" style="display: none;">
                                                        <label for="reg_input">Regione</label>
                                                        <input type="text" id="regione" name="regione" class="form-control" tabindex="9">

                                                    </div>
                                                    <div class="form-group" style="display: none;">
                                                        <label for="reg_input">Stato</label>
                                                        <input type="text" id="stato" name="stato" class="form-control"
                                                               tabindex="10">
                                                    </div>

                                                    <div class="form-group">
                                                        <label for="reg_input">Settore</label>
                                                        <input type="text" id="settore" name="settore"
                                                               class="form-control" tabindex="16">
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="reg_input">N. Addetti </label>
                                                        <input type="text" id="addetti" name="addetti" tabindex="17"
                                                               class="form-control">
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="reg_input">Range Fatturato</label>
                                                        <input type="text" id="range_info" name="range_info" tabindex="18"
                                                               class="form-control">
                                                    </div>

                                                    <div class="form-group" style="display: none;">
                                                        <label for="reg_input">Fatturato</label>
                                                        <input type="text" id="fatturato" name="fatturato" tabindex="19"
                                                               class="form-control">
                                                    </div>

                                                    <div class="form-group">
                                                        <label for="reg_input">Inizio attività</label>
                                                        <input type="text" id="inizio_attività" name="inizio_attività" tabindex="19"
                                                               class="form-control">
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="reg_input">Referente</label>
                                                        <input type="text" id="referente" name="referente" tabindex="19"
                                                               class="form-control">
                                                    </div>



                                                    <div class="form-group">
                                                        <label for="reg_input">Cellulare</label>
                                                        <input type="text" id="cell" name="cell" class="form-control" tabindex="11">
                                                    </div>

                                                    <div class="form-group">
                                                        <label for="reg_input">Email</label>
                                                        <input type="text" id="mail" name="mail" class="form-control"
                                                               tabindex="13">
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="reg_input">Email Secondarie</label>
                                                        <textarea id="email2" name="email2" class="form-control"
                                                               tabindex="13"></textarea>
                                                    </div>



                                                    <div class="form-group">
                                                        <label for="reg_input">Note</label>

                                                        <textarea style="height: 150px;" id="note" name="note" tabindex="19" class="form-control">

                                                        </textarea>
                                                    </div>

                                                </div>
                                                <div class="col-lg-6">
                                                    <label for="reg_input">QUANDO SPENDE PER I VIAGGI ALL’ANNO?</label>
                                                    <input type="number" id="spesaviaggi" name="spesaviaggi" tabindex="20" class="form-control">
                                                </div>
                                                <div class="col-lg-6">
                                                    <label for="reg_input">QUANDO SPENDE PER LA PRENOTAZIONE DI ALBERGHI ALL’ANNO?</label>
                                                    <input type="number" id="spesaalberghi" name="spesaalberghi" tabindex="20" class="form-control">
                                                </div>
                                                <div class="col-lg-6">
                                                    <label for="reg_input">QUANDO SPENDE PER LA PRENOTAZIONE DI VOLI ALL’ANNO?</label>
                                                    <input type="number" id="spesavoli" name="spesavoli" tabindex="20" class="form-control">
                                                </div>
                                                <div class="col-lg-6">
                                                    <label for="reg_input">QUANDO SPENDE PER LA PRENOTAZIONE DI TRENI ALL’ANNO?</label>
                                                    <input type="number" id="spesatreni" name="spesatreni" tabindex="20" class="form-control">
                                                </div>
                                                <div class="col-lg-6">
                                                    <label for="reg_input">QUANDO SPENDE PER LA PRENOTAZIONE DI AUTO ALL’ANNO?</label>
                                                    <input type="number" id="spesaauto" name="spesaauto" tabindex="20" class="form-control">
                                                </div>
                                            </div>

                                        </div>
                                        <div id="relazione" class="tab-pane fade">
                                                <div id="storicoContatti">

                                                    <div class="form-group">
                                                        <label for="reg_input">Aggiorna relazione</label>
                                                        <select type="text" id="status" name="status" class="form-control">
                                                            <option selected disabled value="">Scegli uno step della relazione</option>
                                                            <option value="contattato - SMS">contattato - SMS</option>
                                                            <option value="contattato - Telefono">contattato - Telefono</option>
                                                            <option value="contattato - Recall">contattato - Recall</option>
                                                            <option value="contattato - Visita Diretta">contattato - Visita Diretta</option>
                                                            <option value="contattato - via Email">contattato - via Email</option>
                                                            <option value="da risentire">da risentire</option>
                                                            <option value="interessato">interessato</option>
                                                            <option value="non interessato">non interessato</option>
                                                            <option value="cliente suend">cliente suend</option>
                                                        </select>
                                                    </div>

                                                    <div class="form-group">
                                                        <a href="#" class="btn btn-success btn-md applyStatus" onclick="addStatus()">
                                                          <span class="glyphicon glyphicon-plus"></span> Applica
                                                        </a>
                                                    </div>
                                                    <div class="form-group">


                                                        <!-- Modal -->
                                                        <div id="notaStato" class="modal fade" role="dialog">
                                                          <div class="modal-dialog">

                                                            <!-- Modal content-->
                                                            <div class="modal-content">
                                                              <div class="modal-header">
                                                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                                <h4 class="modal-title">Desideri aggiungere una nota per questo contatto?</h4>
                                                              </div>
                                                              <div class="modal-body">
                                                                <p><input type="text" name="notaStato" placeholder="aggiungi qui la tua nota"></p>
                                                              </div>
                                                              <div class="modal-footer">
                                                                <button type="button" class="btn btn-default" data-dismiss="modal" onclick="addStatusStep2()">Applica e chiudi</button>
                                                              </div>
                                                            </div>

                                                          </div>
                                                        </div>

                                                        <label for="reg_input">Stato relazione</label>
                                                        <listaStati>
                                                            @foreach($customersRelations as $relations)
                                                            <p>l'utente <strong>
                                                              {{ $relations->name }} {{ $relations->surname }}
                                                            </strong> il
                                                            <strong> {{ $relations->timer }} </strong>
                                                            ha aggiornato lo stati in: <br> {{ $relations->status }}
                                                            @if($relations->nota)
                                                              <p>Con la seguente nota: </p>
                                                              <p> <strong> {{ $relations->nota }} </strong></p>
                                                            @endif
                                                          @if(!$customersRelations)
                                                            <p class="defaultMsg">Non sono stati inseriti dati su questo cliente</p>
                                                          @endif
                                                          @endforeach
                                                        </listaStati>
                                                    </div>
                                                </div>
                                        </div>

                                        <div id="coupon" class="tab-pane fade">

                                                    <div class="col-md-10">
                                                        <div class="form-group">
                                                            <label>Gift Card associate</label>
                                                            @if(empty($giftCard[0]))
                                                                <p><b>Non</b> sono presenti coupon assegnate a questo utente</p>
                                                            @endif
                                                            <ul>
                                                              @foreach($giftCard as $card)
                                                              <li>
                                                                <a href="{{route('card edit',['id' => $card->id])}}" target="blank"> Carta: {{$card->code}} </a><br>
                                                                  Creata il {{$card->created_at}} <br>
                                                                  Ultimo utilizzo: {{$card->data_utilizzo}} <br>
                                                                  Credito : {{$card->spesa}} / {{$card->value}} <br>
                                                              </li>
                                                              @endforeach
                                                            </ul>

                                                        </div>
                                                    </div>
                                                    <div class="col-md-10">
                                                        <div class="form-group">
                                                            <label>Coupon associati</label>
                                                            @if(empty($coupons[0]))
                                                                <p><b>Non</b> sono presenti coupon assegnate a questo utente</p>
                                                            @endif
                                                            <ul>
                                                              @foreach($coupons as $coupon)

                                                              <li>
                                                                  <a href="{{route('coupon edit',['id' => $coupon->id])}}" target="blank"> Coupon: {{$coupon->code}} </a><br>
                                                                  Creata il {{$coupon->created_at}} <br>
                                                                  Ultimo utilizzo: {{$coupon->data_utilizzo}} <br>
                                                                  Credito usato: {{$coupon->spesa}} / {{$coupon->value}} <br>
                                                              </li>
                                                              @endforeach
                                                            </ul>

                                                        </div>
                                                    </div>

                                        </div>

                                    </div>
                                </div>
                            </div>
                            <button id="saver" class="btn btn-lg btn-success btn-block confirmsave">Aggiungi nuova Azienda </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

@endsection
@section('footer-plugin')
    <script src="{{ asset('/lib/bootstrap-datepicker/js/bootstrap-datepicker.js') }}"></script>



    <script type="text/javascript">
        $(document).ready(function () {

            ResetForm();
            ResetFormInfo();

            function ResetForm() {
                $("#provincia").val('');
                $("#cap").val('');
                $("#regione").val('');
                $("#stato").val('');
            }


            function ResetFormInfo() {
                $("#info-provincia2").val('');
                $("#info-cap2").val('');
                $("#info-regione2").val('');
                $("#info-stato2").val('');
            }



            $("#citta").focus(function() {
                ResetForm();
            });

            $("#info-citta2").focus(function() {
                ResetFormInfo();
            });


            var path = "{{ route('autocomplete') }}";
            var engine = new Bloodhound({
                datumTokenizer: Bloodhound.tokenizers.whitespace('username'),
                queryTokenizer: Bloodhound.tokenizers.whitespace,
                remote:{
                    url: path + '?term=%QUERY',
                    wildcard: '%QUERY'
                }
            });

            engine.initialize();

            $("#citta").typeahead({
                hint: true,
                highlight: true,
                minLength: 2
            }, {
                source: engine.ttAdapter(),
                // This will be appended to "tt-dataset-" to form the class name of the suggestion menu.
                name: 'comune',
                // the key from the array we want to display (name,id,email,etc...)
                displayKey: 'comune',
                templates: {
                    empty: [
                        '<div class="empty-message">Nessun risultato</div>'
                    ].join('\n'),

                    suggestion: function (data) {
                        return '<div class="citta-search-result"><h4>'+ data.comune +'</h4></div>'
                    }
                },

            });
            $('#citta').bind('typeahead:selected', function(obj, datum, name) {
                $("#provincia").val(datum.provincia);
                $("#cap").val(datum.cap);
                $("#regione").val(datum.regione);
                $("#stato").val(datum.stato);
                $("#codice").val(datum.sigla+"-"+lastAvaibleId);
            });



            $("#info-citta2").typeahead({
                hint: true,
                highlight: true,
                minLength: 2
            }, {
                source: engine.ttAdapter(),
                // This will be appended to "tt-dataset-" to form the class name of the suggestion menu.
                name: 'comune',
                // the key from the array we want to display (name,id,email,etc...)
                displayKey: 'comune',
                templates: {
                    empty: [
                        '<div class="empty-message">Nessun risultato</div>'
                    ].join('\n'),

                    suggestion: function (data) {
                        return '<div class="citta2-search-result"><h4>'+ data.comune +'</h4></div>'
                    }
                },

            });

            $("#info-citta2").bind('typeahead:selected', function(obj, datum, name) {

                $("#info-provincia2").val(datum.provincia);
                $("#info-cap2").val(datum.cap);
                $("#info-regione2").val(datum.regione);
                $("#info-stato2").val(datum.stato);
            });
        });

       var lastAvaibleId=Number('<?php echo $lastId?>');
       var load='<?php echo $load?>';
       var business='<?php
       if($load)
            myjsonencode($business[0])
        ?>'.replace(",}]","}]").replace(/\t/g, '');

        if(load&&business!=""){
            business=JSON.parse(business);
            debugger;

            $(document).ready(function(){

                var keys=Object.keys(business[0]);

                for(var i =0;i<keys.length;i++){
                    if($("[name='"+keys[i]+"']").is("input"))
                        $("[name='"+keys[i]+"']").val(business[0][keys[i]]);
                    if($("[name='"+keys[i]+"']").is("textarea"))
                        $("[name='"+keys[i]+"']").text(business[0][keys[i]]);
                }

                if(keys.length){
                    $("#saver").text("Aggiorna Figura Business");
                    var edithref="/user/business/"+business[0].id;
                }

                $(".ts_datepicker").datepicker({format: "dd/mm/yyyy",});

                $('input').css('text-transform','uppercase');

                if(window.location.search.includes("view")){
                  $("input,select,textarea").attr("readonly","true");
                  $("input[type='checkbox']").attr("disabled","true");
                  $(".btn.btn-lg.btn-success.btn-block").hide();
                  $(".editScheda a").attr("href",edithref);
                  $(".editScheda").show();
                }
                           debugger;
                  $(".tasklink").show();
                  var href=$(".tasklink a").attr('href');
                  $(".tasklink a").attr('href',href.replace("n=#","n="+business[0].id));
            })
        }

String.prototype.replaceAll = function(search, replacement) {
    var target = this;
    return target.replace(new RegExp(search, 'g'), replacement);
};



        function addStatus(){

            if($("#status option:selected").val()=="")
                return;
            $("#notaStato input").val("");
            $("#notaStato").modal();
        }

        function addStatusStep2(){
                $(".defaultMsg").remove();
                var time=new Date();
                    time=time.getDate()+"/"+(time.getMonth()+1)+"/"+time.getFullYear();
                var status=$("#status option:selected").val();
                    if($('[name="notaStato"]').val()!="")
                        var nota="<p>Con la seguente nota: "+$('[name="notaStato"]').val()+"</p>";
                    else
                        var nota="";
                var row="<p>l'utente <strong>"+name+"</strong> il <strong>"+time+"</strong> ha aggiornato lo stati in: <br><strong>"+status+"</strong>"+nota+"</p>";
                $("listaStati").prepend(row);
                $("listaStati").append("<input type='hidden' id='statiStato[]' name='statiStato[]' value='"+status+"'>");
                $("listaStati").append("<input type='hidden' id='statiNota[]' name='statiNota[]' value='"+$('[name="notaStato"]').val()+"'>");
            }

            function validateFs(el){
                if(el.value=="")
                  return;
                if(el.value.length<16)
                    alert("Inserisci un codice fiscale valido");
            }
    </script>
@endsection
