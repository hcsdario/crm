@extends('layouts.master')
@section('header-page-personalization')
    <style>
        .twitter-typeahead, .tt-hint, .tt-input, .tt-menu { width: 100%;  }
        .tt-menu {
            background: #ffffff;
        }
        note{
        	color: white;
		    font-weight: 900;
        }
        .label{
            font-size: 12px;
            padding-left: 0px!important;
            text-align: left;
            display: block;            
        }
    </style>

            @endsection
            @section('content')
                <div class="page_content">

                    <div class="container-fluid">

             @if(session()->has('message'))
                <div class="alert alert-success">
                    {{ session()->get('message') }}
                </div>
            @endif
            @if(session()->has('error'))
                <div class="alert alert-danger">
                    {{ session()->get('error') }}
                </div>
            @endif

            <div class="row well">
                <div class="col-lg-12">
                <h1>Upload Allegati</h1>
                <?php 
                	$isAdmin=false;
               		$adminStyle=' style="display: none" ';
                	if(Auth::user()->admin == 1) 
                		$isAdmin=true;
                	if($isAdmin)
                		$adminStyle="";

                ?>
                <form  action="{{ route('uploadsStore') }}" method="post" class="form" novalidate="novalidate" files="true" enctype="multipart/form-data" <?php echo $adminStyle ?>>
                 {{ csrf_field() }}
                
					<div class="form-group">
					<label for="reg_input">Zona News - Scegli la zona in cui sarà visualizzata la News</label>
						<select id="zone" name="zone" class="form-control">
							<?php 
								$zone=DB::select("SELECT * FROM `newsZone`");
								for($i=0;$i<count($zone);$i++){?>
								<option value="{{$zone[$i]->zone}}" <?php if($i==0) echo "selected"; ?>
								>{{$zone[$i]->zone}}</option>
							<?php } ?>
						</select>
					</div>
					<div class="form-group">
					 <label for="reg_input">Scegli un allegato</label>					
					  <input type="file" name="files" id="files">
                   	 <input type="hidden" name="userid" value="{{Auth::user()->id}}">
                    </div>
                    <button class="btn btn-info btn-sm"><i class="fa fa-upload"></i> <note>&nbsp;Carica</note></button>
                </form>
                  
                   
                    
                </div>

            </div>

            <div class="row well">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                        <div class="panel panel-default">
                            <div class="panel-body">
                                <div class="row">
                                    <div class="col-lg-12">
                                    <h1>Allegati</h1>
                                            <table class="table table-bordered" id="customers_table" >
                                                <thead>
                                                <tr>
                                                    <th>Id</th>
                                                    <th>File</th>
                                                    <th>Caricato il</th>
                                                    <th>Nome Proprietario</th>
                                                    <th>Cognome Proprietario</th>
                                                    <th>Zona</th>
                                                    <th <?php echo $adminStyle ?> >Azione</th>
                                                </tr>
                                                </thead>
                                                <?php 
                                                if(Auth::user()->id!=1)
                                                {
                                                    $sql="SELECT uploads.id,uploads.files,uploads.created_at,uploads.dir, users.username, users.surname FROM `uploads` INNER JOIN users ON uploads.userid=users.id WHERE uploads.dir in(SELECT newsUserZone.userZone FROM newsUserZone WHERE newsUserZone.userid=".Auth::user()->id.") OR uploads.dir='Tutte'";
                                                    $row=DB::select($sql);
                                                }
                                                else
                                                $row=DB::select("SELECT uploads.id,uploads.files,uploads.created_at,uploads.dir, users.username, users.surname FROM `uploads` INNER JOIN users ON uploads.userid=users.id");
                                                for($i=0;$i<count($row);$i++){ ?>
                                                <tr>
                                                    <td>{{$row[$i]->id}}</td>
                                                    <td><a href="../crm/upload/{{$row[$i]->dir}}/{{$row[$i]->files}}" style="margin-left: 10px;" >{{$row[$i]->files}}</a></td>
                                                    <td>{{$row[$i]->created_at}}</td>
                                                    <td>{{$row[$i]->username}}</td>
                                                    <td>{{$row[$i]->surname}}</td>
                                                    <td>{{$row[$i]->dir}}</td>
                                                    <td <?php echo $adminStyle ?>><a href="#" class="btn btn-xs btn-danger news-zama" style="margin-left: 10px;" onclick="deleteRecord(this)"><i class="glyphicon glyphicon-minus"></i>&nbsp;Elimina</a></td>
                                                </tr>
                                                <?php } ?>
                                            </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                </div>
            </div>
    </div>
                    
                </div>

            </div>
        </div>

    </div>
    <script type="text/javascript">
        window['table']="uploads";
    </script>
@endsection
@section('footer-plugin')

@endsection