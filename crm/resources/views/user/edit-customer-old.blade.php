@extends('layouts.master')
@section('header-page-personalization')
@endsection
@section('content')
    <div class="page_content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <form id="login_form" role="form" method="POST" action="{{ route('customer.update', ['id' => $customer->id]) }}">
                        {{ csrf_field() }}
                        {{ method_field('PUT') }}
                    <div class="panel panel-default">

                        <div class="panel-body">
                            <fieldset>
                                <legend><span>Clienti</span></legend>
                            </fieldset>
                            @if(session()->has('message'))
                                <div class="alert alert-success">
                                    {{ session()->get('message') }}
                                </div>
                            @endif
                            @if(session()->has('error'))
                                <div class="alert alert-danger">
                                    {{ session()->get('error') }}
                                </div>
                            @endif

                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <label for="reg_input">Codice Cliente</label>
                                        <input type="text" id="codice" name="codice" class="form-control" tabindex="0" value="{{ $customer->codice }}">
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label for="reg_input">Societa</label>
                                        <input type="text" id="societa" name="societa" class="form-control" tabindex="1" value="{{ $customer->societa }}">
                                    </div>
                                    <div class="form-group">
                                        <label for="reg_input">Cognome</label>
                                        <input type="text" id="cognome" name="cognome" class="form-control" tabindex="3" value="{{ $customer->cognome }}">
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label for="reg_input">Nome</label>
                                        <input type="text" id="nome" name="nome" class="form-control" tabindex="2" value="{{ $customer->nome }}">
                                    </div>
                                    <div class="form-group">
                                        <label for="reg_input">Citta'</label>
                                        <input type="text" id="citta" name="citta" class="form-control" tabindex="4" value="{{ $customer->citta }}">
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label for="reg_input">Provincia</label>
                                        <input type="text" id="provincia" name="provincia" class="form-control" tabindex="5"  value="{{ $customer->provincia }}">
                                    </div>
                                    <div class="form-group">
                                        <label for="reg_input">Cap</label>
                                        <input type="text" id="cap" name="cap" class="form-control" tabindex="7" value="{{ $customer->cap }}">
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label for="reg_input">Regione</label>
                                        <input type="text" id="regione" name="regione" class="form-control" tabindex="6" value="{{ $customer->regione }}">
                                    </div>
                                    <div class="form-group">
                                        <label for="reg_input">Stato</label>
                                        <input type="text" id="stato" name="stato" class="form-control" tabindex="8" value="{{ $customer->stato }}">
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label for="reg_input">Telefono1</label>
                                        <input type="text" id="tel1" name="tel1" class="form-control" tabindex="9" value="{{ $customer->tel1 }}">
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label for="reg_input">Email1</label>
                                        <input type="text" id="email1" name="email1" class="form-control" tabindex="10"  value="{{ $customer->email1 }}">
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                        <div class="panel panel-default">
                            <div class="panel-body">
                                <div class="heading_a">Suend CRM</div>
                                <div class="tabbable tabs-left">
                                    <ul class="nav nav-tabs" id="tabs_c">
                                        <li class="active"><a data-toggle="tab" href="#info">Info</a></li>
                                        <li><a data-toggle="tab" href="#profilo">Profilo</a></li>
                                        <li><a data-toggle="tab" href="#infoviaggi">Info Viaggi</a></li>
                                        <li><a data-toggle="tab" href="#qasuend">Q_A Suend</a></li>
                                        <li><a data-toggle="tab" href="#qaviaggio">Q_A Viaggio</a></li>
                                    </ul>
                                    <div class="tab-content" id="tabs_content_c">
                                        <div id="info" class="tab-pane fade in active">
                                            <div class="row">
                                                <div class="col-lg-8">
                                                    <div class="form-group">
                                                        <label for="reg_input">Indirizzo</label>
                                                        <input type="text" id="info-indirizzo" name="info-indirizzo"
                                                               class="form-control" tabindex="11">
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="reg_input">Sito Web</label>
                                                        <input type="text" id="info-web" name="info-web" class="form-control"
                                                               tabindex="13">
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="reg_input">Partita Iva</label>
                                                        <input type="text" id="info-piva" name="info-piva" class="form-control"
                                                               tabindex="12">
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="reg_input">Codice Fiscale</label>
                                                        <input type="text" id="info-codice_fiscale" name="info-codice_fiscale"
                                                               class="form-control" tabindex="14">
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="reg_input">Data di Nascita</label>
                                                        <input type="text" id="info-data_nascita" name="info-data_nascita"
                                                               class="form-control" tabindex="15">
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="reg_input">Sesso</label>
                                                        <select id="info-sesso" name="info-sesso" class="form-control"
                                                                tabindex="16">
                                                            <option value="MASCHIO">MASCHIO</option>
                                                            <option value="FEMMINA">FEMMINA</option>
                                                        </select>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="reg_input">Posizione</label>
                                                        <input type="text" id="info-posizione" name="info-posizione"
                                                               class="form-control" tabindex="15">
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="reg_input">Suffisso</label>
                                                        <input type="text" id="info-suffisso" name="info-suffisso"
                                                               class="form-control" tabindex="15">
                                                    </div>
                                                </div>
                                                <div class="col-lg-4">
                                                    <div class="form-group">
                                                        <label for="reg_input">Telefono4</label>
                                                        <input type="text" id="info-tel4" name="info-tel4" class="form-control"
                                                               tabindex="22">
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="reg_input">Telefono5</label>
                                                        <input type="text" id="info-tel5" name="info-tel5" class="form-control"
                                                               tabindex="22">
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="reg_input">Fax1</label>
                                                        <input type="text" id="info-fax1" name="info-fax1" class="form-control"
                                                               tabindex="22">
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="reg_input">Fax2</label>
                                                        <input type="text" id="info-fax2" name="info-fax2" class="form-control"
                                                               tabindex="22">
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="reg_input">Email2</label>
                                                        <input type="text" id="info-mail2" name="info-mail2" class="form-control"
                                                               tabindex="22">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-lg-8">
                                                    <div class="form-group">
                                                        <label for="reg_input">Regione Indirizzo2</label>
                                                        <select id="info-regione2" name="info-regione2" class="form-control">
                                                            @foreach ($regioni as $regione)
                                                                <option value="{{ $regione->id_regione }}">{{ $regione->regione }}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="reg_input">Provincia Indirizzo2</label>
                                                        <select id="info-provincia2" name="info-provincia2" class="form-control">
                                                            <option value=""></option>
                                                        </select>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="reg_input">Cap Indirizzo2</label>
                                                        <select id="info-cap2" name="info-cap2" class="form-control">
                                                            <option value=""></option>
                                                        </select>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="reg_input">Citta' Indirizzo2</label>
                                                        <select id="info-citta2" name="info-citta2" class="form-control">
                                                            <option value=""></option>
                                                        </select>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="reg_input">Stato Indirizzo2</label>
                                                        <input type="text" id="info-stato2" name="info-stato2"
                                                               class="form-control"
                                                               tabindex="8">
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="reg_input">Indirizzo 2</label>
                                                        <input type="text" id="info-indirizzo2" name="info-indirizzo2"
                                                               class="form-control" tabindex="11">
                                                    </div>
                                                </div>
                                                <div class="col-lg-4">
                                                    <div class="form-group">
                                                        <label for="privacy" style="text-align: center;">Accettazione
                                                            Privacy</label>
                                                        <input type="checkbox" class="form-control input-sm"
                                                               name="info-privacy" id="info-privacy">
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="privacy_commerciali" style="text-align: center;">Accettazione
                                                            Privacy Commerciale</label>
                                                        <input type="checkbox" class="form-control input-sm"
                                                               name="info-privacy_commerciali" id="info-privacy_commerciali">
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="privacy_profilazione" style="text-align: center;">Accettazione
                                                            Privacy Profilazione</label>
                                                        <input type="checkbox" class="form-control input-sm"
                                                               name="info-privacy_profilazione" id="info-privacy_profilazione">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div id="infoviaggi" class="tab-pane fade">
                                            <div class="row">
                                                <div class="col-lg-8">
                                                    <div class="form-group">
                                                        <label for="reg_input">Vacanze/Anno</label>
                                                        <input type="text" id="infoviaggi-vacanzeanno" name="infoviaggi-vacanzeanno"
                                                               class="form-control">
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="reg_input">Preferenza Viaggi</label>
                                                        <input type="text" id="infoviaggi-preferenze" name="infoviaggi-preferenze"
                                                               class="form-control">
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="reg_input">Durata Media</label>
                                                        <input type="text" id="infoviaggi-duratamedia" name="infoviaggi-duratamedia"
                                                               class="form-control">
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="reg_input">Prossimi Paesi</label>
                                                        <input type="text" id="infoviaggi-prossimipaesi" name="infoviaggi-prossimipaesi"
                                                               class="form-control">
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="reg_input">Fonti</label>
                                                        <input type="text" id="infoviaggi-fonti" name="infoviaggi-fonti"
                                                               class="form-control">
                                                    </div>
                                                </div>
                                                <div class="col-lg-4">
                                                    <div class="col-lg-2">
                                                        <label>Mesi Preferiti</label>
                                                        <div class="checkbox">
                                                            <label>
                                                                <input type="checkbox" name="infoviaggi-mesipreferiti[]" value="1">
                                                                Gennaio
                                                            </label>
                                                        </div>
                                                        <div class="checkbox">
                                                            <label>
                                                                <input type="checkbox" name="infoviaggi-mesipreferiti[]" value="2">
                                                                Febbraio
                                                            </label>
                                                        </div>
                                                        <div class="checkbox">
                                                            <label>
                                                                <input type="checkbox" name="infoviaggi-mesipreferiti[]" value="3">
                                                                Marzo
                                                            </label>
                                                        </div>
                                                        <div class="checkbox">
                                                            <label>
                                                                <input type="checkbox" name="infoviaggi-mesipreferiti[]" value="4">
                                                                Aprile
                                                            </label>
                                                        </div>
                                                        <div class="checkbox">
                                                            <label>
                                                                <input type="checkbox" name="infoviaggi-mesipreferiti[]" value="5">
                                                                Maggio
                                                            </label>
                                                        </div>
                                                        <div class="checkbox">
                                                            <label>
                                                                <input type="checkbox" name="infoviaggi-mesipreferiti[]" value="6">
                                                                Giugno
                                                            </label>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-2">

                                                        <div class="checkbox">
                                                            <label>
                                                                <input type="checkbox" name="infoviaggi-mesipreferiti[]" value="7">
                                                                Luglio
                                                            </label>
                                                        </div>
                                                        <div class="checkbox">
                                                            <label>
                                                                <input type="checkbox" name="infoviaggi-mesipreferiti[]" value="8">
                                                                Agosto
                                                            </label>
                                                        </div>
                                                        <div class="checkbox">
                                                            <label>
                                                                <input type="checkbox" name="infoviaggi-mesipreferiti[]" value="9">
                                                                Settembre
                                                            </label>
                                                        </div>
                                                        <div class="checkbox">
                                                            <label>
                                                                <input type="checkbox" name="infoviaggi-mesipreferiti[]" value="10">
                                                                Ottobre
                                                            </label>
                                                        </div>
                                                        <div class="checkbox">
                                                            <label>
                                                                <input type="checkbox" name="infoviaggi-mesipreferiti[]" value="11">
                                                                Novembre
                                                            </label>
                                                        </div>
                                                        <div class="checkbox">
                                                            <label>
                                                                <input type="checkbox" name="infoviaggi-mesipreferiti[]" value="12">
                                                                Dicembre
                                                            </label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-lg-8">
                                                    <div class="col-lg-4">
                                                        <label>Tematiche</label>
                                                        <div class="checkbox">
                                                            <label>
                                                                <input type="checkbox" name="infoviaggi-tematiche[]" value="Archeologia">
                                                                Archeologia
                                                            </label>
                                                        </div>
                                                        <div class="checkbox">
                                                            <label>
                                                                <input type="checkbox" name="infoviaggi-tematiche[]" value="Avventura">
                                                                Avventura
                                                            </label>
                                                        </div>
                                                        <div class="checkbox">
                                                            <label>
                                                                <input type="checkbox" name="infoviaggi-tematiche[]" value="Cultura">
                                                                Cultura
                                                            </label>
                                                        </div>
                                                        <div class="checkbox">
                                                            <label>
                                                                <input type="checkbox" name="infoviaggi-tematiche[]" value="Ecoturismo">
                                                                Ecoturismo
                                                            </label>
                                                        </div>

                                                    </div>
                                                    <div class="col-lg-4">
                                                        <div class="checkbox">
                                                            <label>
                                                                <input type="checkbox" name="infoviaggi-tematiche[]" value="Enogastronomia">
                                                                Enogastronomia
                                                            </label>
                                                        </div>
                                                        <div class="checkbox">
                                                            <label>
                                                                <input type="checkbox" name="infoviaggi-tematiche[]" value="Fotografico">
                                                                Fotografico
                                                            </label>
                                                        </div>
                                                        <div class="checkbox">
                                                            <label>
                                                                <input type="checkbox" name="infoviaggi-tematiche[]" value="MareRelax">
                                                                Mare e relax
                                                            </label>
                                                        </div>
                                                        <div class="checkbox">
                                                            <label>
                                                                <input type="checkbox" name="infoviaggi-tematiche[]" value="Montagna">
                                                                Montagna
                                                            </label>
                                                        </div>

                                                    </div>
                                                    <div class="col-lg-4">
                                                        <div class="checkbox">
                                                            <label>
                                                                <input type="checkbox" name="infoviaggi-tematiche[]" value="Religioso">
                                                                Religioso
                                                            </label>
                                                        </div>
                                                        <div class="checkbox">
                                                            <label>
                                                                <input type="checkbox" name="infoviaggi-tematiche[]" value="Sportivo">
                                                                Sportivo
                                                            </label>
                                                        </div>
                                                        <div class="checkbox">
                                                            <label>
                                                                <input type="checkbox" name="infoviaggi-tematiche[]" value="StoricoPolitico">
                                                                Storico Politico
                                                            </label>
                                                        </div>
                                                        <div class="checkbox">
                                                            <label>
                                                                <input type="checkbox" name="infoviaggi-tematiche[]" value="Altro">
                                                                Altro
                                                            </label>
                                                        </div>

                                                    </div>
                                                </div>
                                                <div class="col-lg-4">
                                                    <textarea cols="30" rows="5" class="form-control input-sm" name="infoviaggi-tematiche-altro"
                                                              placeholder="Altre tematiche..."></textarea>
                                                </div>


                                            </div>
                                            <div class="row">
                                                <div class="col-lg-8">
                                                    <div class="form-group">
                                                        <label for="reg_input">Destinazioni Preferite</label>
                                                        <input type="text" id="infoviaggi-destinazionipreferite"
                                                               name="infoviaggi-destinazionipreferite"
                                                               class="form-control" tabindex="11">
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="reg_input">Destinazioni Passate</label>
                                                        <input type="text" id="infoviaggi-destinazionipassate"
                                                               name="infoviaggi-destinazionipassate"
                                                               class="form-control" tabindex="11">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div id="qasuend" class="tab-pane fade">
                                            <div class="row">

                                                <div class="col-lg-12">
                                                    <div class="form-group">
                                                        <label for="reg_input">Conoscenza Suend</label>
                                                        <input type="text" id="qasuend-conoscenza" name="qasuend-conoscenza"
                                                               class="form-control">
                                                    </div>
                                                </div>

                                                <div class="col-lg-12">
                                                    <div class="form-group">
                                                        <label for="reg_input">Altre motivazioni conoscenza
                                                            Suend</label>
                                                        <input type="text" id="qasuend-altremotivazione" name="qasuend-altremotivazione"
                                                               class="form-control">
                                                    </div>
                                                </div>
                                                <div class="col-lg-12">
                                                    <div class="form-group">
                                                        <label for="reg_input">Codice Cliente</label>
                                                        <input type="text" id="qasuend-codicecliente" name="qasuend-codicecliente"
                                                               class="form-control"
                                                               tabindex="0">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-lg-2">
                                                    <div class="form-group">
                                                        <label for="reg_input" style="text-align: center;">Già viaggiato
                                                            con noi</label>
                                                        <input type="checkbox" class="form-control input-sm"
                                                               name="qasuend-giaviaggiato" id="qasuend-giaviaggiato" value="true">
                                                    </div>
                                                </div>
                                                <div class="col-lg-2">
                                                    <div class="form-group">
                                                        <label for="reg_input">Quante Volte</label>
                                                        <input type="text" id="qasuend-quantiviaggi" name="qasuend-quantiviaggi"
                                                               class="form-control"
                                                               tabindex="0">
                                                    </div>
                                                </div>
                                                <div class="col-lg-8">
                                                    <div class="form-group">
                                                        <label for="reg_input">Dove</label>
                                                        <input type="text" id="qasuend-doveviaggi" name="qasuend-doveviaggi"
                                                               class="form-control"
                                                               tabindex="0">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-lg-2">
                                                    <div class="form-group">
                                                        <label for="reg_input" style="text-align: center;">Conosce
                                                            Sito</label>
                                                        <input type="checkbox" class="form-control input-sm"
                                                               name="qasuend-conoscesito" id="qasuend-conoscesito" value="true">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-lg-12">
                                                    <div class="form-group">
                                                        <label for="reg_input">Note</label>
                                                        <textarea name="qasuend-note" id="qasuend-note" cols="10" rows="3"
                                                                  class="form-control"></textarea>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div id="qaviaggio" class="tab-pane fade">
                                            <div class="row">
                                                <div class="col-lg-12">
                                                    <div class="form-group">
                                                        <label for="reg_input">Soddisfazione Generale</label>
                                                        <input type="text" id="qaviaggio-soddisfazione" name="qaviaggio-soddisfazione"
                                                               class="form-control">
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="reg_input">Data Partenza</label>
                                                        <input class="form-control ts_datepicker" type="text"
                                                               name="qaviaggio-datapartenza" id="qaviaggio-datapartenza"
                                                               placeholder="gg/mm/aaaa">
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="reg_input">Ultima Rilevazione</label>
                                                        <input class="form-control ts_datepicker" type="text"
                                                               name="qaviaggio-ultimarilevazione" id="qaviaggio-ultimarilevazione"
                                                               placeholder="gg/mm/aaaa">
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="reg_input">Destinazione</label>
                                                        <input type="text" id="qaviaggio-destinazione" name="qaviaggio-destinazione"
                                                               class="form-control">
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="reg_input">Storico</label>
                                                        <textarea cols="30" name="qaviaggio-storico" id="qaviaggio-storico" rows="3"
                                                                  class="form-control input-sm"></textarea>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div id="profilo" class="tab-pane fade">
                                            <div class="row">
                                                <div class="col-md-4">

                                                    <label>Data di nascita</label>
                                                    <input class="form-control ts_datepicker" type="text"
                                                           name="profilo-datanascita" id="profilo-datanascita">
                                                </div>
                                                <div class="col-md-8">
                                                    <div class="form-group">
                                                        <label>Luogo di nascita</label>
                                                        <input type="text" id="profilo-luogonascita" name="profilo-luogonascita"
                                                               class="form-control">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-8">
                                                    <div class="form-group">
                                                        <label>Professione</label>
                                                        <input type="text" id="profilo-professione" name="profilo-professione"
                                                               class="form-control">
                                                    </div>
                                                    <div class="form-group">
                                                        <label>Hobby 1</label>
                                                        <input type="text" id="profilo-hobby1" name="profilo-hobby1"
                                                               class="form-control">
                                                    </div>
                                                    <div class="form-group">
                                                        <label>Hobby 2</label>
                                                        <input type="text" id="profilo-hobby2" name="profilo-hobby2"
                                                               class="form-control">
                                                    </div>
                                                    <div class="form-group">
                                                        <label>Data Scadenza</label>
                                                        <input class="form-control ts_datepicker" type="text"
                                                               name="profilo-datascadenza" id="profilo-datascadenza"
                                                               placeholder="gg/mm/aaaa">
                                                    </div>
                                                    <div class="form-group">
                                                        <label>Scadenza Carta identita'</label>
                                                        <input class="form-control ts_datepicker" type="text"
                                                               name="profilo-datascadenzaidentita" id="profilo-datascadenzaidentita"
                                                               placeholder="gg/mm/aaaa">
                                                    </div>
                                                    <div class="form-group">
                                                        <div class="checkbox">
                                                            <label>
                                                                <input type="checkbox" name="profilo-newsletter" value="true">
                                                                Newsletter
                                                            </label>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <div class="checkbox">
                                                            <label>
                                                                <input type="checkbox" name="profilo-clientediretto" value="true">
                                                                Cliente diretto
                                                            </label>
                                                        </div>
                                                    </div>

                                                </div>
                                                <div class="col-md-4">
                                                    <br>
                                                    <div class="form-group">
                                                        <label>Frequent Flyer</label>
                                                        <textarea cols="30" rows="20" class="form-control input-sm" name="profilo-frequentflyer"
                                                                  placeholder=""></textarea>
                                                    </div>
                                                </div>

                                            </div>
                                            <div class="row">
                                                <div class="col-md-8">
                                                    <div class="col-md-2">

                                                        <label>Adulti</label>
                                                        <input type="text" id="profilo-adulti" name="profilo-adulti"
                                                               class="form-control">
                                                    </div>
                                                    <div class="col-md-2">
                                                        <label>Bambini</label>
                                                        <input type="text" id="profilo-bambini" name="profilo-bambini"
                                                               class="form-control">
                                                    </div>
                                                    <div class="col-md-2">
                                                        <label>Neonati</label>
                                                        <input type="text" id="profilo-neonati" name="profilo-neonati"
                                                               class="form-control">
                                                    </div>
                                                    <div class="col-md-2">

                                                        <label>Figli</label>
                                                        <input type="text" id="profilo-figli" name="profilo-figli"
                                                               class="form-control">
                                                    </div>
                                                    <div class="col-md-2">
                                                        <label>Anziani</label>
                                                        <input type="text" id="profilo-anziani" name="profilo-anziani"
                                                               class="form-control">
                                                    </div>
                                                    <div class="col-md-2">
                                                        <label>Animali</label>
                                                        <input type="text" id="profilo-animali" name="profilo-animali"
                                                               class="form-control">
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label>Note</label>
                                                        <textarea cols="30" rows="5" class="form-control input-sm" name="profilo-note"
                                                                  placeholder=""></textarea>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-8">
                                                    <div class="col-md-2">
                                                        <div class="form-group">
                                                            <div class="checkbox">
                                                                <label>
                                                                    <input type="checkbox" value="true" name="profilo-iscrittoclub">
                                                                    Iscritto Club
                                                                </label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-10">
                                                        <div class="form-group">
                                                            <label>Codice Club</label>
                                                            <input type="text" id="profilo-codiceclub" name="profilo-codiceclub"
                                                                   class="form-control">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <button class="btn btn-lg btn-success btn-block">Aggiungi Cliente</button>
                        </div>



                        <!--
                    <div class="panel panel-default">
                        <div class="panel-body">
                            <fieldset>
                                <legend><span>Info</span></legend>
                            </fieldset>
                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label for="reg_input">Indirizzo</label>
                                        <input type="text" id="indirizzo" name="indirizzo" class="form-control" tabindex="11"  value="{{ $customer->indirizzo }}">
                                    </div>
                                    <div class="form-group">
                                        <label for="reg_input">Sito Web</label>
                                        <input type="text" id="web" name="cognome" class="form-control" tabindex="13"  value="{{ $customer->cognome }}">
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label for="reg_input">Partita Iva</label>
                                        <input type="text" id="piva" name="piva" class="form-control" tabindex="12"  value="{{ $customer->piva }}">
                                    </div>
                                    <div class="form-group">
                                        <label for="reg_input">Codice Fiscale</label>
                                        <input type="text" id="codice_fiscale" name="codice_fiscale" class="form-control" tabindex="14"  value="{{ $customer->codice_fiscale }}">
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label for="reg_input">Data di Nascita</label>
                                        <input type="text" id="data_nascita" name="data_nascita" class="form-control" tabindex="15"  value="{{ $customer->data_nascita }}">
                                    </div>

                                </div>

                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label for="reg_input">Sesso</label>
                                        <select id="sesso" name="sesso" class="form-control" tabindex="16" >
                                            <option value="{{ $customer->codice }}">{{ $customer->codice }}</option>
                                            <option value="MASCHIO">MASCHIO</option>
                                            <option value="FEMMINA">FEMMINA</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label for="reg_input">Telefono2</label>
                                        <input type="text" id="tel2" name="tel2" class="form-control" tabindex="22"  value="{{ $customer->tel2 }}">
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label for="reg_input">Email2</label>
                                        <input type="text" id="email2" name="email2" class="form-control" tabindex="21"  value="{{ $customer->email2 }}">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="form-group">
                                            @if( $customer->privacy == 1)
                                                <label for="privacy" style="text-align: center;">Accettazione Privacy</label>
                                                <input type="checkbox" class="form-control input-sm" name="privacy" id="privacy" checked  disabled >
                                            @else
                                                <label for="privacy" style="text-align: center;">Accettazione Privacy</label>
                                                <input type="checkbox" class="form-control input-sm" name="privacy" id="privacy"  disabled >
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-lg-12">
                                        <div class="form-group">
                                            @if( $customer->privacy_commerciali == 1)
                                                <label for="privacy_commerciali" style="text-align: center;">Accettazione Privacy Commerciale</label>
                                                <input type="checkbox" class="form-control input-sm" name="privacy_commerciali" id="privacy_commerciali" checked  disabled >
                                            @else
                                                <label for="privacy_commerciali" style="text-align: center;">Accettazione Privacy Commerciale</label>
                                                <input type="checkbox" class="form-control input-sm" name="privacy_commerciali" id="privacy_commerciali"  disabled >
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-lg-12">
                                        <div class="form-group">
                                            @if( $customer->privacy_profilazione == 1)
                                                <label for="privacy_profilazione" style="text-align: center;">Accettazione Privacy Profilazione</label>
                                                <input type="checkbox" class="form-control input-sm" name="privacy_profilazione" id="privacy_profilazione" checked  disabled >
                                            @else
                                                <label for="privacy_profilazione" style="text-align: center;">Accettazione Privacy Profilazione</label>
                                                <input type="checkbox" class="form-control input-sm" name="privacy_profilazione" id="privacy_profilazione"  disabled >
                                            @endif
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>-->


                    <button class="btn btn-lg btn-success btn-block">Modifica Cliente</button>

                </div>
            </div>
        </div>
    </div>



@endsection
@section('footer-plugin')
    <!-- datatables -->
    <script src="{{ asset('/lib/DataTables/media/js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('/lib/DataTables/media/js/dataTables.bootstrap.js') }}"></script>
    <script src="{{ asset('/lib/DataTables/extensions/TableTools/js/dataTables.tableTools.min.js') }}"></script>
    <script src="{{ asset('/lib/DataTables/extensions/Scroller/js/dataTables.scroller.min.js') }}"></script>

    <!-- datatables functions -->
    <script src="{{ asset('/js/apps/tisa_datatables.js') }}"></script>
    <script>

        $('#travels_table').DataTable({
            processing: true,
            serverSide: true,
            ajax: '{!! route('travels_data', ['customer_id' => $customer->id ]) !!}',
            columns: [
                {data: 'id'},
                {data: 'choices'},
                {data: 'data_partenza'},
                {data: 'ultima_rilevazione'},
                {data: 'destinazione'},
                {data: 'Visualizza', name: 'azioni', orderable: false, searchable: false}
            ]
        });
    </script>

@endsection
