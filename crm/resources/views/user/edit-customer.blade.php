@extends('layouts.master')
@section('header-page-personalization')
    <style>
        .twitter-typeahead, .tt-hint, .tt-input, .tt-menu { width: 100%;  }
        .tt-menu {
            background: #ffffff;
        }
    </style>
@endsection
@section('content')
    <div class="page_content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <form id="login_form" role="form" method="POST"
                          action="{{ route('customer.update', ['id' => $customer->id]) }}">
                        {{ csrf_field() }}
                        {{ method_field('PUT') }}

                        <div class="panel panel-default">
                            <div class="panel-body">
                                <div class="heading_a">Suend CRM</div>
                                <div class="tabbable tabs-left">
                                    <ul class="nav nav-tabs" id="tabs_c">
                                        <li class="active"><a data-toggle="tab" href="#info">Info</a></li>
                                        <li><a data-toggle="tab" href="#profilo">Profilo</a></li>
                                        <li><a data-toggle="tab" href="#infoviaggi">Info Viaggi</a></li>
                                        <li><a data-toggle="tab" href="#qasuend">Info Suend</a></li>
                                        <li><a data-toggle="tab" href="#qaviaggio">Valutazione Viaggio</a></li>
                                    </ul>
                                    @if(session()->has('message'))
                                        <div class="alert alert-success">
                                            {{ session()->get('message') }}
                                        </div>
                                    @endif
                                    @if(session()->has('error'))
                                        <div class="alert alert-danger">
                                            {{ session()->get('error') }}
                                        </div>
                                    @endif
                                    <div class="tab-content" id="tabs_content_c">
                                        <div id="info" class="tab-pane fade in active">
                                            <div class="row">
                                                <div class="col-lg-8">
                                                    <div class="form-group">
                                                        <label for="reg_input">Cognome</label>
                                                        <input type="text" id="cognome" name="cognome"
                                                               class="form-control"
                                                               value="{{ isset($customer->cognome) ? $customer->cognome : ''  }}"
                                                               tabindex="3">
                                                    </div>

                                                    <div class="form-group">
                                                        <label for="reg_input">Nome</label>
                                                        <input type="text" id="nome" name="nome" class="form-control"
                                                               tabindex="2"
                                                               value="{{ isset($customer->nome) ? $customer->nome : ''  }}">
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="reg_input">Data di Nascita</label>
                                                        <input type="text" id="info_data_nascita"

                                                               value="{{ isset($info->data_nascita) ? $info->data_nascita : '' }}"
                                                               name="info_data_nascita"
                                                               class="form-control" tabindex="15">
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="reg_input">Sesso</label>
                                                        <select id="info_sesso" name="info_sesso" class="form-control"
                                                                tabindex="16">
                                                            <option value="{{ isset($info->sesso) ? $info->sesso : '' }}">
                                                                {{ isset($info->sesso) ? $info->sesso : ''}}
                                                            </option>
                                                            <option value="MASCHIO">MASCHIO</option>
                                                            <option value="FEMMINA">FEMMINA</option>
                                                        </select>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="reg_input">Indirizzo</label>
                                                        <input type="text" id="info_indirizzo" name="info_indirizzo"

                                                               value="{{ isset($info->indirizzo) ? $info->indirizzo : '' }}"
                                                               class="form-control" tabindex="11">
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="reg_input">Citta'</label>
                                                        <input type="text" id="citta" name="citta" class="form-control"
                                                               tabindex="4"
                                                               value="{{ isset($customer->citta) ? $customer->citta : ''  }}">
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="reg_input">Cap</label>
                                                        <input type="text" id="cap" name="cap" class="form-control"
                                                               tabindex="7"
                                                               value="{{ isset($customer->cap) ? $customer->cap : ''  }}">
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="reg_input">Provincia</label>
                                                        <input type="text" id="provincia" name="provincia"
                                                               class="form-control"
                                                               value="{{ isset($customer->provincia) ? $customer->provincia : ''  }}"
                                                               tabindex="5">
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="reg_input">Regione</label>
                                                        <input type="text" id="regione" name="regione"
                                                               class="form-control"
                                                               value="{{ isset($customer->regione) ? $customer->regione : ''  }}"
                                                               tabindex="6">
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="reg_input">Stato</label>
                                                        <input type="text" id="stato" name="stato" class="form-control"
                                                               tabindex="8"
                                                               value="{{ isset($customer->stato) ? $customer->stato : ''  }}">
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="reg_input">Telefono Principale</label>
                                                        <input type="text" id="tel1" name="tel1" class="form-control"
                                                               tabindex="9"
                                                               value="{{ isset($customer->tel1) ? $customer->tel1 : ''  }}">
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="reg_input">Telefono Secondario</label>
                                                        <input type="text" id="info_tel2" name="info_tel2"

                                                               value="{{ isset($info->tel2) ? $info->tel2 : ''  }}"
                                                               class="form-control"
                                                               tabindex="22">
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="reg_input">Email Principale</label>
                                                        <input type="text" id="email1" name="email1"
                                                               class="form-control"
                                                               value="{{ isset($customer->email1) ? $customer->adulti : ''  }}">
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="reg_input">Email Secondaria</label>
                                                        <input type="text" id="info_mail2" name="info_mail2"
                                                               value="{{ isset($info->email2) ? $info->email2 : ''  }}"
                                                               class="form-control"
                                                               tabindex="22">
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="reg_input">Codice Fiscale</label>
                                                        <input type="text" id="info_codice_fiscale"

                                                               value="{{ isset($info->codice_fiscale) ? $info->codice_fiscale : '' }}"
                                                               name="info_codice_fiscale"
                                                               class="form-control" tabindex="14">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div id="infoviaggi" class="tab-pane fade">
                                            <div class="row">
                                                <div class="col-lg-8">
                                                    <div class="form-group">
                                                        <label for="reg_input">Vacanze/Anno</label>
                                                        <input type="text" id="infoviaggi_vacanzeanno"
                                                               name="infoviaggi_vacanzeanno"

                                                               value="{{ isset($infoviaggi->vacanze_anno) ? $infoviaggi->vacanze_anno : ''  }}"
                                                               class="form-control">
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="reg_input">Preferenza Viaggi</label>
                                                        <input type="text" id="infoviaggi_preferenze"
                                                               name="infoviaggi_preferenze"

                                                               value="{{ isset($infoviaggi->preferenza_viaggi) ? $infoviaggi->preferenza_viaggi : ''  }}"
                                                               class="form-control">
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="reg_input">Durata Media</label>
                                                        <input type="text" id="infoviaggi_duratamedia"
                                                               name="infoviaggi_duratamedia"

                                                               value="{{ isset($infoviaggi->durata_media) ? $infoviaggi->durata_media : ''  }}"
                                                               class="form-control">
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="reg_input">Prossimi Paesi</label>
                                                        <input type="text" id="infoviaggi_prossimipaesi"
                                                               name="infoviaggi_prossimipaesi"

                                                               value="{{ isset($infoviaggi->prossimi_paesi) ? $infoviaggi->prossimi_paesi : ''  }}"
                                                               class="form-control">
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="reg_input">Fonti</label>
                                                        <input type="text" id="infoviaggi_fonti"
                                                               name="infoviaggi_fonti"
                                                               value="{{ isset($infoviaggi->fonti) ? $infoviaggi->fonti : ''  }}"

                                                               class="form-control">
                                                    </div>
                                                </div>
                                                <div class="col-lg-4">
                                                    <div class="col-lg-2">
                                                        <label>Mesi Preferiti</label>
                                                        <div class="checkbox">
                                                            <label>
                                                                <input type="checkbox" name="infoviaggi_gennaio"
                                                                       @if(isset($infoviaggi->gennaio) and $infoviaggi->gennaio == 1)
                                                                       checked
                                                                       @endif
                                                                       value="1">
                                                                Gennaio
                                                            </label>
                                                        </div>
                                                        <div class="checkbox">
                                                            <label>
                                                                <input type="checkbox" name="infoviaggi_febbraio"
                                                                       @if(isset($infoviaggi->febbraio) and $infoviaggi->febbraio == 1)
                                                                       checked
                                                                       @endif
                                                                       value="1">
                                                                Febbraio
                                                            </label>
                                                        </div>
                                                        <div class="checkbox">
                                                            <label>
                                                                <input type="checkbox" name="infoviaggi_marzo"
                                                                       @if(isset($infoviaggi->marzo) and $infoviaggi->marzo == 1)
                                                                       checked
                                                                       @endif
                                                                       value="1">
                                                                Marzo
                                                            </label>
                                                        </div>
                                                        <div class="checkbox">
                                                            <label>
                                                                <input type="checkbox" name="infoviaggi_aprile"
                                                                       @if(isset($infoviaggi->aprile) and $infoviaggi->aprile == 1)
                                                                       checked
                                                                       @endif
                                                                       value="1">
                                                                Aprile
                                                            </label>
                                                        </div>
                                                        <div class="checkbox">
                                                            <label>
                                                                <input type="checkbox" name="infoviaggi_maggio"
                                                                       @if(isset($infoviaggi->maggio) and $infoviaggi->maggio == 1)
                                                                       checked
                                                                       @endif
                                                                       value="1">
                                                                Maggio
                                                            </label>
                                                        </div>
                                                        <div class="checkbox">
                                                            <label>
                                                                <input type="checkbox" name="infoviaggi_giugno"
                                                                       @if(isset($infoviaggi->giugno) and $infoviaggi->giugno == 1)
                                                                       checked
                                                                       @endif
                                                                       value="1">
                                                                Giugno
                                                            </label>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-2">

                                                        <div class="checkbox">
                                                            <label>
                                                                <input type="checkbox" name="infoviaggi_luglio"
                                                                       @if(isset($infoviaggi->luglio) and $infoviaggi->luglio == 1)
                                                                       checked
                                                                       @endif
                                                                       value="1">
                                                                Luglio
                                                            </label>
                                                        </div>
                                                        <div class="checkbox">
                                                            <label>
                                                                <input type="checkbox" name="infoviaggi_agosto"
                                                                       @if(isset($infoviaggi->agosto) and $infoviaggi->agosto == 1)
                                                                       checked
                                                                       @endif
                                                                       value="1">
                                                                Agosto
                                                            </label>
                                                        </div>
                                                        <div class="checkbox">
                                                            <label>
                                                                <input type="checkbox" name="infoviaggi_settembre"
                                                                       @if(isset($infoviaggi->settembre) and $infoviaggi->settembre == 1)
                                                                       checked
                                                                       @endif
                                                                       value="1">
                                                                Settembre
                                                            </label>
                                                        </div>
                                                        <div class="checkbox">
                                                            <label>
                                                                <input type="checkbox" name="infoviaggi_ottobre"
                                                                       @if(isset($infoviaggi->ottobre) and $infoviaggi->ottobre == 1)
                                                                       checked
                                                                       @endif
                                                                       value="1">
                                                                Ottobre
                                                            </label>
                                                        </div>
                                                        <div class="checkbox">
                                                            <label>
                                                                <input type="checkbox" name="infoviaggi_novembre"
                                                                       @if(isset($infoviaggi->novembre) and $infoviaggi->novembre == 1)
                                                                       checked
                                                                       @endif
                                                                       value="1">
                                                                Novembre
                                                            </label>
                                                        </div>
                                                        <div class="checkbox">
                                                            <label>
                                                                <input type="checkbox" name="infoviaggi_dicembre"
                                                                       @if(isset($infoviaggi->dicembre) and $infoviaggi->dicembre == 1)
                                                                       checked
                                                                       @endif
                                                                       value="1">
                                                                Dicembre
                                                            </label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-lg-8">
                                                    <div class="col-lg-4">
                                                        <label>Tematiche</label>
                                                        <div class="checkbox">
                                                            <label>
                                                                <input type="checkbox" name="infoviaggi_archeologia"
                                                                       @if(isset($infoviaggi->archeologia) and $infoviaggi->archeologia == 1)
                                                                       checked
                                                                       @endif
                                                                       value="1">
                                                                Archeologia
                                                            </label>
                                                        </div>
                                                        <div class="checkbox">
                                                            <label>
                                                                <input type="checkbox" name="infoviaggi_avventura"
                                                                       @if(isset($infoviaggi->avventura) and $infoviaggi->avventura == 1)
                                                                       checked
                                                                       @endif
                                                                       value="1">
                                                                Avventura
                                                            </label>
                                                        </div>
                                                        <div class="checkbox">
                                                            <label>
                                                                <input type="checkbox" name="infoviaggi_cultura"
                                                                       @if(isset($infoviaggi->cultura) and $infoviaggi->cultura == 1)
                                                                       checked
                                                                       @endif
                                                                       value="1">
                                                                Cultura
                                                            </label>
                                                        </div>
                                                        <div class="checkbox">
                                                            <label>
                                                                <input type="checkbox" name="infoviaggi_ecoturismo"
                                                                       @if(isset($infoviaggi->ecoturismo) and $infoviaggi->ecoturismo == 1)
                                                                       checked
                                                                       @endif
                                                                       value="1">
                                                                Ecoturismo
                                                            </label>
                                                        </div>

                                                    </div>
                                                    <div class="col-lg-4">
                                                        <div class="checkbox">
                                                            <label>
                                                                <input type="checkbox"
                                                                       name="infoviaggi_enogastronomia"
                                                                       @if(isset($infoviaggi->enograstronomia) and $infoviaggi->enogastronomia == 1)
                                                                       checked
                                                                       @endif
                                                                       value="1">
                                                                Enogastronomia
                                                            </label>
                                                        </div>
                                                        <div class="checkbox">
                                                            <label>
                                                                <input type="checkbox" name="infoviaggi_fotografico"
                                                                       @if(isset($infoviaggi->fotografico) and $infoviaggi->fotografico == 1)
                                                                       checked
                                                                       @endif
                                                                       value="1">
                                                                Fotografico
                                                            </label>
                                                        </div>
                                                        <div class="checkbox">
                                                            <label>
                                                                <input type="checkbox" name="infoviaggi_marerelax"
                                                                       @if(isset($infoviaggi->mare_relax) and $infoviaggi->mare_relax == 1)
                                                                       checked
                                                                       @endif
                                                                       value="1">
                                                                Mare e relax
                                                            </label>
                                                        </div>
                                                        <div class="checkbox">
                                                            <label>
                                                                <input type="checkbox" name="infoviaggi_montagna"
                                                                       @if(isset($infoviaggi->montagna) and $infoviaggi->montagna == 1)
                                                                       checked
                                                                       @endif
                                                                       value="1">
                                                                Montagna
                                                            </label>
                                                        </div>

                                                    </div>
                                                    <div class="col-lg-4">
                                                        <div class="checkbox">
                                                            <label>
                                                                <input type="checkbox" name="infoviaggi_religioso"
                                                                       @if(isset($infoviaggi->religioso) and $infoviaggi->religioso == 1)
                                                                       checked
                                                                       @endif
                                                                       value="1">
                                                                Religioso
                                                            </label>
                                                        </div>
                                                        <div class="checkbox">
                                                            <label>
                                                                <input type="checkbox" name="infoviaggi_sportivo"
                                                                       @if(isset($infoviaggi->sportivo) and $infoviaggi->sportivo == 1)
                                                                       checked
                                                                       @endif
                                                                       value="1">
                                                                Sportivo
                                                            </label>
                                                        </div>
                                                        <div class="checkbox">
                                                            <label>
                                                                <input type="checkbox"
                                                                       name="infoviaggi_storicopolitico"
                                                                       @if(isset($infoviaggi->storico_politico) and $infoviaggi->storico_politico == 1)
                                                                       checked
                                                                       @endif
                                                                       value="1">
                                                                Storico Politico
                                                            </label>
                                                        </div>
                                                        <div class="checkbox">
                                                            <label>
                                                                <input type="checkbox" name="infoviaggi_altro"
                                                                       @if(isset($infoviaggi->altro) and $infoviaggi->altro == 1)
                                                                       checked
                                                                       @endif
                                                                       value="1">
                                                                Altro
                                                            </label>
                                                        </div>

                                                    </div>
                                                </div>
                                                <div class="col-lg-4">
                                                    <textarea cols="30" rows="5" class="form-control input-sm"
                                                              name="infoviaggi_tematiche_altro"
                                                              placeholder="Altre tematiche...">
                                                         {{ isset($infoviaggi->note_tematiche) ? $infoviaggi->note_tematiche : ''  }}

                                                    </textarea>
                                                </div>


                                            </div>
                                            <div class="row">
                                                <div class="col-lg-8">
                                                    <div class="form-group">
                                                        <label for="reg_input">Destinazioni Preferite</label>
                                                        <input type="text" id="infoviaggi_destinazionipreferite"
                                                               name="infoviaggi_destinazionipreferite"

                                                               value="{{ isset($infoviaggi->destinazioni_preferite) ? $infoviaggi->destinazioni_preferite : ''  }}"
                                                               class="form-control" tabindex="11">
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="reg_input">Destinazioni Passate</label>
                                                        <input type="text" id="infoviaggi_destinazionipassate"
                                                               name="infoviaggi_destinazionipassate"

                                                               value="{{ isset($infoviaggi->destinazioni_passate) ? $infoviaggi->destinazioni_passate : ''  }}"
                                                               class="form-control" tabindex="11">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div id="qasuend" class="tab-pane fade">
                                            <div class="row">

                                                <div class="col-lg-12">
                                                    <div class="form-group">
                                                        <label for="reg_input">Conoscenza Suend</label>
                                                        <input type="text" id="qasuend_conoscenza"
                                                               name="qasuend_conoscenza"

                                                               value="{{ isset($qasuend->conoscenza_suend) ? $qasuend->conoscenza_suend : ''  }}"
                                                               class="form-control">
                                                    </div>
                                                </div>

                                                <div class="col-lg-12">
                                                    <div class="form-group">
                                                        <label for="reg_input">Altre motivazioni conoscenza
                                                            Suend</label>
                                                        <input type="text" id="qasuend_altremotivazione"
                                                               name="qasuend_altremotivazione"

                                                               value="{{ isset($qasuend->note_conoscenza_suend) ? $qasuend->note_conoscenza_suend : ''  }}"
                                                               class="form-control">
                                                    </div>
                                                </div>
                                                <div class="col-lg-12">
                                                    <div class="form-group">
                                                        <label for="reg_input">Codice Cliente</label>
                                                        <input type="text" id="qasuend_codicecliente"
                                                               name="qasuend_codicecliente"
                                                               class="form-control"

                                                               value="{{ isset($qasuend->codice_cliente) ? $qasuend->codice_cliente : ''  }}"
                                                               tabindex="0">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-lg-2">
                                                    <div class="form-group">
                                                        <label for="reg_input" style="text-align: center;">Già
                                                            viaggiato
                                                            con noi</label>
                                                        <input type="checkbox" class="form-control input-sm"
                                                               name="qasuend_giaviaggiato" id="qasuend_giaviaggiato"
                                                               @if(isset($qasuend->gia_viaggiato) and $qasuend->gia_viaggiato == 1)
                                                               checked
                                                               @endif
                                                               value="true">
                                                    </div>
                                                </div>
                                                <div class="col-lg-2">
                                                    <div class="form-group">
                                                        <label for="reg_input">Quante Volte</label>
                                                        <input type="text" id="qasuend_quantiviaggi"
                                                               name="qasuend_quantiviaggi"

                                                               value="{{ isset($qasuend->quante_volte) ? $qasuend->quante_volte : ''  }}"
                                                               class="form-control"
                                                               tabindex="0">
                                                    </div>
                                                </div>
                                                <div class="col-lg-8">
                                                    <div class="form-group">
                                                        <label for="reg_input">Dove</label>
                                                        <input type="text" id="qasuend_doveviaggi"
                                                               name="qasuend_doveviaggi"

                                                               value="{{ isset($qasuend->luogo_viaggio) ? $qasuend->luogo_viaggio : ''  }}"
                                                               class="form-control"
                                                               tabindex="0">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-lg-2">
                                                    <div class="form-group">
                                                        <label for="reg_input" style="text-align: center;">Conosce
                                                            Sito</label>
                                                        <input type="checkbox" class="form-control input-sm"
                                                               name="qasuend_conoscesito" id="qasuend_conoscesito"
                                                               @if(isset($qasuend->conosce_sito) and $qasuend->conosce_sito == 1)
                                                               checked
                                                               @endif
                                                               value="true">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-lg-12">
                                                    <div class="form-group">
                                                        <label for="reg_input">Note</label>
                                                        <textarea name="qasuend_note" id="qasuend_note" cols="10"
                                                                  rows="3"
                                                                  class="form-control">
                                                         {{ isset($qasuend->note_qa_suend) ? $qasuend->note_qa_suend : ''  }}
                                                        </textarea>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div id="qaviaggio" class="tab-pane fade">
                                            <div class="row">
                                                <div class="col-lg-12">
                                                    <div class="form-group">
                                                        <label for="reg_input">Soddisfazione Generale</label>
                                                        <input type="text" id="qaviaggio_soddisfazione"
                                                               name="qaviaggio_soddisfazione"

                                                               value="{{ isset($qaviaggio->choices) ? $qaviaggio->choices : ''  }}"
                                                               class="form-control">
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="reg_input">Data Partenza</label>
                                                        <input class="form-control ts_datepicker" type="text"
                                                               name="qaviaggio_datapartenza"
                                                               id="qaviaggio_datapartenza"

                                                               value="{{ isset($qaviaggio->data_partenza) ? $qaviaggio->data_partenza : ''  }}"
                                                               placeholder="gg/mm/aaaa">
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="reg_input">Ultima Rilevazione</label>
                                                        <input class="form-control ts_datepicker" type="text"
                                                               name="qaviaggio_ultimarilevazione"
                                                               id="qaviaggio_ultimarilevazione"

                                                               value="{{ isset($qaviaggio->ultima_rilevazione) ? $qaviaggio->ultima_rilevazione : ''  }}"
                                                               placeholder="gg/mm/aaaa">
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="reg_input">Destinazione</label>
                                                        <input type="text" id="qaviaggio_destinazione"
                                                               name="qaviaggio_destinazione"

                                                               value="{{ isset($qaviaggio->destinazione) ? $qaviaggio->destinazione : ''  }}"
                                                               class="form-control">
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="reg_input">Storico</label>
                                                        <textarea cols="30" name="qaviaggio_storico"
                                                                  id="qaviaggio_storico" rows="3"
                                                                  class="form-control input-sm">
                                                            {{ isset($qaviaggio->storico) ? $qaviaggio->storico : ''  }}

                                                        </textarea>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div id="profilo" class="tab-pane fade">
                                            <div class="row">
                                                <div class="col-md-4">

                                                    <label>Data di nascita</label>
                                                    <input class="form-control ts_datepicker" type="text"

                                                           value="{{ isset($profilo->data_nascita) ? $profilo->data_nascita : ''  }}"
                                                           name="profilo_datanascita" id="profilo_datanascita">
                                                </div>
                                                <div class="col-md-8">
                                                    <div class="form-group">
                                                        <label>Luogo di nascita</label>
                                                        <input type="text" id="profilo_luogonascita"
                                                               name="profilo_luogonascita"
                                                               value="{{ isset($profilo->luogonascita) ? $profilo->luogonascita : ''  }}"

                                                               class="form-control">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-8">
                                                    <div class="form-group">
                                                        <label>Professione</label>
                                                        <input type="text" id="profilo_professione"
                                                               name="profilo_professione"
                                                               value="{{ isset($profilo->professione) ? $profilo->professione : ''  }}"

                                                               class="form-control">
                                                    </div>
                                                    <div class="form-group">
                                                        <label>Hobby 1</label>
                                                        <input type="text" id="profilo_hobby1" name="profilo_hobby1"
                                                               value="{{ isset($profilo->hobby1) ? $profilo->hobby1 : ''  }}"
                                                               class="form-control">
                                                    </div>
                                                    <div class="form-group">
                                                        <label>Hobby 2</label>
                                                        <input type="text" id="profilo_hobby2" name="profilo_hobby2"
                                                               value="{{ isset($profilo->hobby2) ? $profilo->hobby2 : ''  }}"

                                                               class="form-control">
                                                    </div>
                                                    <div class="form-group">
                                                        <label>Numero Carta Identita</label>
                                                        <input class="form-control ts_datepicker" type="text"
                                                               name="profilo_cartaidentita" id="profilo_cartaidentita"
                                                               value="{{ isset($profilo->cartaidentita) ? $profilo->cartaidentita : ''  }}"

                                                               placeholder="gg/mm/aaaa">
                                                    </div>
                                                    <div class="form-group">
                                                        <label>Scadenza Carta identita'</label>
                                                        <input class="form-control ts_datepicker" type="text"
                                                               name="profilo_datascadenzaidentita"
                                                               id="profilo_datascadenzaidentita"
                                                               value="{{ isset($profilo->scadenza_passaporto) ? $profilo->scadenza_passaporto : ''  }}"

                                                               placeholder="gg/mm/aaaa">
                                                    </div>
                                                    <div class="form-group">
                                                        <label>Numero Passaporto</label>
                                                        <input class="form-control ts_datepicker" type="text"
                                                               name="profilo_passaporto" id="profilo_passaporto"
                                                               value="{{ isset($profilo->passaporto) ? $profilo->passaporto : ''  }}"

                                                               placeholder="gg/mm/aaaa">
                                                    </div>
                                                    <div class="form-group">
                                                        <label>Scadenza passaporto</label>
                                                        <input class="form-control ts_datepicker" type="text"
                                                               name="profilo_datascadenzapassaporto"
                                                               id="profilo_datascadenzapassaporto"
                                                               value="{{ isset($profilo->scadenza_passaporto) ? $profilo->scadenza_passaporto : ''  }}"

                                                               placeholder="gg/mm/aaaa">
                                                    </div>
                                                    <div class="form-group">
                                                        <div class="checkbox">
                                                            <label>
                                                                <input type="checkbox" name="profilo_newsletter"
                                                                       @if(isset($profilo->newsletter) and $profilo->newsletter == 1)
                                                                       checked
                                                                       @endif
                                                                       value="true">
                                                                Newsletter
                                                            </label>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <div class="checkbox">
                                                            <label>
                                                                <input type="checkbox" name="profilo_clientediretto"
                                                                       @if(isset($profilo->cliente_diretto) and $profilo->cliente_diretto == 1)
                                                                       checked
                                                                       @endif
                                                                       value="true">
                                                                Cliente diretto
                                                            </label>
                                                        </div>
                                                    </div>

                                                </div>
                                                <div class="col-md-4">
                                                    <br>
                                                    <div class="form-group">
                                                        <label>Frequent Flyer</label>
                                                        <textarea cols="30" rows="20" class="form-control input-sm"
                                                                  name="profilo_frequentflyer"
                                                                  placeholder="">
                                                        {{ isset($profilo->frequent_flyer) ? $profilo->frequent_flyer : ''  }}
                                                        </textarea>
                                                    </div>
                                                </div>

                                            </div>
                                            <div class="row">
                                                <div class="col-md-8">
                                                    <div class="col-md-2">

                                                        <label>Adulti</label>
                                                        <input type="text" id="profilo_adulti" name="profilo_adulti"
                                                               value="{{ isset($profilo->adulti) ? $profilo->adulti : ''  }}"

                                                               class="form-control">
                                                    </div>
                                                    <div class="col-md-2">
                                                        <label>Bambini</label>
                                                        <input type="text" id="profilo_bambini"
                                                               name="profilo_bambini"
                                                               value="{{ isset($profilo->bambini) ? $profilo->bambini : ''  }}"

                                                               class="form-control">
                                                    </div>
                                                    <div class="col-md-2">
                                                        <label>Neonati</label>
                                                        <input type="text" id="profilo_neonati"
                                                               name="profilo_neonati"
                                                               value="{{ isset($profilo->neonati) ? $profilo->neonati : ''  }}"

                                                               class="form-control">
                                                    </div>
                                                    <div class="col-md-2">

                                                        <label>Figli</label>
                                                        <input type="text" id="profilo_figli" name="profilo_figli"
                                                               value="{{ isset($profilo->figli) ? $profilo->figli : ''  }}"

                                                               class="form-control">
                                                    </div>
                                                    <div class="col-md-2">
                                                        <label>Anziani</label>
                                                        <input type="text" id="profilo_anziani"
                                                               name="profilo_anziani"
                                                               value="{{ isset($profilo->anziani) ? $profilo->anziani : ''  }}"
                                                               class="form-control">
                                                    </div>
                                                    <div class="col-md-2">
                                                        <label>Animali</label>
                                                        <input type="text" id="profilo_animali"
                                                               name="profilo_animali"

                                                               value="{{ isset($profilo->animali) ? $profilo->animali : ''  }}"
                                                               class="form-control">
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label>Note</label>
                                                        <textarea cols="30" rows="5" class="form-control input-sm"
                                                                  name="profilo_note"
                                                                  placeholder="">
                                                           {{ isset($profilo->note) ? $profilo->note : ' '  }}
                                                        </textarea>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-8">
                                                    <div class="col-md-2">
                                                        <div class="form-group">
                                                            <div class="checkbox">
                                                                <label>
                                                                    <input type="checkbox" value="true"
                                                                           @if(isset($profilo->iscritto_club) and $profilo->iscritto_club == 1)
                                                                           checked
                                                                           @endif
                                                                           name="profilo_iscrittoclub">
                                                                    Iscritto Club
                                                                </label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-10">
                                                        <div class="form-group">
                                                            <label>Codice Club</label>
                                                            <input type="text" id="profilo_codiceclub"
                                                                   name="profilo_codiceclub"
                                                                   value="{{ isset($profilo->codice_club) ? $profilo->codice_club : ' '  }}"
                                                                   class="form-control">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>


                        <button class="btn btn-lg btn-success btn-block">Modifica Cliente</button>
                    </form>

                </div>
            </div>
        </div>
    </div>



@endsection
@section('footer-plugin')
    <!-- datatables -->
    <script src="{{ asset('/lib/DataTables/media/js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('/lib/DataTables/media/js/dataTables.bootstrap.js') }}"></script>
    <script src="{{ asset('/lib/DataTables/extensions/TableTools/js/dataTables.tableTools.min.js') }}"></script>
    <script src="{{ asset('/lib/DataTables/extensions/Scroller/js/dataTables.scroller.min.js') }}"></script>

    <!-- datatables functions -->
    <script src="{{ asset('/js/apps/tisa_datatables.js') }}"></script>
    <script>

        $('#travels_table').DataTable({
            processing: true,
            serverSide: true,
            ajax: '{!! route('travels_data', ['customer_id' => $customer->id ]) !!}',
            columns: [
                {data: 'id'},
                {data: 'choices'},
                {data: 'data_partenza'},
                {data: 'ultima_rilevazione'},
                {data: 'destinazione'},
                {data: 'Visualizza', name: 'azioni', orderable: false, searchable: false}
            ]
        });
    </script>
    <script type="text/javascript">
    debugger;
        $(document).ready(function () {

            ResetForm();
            ResetFormInfo();

            function ResetForm() {
                $("#provincia").val('');
                $("#cap").val('');
                $("#regione").val('');
                $("#stato").val('');
            }


            function ResetFormInfo() {
                $("#info-provincia2").val('');
                $("#info-cap2").val('');
                $("#info-regione2").val('');
                $("#info-stato2").val('');
            }



            $("#citta").focus(function() {
                ResetForm();
            });

            $("#info-citta2").focus(function() {
                ResetFormInfo();
            });


            var path = "{{ route('autocomplete') }}";
            var engine = new Bloodhound({
                datumTokenizer: Bloodhound.tokenizers.whitespace('username'),
                queryTokenizer: Bloodhound.tokenizers.whitespace,
                remote:{
                    url: path + '?term=%QUERY',
                    wildcard: '%QUERY'
                }
            });

            engine.initialize();

            $("#citta").typeahead({
                hint: true,
                highlight: true,
                minLength: 2
            }, {
                source: engine.ttAdapter(),
                // This will be appended to "tt-dataset-" to form the class name of the suggestion menu.
                name: 'comune',
                // the key from the array we want to display (name,id,email,etc...)
                displayKey: 'comune',
                templates: {
                    empty: [
                        '<div class="empty-message">Nessun risultato</div>'
                    ].join('\n'),

                    suggestion: function (data) {
                        return '<div class="citta-search-result"><h4>'+ data.comune +'</h4></div>'
                    }
                },

            });
            $('#citta').bind('typeahead:selected', function(obj, datum, name) {

                $("#provincia").val(datum.provincia);
                $("#cap").val(datum.cap);
                $("#regione").val(datum.regione);
                console.log((datum.cap).length);

                $("#stato").val(datum.stato);
            });



            $("#info-citta2").typeahead({
                hint: true,
                highlight: true,
                minLength: 2
            }, {
                source: engine.ttAdapter(),
                // This will be appended to "tt-dataset-" to form the class name of the suggestion menu.
                name: 'comune',
                // the key from the array we want to display (name,id,email,etc...)
                displayKey: 'comune',
                templates: {
                    empty: [
                        '<div class="empty-message">Nessun risultato</div>'
                    ].join('\n'),

                    suggestion: function (data) {
                        return '<div class="citta2-search-result"><h4>'+ data.comune +'</h4></div>'
                    }
                },

            });

            $("#info-citta2").bind('typeahead:selected', function(obj, datum, name) {

                $("#info-provincia2").val(datum.provincia);
                $("#info-cap2").val(datum.cap);
                $("#info-regione2").val(datum.regione);
                $("#info-stato2").val(datum.stato);
            });
        });
           $(document).ready(function(){
            $('input').css('text-transform','uppercase');
        }) 
    </script>


@endsection
