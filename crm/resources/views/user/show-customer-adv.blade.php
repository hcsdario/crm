@extends('layouts.master')
@section('header-page-personalization')
    <style>
        #travels_table thead,
        #travels_table th {text-align: center;}
        #travels_table tr {text-align: center;}


        #files_table thead,
        #files_table th {text-align: center;}
        #files_table tr {text-align: center;}
    </style>
@endsection
@section('content')
    <div class="page_content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">

                        <div class="panel-body">
                            <fieldset>
                                <legend><span>Clienti</span></legend>
                            </fieldset>
                            @if(session()->has('message'))
                                <div class="alert alert-success">
                                    {{ session()->get('message') }}
                                </div>
                            @endif
                            @if(session()->has('error'))
                                <div class="alert alert-danger">
                                    {{ session()->get('error') }}
                                </div>
                            @endif

                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <label for="reg_input">Codice Cliente</label>
                                        <h5>{{ $customer->codice }}</h5>
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label for="reg_input">Societa</label>
                                        <h5>{{ $customer->societa }}</h5>
                                    </div>
                                    <div class="form-group">
                                        <label for="reg_input">Cognome</label>
                                        <h5>{{ $customer->cognome }}</h5>
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label for="reg_input">Nome</label>
                                        <h5>{{ $customer->nome }}</h5>
                                    </div>
                                    <div class="form-group">
                                        <label for="reg_input">Citta'</label>
                                        <h5>{{ $customer->citta }}</h5>
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label for="reg_input">Stato</label>
                                        <h5>{{ $customer->stato }}</h5>
                                    </div>
                                    <div class="form-group">
                                        <label for="reg_input">Provincia</label>
                                        <h5>{{ $customer->provincia }}</h5>
                                    </div>

                                </div>
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label for="reg_input">Regione</label>
                                        <h5>{{ $customer->regione }}</h5>
                                    </div>

                                    <div class="form-group">
                                        <label for="reg_input">Cap</label>
                                        <h5>{{ $customer->cap }}</h5>
                                    </div>
                                </div>

                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label for="reg_input">Telefono1</label>
                                        <h5>{{ $customer->tel1 }}</h5>
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label for="reg_input">Email1</label>
                                        <h5>{{ $customer->email1 }}</h5>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>


            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-body">
                            <div class="heading_a">Suend CRM</div>
                            <div class="tabbable tabs-left">
                                <ul class="nav nav-tabs" id="tabs_c">
                                    <li class="active"><a data-toggle="tab" href="#info">Info</a></li>
                                    <li><a data-toggle="tab" href="#infoadv">InfoADV</a></li>
                                </ul>
                                <div class="tab-content" id="tabs_content_c">
                                    <div id="info" class="tab-pane fade in active">
                                        <div class="row">
                                            <div class="col-lg-8">
                                                <div class="form-group">
                                                    <label for="reg_input">Indirizzo</label>
                                                    {{ isset($info->indirizzo) ? $info->indirizzo : ' '  }}
                                                </div>
                                                <div class="form-group">
                                                    <label for="reg_input">Sito Web</label>
                                                    {{ isset($info->sito_web) ? $info->sito_web : ' '  }}
                                                </div>
                                                <div class="form-group">
                                                    <label for="reg_input">Partita Iva</label>
                                                    {{ isset($info->partita_iva) ? $info->partita_iva : ' '  }}
                                                </div>
                                                <div class="form-group">
                                                    <label for="reg_input">Codice Fiscale</label>
                                                    {{ isset($info->codice_fiscale) ? $info->codice_fiscale : ' '  }}
                                                </div>
                                                <div class="form-group">
                                                    <label for="reg_input">Data di Nascita</label>
                                                    {{ isset($info->data_nascita) ? $info->data_nascita : ' '  }}
                                                </div>
                                                <div class="form-group">
                                                    <label for="reg_input">Sesso</label>
                                                    {{ isset($info->sesso) ? $info->sesso : ' '  }}
                                                </div>
                                                <div class="form-group">
                                                    <label for="reg_input">Posizione</label>
                                                    {{ isset($info->posizione) ? $info->posizione : ' '  }}
                                                </div>
                                                <div class="form-group">
                                                    <label for="reg_input">Suffisso</label>
                                                    {{ isset($info->suffisso) ? $info->suffisso : ' '  }}
                                                </div>
                                            </div>
                                            <div class="col-lg-4">
                                                <div class="form-group">
                                                    <label for="reg_input">Telefono4</label>
                                                    {{ isset($info->tel4) ? $info->tel4 : ' '  }}
                                                </div>
                                                <div class="form-group">
                                                    <label for="reg_input">Telefono5</label>
                                                    {{ isset($info->tel5) ? $info->tel5 : ' '  }}
                                                </div>
                                                <div class="form-group">
                                                    <label for="reg_input">Fax1</label>
                                                    {{ isset($info->fax1) ? $info->fax1 : ' '  }}
                                                </div>
                                                <div class="form-group">
                                                    <label for="reg_input">Fax2</label>
                                                    {{ isset($info->fax2) ? $info->fax2 : ' '  }}
                                                </div>
                                                <div class="form-group">
                                                    <label for="reg_input">Email2</label>
                                                    {{ isset($info->email2) ? $info->email2 : ' '  }}
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-lg-8">
                                                <div class="form-group">
                                                    <label for="reg_input">Regione Indirizzo2</label>
                                                    {{ isset($info->region2) ? $info->region2 : ' '  }}
                                                </div>
                                                <div class="form-group">
                                                    <label for="reg_input">Provincia Indirizzo2</label>
                                                    {{ isset($info->indirizzo2) ? $info->indirizzo2 : ' '  }}
                                                </div>
                                                <div class="form-group">
                                                    <label for="reg_input">Cap Indirizzo2</label>
                                                    {{ isset($info->cap2) ? $info->cap2 : ' '  }}
                                                </div>
                                                <div class="form-group">
                                                    <label for="reg_input">Citta' Indirizzo2</label>
                                                    {{ isset($info->citta2) ? $info->citta2 : ' '  }}
                                                </div>
                                                <div class="form-group">
                                                    <label for="reg_input">Stato Indirizzo2</label>
                                                    {{ isset($info->stato2) ? $info->stato2 : ' '  }}
                                                </div>
                                                <div class="form-group">
                                                    <label for="reg_input">Indirizzo 2</label>
                                                    {{ isset($info->indirizzo2) ? $info->indirizzo2 : ' '  }}
                                                </div>
                                            </div>
                                            <div class="col-lg-4">
                                                <div class="form-group">
                                                    @if(isset($info) && $info->privacy == 1)
                                                        <label for="privacy" style="text-align: center;">Accettazione Privacy</label>
                                                        <input type="checkbox" class="form-control input-sm" name="info_privacy" id="info_privacy" checked  disabled >
                                                    @else
                                                        <label for="privacy" style="text-align: center;">Accettazione Privacy</label>
                                                        <input type="checkbox" class="form-control input-sm" name="info_privacy" id="info_privacy"  disabled >
                                                    @endif

                                                </div>
                                                <div class="form-group">

                                                    @if(isset($info) && $info->privacy_commerciali == 1)
                                                        <label for="privacy" style="text-align: center;">Accettazione Privacy Commerciali</label>
                                                        <input type="checkbox" class="form-control input-sm" name="info_privacy_commerciali" id="info_privacy_commerciali" checked  disabled >
                                                    @else
                                                        <label for="privacy" style="text-align: center;">Accettazione Privacy Commerciali</label>
                                                        <input type="checkbox" class="form-control input-sm" name="info_privacy_commerciali" id="info_privacy_commerciali"  disabled >
                                                    @endif
                                                </div>
                                                <div class="form-group">
                                                    @if(isset($info) && $info->privacy_profilazione == 1)
                                                        <label for="privacy" style="text-align: center;">Accettazione Privacy Profilazione</label>
                                                        <input type="checkbox" class="form-control input-sm" name="info_privacy_profilazione" id="info_privacy_commerciali" checked  disabled >
                                                    @else
                                                        <label for="privacy" style="text-align: center;">Accettazione Privacy Profilazione</label>
                                                        <input type="checkbox" class="form-control input-sm" name="info_privacy_profilazione" id="info_privacy_commerciali"  disabled >
                                                    @endif
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div id="infoadv" class="tab-pane fade">
                                        <div class="row">
                                            <div class="col-lg-12">
                                                <div class="form-group">
                                                    <label for="reg_input">Fatturato medio Annuo</label>
                                                    {{ isset($infoadv->fatturato_medio) ? $infoadv->fatturato_medio : ''  }}
                                                </div>
                                                <div class="form-group">
                                                    <label for="reg_input">Numero addetti</label>
                                                    {{ isset($infoadv->numero_addetti) ? $infoadv->numero_addetti : ''  }}
                                                </div>
                                                <div class="form-group">
                                                    <label for="reg_input">Clienti</label>
                                                    {{ isset($infoadv->clienti) ? $infoadv->clienti : ''  }}
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-lg-12">
                                                <div class="col-lg-3">
                                                    <div class="form-group">
                                                        <label for="reg_input">Destinazioni più vendute</label>
                                                        {{ isset($infoadv->destinazioni_vendute1) ? $infoadv->destinazioni_vendute1 : ''  }}
                                                    </div>
                                                </div>
                                                <div class="col-lg-3">
                                                    <div class="form-group">
                                                        <label for="reg_input">&nbsp;</label>
                                                        {{ isset($infoadv->destinazioni_vendute2) ? $infoadv->destinazioni_vendute2 : ''  }}
                                                    </div>
                                                </div>
                                                <div class="col-lg-3">
                                                    <div class="form-group">
                                                        <label for="reg_input">&nbsp;</label>
                                                        {{ isset($infoadv->destinazioni_vendute3) ? $infoadv->destinazioni_vendute3 : ''  }}
                                                    </div>
                                                </div>
                                                <div class="col-lg-3">
                                                    <div class="form-group">
                                                        <label for="reg_input">&nbsp;</label>
                                                        {{ isset($infoadv->destinazioni_vendute4) ? $infoadv->destinazioni_vendute4 : ''  }}
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-lg-12">
                                                <div class="col-lg-3">
                                                    <div class="form-group">
                                                        <label for="reg_input">&nbsp;</label>
                                                        {{ isset($infoadv->destinazioni_vendute5) ? $infoadv->destinazioni_vendute5 : ''  }}
                                                    </div>
                                                </div>
                                                <div class="col-lg-3">
                                                    <div class="form-group">
                                                        <label for="reg_input">&nbsp;</label>
                                                        {{ isset($infoadv->destinazioni_vendute6) ? $infoadv->destinazioni_vendute6 : ''  }}
                                                    </div>
                                                </div>
                                                <div class="col-lg-3">
                                                    <div class="form-group">
                                                        <label for="reg_input">&nbsp;</label>
                                                        {{ isset($infoadv->destinazioni_vendute7) ? $infoadv->destinazioni_vendute7 : ''  }}
                                                    </div>
                                                </div>
                                                <div class="col-lg-3">
                                                    <div class="form-group">
                                                        <label for="reg_input">&nbsp;</label>
                                                        {{ isset($infoadv->destinazioni_vendute8) ? $infoadv->destinazioni_vendute8 : ''  }}
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-lg-12">

                                                <div class="form-group">
                                                    <label for="reg_input">Fatturato Business Travel</label>
                                                    {{ isset($infoadv->business_travel) ? $infoadv->business_travel : ''  }}
                                                </div>
                                                <div class="form-group">
                                                    <label for="reg_input">Leisure </label>
                                                    {{ isset($infoadv->leisure) ? $infoadv->leisure : ''  }}
                                                </div>
                                                <div class="form-group">
                                                    <label for="reg_input">Biglietteria </label>
                                                    {{ isset($infoadv->biglietteria) ? $infoadv->biglietteria : ''  }}
                                                </div>

                                            </div>
                                        </div>

                                    </div>


                                </div>
                            </div>
                        </div>

                    </div>

                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-body">

                            <div class="panel panel-default">

                                <div class="panel-body">
                                    <fieldset>
                                        <legend><span>Files Associati</span></legend>
                                    </fieldset>


                                    <div class="row">
                                        <div class="col-lg-12">
                                            <table class="table table-bordered" id="files_table">
                                                <thead>
                                                <tr>
                                                    <th>Id</th>
                                                    <th>Nome File</th>
                                                    <th>Data Caricamento</th>
                                                    <th>Azioni</th>
                                                </tr>
                                                </thead>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <button class="btn btn-lg btn-success btn-block"  data-toggle="modal" data-target="#modal_upload_file">Aggiungi File Pdf</button>
                    <div class="modal fade" id="modal_add_travel">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-body">
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                         <div class="panel panel-default">
                                            <div class="panel-body">
                                                <fieldset>
                                                    <legend><span></span></legend>
                                                </fieldset>
                                                <div class="row">
                                                    <div class="col-lg-12">
                                                        <div class="form-group">
                                                            <label for="reg_input">Soddisfazione generale</label>
                                                            <input type="text" id="soddisfazione" name="soddisfazione" class="form-control" tabindex="1" required>
                                                        </div>

                                                        <div class="form-group">
                                                            <label for="reg_input">Data Partenza</label>
                                                            <input type="text" id="data_partenz" name="data_partenza" class="form-control" tabindex="2" required>
                                                        </div>

                                                        <div class="form-group">
                                                            <label for="reg_input">Ultima rilevazione</label>
                                                            <input type="text" id="ultima_rilevazione" name="ultima_rilevazione" class="form-control" tabindex="3" required>
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="reg_input">Destinazionee</label>
                                                            <input type="text" id="destinazione" name="destinazione" class="form-control" tabindex="4" required>
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="reg_input">Storico</label>
                                                            <input type="textaere" id="storico" name="storico" class="form-control" tabindex="5" required>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="modal_add_travel">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-body">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h3>Inserisci Viaggio</h3>
                    @if(session()->has('message'))
                        <div class="alert alert-success">
                            {{ session()->get('message') }}
                        </div>
                    @endif
                    @if(session()->has('error'))
                        <div class="alert alert-error">
                            {{ session()->get('error') }}
                        </div>
                    @endif
                    <form id="login_form" role="form" method="POST" action="{{ route('travel.store') }}">
                        {{ csrf_field() }}
                        <div class="panel panel-default">
                            <div class="panel-body">
                                <fieldset>
                                    <legend><span></span></legend>
                                </fieldset>
                                <input type="hidden" name="customer_id" value=" {{$customer->id}}">

                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="form-group">
                                            <label for="reg_input">Soddisfazione generale</label>
                                            <input type="text" id="soddisfazione" name="soddisfazione" class="form-control" tabindex="1" required>
                                        </div>

                                        <div class="form-group">
                                            <label for="reg_input">Data Partenza</label>
                                            <input type="text" id="data_partenz" name="data_partenza" class="form-control" tabindex="2" required>
                                        </div>

                                        <div class="form-group">
                                            <label for="reg_input">Ultima rilevazione</label>
                                            <input type="text" id="ultima_rilevazione" name="ultima_rilevazione" class="form-control" tabindex="3" required>
                                        </div>
                                        <div class="form-group">
                                            <label for="reg_input">Destinazionee</label>
                                            <input type="text" id="destinazione" name="destinazione" class="form-control" tabindex="4" required>
                                        </div>


                                        <div class="form-group">
                                            <label for="reg_input">Storico</label>
                                            <input type="textaere" id="storico" name="storico" class="form-control" tabindex="5" required>
                                        </div>


                                        <button class="btn btn-lg btn-success btn-block">Aggiungi Cliente</button>
                                    </div>

                                </div>
                            </div>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="modal_upload_file">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-body">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h3>Carica Files</h3>
                    @if(session()->has('message'))
                        <div class="alert alert-success">
                            {{ session()->get('message') }}
                        </div>
                    @endif
                    @if(session()->has('error'))
                        <div class="alert alert-error">
                            {{ session()->get('error') }}
                        </div>
                    @endif
                    <form id="files_form" role="form" method="POST" action="{{ route('file.store') }}" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        <div class="panel panel-default">
                            <div class="panel-body">

                                <input type="hidden" name="customer_id" value=" {{$customer->id}}">

                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="form-group">
                                            <label for="reg_input">Carica del File</label>
                                            <input type="file" name="file_upload" id="file_upload" />
                                        </div>

                                    </div>
                                    <button class="btn btn-lg btn-success btn-block">Upload File</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>



@endsection
@section('footer-plugin')
    <!-- datatables -->
    <script src="{{ asset('/lib/DataTables/media/js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('/lib/DataTables/media/js/dataTables.bootstrap.js') }}"></script>
    <script src="{{ asset('/lib/DataTables/extensions/TableTools/js/dataTables.tableTools.min.js') }}"></script>
    <script src="{{ asset('/lib/DataTables/extensions/Scroller/js/dataTables.scroller.min.js') }}"></script>

    <!-- datatables functions -->
    <script src="{{ asset('/js/apps/tisa_datatables.js') }}"></script>
    <script>

        $('#travels_table').DataTable({
            processing: true,
            serverSide: true,
            ajax: '{!! route('travels_data', ['customer_id' => $customer->id ]) !!}',
            columns: [
                {data: 'id'},
                {data: 'choices'},
                {data: 'data_partenza'},
                {data: 'ultima_rilevazione'},
                {data: 'destinazione'},
                {data: 'azioni', orderable: false, searchable: false}
            ]
        });
        $('#files_table').DataTable({
            processing: true,
            serverSide: true,
            ajax: '{!! route('files_data', ['customer_id' => $customer->id ]) !!}',
            columns: [
                {data: 'id'},
                {data: 'file_name'},
                {data: 'created_at'},
                {data: 'azioni', orderable: false, searchable: false}
            ]
        });
    </script>

@endsection
