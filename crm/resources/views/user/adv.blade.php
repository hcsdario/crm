@extends('layouts.master')
@section('header-page-personalization')
    <!-- page specific stylesheets -->

    <!-- datatables -->
    <link rel="stylesheet" href="{{ asset('/lib/DataTables/media/css/jquery.dataTables.min.css') }}">
    <link rel="stylesheet" href="{{ asset('/lib/DataTables/extensions/TableTools/css/dataTables.tableTools.min.css') }}">
    <link rel="stylesheet" href="{{ asset('/lib/DataTables/extensions/Scroller/css/dataTables.scroller.min.css') }}">

    <!-- main stylesheet -->
    <link href="{{ asset('/css/style.css') }}" rel="stylesheet" media="screen">

    <!-- google webfonts -->
    <link href='https://fonts.googleapis.com/css?family=Source+Sans+Pro:400&amp;subset=latin-ext,latin' rel='stylesheet' type='text/css'>


    <!-- moment.js (date library) -->
    <script src="{{ asset('/lib/moment-js/moment.min.js') }}"></script>
    <style>
        #customers_table thead,
        #customers_table th {text-align: center;}
        #customers_table tr {text-align: center;}

        #customers_adv_table thead,
        #customers_adv_table th {text-align: center;}
        #customers_adv_table tr {text-align: center;}
    </style>
@endsection
@section('content')

    <div class="page_content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">

                        <div class="panel-body">
                            <fieldset>
                                <legend><span>Agenzie di viaggio italiane</span></legend>
                            </fieldset>


                            <div class="row">
                                <div class="col-lg-12">
                                    <table class="table table-bordered" id="customers_adv_table" >
                                                <thead>
                                                    <th>Id</th>
                                                    <th>Codice</th>
                                                    <th>Societa</th>
                                                    <th>Cognome</th>
                                                    <th>Nome</th>
                                                    <th>Data di nascita</th>
                                                    <th>Citta'</th>
                                                    <th>Provincia</th>
                                                    <th>Cap</th>
                                                    <th>Regione</th>
                                                    <th>Stato</th>
                                                    <th>Telefono principale</th>
                                                    <th>Contatto telefonico</th>
                                                    <th>Contatto telefonico</th>
                                                    <th>Email</th>
                                                    <th>Indirizzo</th>
                                                    <th>Stato relazione</th>
                                                    <th>Ultima modifica</th>
                                                    <th>Azioni</th>
                                                </thead>
                                                <tbody>
                                                @foreach($aziendeItaliane as $azienda)
                                                    <tr>
                                                        <td>{{ $azienda->id }}</td>
                                                        <td>{{ $azienda->codice }}</td>
                                                        <td>{{ $azienda->societa }}</td>
                                                        <td>{{ $azienda->cognome }}</td>
                                                        <td>{{ $azienda->nome }}</td>
                                                        <td>{{ $azienda->data_nascita }}</td>
                                                        <td>{{ $azienda->citta }}</td>
                                                        <td>{{ $azienda->provincia }}</td>
                                                        <td>{{ $azienda->cap }}</td>
                                                        <td>{{ $azienda->regione }}</td>
                                                        <td>{{ $azienda->stato }}</td>
                                                        <td>{{ $azienda->tel1 }}</td>
                                                        <td>{{ $azienda->tel2 }}</td>
                                                        <td>{{ $azienda->tel3 }}</td>
                                                        <td>{{ $azienda->email1 }}</td>
                                                        <td>{{ $azienda->indirizzo2 }}</td>
                                                        <td>{{ $azienda->status }}</td>
                                                        <td>{{ $azienda->updated_at }}</td>
                                                        <td>
                                                            <a href="../admin/adv/edit/{{ $azienda->id }}" class="btn btn-xs btn-secondary"><i class="glyphicon glyphicon-edit"></i>&nbsp;Modifica</a>
                                                            <a href="#" class="btn btn-xs btn-danger news-zama" style="margin-left: 10px;" onclick="deleteRecord(this)"><i class="glyphicon glyphicon-minus"></i>&nbsp;Elimina</a>
                                                        </td>
                                                    </tr>
                                                     @endforeach                                                      
                                                </tbody>
                                    </table>
                                    <div class="addNew">
                                        <button class="btn btn-lg btn-success btn-block" ><a href="{{ route('newadv') }}">Nuova Adv Italiana</a></button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>



@endsection
@section('footer-plugin')
    <!-- datatables -->
    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.18/pdfmake.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.2.4/js/dataTables.buttons.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.2.4/js/buttons.bootstrap.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.2.4/js/buttons.print.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.2.4/js/buttons.html5.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <script src="//cdn.datatables.net/buttons/1.4.2/js/buttons.colVis.min.js"></script>
    <script src="{{ asset('/lib/DataTables/extensions/Scroller/js/dataTables.scroller.min.js') }}"></script>
    <script src="https://crm.suend.it/js/tinynav.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.perfect-scrollbar/0.8.1/js/perfect-scrollbar.jquery.min.js"></script>
    <script src="{{ asset('/js/apps/tisa_datatables.js') }}"></script>
    <script src="{{ asset('/lib/moment-js/moment.min.js') }}"></script>

    <script src="{{ asset('/lib/DataTables/media/js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('/lib/DataTables/media/js/dataTables.bootstrap.js') }}"></script>
    <script src="{{ asset('/lib/DataTables/extensions/TableTools/js/dataTables.tableTools.min.js') }}"></script>
    <script src="{{ asset('/lib/DataTables/extensions/Scroller/js/dataTables.scroller.min.js') }}"></script>
    <script src="{{ asset('/js/apps/tisa_datatables.js') }}"></script>
    
    <script>

        $('#customers_adv_table').DataTable({
            oLanguage: {sSearch: "Filtra: "},
            dom: 'Bfrtip',
            buttons: [
                { extend: 'colvis', text: 'Mostra/Nascondi colonne' }, 
                { extend: 'print', text: 'Stampa' },
                "excelHtml5",
                "pdfHtml5"
            ],
            "columnDefs": [
                {
                    "targets": [0,2,7,8,9,10,12,13,15],
                    "visible": false,
                }
            ]
        });

    window['table']="customers";
    </script>
@endsection
