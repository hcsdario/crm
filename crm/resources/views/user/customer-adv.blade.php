@extends('layouts.master')
@section('header-page-personalization')
    <style>
        .twitter-typeahead, .tt-hint, .tt-input, .tt-menu { width: 100%;  }
        .tt-menu {
            background: #ffffff;
        }
    </style>
@endsection
@section('content')
    <div class="page_content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <button class="btn btn-lg btn-success btn-block" onclick="window.location='{{ url("/user/adv") }}'">Nuovo Cliente Adv</button>
                    <form id="login_form" role="form" method="POST" action="{{ route('customer_adv.store') }}">
                    {{ csrf_field() }}

                        <div class="panel panel-default">
                            <div class="panel-body">
                                <div class="heading_a">Suend CRM</div>
                                <div class="tabbable tabs-left">
                                    <ul class="nav nav-tabs" id="tabs_c">
                                        <li class="active"><a data-toggle="tab" href="#info">Info</a></li>
                                        <li><a data-toggle="tab" href="#infosdv">Info ADV</a></li>
                                    </ul>
                                    <div class="tab-content" id="tabs_content_c">
                                        <div id="info" class="tab-pane fade in active">
                                            <div class="row">
                                                <div class="col-lg-8">
                                                    <div class="form-group" style="display: none;">
                                                        <label for="reg_input">Codice</label>
                                                        <input type="text" id="codice" name="codice" class="form-control"
                                                               tabindex="1">
                                                    </div>

                                                    <div class="form-group">
                                                        <label for="reg_input">Cognome</label>
                                                        <input type="text" id="cognome" name="cognome" class="form-control"
                                                               tabindex="1">
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="reg_input">Nome</label>
                                                        <input type="text" id="nome" name="nome" class="form-control" tabindex="2">
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="reg_input">Data di Nascita</label>
                                                        <input type="text" id="info_data_nascita" name="info_data_nascita"
                                                               class="form-control" tabindex="3">
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="reg_input">Sesso</label>
                                                        <select id="info_sesso" name="info_sesso" class="form-control"
                                                                tabindex="4">
                                                            <option value="MASCHIO">MASCHIO</option>
                                                            <option value="FEMMINA">FEMMINA</option>
                                                        </select>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="reg_input">Indirizzo</label>
                                                        <input type="text" id="info_indirizzo" name="info_indirizzo"
                                                               class="form-control" tabindex="5">
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="reg_input">Citta'</label>
                                                        <input type="text" id="citta" name="citta" class="form-control" tabindex="6" placeholder="Inserisci citta">
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="reg_input">Cap</label>
                                                        <input type="text" id="cap" name="cap" class="form-control" tabindex="7">

                                                    </div>
                                                    <div class="form-group">
                                                        <label for="reg_input">Provincia</label>
                                                        <input type="text" id="provincia" name="provincia" class="form-control" tabindex="8">
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="reg_input">Regione</label>
                                                        <input type="text" id="regione" name="regione" class="form-control" tabindex="9">

                                                    </div>
                                                    <div class="form-group">
                                                        <label for="reg_input">Stato</label>
                                                        <input type="text" id="stato" name="stato" class="form-control"
                                                               tabindex="10">
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="reg_input">Telefono1</label>
                                                        <input type="text" id="tel1" name="tel1" class="form-control" tabindex="11">
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="reg_input">Telefono2</label>
                                                        <input type="text" id="tel2" name="tel2" class="form-control" tabindex="12">
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="reg_input">Email1</label>
                                                        <input type="text" id="email1" name="email1" class="form-control"
                                                               tabindex="13">
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="reg_input">Email2</label>
                                                        <textarea id="info_mail2" name="info_mail2" class="form-control"
                                                               tabindex="14"></textarea
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="reg_input">Codice Fiscale</label>
                                                        <input type="text" id="info_codice_fiscale" name="info_codice_fiscale"
                                                               class="form-control" tabindex="15">
                                                    </div>

                                                    <select type="text" id="status" name="status" class="form-control">
                                                        <option selected disabled>Stato</option>
                                                        <option>contattato</option>
                                                        <option>da risentire</option>
                                                        <option>interessato</option>
                                                        <option>non interessato</option>
                                                        <option>cliente suend</option>
                                                    </select>

                                                </div>
                                            </div>

                                        </div>
                                        <div id="infosdv" class="tab-pane fade">
                                            <div class="row">
                                                <div class="col-lg-12">
                                                    <div class="form-group">
                                                        <label for="reg_input">Fatturato medio Annuo</label>
                                                        <input type="text" id="infoadv_fatturato"
                                                               name="infoadv_fatturato"
                                                               class="form-control">
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="reg_input">Numero addetti</label>
                                                        <input type="text" id="infoadv_addetti" name="infoadv_addetti"
                                                               class="form-control">
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="reg_input">Clienti</label>
                                                        <input type="text" id="infoadv_clienti" name="infoadv_clienti"
                                                               class="form-control">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-lg-12">
                                                    <div class="col-lg-3">
                                                        <div class="form-group">
                                                            <label for="reg_input">Destinazioni più vendute</label>
                                                            <input type="text" id="infoadv_destinazione1"
                                                                   name="infoadv_destinazione1"
                                                                   class="form-control">
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-3">
                                                        <div class="form-group">
                                                            <label for="reg_input">&nbsp;</label>
                                                            <input type="text" id="infoadv_destinazione2"
                                                                   name="infoadv_destinazione2"
                                                                   class="form-control">
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-3">
                                                        <div class="form-group">
                                                            <label for="reg_input">&nbsp;</label>
                                                            <input type="text" id="infoadv_destinazione3"
                                                                   name="infoadv_destinazione3"
                                                                   class="form-control">
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-3">
                                                        <div class="form-group">
                                                            <label for="reg_input">&nbsp;</label>
                                                            <input type="text" id="infoadv_destinazione4"
                                                                   name="infoadv_destinazione4"
                                                                   class="form-control">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-lg-12">
                                                    <div class="col-lg-3">
                                                        <div class="form-group">
                                                            <label for="reg_input">&nbsp;</label>
                                                            <input type="text" id="infoadv_destinazione5"
                                                                   name="infoadv_destinazione5"
                                                                   class="form-control">
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-3">
                                                        <div class="form-group">
                                                            <label for="reg_input">&nbsp;</label>
                                                            <input type="text" id="infoadv_destinazione6"
                                                                   name="infoadv_destinazione6"
                                                                   class="form-control">
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-3">
                                                        <div class="form-group">
                                                            <label for="reg_input">&nbsp;</label>
                                                            <input type="text" id="infoadv_destinazione7"
                                                                   name="infoadv_destinazione7"
                                                                   class="form-control">
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-3">
                                                        <div class="form-group">
                                                            <label for="reg_input">&nbsp;</label>
                                                            <input type="text" id="infoadv_destinazione8"
                                                                   name="infoadv_destinazione8"
                                                                   class="form-control">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-lg-12">

                                                    <div class="form-group">
                                                        <label for="reg_input">Fatturato Business Travel</label>
                                                        <input type="text" id="infoadv_businesstravel" name="infoadv_businesstravel"
                                                               class="form-control">
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="reg_input">Leisure </label>
                                                        <input type="text" id="infoadv_leisure" name="infoadv_leisure"
                                                               class="form-control">
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="reg_input">Biglietteria </label>
                                                        <input type="text" id="infoadv_biglietteria" name="infoadv_biglietteria"
                                                               class="form-control">
                                                    </div>

                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                                <button class="btn btn-lg btn-success btn-block">Aggiungi Cliente</button>
                            </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

@endsection
@section('footer-plugin')
    <script src="{{ asset('/lib/bootstrap-datepicker/js/bootstrap-datepicker.js') }}"></script>



    <script type="text/javascript">
        $(document).ready(function () {

            ResetForm();
            ResetFormInfo();

            function ResetForm() {

                $("#provincia").val('');
                $("#cap").val('');
                $("#regione").val('');
                $("#stato").val('');
            }


            function ResetFormInfo() {

                $("#info-provincia2").val('');
                $("#info-cap2").val('');
                $("#info-regione2").val('');
                $("#info-stato2").val('');
            }


            var defaultText = '';
            $("#citta").focus(function() {
            //    ResetForm();
            });

            $("#info-citta2").focus(function() {
             //   ResetFormInfo();
            });


            var path = "{{ route('autocomplete') }}";
            var engine = new Bloodhound({
                datumTokenizer: Bloodhound.tokenizers.whitespace('username'),
                queryTokenizer: Bloodhound.tokenizers.whitespace,
                remote:{
                    url: path + '?term=%QUERY',
                    wildcard: '%QUERY'
                }
            });

            engine.initialize();

            $("#citta").typeahead({
                hint: true,
                highlight: true,
                minLength: 2
            }, {
                source: engine.ttAdapter(),
                // This will be appended to "tt-dataset-" to form the class name of the suggestion menu.
                name: 'comune',
                // the key from the array we want to display (name,id,email,etc...)
                displayKey: 'comune',
                templates: {
                    empty: [
                        '<div class="empty-message">Nessun risultato</div>'
                    ].join('\n'),

                    suggestion: function (data) {
                        return '<div class="citta-search-result"><h4>'+ data.comune +'</h4></div>'
                    }
                },

            });
            var lastAvaibleId=Number('<?php echo $lastId?>');
            $('#citta').bind('typeahead:selected', function(obj, datum, name) {
                $("#provincia").val(datum.provincia);
                $("#cap").val(datum.cap);
                $("#regione").val(datum.regione);
                $("#stato").val(datum.stato);
                $("#codice").val(datum.sigla+"-"+lastAvaibleId);
            });



            $("#info-citta2").typeahead({
                hint: true,
                highlight: true,
                minLength: 2
            }, {
                source: engine.ttAdapter(),
                // This will be appended to "tt-dataset-" to form the class name of the suggestion menu.
                name: 'comune',
                // the key from the array we want to display (name,id,email,etc...)
                displayKey: 'comune',
                templates: {
                    empty: [
                        '<div class="empty-message">Nessun risultato</div>'
                    ].join('\n'),

                    suggestion: function (data) {
                        return '<div class="citta2-search-result"><h4>'+ data.comune +'</h4></div>'
                    }
                },

            });

            $("#info-citta2").bind('typeahead:selected', function(obj, datum, name) {

                $("#info-provincia2").val(datum.provincia);
                $("#info-cap2").val(datum.cap);
                $("#info-regione2").val(datum.regione);
                $("#info-stato2").val(datum.stato);
            });
        });
      window['table']="customers";
              $(document).ready(function(){
                $('input').css('text-transform','uppercase');
                var navbar=$("#side_nav").detach();
                $("body").append(navbar);
            })
    </script>
@endsection
