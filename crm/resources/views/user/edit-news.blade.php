@extends('layouts.master')
@section('header-page-personalization')
    <style>
        #news_table thead,
        #news_table th {
            text-align: center;
        }

        #news_table tr {
            text-align: center;
        }

        .disabled {
            pointer-events: none;
            cursor: default;
        }
    </style>
@endsection
@section('content')
    <div class="page_content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <form id="login_form" role="form" method="POST" action="{{ route('news.store') }}">
                        {{ csrf_field() }}
                        <div class="panel panel-default">

                            <div class="panel-body">
                                <fieldset>
                                    <legend><span>Viaggio</span></legend>
                                </fieldset>
                                @if(session()->has('message'))
                                    <div class="alert alert-success">
                                        {{ session()->get('message') }}
                                    </div>
                                @endif
                                @if(session()->has('error'))
                                    <div class="alert alert-error">
                                        {{ session()->get('error') }}
                                    </div>
                                @endif
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="form-group">
                                            <label for="reg_input">Titolo News</label>
                                            <input type="text" id="news_title" name="news_title" class="form-control" value="{{$news->news_title}}">
                                        </div>
                                    </div>


                                             <?php 
                                                $error=false;
                                                $userid = Auth::user()->id;
                                                if($news->userid!=$userid)
                                                    $error=true;
                                                $zone=DB::select("SELECT userZone FROM `newsUserZone` WHERE userid='$userid'");
                                            if(empty($zone))
                                                $zone="Tutte le zone";
                                             else
                                                $zone=$zone[0]->userZone;
                                            ?>
                                        <input type="hidden" id="id" name="id" value="{{$news->id}}">
                                        <input type="hidden" id="zone" name="zone" value="{{$news->zone}}">
                                        <input type="hidden" id="userid" name="userid" value="{{$news->userid}}">

                                                                           
                                    <div class="col-lg-12">
                                        <div class="form-group pull-left">
                                            <label for="Evidenza" style="text-align: center;">
                                                News in evidenza
                                            </label>

                                            <input type="checkbox" class="form-control input-sm" name="evidenza_news" id="evidenza_news" 
                                                <?php if($news->evidenza==1)
                                                        echo "checked";
                                                ?>
                                            >
                                        </div>
                                    </div>
                                    <div class="col-lg-12">
                                        <div class="panel panel-default">
                                            <div class="panel-heading">Testo della news</div>
                                            <div class="panel_body_a">
                                            <textarea name="wysiwg_full" id="wysiwg_full" cols="30" rows="4"
                                                      class="form-control" value="{{$news->news_body}}">
                                                      {{$news->news_body}}

                                            </textarea>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                        <button class="btn btn-lg btn-success btn-block">Modifica News</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
 
<script type="text/javascript">
    var error='<?php echo $error?>';
    if(eval(error))
        window.location.href="https://crm.suend.it/user/news";
</script>


@endsection
@section('footer-plugin')
    <!-- wysiwg editor -->
    <script src="{{ asset('/lib/ckeditor/ckeditor.js') }}"></script>
    <script src="{{ asset('/lib/ckeditor/adapters/jquery.js') }}"></script>
    <!-- wizard functions -->
    <script src="{{ asset('/js/apps/tisa_wysiwg.js') }}"></script>
    <!-- datatables -->
    <script src="{{ asset('/lib/DataTables/media/js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('/lib/DataTables/media/js/dataTables.bootstrap.js') }}"></script>
    <script src="{{ asset('/lib/DataTables/extensions/TableTools/js/dataTables.tableTools.min.js') }}"></script>
    <script src="{{ asset('/lib/DataTables/extensions/Scroller/js/dataTables.scroller.min.js') }}"></script>

    <!-- datatables functions -->
    <script src="{{ asset('/js/apps/tisa_datatables.js') }}"></script>
@endsection
