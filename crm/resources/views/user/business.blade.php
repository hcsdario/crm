@extends('layouts.master')
@section('header-page-personalization')
    <link href="{{ asset('/css/style.css') }}" rel="stylesheet" media="screen">
    <link href='https://fonts.googleapis.com/css?family=Source+Sans+Pro:400&amp;subset=latin-ext,latin' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="{{ asset('/lib/DataTables/media/css/jquery.dataTables.min.css') }}">
    
    <style>
        #customers_table thead,
        #customers_table th {text-align: center;}
        #customers_table tr {text-align: center;}

        #customers_adv_table thead,
        #customers_adv_table th {text-align: center;}
        #customers_adv_table tr {text-align: center;}
        .hideBecauseDatatablesSucks{
          /*  display: none;*/
        }
        th{
            max-width: 125px;
        }
        td a{
            margin: 5px;
        }
    </style>
@endsection
@section('content')

    <div class="page_content page_business">
        <div class="container-fluid">
            <div class="row">

                        <div class="panel panel-default">
                            <div class="panel-body">
                                <fieldset>
                                    <legend><span class="big-title">Elenco Aziende</span></legend>
                                </fieldset>
                                <div class="row">
                                    <div class="col-lg-12">
                                            <table class="table table-bordered aziende table-striped" id="customers_table_ext"  data-tablename="business">
                                                <thead>
                                                    <th>Codice</th>
                                                    <th>Azienda</th>
                                                    <th>Codice Fiscale/ Partita iva</th>
                                                    <th>Ragione sociale</th>
                                                    <th>Stato</th>
                                                    <th>Fisso</th>
                                                    <th>Mail</th>
                                                    <th>Ultimo aggiornamento</th>
                                                    <th>Inserito da</th>
                                                    <th>Azioni</th>                                           
                                                </thead>
                                                <tbody>
                                            <?php 

                                                 if(!empty($_GET)) {
                                                        foreach ($aziende as $azienda) { ?>
                                                        <tr>
                                                            <td>{{$azienda->codice}}</td>
                                                            <td>{{$azienda->azienda}}</td>
                                                            <td>{{$azienda->fiscale_iva}}</td>
                                                            <td>{{$azienda->ragione}}</td>
                                                            <td>{{$azienda->status}}</td>
                                                            <td>{{$azienda->fisso}}</td>
                                                            <td>{{$azienda->mail}}</td>
                                                            <td>{{$azienda->updated_at}}</td>
                                                            <td>admin</td>
                                                            <td>
                                                                <a href="../user/business/{{$azienda->id}}?action=view" class="btn btn-xs btn-info" data-toggle="tooltip" title="Visualizza">><i class="glyphicon glyphicon-search"></i></a>
                                                                <a href="../user/business/{{$azienda->id}}" class="btn btn-xs btn-secondary"><i class="glyphicon glyphicon-edit" data-toggle="tooltip" title="Modifica"></i></a>
                                                                <a href="#" class="btn btn-xs btn-danger news-zama" style="margin-left: 10px;" onclick="deleteRecord(this,['business',{{$azienda->id}}])" data-toggle="tooltip" title="Elimina"><i class="glyphicon glyphicon-minus"></i></a>
                                                            </td>
                                                        </tr>
                                                    <?php }
                                                }
                                            ?>
                                                </tbody>
                                            </table>
                                            <div class="addNew">
                                                <button class="btn btn-lg btn-success btn-block" onclick="window.location='{{ url("/user/business") }}/0'">Aggiungi una nuova Azienda</button>
                                            </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                </div>
            </div>
        </div>
    </div>

  

@endsection
@section('footer-plugin')
    <!-- datatables -->
    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.18/pdfmake.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.2.4/js/dataTables.buttons.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.2.4/js/buttons.bootstrap.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.2.4/js/buttons.print.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.2.4/js/buttons.html5.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <script src="//cdn.datatables.net/buttons/1.4.2/js/buttons.colVis.min.js"></script>
    <script src="{{ asset('/lib/DataTables/extensions/Scroller/js/dataTables.scroller.min.js') }}"></script>
    <script src="https://crm.suend.it/js/tinynav.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.perfect-scrollbar/0.8.1/js/perfect-scrollbar.jquery.min.js"></script>
    <script src="{{ asset('/js/apps/tisa_datatables.js') }}"></script>
    <script src="{{ asset('/lib/moment-js/moment.min.js') }}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.2.0/jquery-confirm.min.js"></script>
    <script src="{{ asset('/lib/jquery.confirm/jquery-confirm.min.js') }}"></script>

    <script src="{{ asset('/lib/DataTables/media/js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('/lib/DataTables/media/js/dataTables.bootstrap.js') }}"></script>
    <script src="{{ asset('/lib/DataTables/extensions/TableTools/js/dataTables.tableTools.min.js') }}"></script>
    <script src="{{ asset('/lib/DataTables/extensions/Scroller/js/dataTables.scroller.min.js') }}"></script>
    <script src="{{ asset('/js/apps/tisa_datatables.js') }}"></script>

    
    <script>

		var my_preference=localStorage.getItem('page_length_preference');
		if(my_preference==null)
			my_preference=10;

        if(window.location.search!=""){

            var config_datatables={
                responsive: true,
                oLanguage: {sSearch: "Filtra: "},
                dom: 'Bfrtip',
                buttons: [
                { extend: 'print', text: 'Stampa' },"excelHtml5"]
            };


            if(is_tablet()||is_tablet_pro()){
                config_datatables.columnDefs=[ 
                    {
                    "targets": [0],
                    "visible": false
                    },{
                    "targets": [2],
                    "visible": false
                    },{
                    "targets": [7],
                    "visible": false
                    }
                ];
            }

            $('#customers_table_ext').DataTable(config_datatables);            
        }
        else{

            var remote_config_datatables={
                processing: true,
                serverSide: true,
                ajax: '{!! route('clienti_data_ext') !!}',
                lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "All"]],
                columns: [
                    {data: 'codice'},
                    {data: 'azienda'},
                    {data: 'fiscale_iva'},
                    {data: 'ragione'},
                    {data: 'status'},
                    {data: 'fisso'},
                    {data: 'mail'},
                    {data: 'updated_at'},
                    {data: 'insertBy'},
                    {data: 'azioni', name: 'action', orderable: false, searchable: false}
                ],
                pageLength:my_preference,
                oLanguage: {sSearch: "Filtra: "},
                responsive: true,
                dom: 'Bfrtip',
                buttons: [
                "excelHtml5" ]
            };

            if(is_tablet()||is_tablet_pro()){
                remote_config_datatables.columnDefs=[ 
                    {
                    "targets": [0],
                    "visible": false
                    },{
                    "targets": [2],
                    "visible": false
                    },{
                    "targets": [7],
                    "visible": false
                    }
                ];
            }

            var table=$('#customers_table_ext').DataTable(remote_config_datatables);
            table.ajax.url( '{!! route('clienti_data_ext') !!}' ).load(function(){
                $('[data-toggle="tooltip"]').tooltip(); 
            });
        }

    window['table']="dataInTable";

    function add_page(el){
    	localStorage.setItem('page_length_preference',el.value);
    	window.location.reload(true);
    }

    function get_page_length(my_preference){
		var page_content=
		'<div class="dataTables_length" id="example_length" style="float: left;"><label>'+
		'Showing <select onchange="add_page(this)" name="example_length" aria-controls="example" class="">';
		var dim=[10,25,50,100,250,500,1000,2000];

		for(var i =0;i<dim.length;i++){
			if(my_preference==dim[i])
				page_content+='<option value="'+dim[i]+'" selected>'+dim[i]+'</option>'    	
			else
				page_content+='<option value="'+dim[i]+'">'+dim[i]+'</option>'    	
		}

		page_content+='</select> entries </label></div>'  ;
		return page_content; 	
    }

    $(document).ready(function(){
        $('[data-toggle="tooltip"]').tooltip(); 
        var pl=get_page_length(my_preference);
        $(".btn-default.buttons-excel.buttons-html5").after(pl);
    });

    console.error("user/business");
    </script>
@endsection
