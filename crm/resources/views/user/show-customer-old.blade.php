@extends('layouts.master')
@section('header-page-personalization')
    <style>
        #travels_table thead,
        #travels_table th {text-align: center;}
        #travels_table tr {text-align: center;}


        #files_table thead,
        #files_table th {text-align: center;}
        #files_table tr {text-align: center;}
    </style>
@endsection
@section('content')
    <div class="page_content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">

                        <div class="panel-body">
                            <fieldset>
                                <legend><span>Clienti</span></legend>
                            </fieldset>
                            @if(session()->has('message'))
                                <div class="alert alert-success">
                                    {{ session()->get('message') }}
                                </div>
                            @endif
                            @if(session()->has('error'))
                                <div class="alert alert-danger">
                                    {{ session()->get('error') }}
                                </div>
                            @endif

                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <label for="reg_input">Codice Cliente</label>
                                        <h5>{{ $customer->codice }}</h5>
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label for="reg_input">Societa</label>
                                        <h5>{{ $customer->societa }}</h5>
                                    </div>
                                    <div class="form-group">
                                        <label for="reg_input">Cognome</label>
                                        <h5>{{ $customer->cognome }}</h5>
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label for="reg_input">Nome</label>
                                        <h5>{{ $customer->nome }}</h5>
                                    </div>
                                    <div class="form-group">
                                        <label for="reg_input">Citta'</label>
                                        <h5>{{ $customer->citta }}</h5>
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label for="reg_input">Stato</label>
                                        <h5>{{ $customer->stato }}</h5>
                                    </div>
                                    <div class="form-group">
                                        <label for="reg_input">Provincia</label>
                                        <h5>{{ $customer->provincia }}</h5>
                                    </div>

                                </div>
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label for="reg_input">Regione</label>
                                        <h5>{{ $customer->regione }}</h5>
                                    </div>

                                    <div class="form-group">
                                        <label for="reg_input">Cap</label>
                                        <h5>{{ $customer->cap }}</h5>
                                    </div>
                                </div>

                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label for="reg_input">Telefono1</label>
                                        <h5>{{ $customer->tel1 }}</h5>
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label for="reg_input">Email1</label>
                                        <h5>{{ $customer->email1 }}</h5>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-body">
                            <fieldset>
                                <legend><span>Info</span></legend>
                            </fieldset>

                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label for="reg_input">Indirizzo</label>
                                    <h5>{{ $customer->indirizzo }}</h5>
                                </div>
                                <div class="form-group">
                                    <label for="reg_input">Sito Web</label>
                                    <h5>{{ $customer->sito_web }}</h5>
                                </div>
                            </div>

                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label for="reg_input">Partita Iva</label>
                                    <h5>{{ $customer->partita_iva }}</h5>
                                </div>
                                <div class="form-group">
                                    <label for="reg_input">Codice Fiscale</label>
                                    <h5>{{ $customer->codice_fiscale }}</h5>
                                </div>
                            </div>

                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label for="reg_input">Data di Nascita</label>
                                    <h5>{{ $customer->data_nascita }}</h5>
                                </div>
                                <div class="form-group">
                                    <label for="reg_input">Sesso</label>
                                    <h5>{{ $customer->sesso }}</h5>
                                </div>
                            </div>

                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label for="reg_input">Telefono2</label>
                                    <h5>{{ $customer->tel2 }}</h5>
                                </div>
                                <div class="form-group">
                                    <label for="reg_input">Email2</label>
                                    <h5>{{ $customer->email2 }}</h5>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="form-group">
                                        @if( $customer->privacy_profilazione == 1)
                                            <label for="privacy" style="text-align: center;">Accettazione Privacy</label>
                                            <input type="checkbox" class="form-control input-sm" name="privacy" id="privacy" checked  disabled >
                                        @else
                                            <label for="privacy" style="text-align: center;">Accettazione Privacy</label>
                                            <input type="checkbox" class="form-control input-sm" name="privacy" id="privacy"  disabled >
                                        @endif
                                    </div>
                                </div>
                                <div class="col-lg-12">
                                    <div class="form-group">
                                        @if( $customer->privacy_commerciali == 1)
                                            <label for="privacy_commerciali" style="text-align: center;">Accettazione Privacy Commerciale</label>
                                            <input type="checkbox" class="form-control input-sm" name="privacy_commerciali" id="privacy_commerciali" checked  disabled >
                                        @else
                                            <label for="privacy_commerciali" style="text-align: center;">Accettazione Privacy Commerciale</label>
                                            <input type="checkbox" class="form-control input-sm" name="privacy_commerciali" id="privacy_commerciali"  disabled >
                                        @endif
                                    </div>
                                </div>
                                <div class="col-lg-12">
                                    <div class="form-group">
                                        @if( $customer->privacy_profilazione == 1)
                                            <label for="privacy_profilazione" style="text-align: center;">Accettazione Privacy Profilazione</label>
                                            <input type="checkbox" class="form-control input-sm" name="privacy_profilazione" id="privacy_profilazione" checked  disabled >
                                        @else
                                            <label for="privacy_profilazione" style="text-align: center;">Accettazione Privacy Profilazione</label>
                                            <input type="checkbox" class="form-control input-sm" name="privacy_profilazione" id="privacy_profilazione"  disabled >
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-body">

                            <div class="panel panel-default">

                                <div class="panel-body">
                                    <fieldset>
                                        <legend><span>Viaggi Effettuati</span></legend>
                                    </fieldset>


                                    <div class="row">
                                        <div class="col-lg-12">
                                            <table class="table table-bordered" id="travels_table">
                                                <thead>
                                                <tr>
                                                    <th>Id</th>
                                                    <th>Soddisfazione Generale</th>
                                                    <th>Data Partenza</th>
                                                    <th>Ultima Rilevazione</th>
                                                    <th>Destinazione</th>
                                                    <th>Visualizza</th>

                                                </tr>
                                                </thead>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <button class="btn btn-lg btn-success btn-block"  data-toggle="modal" data-target="#modal_add_travel">Aggiungi Viaggio</button>

                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-body">

                            <div class="panel panel-default">

                                <div class="panel-body">
                                    <fieldset>
                                        <legend><span>Files Associati</span></legend>
                                    </fieldset>


                                    <div class="row">
                                        <div class="col-lg-12">
                                            <table class="table table-bordered" id="files_table">
                                                <thead>
                                                <tr>
                                                    <th>Id</th>
                                                    <th>Nome File</th>
                                                    <th>Data Caricamento</th>
                                                    <th>Azioni</th>
                                                </tr>
                                                </thead>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <button class="btn btn-lg btn-success btn-block"  data-toggle="modal" data-target="#modal_upload_file">Aggiungi File Pdf</button>
                    <div class="modal fade" id="modal_add_travel">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-body">
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                         <div class="panel panel-default">
                                            <div class="panel-body">
                                                <fieldset>
                                                    <legend><span></span></legend>
                                                </fieldset>
                                                <div class="row">
                                                    <div class="col-lg-12">
                                                        <div class="form-group">
                                                            <label for="reg_input">Soddisfazione generale</label>
                                                            <input type="text" id="soddisfazione" name="soddisfazione" class="form-control" tabindex="1" required>
                                                        </div>

                                                        <div class="form-group">
                                                            <label for="reg_input">Data Partenza</label>
                                                            <input type="text" id="data_partenz" name="data_partenza" class="form-control" tabindex="2" required>
                                                        </div>

                                                        <div class="form-group">
                                                            <label for="reg_input">Ultima rilevazione</label>
                                                            <input type="text" id="ultima_rilevazione" name="ultima_rilevazione" class="form-control" tabindex="3" required>
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="reg_input">Destinazionee</label>
                                                            <input type="text" id="destinazione" name="destinazione" class="form-control" tabindex="4" required>
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="reg_input">Storico</label>
                                                            <input type="textaere" id="storico" name="storico" class="form-control" tabindex="5" required>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="modal_add_travel">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-body">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h3>Inserisci Viaggio</h3>
                    @if(session()->has('message'))
                        <div class="alert alert-success">
                            {{ session()->get('message') }}
                        </div>
                    @endif
                    @if(session()->has('error'))
                        <div class="alert alert-error">
                            {{ session()->get('error') }}
                        </div>
                    @endif
                    <form id="login_form" role="form" method="POST" action="{{ route('travel.store') }}">
                        {{ csrf_field() }}
                        <div class="panel panel-default">
                            <div class="panel-body">
                                <fieldset>
                                    <legend><span></span></legend>
                                </fieldset>
                                <input type="hidden" name="customer_id" value=" {{$customer->id}}">

                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="form-group">
                                            <label for="reg_input">Soddisfazione generale</label>
                                            <input type="text" id="soddisfazione" name="soddisfazione" class="form-control" tabindex="1" required>
                                        </div>

                                        <div class="form-group">
                                            <label for="reg_input">Data Partenza</label>
                                            <input type="text" id="data_partenz" name="data_partenza" class="form-control" tabindex="2" required>
                                        </div>

                                        <div class="form-group">
                                            <label for="reg_input">Ultima rilevazione</label>
                                            <input type="text" id="ultima_rilevazione" name="ultima_rilevazione" class="form-control" tabindex="3" required>
                                        </div>
                                        <div class="form-group">
                                            <label for="reg_input">Destinazionee</label>
                                            <input type="text" id="destinazione" name="destinazione" class="form-control" tabindex="4" required>
                                        </div>


                                        <div class="form-group">
                                            <label for="reg_input">Storico</label>
                                            <input type="textaere" id="storico" name="storico" class="form-control" tabindex="5" required>
                                        </div>


                                        <button class="btn btn-lg btn-success btn-block">Aggiungi Cliente</button>
                                    </div>

                                </div>
                            </div>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="modal_upload_file">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-body">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h3>Carica Files</h3>
                    @if(session()->has('message'))
                        <div class="alert alert-success">
                            {{ session()->get('message') }}
                        </div>
                    @endif
                    @if(session()->has('error'))
                        <div class="alert alert-error">
                            {{ session()->get('error') }}
                        </div>
                    @endif
                    <form id="files_form" role="form" method="POST" action="{{ route('file.store') }}" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        <div class="panel panel-default">
                            <div class="panel-body">

                                <input type="hidden" name="customer_id" value=" {{$customer->id}}">

                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="form-group">
                                            <label for="reg_input">Carica del File</label>
                                            <input type="file" name="file_upload" id="file_upload" />
                                        </div>

                                    </div>
                                    <button class="btn btn-lg btn-success btn-block">Upload File</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>



@endsection
@section('footer-plugin')
    <!-- datatables -->
    <script src="{{ asset('/lib/DataTables/media/js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('/lib/DataTables/media/js/dataTables.bootstrap.js') }}"></script>
    <script src="{{ asset('/lib/DataTables/extensions/TableTools/js/dataTables.tableTools.min.js') }}"></script>
    <script src="{{ asset('/lib/DataTables/extensions/Scroller/js/dataTables.scroller.min.js') }}"></script>

    <!-- datatables functions -->
    <script src="{{ asset('/js/apps/tisa_datatables.js') }}"></script>
    <script>

        $('#travels_table').DataTable({
            processing: true,
            serverSide: true,
            ajax: '{!! route('travels_data', ['customer_id' => $customer->id ]) !!}',
            columns: [
                {data: 'id'},
                {data: 'choices'},
                {data: 'data_partenza'},
                {data: 'ultima_rilevazione'},
                {data: 'destinazione'},
                {data: 'azioni', orderable: false, searchable: false}
            ]
        });
        $('#files_table').DataTable({
            processing: true,
            serverSide: true,
            ajax: '{!! route('files_data', ['customer_id' => $customer->id ]) !!}',
            columns: [
                {data: 'id'},
                {data: 'file_name'},
                {data: 'created_at'},
                {data: 'azioni', orderable: false, searchable: false}
            ]
        });
    </script>

@endsection
