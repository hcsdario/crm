@extends('layouts.master')
@section('header-page-personalization')
    <style>
        .twitter-typeahead, .tt-hint, .tt-input, .tt-menu { width: 100%;  }
        .tt-menu {
            background: #ffffff;
        }
  button.accordion {
    background-color: #eee;
    color: #444;
    cursor: pointer;
    padding: 18px;
    width: 100%;
    border: none;
    text-align: left;
    outline: none;
    font-size: 15px;
    transition: 0.4s;
}

button.accordion.active, button.accordion:hover {
    background-color: #ddd;
}

button.accordion:after {
    content: '\002B';
    color: #777;
    font-weight: bold;
    float: right;
    margin-left: 5px;
}

button.accordion.active:after {
    content: "\2212";
}

div.panelacc {
    padding: 0 18px;
    background-color: white;
    max-height: 0;
    overflow: hidden;
    transition: max-height 0.2s ease-out;
}      
    </style>
@endsection
@section('content')

    <div class="page_content">
        <div class="container-fluid">
                           @if(session()->has('message'))
                                <div class="alert alert-success">
                                    {{ session()->get('message') }}
                                </div>
                            @endif         
            <div class="row">
                <div class="col-lg-12">
                  <form id="register_form" role="form" method="POST" action="{{route('reparti.store')}}">
                      {{ csrf_field() }}
                      <h1 class="login_heading">Crea un Nuovo reparto </span></h1>
                      <div class="form-group">
                          <label for="register_username">Descrizione</label>
                          <input type="hidden" name="id" value="<?php if(isset($reparto)&&isset($reparto['id'])&&!empty($reparto['id'])) echo $reparto['id']; else echo '-1'; ?>">
                          <input type="text" class="form-control input-lg" name="name" id="name" value="<?php if(isset($reparto)&&isset($reparto['reparti'])&&!empty($reparto['reparti'])) echo $reparto['reparti'] ?>" required
                                 autofocus>
                      </div>

                      <div class="submit_section">
                          <button type="submit" class="btn btn-lg btn-success btn-block confirmsave">Salva</button>

                      </div>
                  </form>
                </div>

            </div>
        </div>


        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                        <div class="panel panel-default">

                            <div class="panel-body">
                                <fieldset>
                                    <legend><span>Reparti</span></legend>
                                    <p>Gestisci i reparti della piattaforma</p>
                                </fieldset>
                                <div class="row">
                                    <div class="col-lg-12">



      <table class="table table-bordered" id="user_table" >
        <thead>

          <tr>
            <th>Descrizione</th>
            <th>Ultima modifica</th>
            <th>Modifica</th>
            <th>Elimina</th>
          </tr>

        </thead>

        <tbody>

        @foreach ($reparti as $reparto)
          <tr>
            <td style="display: none;">{{$reparto->id}}</td>
            <td>{{$reparto->reparti}}</td>
            <td>{{$reparto->updated_at}}</td>
            <?php if($reparto->id!=1){ ?>
            <?php if(isset($isedit)){ ?>
              <td><a href="{{$reparto->id}}" class="btn btn-xs btn-secondary"><i class="glyphicon glyphicon-edit"></i>&nbsp;Modifica</a></td>
            <?php }else { ?>
              <td><a href="reparti/{{$reparto->id}}" class="btn btn-xs btn-secondary"><i class="glyphicon glyphicon-edit"></i>&nbsp;Modifica</a></td>
            <?php } ?>
              <td><a href="#" class="btn btn-xs btn-danger news-zama" style="margin-left: 10px;" onclick="deleteRecord(this)"><i class="glyphicon glyphicon-minus" class="disabled"></i>&nbsp;Elimina</a></td>
            <?php }else { ?>
              <td><p>Non è possibile modificare questo gruppo</p></td>
              <td><p>Non è possibile eliminare questo gruppo</p></td>
            <?php } ?>
          </tr>
        @endforeach

        </tbody>

      </table>

                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-lg-12">
<style type="text/css">
  #lost th, #lost td{
    width: 150px;
    text-align: center;
  }
</style>
      <div class="row">
        <div class="col-lg-12">
        <h1>Appartenenza degli utenti ai reparti </h1>
        <?php 
          if(!isset($utentiIngruppo))
            $utentiIngruppo=[];
          $utentiIngruppoFormattati=[];
          for($i=0;$i<count($utentiIngruppo);$i++)
              $utentiIngruppoFormattati[$utentiIngruppo[$i]->repId]=explode(",", $utentiIngruppo[$i]->userIdList);

        

        ?>
        @foreach ($reparti as $reparto)

        <button class="accordion">{{$reparto->reparti}} <span>(<?php if(isset($utentiIngruppoFormattati[$reparto->id])) echo count($utentiIngruppoFormattati[$reparto->id]); else echo "0 Utenti"; ?>
        <?php if(isset($utentiIngruppoFormattati[$reparto->id])&&count($utentiIngruppoFormattati[$reparto->id])!=1) echo "Utenti"; else echo "Utente"; ?>
        )</span></button>
        <div class="panelacc">
          <table id="lost">
          <thead>
            <th>Nome utente</th>
            <th>Appartiene a questo gruppo?</th>
          </thead>
            <tbody>
              @foreach ($allusers as $user)
              <tr>

                <td>{{$user->name}}</td>
                <?php if(isset($utentiIngruppoFormattati[$reparto->id])&&in_array($user->id, $utentiIngruppoFormattati[$reparto->id])){ ?>
                  <td><input type="checkbox" name="isInGroup" data-userid="{{$user->id}}" data-group="{{$reparto->id}}" checked onclick="removeGroup(this)"></td>
                <?php }else{ ?>
                <td><input type="checkbox" name="isInGroup" data-userid="{{$user->id}}" data-group="{{$reparto->id}}" onclick="addGroup(this)"></td>
                <?php } ?>

              </tr>
               @endforeach
            </tbody>
          </table>
        </div>
          @endforeach
        </div>
      </div>
  </div>
</div>



<script>

var acc = document.getElementsByClassName("accordion");
var i;

for (i = 0; i < acc.length; i++) {
  acc[i].onclick = function() {
    this.classList.toggle("active");
    var panel = this.nextElementSibling;
    if (panel.style.maxHeight){
      panel.style.maxHeight = null;
    } else {
      panel.style.maxHeight = panel.scrollHeight + "px";
    } 
  }
}
</script>

                            </div>
                        </div>
                </div>
            </div>
        </div>

    </div>
@endsection
@section('footer-plugin')
<script type="text/javascript">
  window['table']="reparti";


var apiUrl="/";
function addGroup(el){
  var data=$(el).data();

  $.ajax({
         url: apiUrl+'api.php',        
         type: "POST",
         data: { qr:'addGroup', userid:data.userid,group:data.group},
         success: function(res){
        debugger;
    },
    error: function(){
      console.log("ajax error ");

    }
  });   
}

function removeGroup(el){
  var data=$(el).data();

  $.ajax({
         url: apiUrl+'api.php',        
         type: "POST",
         data: { qr:'removeGroup', userid:data.userid,group:data.group},
         success: function(res){
        debugger;
    },
    error: function(){
      console.log("ajax error ");

    }
  });   
}

</script>
@endsection