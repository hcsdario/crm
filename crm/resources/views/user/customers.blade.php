@extends('layouts.master')
@section('header-page-personalization')
    <!-- page specific stylesheets -->
<?php 
ini_set('get_max_size' , '20M');
?>
    <link href="{{ asset('/css/style.css') }}" rel="stylesheet" media="screen">

    <!-- google webfonts -->
    <link href='https://fonts.googleapis.com/css?family=Source+Sans+Pro:400&amp;subset=latin-ext,latin' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="{{ asset('/lib/DataTables/media/css/jquery.dataTables.min.css') }}">
    
    <style>
        #customers_table thead,
        #customers_table th {text-align: center;}
        #customers_table tr {text-align: center;}

        #customers_adv_table thead,
        #customers_adv_table th {text-align: center;}
        #customers_adv_table tr {text-align: center;}
        .hideBecauseDatatablesSucks{
          /*  display: none;*/
        }
    </style>
@endsection
@section('content')

    <div class="page_content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <button class="btn btn-lg btn-success btn-block" onclick="window.location='{{ url("/admin/cliente") }}'">Aggiungi un nuovo privato</button>
                        <div class="panel panel-default">
                            <div class="panel-body">
                                <fieldset>
                                    <legend><span>Privati</span></legend>
                                </fieldset>
                                <div class="row">
                                            <table class="table table-bordered privati" id="customers_table" data-tablename="customers">
                                                <thead>
                                                    <th class="hideBecauseDatatablesSucks">Id</th>
                                                    <th>Codice</th>
                                                    <th>Societa</th>
                                                    <th>Cognome</th>
                                                    <th>Nome</th>
                                                    <th>Data di nascita</th>
                                                    <th>Citta'</th>
                                                    <th class="hideBecauseDatatablesSucks">Provincia</th>
                                                    <th>Cap</th>
                                                    <th>Regione</th>
                                                    <th>Stato</th>
                                                    <th>Telefono principale</th>
                                                    <th class="hideBecauseDatatablesSucks">Contatto telefonico</th>
                                                    <th class="hideBecauseDatatablesSucks">Contatto telefonico</th>
                                                    <th>Email</th>
                                                    <th class="hideBecauseDatatablesSucks">Indirizzo</th>
                                                    <th>status</th>
                                                    <th class="hideBecauseDatatablesSucks">Ultimo aggiornamento</th>
                                                    <th class="hideBecauseDatatablesSucks">Professione</th>
                                                    <th class="hideBecauseDatatablesSucks">Hobby1</th>
                                                    <th class="hideBecauseDatatablesSucks">Hobby2</th>
                                                    <th class="hideBecauseDatatablesSucks">Scadenza passaporto</th>
                                                    <th class="hideBecauseDatatablesSucks">Passaporto</th>
                                                    <th class="hideBecauseDatatablesSucks">Carta d'identita</th>
                                                    <th class="hideBecauseDatatablesSucks">Scadenza carta identita</th>
                                                    <th class="hideBecauseDatatablesSucks">Frequent flyer</th>
                                                    <th class="hideBecauseDatatablesSucks">Newsletter</th>
                                                    <th class="hideBecauseDatatablesSucks">Cliente diretto</th>
                                                    <th class="hideBecauseDatatablesSucks">Adulti</th>
                                                    <th class="hideBecauseDatatablesSucks">Bambini</th>
                                                    <th class="hideBecauseDatatablesSucks">Neonati</th>
                                                    <th class="hideBecauseDatatablesSucks">Figli</th>
                                                    <th class="hideBecauseDatatablesSucks">Anziani</th>
                                                    <th class="hideBecauseDatatablesSucks">Animali</th>
                                                    <th class="hideBecauseDatatablesSucks">Iscritto_club</th>
                                                    <th class="hideBecauseDatatablesSucks">Codice club</th>
                                                    <th class="hideBecauseDatatablesSucks">note</th>    
                                                    <th>Azioni</th>
                                                </thead>
                                                <tbody>
                                                @foreach($privati as $privato)
                                                    <tr>
                                                        <td class="hideBecauseDatatablesSucks">{{ $privato->id }}</td>
                                                        <td>{{ $privato->codice }}</td>
                                                        <td>{{ $privato->societa }}</td>
                                                        <td>{{ $privato->cognome }}</td>
                                                        <td>{{ $privato->nome }}</td>
                                                        <td>{{ $privato->data_nascita }}</td>
                                                        <td>{{ $privato->citta }}</td>
                                                        <td class="hideBecauseDatatablesSucks">{{ $privato->provincia }}</td>
                                                        <td>{{ $privato->cap }}</td>
                                                        <td>{{ $privato->regione }}</td>
                                                        <td>{{ $privato->stato }}</td>
                                                        <td>{{ $privato->tel1 }}</td>
                                                        <td class="hideBecauseDatatablesSucks">{{ $privato->tel2 }}</td>
                                                        <td class="hideBecauseDatatablesSucks">{{ $privato->tel3 }}</td>
                                                        <td>{{ $privato->email1 }}</td>
                                                        <td class="hideBecauseDatatablesSucks">{{ $privato->indirizzo2 }}</td>
                                                        <td>{{ $privato->status }}</td>
                                                        <td>{{ $privato->updated_at }}</td>
                                                        <td class="hideBecauseDatatablesSucks">{{ $privato->professione }}</td>
                                                        <td class="hideBecauseDatatablesSucks">{{ $privato->hobby1 }}</td>
                                                        <td class="hideBecauseDatatablesSucks">{{ $privato->hobby2 }}</td>
                                                        <td class="hideBecauseDatatablesSucks">{{ $privato->scadenza_passaporto }}</td>
                                                        <td class="hideBecauseDatatablesSucks">{{ $privato->passaporto }}</td>
                                                        <td class="hideBecauseDatatablesSucks">{{ $privato->carteidentita }}</td>
                                                        <td class="hideBecauseDatatablesSucks">{{ $privato->scadenza_cartaidentita }}</td>
                                                        <td class="hideBecauseDatatablesSucks">{{ $privato->frequent_flyer }}</td>
                                                        <td class="hideBecauseDatatablesSucks">{{ $privato->newsletter }}</td>
                                                        <td class="hideBecauseDatatablesSucks">{{ $privato->cliente_diretto }}</td>
                                                        <td class="hideBecauseDatatablesSucks">{{ $privato->adulti }}</td>
                                                        <td class="hideBecauseDatatablesSucks">{{ $privato->bambini }}</td>
                                                        <td class="hideBecauseDatatablesSucks">{{ $privato->neonati }}</td>
                                                        <td class="hideBecauseDatatablesSucks">{{ $privato->figli }}</td>
                                                        <td class="hideBecauseDatatablesSucks">{{ $privato->anziani }}</td>
                                                        <td class="hideBecauseDatatablesSucks">{{ $privato->animali }}</td>
                                                        <td class="hideBecauseDatatablesSucks">{{ $privato->iscritto_club }}</td>
                                                        <td class="hideBecauseDatatablesSucks">{{ $privato->codice_club }}</td>
                                                        <td class="hideBecauseDatatablesSucks">{{ $privato->note }}</td>
                                                        <td>
                                                            <a href="../admin/cliente/edit/{{ $privato->id }}" class="btn btn-xs btn-secondary"><i class="glyphicon glyphicon-edit"></i>&nbsp;Modifica</a>
                                                            <a href="#" class="btn btn-xs btn-danger news-zama" style="margin-left: 10px;" onclick="deleteRecord(this)"><i class="glyphicon glyphicon-minus"></i>&nbsp;Elimina</a>
                                                        </td>
                                                    </tr>
                                                     @endforeach                                                
                                                </tbody>
                                            </table>
                                    </div>
                                </div>
                            </div>
                        </div>

                </div>
            </div>
        </div>
    </div>

  

@endsection
@section('footer-plugin')
    <!-- datatables -->
    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.18/pdfmake.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.2.4/js/dataTables.buttons.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.2.4/js/buttons.bootstrap.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.2.4/js/buttons.print.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.2.4/js/buttons.html5.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <script src="//cdn.datatables.net/buttons/1.4.2/js/buttons.colVis.min.js"></script>
    <script src="{{ asset('/lib/DataTables/extensions/Scroller/js/dataTables.scroller.min.js') }}"></script>
    <script src="https://crm.suend.it/js/tinynav.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.perfect-scrollbar/0.8.1/js/perfect-scrollbar.jquery.min.js"></script>
    <script src="{{ asset('/js/apps/tisa_datatables.js') }}"></script>
    <script src="{{ asset('/lib/moment-js/moment.min.js') }}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.2.0/jquery-confirm.min.js"></script>
    <script src="{{ asset('/lib/jquery.confirm/jquery-confirm.min.js') }}"></script>

    <script>


        $('#customers_table').DataTable({
            "pagingType": "full_numbers",
            oLanguage: {sSearch: "Filtra: "},
            dom: 'Bfrtip',
            buttons: [
                { extend: 'colvis', text: 'Mostra/Nascondi colonne' }, 
                { extend: 'print', text: 'Stampa' },
                "excelHtml5",
                "pdfHtml5"
            ],
            "columnDefs": [
                {
                    "targets": [ 8,12,13,15,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36 ],
                    "visible": false,
                }
            ]
        });

  
    window['table']="dataInTable";
    </script>
@endsection
