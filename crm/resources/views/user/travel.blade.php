@extends('layouts.master')
@section('header-page-personalization')
@endsection
@section('content')
    <div class="page_content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <form id="login_form" role="form" method="POST" action="{{ route('travel.store') }}">
                        {{ csrf_field() }}
                        <div class="panel panel-default">

                            <div class="panel-body">
                                <fieldset>
                                    <legend><span>Viaggio</span></legend>
                                </fieldset>
                                @if(session()->has('message'))
                                    <div class="alert alert-success">
                                        {{ session()->get('message') }}
                                    </div>
                                @endif
                                @if(session()->has('error'))
                                    <div class="alert alert-error">
                                        {{ session()->get('error') }}
                                    </div>
                                @endif
                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label for="reg_input">Soddisfazione</label>
                                            <input type="text" id="societa" name="societa" class="form-control">
                                        </div>
                                        <div class="form-group">
                                            <label for="reg_input">Data Partenza</label>
                                            <input type="text" id="cognome" name="cognome" class="form-control">
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label for="reg_input">Ultima Rilevazione</label>
                                            <input type="text" id="nome" name="nome" class="form-control">
                                        </div>
                                        <div class="form-group">
                                            <label for="reg_input">Destinazione</label>
                                            <input type="text" id="citta" name="citta" class="form-control">
                                        </div>
                                    </div>
                                    <div class="col-lg-12">
                                        <div class="form-group">
                                            <label for="reg_input">Storico</label>
                                            <input type="text" id="provincia" name="provincia" class="form-control">
                                        </div>

                                    </div>

                                </div>
                            </div>
                        </div>

                        <button class="btn btn-lg btn-success btn-block">Aggiungi Viaggio</button>
                    </form>
                </div>
            </div>
        </div>
    </div>

@endsection
@section('footer-plugin')

@endsection
