@extends('layouts.master')
@section('header-page-personalization')
    <!-- page specific stylesheets -->
    <!-- nvd3 charts -->
    <link rel="stylesheet" href="{{ asset('/lib/novus-nvd3/nv.d3.min.css') }}">
    <!-- owl carousel -->
    <link rel="stylesheet" href="{{ asset('/lib/owl-carousel/owl.carousel.css') }}">

    <!-- main stylesheet -->
    <link href="{{ asset('/css/style.css" rel="stylesheet') }}" media="screen">

    <!-- google webfonts -->
    <link href='https://fonts.googleapis.com/css?family=Source+Sans+Pro:400&amp;subset=latin-ext,latin' rel='stylesheet' type='text/css'>

    <!-- moment.js (date library) -->
    <script src="{{ asset('/lib/moment-js/moment.min.js') }}"></script>
    <style>
        .fa-times{
            margin-left: 15px;
        }

        .newspopup .modal-body *{
          max-width: 540px!important;
        }
        .quote-container {
            margin-top: 50px;
            position: relative;
        }

        .note {
            color: #333;
            position: relative;
            width: 200px;
            margin: 0 auto;
            padding: 20px;
            font-family: Satisfy;
            font-size: 20px;
            box-shadow: 0 10px 10px 2px rgba(0,0,0,0.3);
        }

        .note .author {
            display: block;
            margin: 40px 0 0 0;
            text-align: right;
        }

        .yellow {
            background: #eae672;
            border-left-width: 0px;
            -webkit-transform: rotate(2deg);
            -moz-transform: rotate(2deg);
            -o-transform: rotate(2deg);
            -ms-transform: rotate(2deg);
            transform: rotate(2deg);
        }

        .pin {
            background-color: #aaa;
            display: block;
            height: 32px;
            width: 2px;
            position: absolute;
            left: 50%;
            top: -16px;
            z-index: 1;
        }

        .pin:after {
            background-color: #A31;
            background-image: radial-gradient(25% 25%, circle, hsla(0,0%,100%,.3), hsla(0,0%,0%,.3));
            border-radius: 50%;
            box-shadow: inset 0 0 0 1px hsla(0,0%,0%,.1),
            inset 3px 3px 3px hsla(0,0%,100%,.2),
            inset -3px -3px 3px hsla(0,0%,0%,.2),
            23px 20px 3px hsla(0,0%,0%,.15);
            content: '';
            height: 12px;
            left: -5px;
            position: absolute;
            top: -10px;
            width: 12px;
        }

        .pin:before {
            background-color: hsla(0,0%,0%,0.1);
            box-shadow: 0 0 .25em hsla(0,0%,0%,.1);
            content: '';

            height: 24px;
            width: 2px;
            left: 0;
            position: absolute;
            top: 8px;

            transform: rotate(57.5deg);
            -moz-transform: rotate(57.5deg);
            -webkit-transform: rotate(57.5deg);
            -o-transform: rotate(57.5deg);
            -ms-transform: rotate(57.5deg);

            transform-origin: 50% 100%;
            -moz-transform-origin: 50% 100%;
            -webkit-transform-origin: 50% 100%;
            -ms-transform-origin: 50% 100%;
            -o-transform-origin: 50% 100%;
        }
        .fa-pencil-square-o{
            margin: 10px;
        }
        .newstitlehref:hover{
            color: #2986b9;
        }
        addnote *{
            cursor: pointer;
        }
        .fa-check-circle{
            font-size: 20px;
            float: right;
            color: #64b92a;         
        }
        .todo_title a{
            color: #428bca!important;            
        }
        .modal-dialog{
            margin-top: 230px;
        }
        .fa.fa-plus{
            cursor: pointer;
        }
        .fa-times{
            margin-left: 15px;
        }
        reparto{
            display: none;
        }
    </style>


@endsection
@section('content')
<?php  $userid=Auth::user()->id; ?>
    <div class="page_content page_dashboard">
    <div class="container-fluid">
                    @if(session()->has('success'))
                        <div class="alert alert-success">
                            {{ session()->get('success') }}
                        </div>
                    @endif
                    @if(session()->has('error'))
                        <div class="alert alert-danger">
                            {{ session()->get('error') }}
                        </div>
                    @endif
        <div class="row div_tasks col-lg-6">
            <div class="">
                <div class="todo_section panel panel-default">
                    <div class="todo_date">
                        <h4 class="big-title"><span class="label label-default">{{ $task_total }}</span> Task Pendenti</h4>

                    </div>

                    @foreach($tasks as $task)
                        <ul class="todo_list_wrapper">
                            <li data-task-title="{{ $task->task_note}}" data-task-label="{{ $task->task_note}}"
                                data-task-date="{{ $task->created_at}}">

                                @if($task->user_id)
                                    <span class="label color_e pull-right">{{ \App\Http\Controllers\CategoryController::getUserData($task->user_id) }}</span>
                                @endif

                                <span class="label color_{{ \App\Http\Controllers\CategoryController::showCategoryColor($task->category_id) }} pull-right">{{ \App\Http\Controllers\CategoryController::showCategoryName($task->category_id)}}</span>

                                @if($task->preventivo>0)
                                    <span class="label color_d pull-right">PREVENTIVO</span>
                                @endif

                                <span>Task creato da: <strong> {{\App\Http\Controllers\DashboardController::whoHasCreatedTask($task->user_assigned_id)}} </strong></span>

                                <h4 class="todo_title">

                                    <a href="#todo_task_modal_{{ $task->id }}"><i class="glyphicon glyphicon-search" class="disabled"></i> {{ $task->task_note}}</a>

                                </h4>
                                <span> Dal <strong>{{\Carbon\Carbon::parse($task->created_at)->format('d-m-Y') }}</strong></span>
                                <span>al <strong> {{\Carbon\Carbon::parse($task->task_deadline)->format('d-m-Y') }}</strong></span>
                                <br>
                            </li>
                        

                        <div class="modal fade taskpopup" id="todo_task_modal_{{ $task->id }}" data-id="{{ $task->id }}>">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <form  id="login_form" role="form" method="POST" action="{{ route('send-mail') }}">
                                        {{ csrf_field() }}
                                        <input type="hidden" name="task_id" value="{{ $task->id}}">
                                        <input type="hidden" name="task_action" value="">           
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                            <h4 class="modal-title">Dettagli Task <reparto> di gruppo</reparto></h4>
                                            <input type="hidden" name="reparto" value="0">
                                                </div>
                                        <div class="modal-body">
                                            <div class="form-group">
                                                <div class="row">
                                                    <div class="col-sm-12">
                                                        <h5 class="taskNote">{{ $task->task_note}}</h5>
                                                    </div>

                                                    <notelist class="col-sm-12">
                                                        <p>Note:</p>
                                                        <ul>
                                                            <li class="default">
                                                                <p>Nessuna nota</p>
                                                            </li>
                                                        </ul>
                                                     </notelist>
                                                    <noteform class="col-sm-12">
                                                            <div>
                                                            <h4>
                                                                <a id="addnote" class="addnote btn btn-primary" href="#" class="btn btn-xs btn-primary news-zama" onclick="addnote(this,{{ $task->id}})">
                                                                    <i class="glyphicon glyphicon-plus"></i>&nbsp;Aggiungi nota
                                                                </a>    
                                                                <a style="display: none;" id="savenote" class="savenote btn btn-success news-zama" href="#" onclick="savenote(this,{{ $task->id}})">
                                                                    <i class="glyphicon glyphicon-check"></i>&nbsp;Salva nota
                                                                </a>                                                                  
                                                            </h4> 
                                                            </div>                                                        
                                                    </noteform>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                            <repartonote>
                                                Sei il supervisore di questo task. Puoi segnare il compito come eseguito per l'intero reparto
                                            </repartonote>
                                            </div>
                                        </div>
                                        <div class="modal-footer">
                                        <button type="button" class="btn btn-success" onclick="requestConfirm(this,'Confermi che il compito di questo task è stato svolto?')">Segnala il task come concluso</button>
                                        <reparto> <button type="button" class="btn btn-success" onclick="requestConfirm(this,'Vuoi chiudere il task per l\'intero reparto?')">Chiudi il task per il reparto</button></reparto>
                                            @if(Auth::user()->admin == 1)
                                               <button type="button" class="btn btn-danger" onclick="requestConfirm(this)">Rifiuta task</button>
                                            @endif
                                        </div>
                                    </form>

                                </div>
                            </div>
                        </div>
                        </ul>
                    @endforeach
                </div>
            </div>
        </div>

            <script type="text/javascript">
                function deletenote(id,el){
                    window['tmp']=el
                    $.post("../api.php",{qr:'delnote',id:id},function(response){
                        $(window['tmp']).parents("li").remove();
                    })
                }
    var savenoteflag=false;
    function savenote(el,taskid){
        var value=$(el).parents(".modal-body").find("textarea").val();
        var note=$(el).parents(".modal-body").find(".taskNote").text();
        var title=$(el).parents(".modal-body").find(".taskNote").text();
        if(value==""){
            $(el).parents("noteform").html("");
            return;
        }
        if(savenoteflag)
            return;
        savenote=true;
        window['el']=el;

        $.post("../api.php",{qr:'addnote',taskid:taskid,text:value,note:note,title:title},function(response){
            window.location.reload(true);
        })
    }
                function addnote(el,id){
                    if($(".noteformcontainer:visible").length)
                        return;
                    var html=
                    '<div class="form-group noteformcontainer">'+
                      '<div class="">'+
                      '<textarea id="textinput" name="textinput" type="text" placeholder="Nota di testo" style="width:300px;height:100px;" class="form-control"></textarea>'+
                      '<span class="help-block">'+'</span>  '+
                      '</div>';
                      $(el).parents("noteform").prepend(html);
                      $("#savenote").attr("data-id",id);
                      $(el).parents("noteform").find(".addnote").toggle();
                      $(el).parents("noteform").find(".savenote").toggle();

                }
            </script>

            <div class="col-lg-6 div_news">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <div class="easy_chart_desc">
                            <h4 class="big-title">News</h4>
                        </div>
                        <div class="">

                            <div class="panel-body">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <table class="table table-bordered table-striped" id="news_table" >
                                            <thead>
                                            <tr>
                                                <th>Titolo</th>
                                                <th>Autore</th>
                                                <th width="10%">Azioni</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @foreach($news as $new)
                                            
                                            <?php 
                                            
                                               if(!empty($new->evidenzadeadline))
                                                {
                                                    $news_deadline=new Datetime($new->evidenzadeadline);
                                                    $today=new Datetime();
                                                    if($news_deadline<$today)
                                                        $new->evidenza=0;
                                                }

                                                if($new->userid == '-3'){
                                                    $a=unserialize($new->news_title);
                                                    $b=unserialize($new->news_body);
                                                    $head="";
                                                    $body="";
                                                    if(isset($a['h1']))
                                                        $head.=$a['h1']." compie gli anni oggi!";

                                                    if(isset($b['b1']))
                                                        $body.=" <span class='glyphicon glyphicon-earphone'></span> ".$b['b1']." ";
                                                    if(isset($b['b2']))
                                                        $body.=" <span class='glyphicon  glyphicon-phone-alt'></span>".$b['b2']." ";
                                                    if(isset($b['b3']))
                                                        $body.=" <span class='glyphicon  glyphicon-envelope'></span> ".$b['b3']." ";
                                                    $body.="";
                                                    $new->news_title=$head;
                                                    $new->news_body=$body;
                                                }
                                            ?>

                                                <tr>

                                                    <td>
                                                        @if($new->evidenza == '1')
                                                            @if($new->userid == '-3')
                                                                <p><img src="../img/cake.png" alt="Compleanno" width="30" height="30"> Compleanno!<br></p>
                                                            @else
                                                                <span class="label color_h pull-left newsitem">Evidenza</span>&nbsp;&nbsp;
                                                            @endif
                                                        @endif

                                                        {{ $new->news_title }}
                                                    </td>
                                                    <td>{{$new->username}}</td>
                                                    <td>
                                                             @if($new->userid == '-3')
                                                                <a href="#" class="btn btn-xs news-zama" style="background: #e67e22;color:white" data-toggle="modal" data-target="#news-modal-{{ $new->id }}">
                                                                <i class="glyphicon glyphicon-earphone" class="disabled"></i>&nbsp;Contattalo
                                                                </a>
                                                            @else
                                                                <a href="#" class="btn btn-xs btn-primary news-zama" data-toggle="modal" data-target="#news-modal-{{ $new->id }}">
                                                                        <i class="glyphicon glyphicon-search" class="disabled"></i>&nbsp;Visualizza
                                                                </a>
                                                            @endif
                                                    </td>
                                                </tr>
                                                <div class="modal fade" id="news-modal-{{ $new->id }}" tabIndex="-1" style="display: none;">
                                                    <div class="modal-dialog">
                                                        <div class="modal-content">
                                                            <div class="modal-header">

                                                                @if($new->evidenza == '1')
                                                                    @if($new->userid == '-3')
                                                                        <p><img src="../img/cake.png" alt="Compleanno" width="30" height="30"> <strong>Contatta il festeggiato</strong></p>
                                                                        
                                                                    @else
                                                                        <span class="label color_h pull-left"> News in Evidenza</span>&nbsp;&nbsp;
                                                                        Titolo: {{ htmlspecialchars_decode ($new->news_title) }}
                                                                        <h6>
                                                                          Creata il: {{\Carbon\Carbon::parse($new->created_at)->format('d-m-yy') }}
                                                                            alle ore:{{\Carbon\Carbon::parse($new->created_at)->format('H:i:s') }}
                                                                        </h6>
                                                                    @endif
                                                                @endif
                                                                

                                                            </div>
                                                            <div class="modal-body">

                                                                {!!   htmlspecialchars_decode ($new->news_body)  !!}
                                                            </div>
                                                            <div class="modal-footer">
                                                                <button type="button" class="btn btn-danger" data-dismiss="modal" aria-hidden="true">Chiudi</button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            @endforeach
                                            </tbody>
                                        </table>


                                    </div>
                                </div>


                            </div>
                        </div>
                    </div>
                </div>
            </div>


    </div>
    </div>
    </div>
<script type="text/javascript">
    console.log("user");
</script>
@endsection
@section('footer-plugin')

    <!-- textarea autosize -->
    <script src="{{ asset('/lib/autosize/jquery.autosize.min.js') }}"></script>
    <script src="{{ asset('/lib/d3/d3.min.js') }}"></script>
    <script src="{{ asset('/lib/novus-nvd3/nv.d3.min.js') }}"></script>
    <!-- flot charts-->
    <script src="{{ asset('/lib/flot/jquery.flot.min.js') }}"></script>
    <script src="{{ asset('/lib/flot/jquery.flot.pie.min.js') }}"></script>
    <script src="{{ asset('/lib/flot/jquery.flot.resize.min.js') }}"></script>
    <script src="{{ asset('/lib/flot/jquery.flot.tooltip.min.js') }}"></script>
    <!-- clndr -->
    <script src="{{ asset('/lib/underscore-js/underscore-min.js') }}"></script>
    <script src="{{ asset('/lib/CLNDR/src/clndr.js') }}"></script>
    <!-- easy pie chart -->
    <script src="{{ asset('/lib/easy-pie-chart/dist/jquery.easypiechart.min.js') }}"></script>
    <!-- owl carousel -->
    <script src="{{ asset('/lib/owl-carousel/owl.carousel.min.js') }}"></script>

    <!-- dashboard functions -->
    <script src="{{ asset('/js/apps/tisa_dashboard.js') }}"></script>
    <!-- datatables -->
    <script src="{{ asset('/lib/DataTables/media/js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('/lib/DataTables/media/js/dataTables.bootstrap.js') }}"></script>
    <script src="{{ asset('/lib/DataTables/extensions/TableTools/js/dataTables.tableTools.min.js') }}"></script>
    <script src="{{ asset('/lib/DataTables/extensions/Scroller/js/dataTables.scroller.min.js') }}"></script>

    <!-- datatables functions -->
    <script src="{{ asset('/js/apps/tisa_datatables.js') }}"></script>
    <!-- todo functions -->
    <script src="{{ asset('/js/apps/tisa_todo.js') }}"></script>

<script type="text/javascript">
    window['table']="tasks";
    window['isntable']="li";
    var userid="<?php echo $userid;?>";

    $(document).ready(function () {

        var l=$('.todo_list_wrapper').length/2;
        $('#side_nav .label.label-danger').text(l);
        $(".todo_title a").on('click', function () {

            var href=$(this).attr("href");
            var id=href.replace("#todo_task_modal_","");
            window['popup']=href;

            $.post("../api.php",{qr:'loadnote',id:id},function(response){
                response=JSON.parse(response);
                $(window['popup']+" reparto").hide();
                if(response.length==0)
                    return;
                var el=window['popup'];
                $(el+" notelist ul").html("");

                for(var i=0;i<response.data.length;i++){
                    var pre=response.data[i].username+" / alle ore : "+response.data[i].timer+" ha aggiunto una nota"+'<i class="fa fa-times" style="margin-left:15px;color:#9F2F24" onclick="deletenote('+response.data[i].id+',this)" aria-hidden="true"></i>';

                    if($(el).find("notelist ul li.default"))
                        $(el).find("notelist ul li.default").remove();
                    
                    if(userid==response.data[i].userid)
                       $(el).find("notelist ul").append("<li><p>"+pre+"</p>"+response.data[i].note+"</li>");    
                    else                
                       $(el).find("notelist ul").append("<li><p>"+pre+"</p>"+response.data[i].note+"</li>");  
                }

                /* task di gruppo */
                if(response.info[0].user_assigned_id==response.info[0].task_supervisor)
                {
                    $(el+" .modal-footer button:first").hide()
                    $(window['popup']+" reparto").show();
                    $(window['popup']+" repartonote").show();
                    $(window['popup']+" [name='reparto']").val(response.info);
                }

            })
        })

    })
</script>
@endsection
