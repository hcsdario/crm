<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Auth::routes();

Route::get('/', 'Controller@redirectTo')->name('home')->middleware('auth');
Route::get('/home', 'Controller@redirectTo')->name('home')->middleware('auth');
Route::get('/index', 'Controller@redirectTo')->name('home')->middleware('auth');

Route::get('/admin/profile', 'HomeController@index_admin')->name('profile_admin')->middleware('auth');
Route::get('/admin/dashboard', 'DashboardController@index')->name('dashboard_admin')->middleware('auth');
Route::get('/admin/cliente', 'CustomerController@index')->name('cliente_admin')->middleware('auth');
Route::get('/admin/cliente_ext', 'CustomerController@index_ext')->name('cliente_admin')->middleware('auth');
Route::get('/admin/cerca_privato', 'CustomersController@search_privato')->name('search_privato')->middleware('auth');
Route::get('/admin/cerca_azienda', 'CustomersController@search_privato_ext')->name('search_privato_ext')->middleware('auth');
Route::get('/admin/cerca_adv', 'AdvController@search_adv')->name('search_adv')->middleware('auth');
Route::get('/admin/cerca_advext', 'AdvController@search_adv_ext')->name('search_adv_ext')->middleware('auth');

Route::get('/admin/cliente/{id}', 'CustomerController@edit')->name('cliente_profile_admin')->middleware('auth');
Route::get('/admin/customerUpdate/{id}', 'CustomerController@update')->name('customerUpdate')->middleware('auth');

Route::get('/admin/adv', 'AdvController@index')->name('adv_admin')->middleware('auth');
Route::get('/admin/adv_ext', 'AdvController@index_ext')->name('adv_admin_ext')->middleware('auth');
Route::get('/admin/adv/{id}', 'AdvController@show')->name('adv_profile_admin')->middleware('auth');
Route::get('/admin/adv/edit/{id}', 'AdvController@edit')->name('adv_profile_edit_admin')->middleware('auth');

Route::get('/admin/clienti', 'CustomersController@index')->name('clienti_admin')->middleware('auth');
Route::get('/admin/clienti/categories', 'CategoryCustomersController@index')->name('categories_customers.index')->middleware('auth');
Route::post('/admin/clienti/category/store', 'CategoryCustomersController@store')->name('category_customers.store')->middleware('auth');
Route::get('/admin/clienti/category', 'CategoryCustomersController@create')->name('category_customers.new')->middleware('auth');
Route::get('/admin/clienti/category/{id}', 'CategoryCustomersController@create')->name('category_customers.edit')->middleware('auth');
Route::get('/admin/clienti/category/{id}/delete', 'CategoryCustomersController@delete')->name('category_customers.delete')->middleware('auth');
Route::get('/admin/preventivi', 'PreventivoController@index')->name('preventivi')->middleware('auth');
Route::get('/admin/preventivo', 'PreventivoController@create')->name('preventivo')->middleware('auth');
Route::get('/admin/preventivo/edit/{id}', 'PreventivoController@edit')->name('preventivo')->middleware('auth');

Route::post('/admin/preventivo/topdf/{id}', 'PreventivoController@topdf')->name('preventivo')->middleware('auth');
Route::get('/admin/preventivo/getpdf/{name}', 'PreventivoController@getpdf')->name('preventivo')->middleware('auth');

Route::get('/admin/aziende', 'CustomersController@index_ext')->name('clienti_admin_ext')->middleware('auth');
Route::get('/admin/travel', 'TravelController@index')->name('travel_admin')->middleware('auth');

Route::get('/admin/tasks', 'TaskController@index')->name('tasks_admin')->middleware('auth');
Route::get('/admin/news', 'NewsController@index')->name('news_admin')->middleware('auth');
Route::get('/admin/newUser', 'newUserController@index')->name('new_user')->middleware('auth');
Route::get('/admin/reparti', 'RepartiController@index')->name('reparti')->middleware('auth');

Route::get('/admin/newUser/{id}', 'newUserController@edit')->name('edituser')->middleware('auth');
Route::get('/admin/newUser2', 'newUserController@storeedit')->name('storeedit')->middleware('auth');
Route::get('/admin/reparti/store', 'RepartiController@store')->name('repartoStore')->middleware('auth');
Route::get('/admin/reparti/{id}', 'RepartiController@edit')->name('repartoEdit')->middleware('auth');

Route::get('/admin/preventiviSearch', 'PreventivoController@preventiviSearch')->name('preventiviSearch')->middleware('auth');

Route::get('/admin/uploads', 'uploadsController@index')->name('uploads')->middleware('auth');
Route::post('/admin/uploads', 'uploadsController@store')->name('uploadsStore')->middleware('auth');

Route::get('/admin/settings', 'uploadsController@index_settings')->name('index_settings')->middleware('auth');
Route::post('/admin/settings', 'uploadsController@update_settings')->name('update_settings')->middleware('auth');

Route::get('/user/business', 'BusinessController@index')->name('newbusiness')->middleware('auth');
Route::get('/user/business/{id}', 'BusinessController@edit')->name('editbusiness')->middleware('auth');

Route::get('/user/profile', 'HomeController@index_user')->name('profile_user')->middleware('auth');
Route::get('/user/dashboard', 'DashboardController@index')->name('dashboard_user')->middleware('auth');
Route::get('/test', 'DashboardController@test')->name('test')->middleware('auth');
Route::get('/user/cliente', 'CustomerController@index')->name('cliente_user');
Route::get('/user/cliente/{id}', 'CustomerController@show')->name('cliente_profile_user');
Route::get('/user/cliente/edit/{id}', 'CustomerController@edit')->name('cliente_profile_edit_user');


Route::get('/user/adv', 'AdvController@index')->name('adv_user')->middleware('auth');
Route::get('/user/adv_ext', 'AdvController@index_ext')->name('adv_user_ext')->middleware('auth');
Route::get('/user/adv/{id}', 'AdvController@show')->name('adv_profile_user')->middleware('auth');
Route::get('/user/adv/edit/{id}', 'AdvController@edit')->name('adv_profile_edit_user')->middleware('auth');
Route::get('/user/new', 'AdvController@newadv')->name('newadv')->middleware('auth');
Route::get('/user/new_ext', 'AdvController@newadv_ext')->name('newadv_ext')->middleware('auth');

Route::get('/user/clienti', 'CustomersController@index')->name('clienti_user')->middleware('auth');
Route::get('/user/clienti_ext', 'CustomersController@index_ext')->name('clienti_user_ext')->middleware('auth');
Route::get('/user/tasks', 'TaskController@index')->name('tasks_user')->middleware('auth');
Route::get('/user/travel', 'TravelController@index')->name('travel_user')->middleware('auth');
Route::get('/user/news', 'NewsController@index')->name('news_user')->middleware('auth');
Route::get('/user/news/{id}', 'NewsController@edit')->name('editnews')->middleware('auth');

Route::get('/clienti-data', 'CustomersController@data')->name('clienti_data')->middleware('auth');
Route::get('/clienti-data_ext', 'CustomersController@data_ext')->name('clienti_data_ext')->middleware('auth');

Route::get('/user-data', 'newUserController@data')->name('user_data')->middleware('auth');
Route::get('/clienti-adv-data', 'AdvController@data')->name('clienti_adv_data')->middleware('auth');
Route::get('/clienti-adv-data2', 'AdvController@data_ext')->name('clienti_adv_data_ext')->middleware('auth');

Route::get('/travels-data/{customer_id}', 'TravelController@getTravelByCustomerId')->name('travels_data')->middleware('auth');
Route::get('/files-data/{customer_id}', 'FilesController@data')->name('files_data')->middleware('auth');
Route::get('/file/delete/{id_file}', 'FileController@logicDelete')->name('delete_file')->middleware('auth');

Route::get('/news-data', 'NewsController@data')->name('news_data')->middleware('auth');
Route::get('/task/marked/{id}', 'TaskController@logicDelete')->name('task_marked')->middleware('auth');
Route::get('/news/read/{id}', 'NewsController@readNews')->name('read_news')->middleware('auth');


Route::get('/admin/cards', 'CardController@index')->name('cards')->middleware('auth');
Route::get('/admin/card/new', 'CardController@create')->name('cards new')->middleware('auth');
Route::get('/admin/card/edit/{id}', 'CardController@edit')->name('card edit')->middleware('auth');
Route::post('/admin/cards/duplify', 'CardController@copy')->name('copycards')->middleware('auth');

Route::get('/admin/coupon', 'CouponController@index')->name('coupon')->middleware('auth');
Route::get('/admin/coupon/new', 'CouponController@create')->name('coupon new')->middleware('auth');
Route::get('/admin/coupon/edit/{id}', 'CouponController@edit')->name('coupon edit')->middleware('auth');
Route::post('/admin/coupon/duplify', 'CouponController@copy')->name('copycoupon')->middleware('auth');

Route::get('/autocomplete', 'ComuniController@autocomplete')->name('autocomplete')->middleware('auth');
Route::get('/autocomplete_ext', 'ComuniController@autocomplete_ext')->name('autocomplete_ext')->middleware('auth');
Route::post('/task/mail/', 'TaskController@sendMail')->name('send-mail')->middleware('auth');

Route::resource('preventivo', 'PreventivoController');
Route::resource('card', 'CardController');
Route::resource('coupon', 'CouponController');
Route::resource('customer', 'CustomerController');
Route::resource('customers', 'CustomersController');
Route::resource('customer_adv', 'AdvController');
Route::resource('newUser', 'newUserController');
Route::resource('uploads', 'uploadsController');
Route::resource('travel', 'TravelController');
Route::resource('tasks', 'TaskController');
Route::resource('news', 'NewsController');
Route::resource('file', 'FileController');
Route::resource('reparti', 'RepartiController');
Route::resource('business', 'BusinessController');
